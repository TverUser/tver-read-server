//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Server.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReadedArchivesSuccess
    {
        public int StationEquipmentID { get; set; }
        public System.DateTime ReadDate { get; set; }
        public int ArchiveType { get; set; }
        public bool Status_GOOD { get; set; }
        public bool Status_NOARCHIVES { get; set; }
        public bool Status_BAD { get; set; }
    }
}
