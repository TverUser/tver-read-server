//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Server.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class FinSanctions
    {
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public byte Type { get; set; }
        public System.DateTime UpDate { get; set; }
        public int UpUserId { get; set; }
        public string UpComment { get; set; }
        public bool UpSentMail { get; set; }
        public Nullable<System.DateTime> DownDate { get; set; }
        public Nullable<int> DownUserId { get; set; }
        public string DownComment { get; set; }
        public Nullable<bool> DownSentMail { get; set; }
    
        public virtual Companies Companies { get; set; }
    }
}
