//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Server.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class ALTNAMES
    {
        public string OLDCODE { get; set; }
        public string NEWCODE { get; set; }
        public string LEVEL { get; set; }
        public long ID { get; set; }
    }
}
