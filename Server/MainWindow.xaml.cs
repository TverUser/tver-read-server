﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MasterParent;
using System.Threading;
using System.IO.Pipes;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using Server.DB;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using VKT7;
using VZLET_024M;
using TEM104n;
using TEM106n;
using namespaceTV7;
using KM_5;
/*
using System.Collections.ObjectModel;
using Mercury_230;

using VKT_5;

using vct9 
using Multical;
using VZLET_TSRV_026M;
using Vzlyot_TCPB_030;*/
using System.Windows.Documents;
using System.Windows.Media;
using static MasterParent.Driver;
using System.Threading.Tasks;

namespace Server
{

    /// <summary>
    /// Encapsulate item data source for a CheckBoxListView item
    /// </summary>
    public class CheckBoxListViewItemSource : INotifyPropertyChanged
    {
        public CheckBoxListViewItemSource(String text, bool IsChecked, int StationEquipmentIDin)
        {
            m_text = text;
            m_checked = IsChecked;
            StationEquipmentID = StationEquipmentIDin;
        }

        public bool IsChecked
        {
            get { return m_checked; }
            set
            {
                if (m_checked == value) return;
                m_checked = value;
                RaisePropertyChanged("Checked");
            }
        }

        public String Text
        {
            get { return m_text; }
            set
            {
                if (m_text == value) return;
                m_text = value;
                RaisePropertyChanged("Text");
            }
        }

        public override string ToString()
        {
            return Text;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propName)
        {
            PropertyChangedEventHandler eh = PropertyChanged;
            if (eh != null)
            {
                eh(this, new PropertyChangedEventArgs(propName));
            }
        }

        private bool m_checked;
        private String m_text;
        public int StationEquipmentID;
    }

    public class ColoredConsoleLIne
    {
        public string paragraph;
        public int Type;
        public string DriverFeature;
        public DateTime Time;
        public ColoredConsoleLIne()
        {
            paragraph = ("-");
            Time = DateTime.Now;
            DriverFeature = "-";
        }
    }

    public static class ColoredConsole
    {
        public static object toblockerToLogs = new object();

        public const int TYPE_ALL = 0;
        public const int TYPE_ERROR = 1;
        public const int TYPE_WARNING = 2;
        public const int TYPE_MESSAGE = 3;
        public const int TYPE_DEBUG = 4;

        static public List<ColoredConsoleLIne> Text = new List<ColoredConsoleLIne>();

        private static void WriteLin(int Type, string driverFeature, string message)
        {
            if (message.Trim() != "")
            {


                ColoredConsoleLIne h = new ColoredConsoleLIne();

                h.Type = Type;
                h.paragraph = message;
                h.DriverFeature = driverFeature;
                h.Time = DateTime.Now;
                lock (toblockerToLogs)
                {
                    Text.Add(h);
                }
                NeedRedraw = true;

                reDrawLogf();
            }
        }

        public static void WriteLine(int Type, string driverFeature, string message)
        {
            WriteLin(Type, driverFeature, message);
        }

        public static void WriteLine(int Type, string message)
        {
            WriteLin(Type, "-", message);
        }

        public static void WriteLine(string message)
        {
            WriteLin(ColoredConsole.TYPE_DEBUG, "-", message);
        }

        public static void WriteLine(double message)
        {
            WriteLin(ColoredConsole.TYPE_DEBUG, "-", message.ToString());
        }

        public static void WriteLine(byte[] message)
        {
            WriteLin(ColoredConsole.TYPE_DEBUG, "-", message.ToString());
        }

        public static void WriteLine(int message)
        {
            WriteLin(ColoredConsole.TYPE_DEBUG, "-", message.ToString());
        }

        public static void WriteLine(uint message)
        {
            WriteLin(ColoredConsole.TYPE_DEBUG, "-", message.ToString());
        }

        public static void WriteLine()
        {
        }
        static RichTextBox place;
        public static string DriverFeature = "<All>";
        public static bool NeedRedraw = false;
        public static void ColoredConsol(RichTextBox richTextBox)
        {
            place = richTextBox;
        }
        public static List<int> TypeUsed = new List<int>();

        public static void reDrawLogf()
        {
            if (NeedRedraw)
                place.Dispatcher.BeginInvoke(new Action(delegate ()
                {

                    //if ("<All>" == "<All>" && 0 == 0)


                    {
                        lock (toblockerToLogs)
                        {
                            if (Text.Count > 300)
                            {
                                Text.RemoveRange(0, 200);
                            }

                            place.Document.Blocks.Clear();
                            /*
                            int pos = Text.Count - 100;
                            if (pos < 0) pos = 0;
                            for(int  it = pos; it < Text.Count; it++)
                           */
                            foreach (var a in Text)
                            {
                                // var a = Text.ElementAt(it);
                                bool DriverNeed = true;
                                bool MessageTypeNeed = true;


                                if (TypeUsed.Count == 0)
                                {
                                    MessageTypeNeed = true;
                                }
                                else
                                {
                                    if (!ColoredConsole.TypeUsed.Contains(0))
                                        if (ColoredConsole.TypeUsed.Contains(a.Type))
                                        {
                                            MessageTypeNeed = true;
                                        }
                                        else
                                        {
                                            MessageTypeNeed = false;
                                        }

                                    else
                                        MessageTypeNeed = true;
                                }


                                if (DriverNeed && MessageTypeNeed)
                                {

                                    Paragraph p = new Paragraph(new Run(a.Time.ToString() + "  " + a.paragraph));
                                    p.FontSize = 14;
                                    p.FontStyle = FontStyles.Italic;
                                    p.TextAlignment = TextAlignment.Left;
                                    p.Foreground = Brushes.Gray;

                                    //static public int TYPE_ERROR = 1;
                                    //static public int TYPE_WARNING = 2;
                                    //static public int TYPE_MESSAGE = 3; 
                                    switch (a.Type)
                                    {
                                        case 1:
                                            p.Foreground = Brushes.Red;
                                            break;

                                        case 2:
                                            p.Foreground = Brushes.DarkGreen;
                                            break;
                                        case 3:
                                            p.Foreground = Brushes.Blue;
                                            break;
                                        case 4:
                                            p.Foreground = Brushes.DarkGray;
                                            break;
                                        default:
                                            p.Foreground = Brushes.Gray;
                                            break;
                                    }

                                    place.Document.Blocks.Add(p);
                                }

                            }
                        }
                    }
                }
             ));
        }
    }
    public partial class ServerCommandsPoolcustom  
    {
        public int ID { get; set; }
        public int StationEquipmentID { get; set; }
        public int InformReadType { get; set; }
        public System.DateTime DateFrom { get; set; }
        public System.DateTime DateTo { get; set; }

        public List<ReadedArchivesSuccess> Readed;
        public ServerCommandsPoolcustom()
        {
            ID = 0;
            StationEquipmentID = 0;

        }

        public ServerCommandsPoolcustom(TServerCommandsPool el)
        {
            this.ID = el.ID;
            this.StationEquipmentID = el.StationEquipmentID;
            this.InformReadType = el.InformReadType;
            this.DateFrom = el.DateFrom;
            this.DateTo = el.DateTo;
        }

    }



    public class CsdList
    {
        public List<StationEquipmentData> ListOfEquipmemnts;
        public bool commandExecuted;
        public ServerCommandsPoolcustom Ccommand;
        public CsdList()
        {
            Ccommand = new ServerCommandsPoolcustom();
            commandExecuted = true;
            ListOfEquipmemnts = new List<StationEquipmentData>();
        }
    }

    public partial class MainWindow : Window
    {
        List<Thread> threadPull = new List<Thread>();
        List<StationEquipmentData> devicesData = new List<StationEquipmentData>();
        List<Driver> driverData = new List<Driver>();
        List<DataParameter> MainData = new List<DataParameter>();

        public object locker = new object();
        public object locker1 = new object();

        public NamedPipeServerStream serverPipe;

        public object ConfigurationManager { get; private set; }

        public List<string> Equipments;
        public List<string> EquipmentsStationName;

        List<Parameters> ParametersInfo = new List<Parameters>();

        List<StationEquipmentData> EquipmentsInfo = new List<StationEquipmentData>();
        List<EquipmentType> EquipmentsTypesInfo = new List<EquipmentType>();
        public List<List<EquipmentTypeParameters>> EquipmentsDatas;
        public List<List<string>> EquipmentsDataValus;
        public List<List<string>> EquipmentsDataDates;

        List<StationEquipmentData> dDList_Tcp = new List<StationEquipmentData>();
        //CSD
        public static List<StationEquipmentData> dDList_Csd = new List<StationEquipmentData>();

        public static List<CsdList> dDList_CsdByModem = new List<CsdList>(); // Списоки модемов+все еквипменты к ним.  (Первый в списке модем)

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var h = (CheckBox)sender;

                // int i = h.StationEquipmentID;
                string j = h.DataContext.ToString().Split('.')[0];

                var tel = dDList_Csd[Convert.ToInt32(j)];
                tel.NeedToBeRead = h.IsChecked.Value;
                // U 


                DBEntities_f DB = new DBEntities_f();
                string H = "UPDATE  [dbo].[StationEquipment] SET  [NeedToRead] = '" + h.IsChecked.Value + "' WHERE[Id] = " + tel.StationEquipmentId;
                DB.Database.ExecuteSqlCommand(H);

            }
            catch (Exception)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось сохранитиь состояние считывать/не считывать    в базу");

                throw;
            }

        }

        private void comboTypeMessage_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBoxItem cbi1;
            var comboBox = sender as ComboBox;
            cbi1 = new ComboBoxItem(); cbi1.Background = Brushes.White; cbi1.Content = "   <All>    "; comboBox.Items.Add(cbi1);
            cbi1 = new ComboBoxItem(); cbi1.Background = Brushes.Red; cbi1.Content = "TYPE_ERROR  "; comboBox.Items.Add(cbi1);
            cbi1 = new ComboBoxItem(); cbi1.Background = Brushes.Green; cbi1.Content = "TYPE_WARNING"; comboBox.Items.Add(cbi1);
            cbi1 = new ComboBoxItem(); cbi1.Background = Brushes.Blue; cbi1.Content = "TYPE_MESSAGE"; comboBox.Items.Add(cbi1);
            cbi1 = new ComboBoxItem(); cbi1.Background = Brushes.Gray; cbi1.Content = "TYPE_DEBUG  "; comboBox.Items.Add(cbi1);
            comboBox.SelectedIndex = 0;

        }

        private void comboBoxDriverFeature_Loaded(object sender, RoutedEventArgs e)
        {

            List<string> data = new List<string>();
            data.Add("Boo1k");
            data.Add("Computer");
            data.Add("Chair");
            data.Add("Mug");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;

        }


        private void comboTypeMessage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;

            ColoredConsole.TypeUsed.Clear();

            // ... Set SelectedItem as Window Title.
            int value = comboBox.SelectedIndex;

            switch (value)
            {
                case 0: ColoredConsole.TypeUsed.Add(ColoredConsole.TYPE_ALL); ColoredConsole.reDrawLogf(); break;
                case 1: ColoredConsole.TypeUsed.Add(ColoredConsole.TYPE_ERROR); ColoredConsole.reDrawLogf(); break;
                case 2: ColoredConsole.TypeUsed.Add(ColoredConsole.TYPE_WARNING); ColoredConsole.reDrawLogf(); break;
                case 3: ColoredConsole.TypeUsed.Add(ColoredConsole.TYPE_MESSAGE); ColoredConsole.reDrawLogf(); break;
                case 4: ColoredConsole.TypeUsed.Add(ColoredConsole.TYPE_DEBUG); ColoredConsole.reDrawLogf(); break;



                default:
                    break;
            }

            ColoredConsole.NeedRedraw = true;
            ColoredConsole.reDrawLogf();

        }

        private void comboBoxDriverFeature_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;

            // ... Set SelectedItem as Window Title.
            string value = comboBox.SelectedItem as string;
            this.Title = "Selected: " + value;
        }

        public void CheckForReadArchive()
        {
            DBEntities_f DB = new DBEntities_f();
            while (true)
            {
                List<TServerCommandsPool> TServerCommandsPool_artemp = DB.TServerCommandsPool.SqlQuery("SELECT *  FROM [SPOU].[dbo].[TServerCommandsPool] where isDeleted=0 order by Priority desc").ToList<TServerCommandsPool>();
                List<ServerCommandsPoolcustom> TServerCommandsPool_ar = new List<ServerCommandsPoolcustom>();
                foreach (var a in TServerCommandsPool_artemp)
                {
                    TServerCommandsPool_ar.Add(new ServerCommandsPoolcustom(a));
                }

                foreach (var csd_list in dDList_CsdByModem)
                {
                    foreach (var com in TServerCommandsPool_ar)
                    {
                        int i = 0;
                        foreach (var element in csd_list.ListOfEquipmemnts)
                        {
                            if (i != 0)// если не модем собственно
                            {

                                if (element.StationEquipmentId == com.StationEquipmentID && csd_list.commandExecuted)
                                {
                                    var arch = DB.ReadedArchivesSuccess.Where(x =>
                                    x.StationEquipmentID == com.StationEquipmentID &&
                                    x.ReadDate >= com.DateFrom &&
                                    x.ReadDate <= com.DateTo
                                    ).ToList();

                                    com.Readed = arch;

                                    csd_list.Ccommand = com;
                                    csd_list.commandExecuted = false;

                                    goto finded;
                                }
                            }
                            i++;
                        }

                    }
                finded: int temp = 0;
                }
                Thread.Sleep(6000);
            }
        }

        public static void UpdateOneCommandFromPool(int id, int ReadStatus, string ExceptionDescription)
        {
            var com = DBEntityForCommans.TServerCommandsPool.Where(x => x.ID == id).ToList().First();
            if (ExceptionDescription != "")
            {
                com.ExceptionDescription = ExceptionDescription;
                ReadStatus = 2;
            }
            com.ReadStatus = ReadStatus;

            if (ReadStatus != 0)
            {
                DBEntityForCommans.Database.ExecuteSqlCommand("UPDATE [dbo].[TServerCommandsPool] SET  [isDeleted] = \'true\'  WHERE Id=" + id);
                com.isDeleted = true;
            }
            DBEntityForCommans.SaveChanges();

        }

        public class NameAdress
        {
            public string SOCR_Street { get; set; }
            public string Name_Street { get; set; }
            public string Name_Town { get; set; }
            public string Name_District { get; set; }
            public string Name_City { get; set; }
            public string Name_TimeZone { get; set; }
            public string House { get; set; }
        }

        public static DBEntities_f DBEntityForCommans;
        public static bool NeedToDoEVERYTHING = true;//for DDeBUG


        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            Current_ArchiveWindow.Close();
        }

        public void MainWork()
        {
            this.Dispatcher.BeginInvoke(new Action(delegate ()
            {



                DBEntityForCommans = new DBEntities_f();
                Current_ArchiveWindow = new ArchiveWindow();
                if (NeedToDoEVERYTHING)
                {


                    ColoredConsole.ColoredConsol(LogCponsole);

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "-", "Hello");

                    DBEntities_f DB = new DBEntities_f();

                    Equipments = new List<string>();
                    EquipmentsStationName = new List<string>();
                    EquipmentsDatas = new List<List<EquipmentTypeParameters>>();
                    EquipmentsDataValus = new List<List<string>>();
                    EquipmentsDataDates = new List<List<string>>();

                    var Equipment = DB.Equipment.SqlQuery("Select * from Equipment").ToList();
                    List<ReferencePoints> StationEquipParameters_ar = DB.ReferencePoints.SqlQuery("Select * from ReferencePoints").ToList<ReferencePoints>();
                    EquipmentsInfo = new List<StationEquipmentData>();
                    List<StationEquipment> StationEquipment_ar = DB.StationEquipment.SqlQuery("Select * from StationEquipment").ToList<StationEquipment>();
                    ParametersInfo = DB.Parameters.SqlQuery("Select * from Parameters").ToList<Parameters>();
                    EquipmentsTypesInfo = DB.EquipmentType.SqlQuery("Select * from EquipmentType").ToList<EquipmentType>();

                    StationEquipmentData Parameters;
                    foreach (var param in StationEquipment_ar)
                    {
                        Parameters = new StationEquipmentData();
                        Parameters.StationEquipmentId = param.Id;
                        Parameters.PhoneDialing = param.PhoneDialing;
                        Parameters.ServerPort = param.ServerPort;
                        if (param.SlaveNumber.HasValue) Parameters.SlaveNumber = (int)param.SlaveNumber.Value;
                        Parameters.SurveyTime = param.SurveyTime;
                        Parameters.SurveyType = param.SurveyType;
                        Parameters.DevicePort = param.DevicePort;
                        // Parameters.PointName= param.Equipment.
                        //Parameters.DeviceIp = WithoutSpaces(param.DeviceIp);
                        Parameters.ConnectionType = param.ConnectionType.Value;
                        Parameters.CommunicationChannel = (int)param.CommunicationChannel;
                        Parameters.DeviseAdress = Convert.ToInt32(param.DeviceAddress);
                        Parameters.DriverFeatures = param.DriverFeatures;
                        param.COM_Name = param.COM_Name.Trim().Replace(" ", string.Empty);
                        Parameters.Com_Name = param.COM_Name;
                        if (param.LastPollingTimeAgo.HasValue)
                            Parameters.LastPollingTimeAgo = param.LastPollingTimeAgo.Value;


                        if (!param.NeedToRead.HasValue)
                        {
                            Parameters.NeedToBeRead = true;
                        }
                        else
                        {
                            Parameters.NeedToBeRead = param.NeedToRead.Value;

                        }

                        if (param.BadPollsNumber.HasValue)
                            Parameters.BadPollsNumber = param.BadPollsNumber.Value;

                        foreach (var par in Equipment)
                        {
                            if (param.EquipmentId == par.Id)
                            {
                                Parameters.EquipmentTypeId = Convert.ToInt32(par.EquipmentTypeId);
                                Parameters.StationId = Convert.ToInt32(par.ReferencePointId);
                                break;
                            }
                        }
                        Parameters.Com_Name = param.COM_Name.Trim();
                        Parameters.Com_Speed = param.COM_Speed.ToString();

                        /////VIEW
                        EquipmentsInfo.Add(Parameters);

                        List<EquipmentTypeParameters> EquipmentTypeParameters_ar = DB.Database.SqlQuery<EquipmentTypeParameters>("Select * from EquipmentTypeParameters where EquipmentTypeId = " + param.Equipment.EquipmentTypeId).ToList<EquipmentTypeParameters>();
                        Equipments.Add(Parameters.DriverFeatures);

                        if (Parameters.StationId == 0)
                        {
                            EquipmentsStationName.Add("Modem");
                        }
                        else
                        {
                            EquipmentsStationName.Add(DB.Database.SqlQuery<ReferencePoints>
                                                    ("Select * from ReferencePoints where Id = " + Parameters.StationId).ToList<ReferencePoints>()[0].Name);
                        }

                        string s5NameEqType = "not found name";
                        foreach (var g in EquipmentsTypesInfo)
                        {
                            if (g.EquipmentTypeId == Parameters.EquipmentTypeId)
                            {
                                s5NameEqType = g.Name;
                                break;
                            }
                        }

                        string Adress = "NOADRESS: ";
                        try
                        {


                            if (!(Parameters.StationId == 0)) //ДЛя модема нет смісла искать адресс
                                try
                                {

                                    var g = DB.Database.SqlQuery<NameAdress>(@"Select 
                                  S.SOCR as SOCR_Street
                                 ,S.Name as Name_Street
                                 ,isnull(T.Name, '') as Name_Town
                                 ,isnull(D.Name, '') as Name_District
                                 ,isnull(CC.Name, '') as Name_City
                                 ,TZ.Name as Name_TimeZone
                                 ,C.House
                                from ControlObjects C
                                inner join STREET S on S.ID = C.StreetID
                                 inner join SOCRBASE SB_S on (SB_S.SCNAME =S.SOCR and isnull(SB_S.LEVEL, '') ='5')
                                  inner join TimeZones TZ on Convert(int, SUBSTRING(S.GNINMB,1, 2))= CONVERT(int, TZ.GNINMB)
                                left join KLADR T on T.ID = C.TownID
                                left join KLADR D on D.ID = C.DistrictID
                                left join KLADR CC on CC.ID = C.CityID
                                left join ReferencePoints RP on (RP.ObjectId = C.Id)
                                 left join StationEquipment SE on (SE.ReferencePointId = RP.Id)
                                where SE.Id = " + param.Id).ToList().First();
                                    Adress = g.Name_TimeZone + " " + g.Name_City + " " + g.Name_Town + " " + g.Name_District + " " + g.SOCR_Street + " " + g.Name_Street + " " + g.House;
                                    Adress = Adress.Trim();

                                    /*
                                    int gCid = DB.Database.SqlQuery<int>(
                                                   @"Select C.Id from ControlObjects C left join  ReferencePoints RP on (RP.ObjectId = C.Id) left join StationEquipment SE on (SE.ReferencePointId = RP.Id)
                                                   where SE.Id =1028").ToList().First();

                                    var h1 =DB.ControlObjects.Where(x => x.Id == gCid).ToList().First();
                                    var g1 = ObjectAddress(h1);
                                    Adress = g1;*/
                                }
                                catch (Exception)
                                {
                                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не получилось узнать адрес устройства с номером:" + Parameters.PhoneDialing);
                                }
                            Parameters.AdressCity = Adress;
                            listView.Items.Add(Adress + " : " +
                            " " + s5NameEqType + " " + Parameters.PhoneDialing + " " + Parameters.DriverFeatures + "   " + Parameters.ServerPort);
                        }
                        catch (Exception ex)
                        {
                            ColoredConsole.WriteLine(ex.Message + " Select request");
                            listView.Items.Add("Modem " + s5NameEqType + " " + Parameters.Com_Name);
                        }

                        EquipmentsDatas.Add(EquipmentTypeParameters_ar);
                        List<string> fgas = new List<string>();
                        List<string> fgas2 = new List<string>();

                        for (int i = 0; i < EquipmentTypeParameters_ar.Count; i++)
                        {
                            fgas.Add("-");
                            fgas2.Add("-");
                        }
                        lock (locker1)
                        {
                            EquipmentsDataValus.Add(fgas);
                            EquipmentsDataDates.Add(fgas2);
                        }

                        Parameters.OptionParams = new List<StationEquipmentDataParameters>();
                        foreach (var Station in EquipmentTypeParameters_ar)
                        {
                            StationEquipmentDataParameters h = new StationEquipmentDataParameters();

                            h.ObisCode = Station.ObisCode.Value;
                            h.ParameterNumber = Station.ParameterNumber;

                            Parameters.OptionParams.Add(h);
                        }

                        devicesData.Add(Parameters);
                        ///////VIEW
                    }

                    List<int> EqTypes = new List<int>();
                    EqTypes.Add(19);
                    EqTypes.Add(7);
                    //  EqTypes.Add(15);
                    EqTypes.Add(17);

                    //Make Lists
                    foreach (var pr in devicesData)
                    {
                        if (pr.CommunicationChannel == 0)
                        {
                            // if (EqTypes.IndexOf(pr.EquipmentTypeId) != -1)
                            dDList_Csd.Add(pr);
                        }
                        else if (pr.CommunicationChannel == 1)
                        {
                            dDList_Tcp.Add(pr);
                        }
                        else
                        {

                        }
                    }

                    try
                    {

                        CSD_Work();


                        Thread CheckForReadArchiveThread = new Thread(CheckForReadArchive);
                        CheckForReadArchiveThread.IsBackground = true;
                        CheckForReadArchiveThread.Start();
                    }
                    catch (Exception ex)
                    {
                        var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                        //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

            }
            ));
        }

        public MainWindow()
        {
            InitializeComponent();

            new Thread(() => MainWork()).Start();

        }

        public static string ObjectAddress(ControlObjects entry)
        {
            DBEntities_f db = new DBEntities_f();
            string s = string.Empty;
            int str = Convert.ToInt32(entry.StreetCode.Substring(0, 2));
            var obl = db.TimeZones.First(item => item.ID == str);
            bool city = obl.SOCR != "г" ? false : true;
            s += obl.NAME + " " + obl.SOCR + "., ";
            if (!city)
            {
                if (entry.DistrictID != null)
                    s += entry.KLADR1.NAME + " " + entry.KLADR1.SOCR + "., ";
                if (entry.CityID != null)
                    s += entry.KLADR.SOCR + ". " + entry.KLADR.NAME + ", ";
                if (entry.TownID != null)
                    s += entry.KLADR2.SOCR + ". " + entry.KLADR2.NAME + ", ";
            }
            s += entry.STREET.SOCR + ". " + entry.STREET.NAME + ", ";
            s += entry.House;
            return s;
        }


        public void SeeListSCDlistView()
        {
            Thread.Sleep(1000);

            listViewCSD.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                try
                {

                    /*
                    ObservableCollection<CheckBoxListViewItemSource> itemSources = new ObservableCollection<CheckBoxListViewItemSource>();
                    myListView.ItemsSource = itemSources;
                    int ii = 1;
                    for (int i = 0; i < 10; i++)
                    {
                        itemSources.Add(new CheckBoxListViewItemSource(String.Format("Short Item {0}", ii++)));
                    }
                    for (int i = 0; i < 5; i++)
                    {
                        itemSources.Add(new CheckBoxListViewItemSource(String.Format("Long Item {0} with long long long long long long long long long text", ii++)));
                    }
                    /**/

                    listViewCSD.Items.Clear();
                    int i = 0;
                    foreach (var device in dDList_Csd)
                    {
                        string s5 = "not found name";
                        foreach (var g in EquipmentsTypesInfo)
                        {
                            if (g.EquipmentTypeId == device.EquipmentTypeId)
                            {
                                s5 = g.Name;
                                break;
                            }
                        }
                        listViewCSD.Items.Add(new CheckBoxListViewItemSource(String.Format(i++ + ". " + device.AdressCity + "\n" + s5 + " " + device.PhoneDialing), device.NeedToBeRead, device.StationEquipmentId));
                        //(s5 + " " + device.PhoneDialing);
                    }
                }
                catch (Exception ex)
                {
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }));
        }
        public void SeeListSCD_Tree()
        {
            Thread.Sleep(1000);
            treeView.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                try
                {
                    treeView.Items.Clear();


                    foreach (var l in dDList_CsdByModem)
                    {
                        TreeViewItem item = new TreeViewItem();
                        item.Header = l.ListOfEquipmemnts.First().Com_Name;
                        List<string> s = new List<string>();
                        bool bo = false;
                        foreach (var l1 in l.ListOfEquipmemnts)
                        {
                            if (bo)
                            {
                                string s5 = "not found name";
                                foreach (var g in EquipmentsTypesInfo)
                                {
                                    if (g.EquipmentTypeId == l1.EquipmentTypeId)
                                    {
                                        s5 = g.Name;
                                        break;
                                    }
                                }

                                string ReadedTime = "Никогда";
                                try
                                {
                                    ReadedTime = l1.LastPollingTimeAgo.ToString();
                                    if (ReadedTime == "0") ReadedTime = "Никогда";

                                    if (l1.OutOfTurn)
                                    {
                                        ReadedTime = "Вне очереди";
                                    }

                                    if (!l1.NeedToBeRead)
                                    {
                                        ReadedTime = "Не нужно считывать";

                                    }
                                }
                                catch (Exception)
                                {

                                }

                                s.Add(s5 + " " + l1.PhoneDialing + " " + ReadedTime);
                            }
                            bo = true;
                        }
                        item.ItemsSource = s.ToArray();
                        item.IsExpanded = true;

                        treeView.Items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }));

        }

        public void CSD_Work()
        {
            try
            {
                // Заполнить модемами
                foreach (var g in dDList_Csd)
                {
                    if (g.EquipmentTypeId == 19)
                    {
                        CsdList l = new CsdList();
                        List<StationEquipmentData> d_Csd = new List<StationEquipmentData>();
                        d_Csd.Add(g);
                        l.ListOfEquipmemnts = d_Csd;
                        dDList_CsdByModem.Add(l);
                    }
                }

                // Заполнить всеми остальными
                foreach (var g in dDList_Csd)
                {
                    if (g.EquipmentTypeId != 19)
                    {
                        foreach (var l in dDList_CsdByModem)
                        {
                            if (g.Com_Name.Trim() == l.ListOfEquipmemnts.First().Com_Name.Trim())
                            {
                                l.ListOfEquipmemnts.Add(g);
                            }
                        }
                    }
                }

                // По потоку на модем (на COM)
                List<Thread> CSD_byModThreads = new List<Thread>();

                for (int i = 0; i < dDList_CsdByModem.Count; i++)
                {
                    int h = i;
                    Thread CSD_mod = new Thread(usused => CSD_BY_ModemWork(h));
                    CSD_mod.IsBackground = true;
                    CSD_byModThreads.Add(CSD_mod);
                }
                //Запустить потоки

                foreach (var g in CSD_byModThreads)
                {
                    g.Start();
                }
            }
            catch (Exception ex)
            {
                var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        struct twoInts
        {
            public int ID;
            public int Value;
        }

        public class StationEquipmentComparer : IComparer<StationEquipmentData>
        {
            public int Compare(StationEquipmentData x, StationEquipmentData y)
            {
                if (x.EquipmentTypeId == 19) return -1;
                if (y.EquipmentTypeId == 19) return 1;

                if (x.OutOfTurn) return -1;
                if (y.OutOfTurn) return 1;

                if (!x.NeedToBeRead) return 1;
                if (!y.NeedToBeRead) return -1;



                {
                    int xt = 400;

                    int yt = 400;

                    try
                    {
                        xt = Convert.ToInt32(x.SurveyTime);

                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Error in converting surveyTime variable :" + ex.Message);
                    }
                    try
                    {
                        yt = Convert.ToInt32(y.SurveyTime);

                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Error in surveyTime variable:" + ex.Message);

                    }


                    if ((x.LastPollingTimeAgo - DateTime.Now).TotalSeconds + xt < (y.LastPollingTimeAgo - DateTime.Now).TotalSeconds + yt)
                        return -1;
                    else
                        return 1;
                }


            }
        }

        public Driver ChooseDriver(StationEquipmentData device, int EquipmentTypeId)
        {
            Driver driver;
            switch (EquipmentTypeId)
            {
                case 4:
                    {
                        driver = new VKT_7(device);

                    }
                    break;
                /*
            case 5:
                {
                    driver = new Mercury230(device);
                }
                break;*/
                case 6:
                    {
                        driver = new TV7(device);
                    }
                    break;
                case 7:
                    {
                        driver = new TEM104(device);
                    }
                    break;/*
            case 8:
                {
                    driver = new VKT5(device);
                }
                break;
            //  case 9:
            //      {
            //          driver = new ADI(device); 
            //      }
            //      break;*/
                case 10:
                    {
                        driver = new KM5(device);
                    }
                    break;

                case 15:
                    {
                        driver = new TEM106(device);
                    }
                    break;
                /*
        case 16:
            {
                driver = new VCT9(device);
            }
            break;
        case 11:
            {
                driver = new Multical_602(device);
            }
            break;
        case 12:
            {
                driver = new VZLET_026M(device);
            }
            break;
        case 13:
            {
                driver = new TCPB_030(device);
            }
            break;
        //case 14:
        //    {
        //        driver = new VTE_1K(device); 
        //    }
        //    break;
        case 17:
            {
                driver = new Magika(device);
            }
            break;*/
                case 18:
                    {
                        driver = new VZLET_TCPB_024M(device);
                    }
                    break;
                default:
                    {

                        driver = new Driver(device);
                    }
                    break;

            }

            return driver;
        }

        public void CSD_BY_ModemWork(int CSD_Num)
        {
            StationEquipmentComparer SEC0 = new StationEquipmentComparer();
            dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.Sort(SEC0);
            SeeListSCDlistView();
            while (true)
            {
                // Просто графически обновляем то что считало
                SeeListSCD_Tree();

                if (dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.Count > 1)
                {
                    //ТУТ РАЗБИРАЕМ КОМАНДЫ
                    try
                    {
                        //1) натии еквипмент и на первое место
                        bool itFined = false;
                        int statID = dDList_CsdByModem.ElementAt(CSD_Num).Ccommand.StationEquipmentID;
                        if (statID != 0 && !dDList_CsdByModem.ElementAt(CSD_Num).commandExecuted)//Есть ли вообще команда и не модеи ли это 0-заглушка
                        {


                            for (int i = 1; i < dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.Count; i++)
                            {
                                if (dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts[i].StationEquipmentId == statID)
                                {
                                    //на начало его
                                    var devic2 = dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.ElementAt(i);
                                    dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.RemoveAt(i);
                                    dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.Insert(1, devic2);

                                    itFined = true;
                                    break;
                                }
                            }

                            //Если не на первом месте вообще тот (Ошибка очень маловероятна но все же...
                            if (itFined)
                            {

                                //2) заполнить для считывания
                                var device = dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.ElementAt(1);

                                var com = dDList_CsdByModem.ElementAt(CSD_Num).Ccommand;

                                if (com.InformReadType != 0)
                                {
                                    device.NeedToReadArchives = true;
                                    device.ArchivesParameter = com.DateFrom + "|" + com.DateTo;
                                    device.ArchivesType = com.InformReadType;
                                    device.ArchiveReaded = com.Readed;
                                }
                                device.CSDType = dDList_CsdByModem.ElementAt(CSD_Num).ListOfEquipmemnts.First().SlaveNumber;

                                Driver driver = ChooseDriver(device, device.EquipmentTypeId);
                                driver.Config.CurrentCommand = com;

                                //3) считать
                                List<DataParameter> List = new List<DataParameter>();
                                ArcHiveReadedPack ListArc = new ArcHiveReadedPack();

                                //ddEBUG
                                if (com.InformReadType == 0)
                                {

                                    try
                                    {
                                        UpdateOneCommandFromPool(com.ID, 0, "");
                                        if (device.NeedToBeRead)
                                        {
                                            List = (driver.ReadCurrent());
                                        }
                                        else
                                        {
                                            UpdateOneCommandFromPool(com.ID, 2, "Считано не будет, так как прибор отмечен как *не считывать* ");
                                            ColoredConsole.WriteLine("Считано не будет, так как прибор отмечен как \"не считывать\" ");
                                        }

                                        //List = new List<DataParameter>();
                                        UpdateOneCommandFromPool(com.ID, 1, "");
                                    }
                                    catch (Exception ex)
                                    {
                                        UpdateOneCommandFromPool(com.ID, 2, ex.Message);
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        UpdateOneCommandFromPool(com.ID, 0, "");
                                        if (device.NeedToBeRead)
                                        {

                                            ListArc = (driver.ReadArchive());
                                        }
                                        else
                                        {
                                            UpdateOneCommandFromPool(com.ID, 2, "Считано не будет, так как прибор отмечен как \"не считывать\" ");
                                            ColoredConsole.WriteLine("Считано не будет, так как прибор отмечен как \"не считывать\" ");
                                        }


                                        //ListArc = new ArcHiveReadedPack();
                                        dDList_CsdByModem.ElementAt(CSD_Num).commandExecuted = true;
                                        UpdateOneCommandFromPool(com.ID, 1, ListArc.errorMessages);
                                        // WriteToDB(ListArc.Data, ListArc.ReadedArchiveType); WASbEFORE now write driver little pack

                                    }
                                    catch (Exception ex)
                                    {
                                        UpdateOneCommandFromPool(com.ID, 2, ex.Message);
                                    }

                                    try
                                    {
                                        Current_ArchiveWindow.Dispatcher.BeginInvoke(new Action(delegate ()
                                        {
                                            try
                                            {
                                                Current_ArchiveWindow.Show();
                                            }
                                            catch (Exception ex)
                                            {
                                                //ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка при обображении Архивов:" + ex.Message);
                                            }
                                        }
                                        ));

                                        Current_ArchiveWindow.slider.Dispatcher.BeginInvoke(new Action(delegate ()
                                        {
                                            Current_ArchiveWindow.SEE_Archives(ListArc.Data);
                                        }
                                        ));

                                    }
                                    catch (Exception ex)
                                    {
                                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "P0 (закрыли окно, теперь ничего не увидите): " + ex.Message);
                                    }



                                }

                                //3) разобрать последствия (запись в базу)


                            }
                            else
                            {
                                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Есть команда но нет устройства, беееееда. (прожку чтоль перезапустить)");

                            }
                            dDList_CsdByModem.ElementAt(CSD_Num).commandExecuted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Неизведанная ошибка:" + ex.Message);
                    }


                }
                else
                    Thread.Sleep(5000);


                Thread.Sleep(500);
            }
        }

        public class Thing2
        {
            public string Option { get; set; }
            public string Value { get; set; }
            public Thing2(string Option, string Value) { this.Option = Option; this.Value = Value; }

        }
        public class Thing
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string DateCreate { get; set; }
            public string Register { get; set; }

            public string ObisCode { get; set; }
            public string ParamNum { get; set; }
            public string ParamName_BD { get; set; }

            public Thing(string Name, string Value, string datecreate, string register, string obisCode, string paramNum, string paramName_BD)
            {
                this.Name = Name;
                this.Value = Value;
                this.DateCreate = datecreate;
                this.Register = register;
                this.ObisCode = obisCode;
                this.ParamNum = paramNum;
                this.ParamName_BD = paramName_BD;
            }
        }

        private void getSelectedItem(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem lbi = ((sender as ListBox).SelectedItem as ListBoxItem);
            dataGrid.Items.Clear();

            int allNum = 0;
            int bedNum = 0;
            try
            {
                for (int i = 0; i < EquipmentsDatas[listView.SelectedIndex].Count; i++)
                {
                    string s1 = EquipmentsDatas[listView.SelectedIndex][i].Description;
                    string s2 = EquipmentsDataValus[listView.SelectedIndex][i];
                    string s3 = EquipmentsDataDates[listView.SelectedIndex][i];
                    string s4 = "-";

                    string s5 = EquipmentsDatas[listView.SelectedIndex][i].ObisCode.ToString();
                    string s6 = EquipmentsDatas[listView.SelectedIndex][i].ParameterNumber.ToString();

                    string s7 = "noNameInBase";

                    foreach (var a in ParametersInfo)
                    {
                        if (a.ObisCode == EquipmentsDatas[listView.SelectedIndex][i].ObisCode)
                        {
                            s7 = a.NameRus;
                            break;
                        }
                    }

                    if (EquipmentsDataValus[listView.SelectedIndex][i] == "-") bedNum++;
                    allNum++;

                    dataGrid.Items.Add(new Thing(s1, s2, s3, s4, s5, s6, s7));
                }
            }
            catch (Exception ex)
            {
                var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            try
            {
                {
                    string s1 = Equipments[listView.SelectedIndex];
                    string s2 = EquipmentsStationName[listView.SelectedIndex];
                    string s3 = EquipmentsInfo[listView.SelectedIndex].ServerPort;
                    string s4 = EquipmentsInfo[listView.SelectedIndex].DeviseAdress.ToString();

                    string s5 = "not found(";
                    foreach (var g in EquipmentsTypesInfo)
                    {
                        if (g.EquipmentTypeId == EquipmentsInfo[listView.SelectedIndex].EquipmentTypeId)
                        {
                            s5 = g.Name;
                            break;
                        }
                    }

                    dataGrid_StatHelp.Items.Clear();
                    dataGrid_StatHelp.Items.Add(new Thing2("Имя станции", s2));
                    dataGrid_StatHelp.Items.Add(new Thing2("Идентификатор", s1));
                    dataGrid_StatHelp.Items.Add(new Thing2("Порт на сервере", s3));
                    dataGrid_StatHelp.Items.Add(new Thing2("Devise Adress", s4));

                    dataGrid_StatHelp.Items.Add(new Thing2("Тип прибора", s5));
                }
            }
            catch (Exception ex)
            {
                var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void getSelectedItemCSD(object sender, MouseButtonEventArgs e)
        {
            try
            {
                int h = listViewCSD.SelectedIndex;

                string s5 = "not found name";
                foreach (var g in EquipmentsTypesInfo)
                {
                    if (g.EquipmentTypeId == dDList_Csd[h].EquipmentTypeId)
                    {
                        s5 = g.Name;
                        break;
                    }
                }
                label.Content = (s5 + "| " + dDList_Csd[h].DeviseAdress + " |" + dDList_Csd[h].PhoneDialing);


            }
            catch (Exception ex)
            {
                var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            for (int i = 0; i < threadPull.Count; i++)
            {
                try
                {
                    threadPull[i].Abort();
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ex.Message);
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    // MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        public void ServerThread()
        {
            XmlSerializer SerializerObj = new XmlSerializer(typeof(List<DataParameter>));

            DBEntities_f DB = new DBEntities_f();

            while (true)
            {
                try
                {
                    serverPipe = new NamedPipeServerStream("TEST_56_Irpen");
                    lock (locker)
                    {
                        serverPipe.WaitForConnection();
                        using (StreamReader reader = new StreamReader(serverPipe))
                        {
                            lock (locker)
                            {
                                foreach (var a in (List<DataParameter>)SerializerObj.Deserialize(reader)) MainData.Add(a);
                            }
                        }
                        serverPipe.Close();
                        serverPipe.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ex.Message);
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    continue;
                }
            }
        }

        public async Task WriteToDB(List<List<DataParameter>> list, int ArchiveType)
        {
            List<DataParameter> ret = new List<DataParameter>();
            foreach (var a in list)
            {
                foreach (var b in a)
                {
                    ret.Add(b);
                }
            }
            WriteToDB(ret, ArchiveType);
        }

        public void WriteToDB(List<DataParameter> list, int ArchiveType)
        {
            string _connString = "data source=localhost\\LERS;initial catalog=SPOU;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;";
            SqlConnection _con = new SqlConnection(_connString);
            foreach (var toSave in list)
            {
                try
                {
                    int ij1 = Equipments.IndexOf(toSave.DriverFeatures);
                    bool par_created = false;
                    if (ij1 >= 0)
                        for (int j = 0; j < EquipmentsDatas[ij1].Count; j++)
                        {
                            if (toSave.ParameterNumber == EquipmentsDatas[ij1][j].ParameterNumber &&
                                                                                      toSave.obisCode == EquipmentsDatas[ij1][j].ObisCode)
                            {
                                lock (locker1)
                                {
                                    par_created = true;
                                    if (toSave.value_str == "-")
                                        EquipmentsDataValus[ij1][j] = toSave.value.ToString();
                                    else
                                        EquipmentsDataValus[ij1][j] = toSave.value_str;

                                    EquipmentsDataDates[ij1][j] = toSave.DateCreate.ToString();

                                }
                            }
                        }

                    if (!par_created) ///CREATE NEW PARAMETER
                    {
                        EquipmentTypeParameters padf = new EquipmentTypeParameters();
                        padf.Description = "-";
                        padf.EquipmentTypeId = toSave.EquipmentTypeID;
                        padf.Description = "_Unnamed_New_Param";
                        padf.ObisCode = toSave.obisCode;
                        padf.ParameterNumber = toSave.ParameterNumber;


                        EquipmentsDatas[ij1].Add(padf);
                        EquipmentsDataValus[ij1].Add(toSave.value.ToString());
                        EquipmentsDataDates[ij1].Add(toSave.DateCreate.ToString());
                    }
                    if (toSave.value_str == "-")
                    {
                        DataTable query = new DataTable();

                        if (ArchiveType == 0)
                        {
                            using (SqlCommand cmd = new SqlCommand("ParametersToSet", _con))
                            {
                                if (Double.IsNaN(toSave.value)) toSave.value = -1;

                                String driverNumber = Convert.ToString(toSave.DriverFeatures);

                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = driverNumber;
                                cmd.Parameters.Add("@EquipmentTypeID", SqlDbType.Int).Value = toSave.EquipmentTypeID;
                                cmd.Parameters.Add("@ObisCode", SqlDbType.Int).Value = toSave.obisCode;
                                cmd.Parameters.Add("@ParameterNumber", SqlDbType.Int).Value = toSave.ParameterNumber;
                                cmd.Parameters.Add("@Value", SqlDbType.Float).Value = toSave.value;
                                cmd.Parameters.Add("@QualityId", SqlDbType.Int).Value = toSave.quality;
                                cmd.Parameters.Add("@DateToRead", SqlDbType.DateTime).Value = toSave.DateCreate;

                                try
                                {
                                    SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                                    _dap.Fill(query);
                                }
                                catch (Exception ex)
                                {
                                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                                }

                                foreach (DataRow par in query.Rows) { }
                            }
                        }
                        else
                        {
                            if (toSave.obisCode != 999)
                            {
                                using (SqlCommand cmd = new SqlCommand("ParametersToSet_Archive", _con))
                                {
                                    if (Double.IsNaN(toSave.value)) toSave.value = -1;

                                    String driverNumber = Convert.ToString(toSave.DriverFeatures);

                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = driverNumber;
                                    cmd.Parameters.Add("@EquipmentTypeID", SqlDbType.Int).Value = toSave.EquipmentTypeID;
                                    cmd.Parameters.Add("@ObisCode", SqlDbType.Int).Value = toSave.obisCode;
                                    cmd.Parameters.Add("@ParameterNumber", SqlDbType.Int).Value = toSave.ParameterNumber;
                                    cmd.Parameters.Add("@Value", SqlDbType.Float).Value = toSave.value;
                                    cmd.Parameters.Add("@QualityId", SqlDbType.Int).Value = toSave.quality;
                                    cmd.Parameters.Add("@DateToRead", SqlDbType.DateTime).Value = toSave.DateCreate;
                                    cmd.Parameters.Add("@ArchiveType", SqlDbType.Int).Value = ArchiveType;

                                    try
                                    {
                                        SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                                        _dap.Fill(query);

                                    }
                                    catch (Exception ex)
                                    {
                                        var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                                        //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                                    }

                                    foreach (DataRow par in query.Rows)
                                    {
                                    }

                                }
                            }
                            else
                            {
                                // was ObisCode 999
                            }
                        }




                    }
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ex.Message);
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    // MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int h = listViewCSD.SelectedIndex;

            // List < List < StationEquipmentData >>
            if (h == -1)
            {
                MessageBox.Show("Сперва выделите елемент которым хотите управлять!");
            }
            else
            {
                int c = dDList_Csd[h].StationEquipmentId;

                foreach (var hop in dDList_CsdByModem)
                {

                    var d = hop.ListOfEquipmemnts;
                    foreach (StationEquipmentData g in d)
                    {
                        if (g.StationEquipmentId == c)
                        {
                            if (d.IndexOf(g) > 1)
                            {
                                // REmove as first
                                d.Remove(g);
                                d.Insert(2, g);

                                g.OutOfTurn = true;
                            }
                            else
                            {
                                g.OutOfTurn = true;
                            }
                            break;
                        }
                    }
                }

            }


        }

        public static bool WindowShowed = false;
        public static int CurrentIndex = 0;
        public static ArchiveWindow Current_ArchiveWindow;
        private void button_ClickArchivre(object sender, RoutedEventArgs e)
        {
            if (!WindowShowed)
            {

                int h = listViewCSD.SelectedIndex;

                CurrentIndex = h;

                // List < List < StationEquipmentData >>
                if (h == -1)
                {
                    MessageBox.Show("Сперва выделите елемент которым хотите управлять!");
                }
                else
                {
                    WindowShowed = true;

                    Current_ArchiveWindow.Show();

                    /*
                    int c = dDList_Csd[h].StationEquipmentId;

                    foreach (var d in dDList_CsdByModem)
                    {


                        foreach (StationEquipmentData g in d)
                        {
                            if (g.StationEquipmentId == c)
                            {
                                if (d.IndexOf(g) > 1)
                                {,
                                    // REmove as first
                                    d.Remove(g);
                                    d.Insert(2, g);

                                    g.OutOfTurn = true;
                                }
                                else
                                //{
                                //    g.OutOfTurn = true;
                                }
                                break;
                            }
                        }
                    }*/

                }
            }
            else
            {
                //MessageBox.Show("Окно опроса архивов уже открыто. Сперва закройте окно!");
            }
        }

        public static void SetAcrhivesNEXT_READ(int Type, string Date_Time_from, string Date_Time_to, bool FromSite, int StEqID)
        {
            int h = CurrentIndex;
            int c;
            if (FromSite == true)
            {
                c = StEqID;
            }
            else
            {
                c = dDList_Csd[h].StationEquipmentId;
            }

            foreach (var hop in dDList_CsdByModem)
            {
                var d = hop.ListOfEquipmemnts;

                foreach (StationEquipmentData g in d)
                {
                    if (g.StationEquipmentId == c)
                    {
                        if (d.IndexOf(g) > 1)
                        {
                            // REmove as first
                            d.Remove(g);
                            d.Insert(2, g);

                            g.OutOfTurn = true;
                            g.NeedToReadArchives = true;
                            g.ArchivesParameter = Date_Time_from + "|" + Date_Time_to;
                            g.ArchivesType = Type;
                        }
                        else
                        {
                            g.OutOfTurn = true;

                            g.NeedToReadArchives = true;
                            g.ArchivesParameter = Date_Time_from + "|" + Date_Time_to;
                            g.ArchivesType = Type;
                        }
                        break;
                    }
                }
            }

            MessageBox.Show(Type + "|" + Date_Time_from + "|" + Date_Time_to);
        }

        private void listViewCSD_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}


