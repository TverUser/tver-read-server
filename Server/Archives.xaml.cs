﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MasterParent;
using System.Windows.Controls;
using System.ComponentModel;

namespace Server
{
    public partial class ArchiveWindow : Window
    {

        public ArchiveWindow()
        {
            this.Dispatcher.BeginInvoke(new Action(delegate ()
            {

                InitializeComponent();
                myData = new List<List<DataParameter>>();

                Closing += OnWindowClosing;

            }
            ));
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            MainWindow.WindowShowed = false; 
            // Handle closing logic, set e.Cancel as needed
        }

        private void ReadArchivesNEW(object sender, RoutedEventArgs e)
        {
           
        }

        private void textBox_Copy_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        List<List<DataParameter>> myData;

        public void SEE_Archives(List<List<DataParameter>> Data)
        {
            myData = Data;
            slider.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                slider.Maximum = Data.Count - 1;
                SliderArch_DragCompleted(0);
            }

            ));
        }

        public class Thinggh
        {
            public string Description { get; set; }
            public string Info1 { get; set; }
            public string Value { get; set; }
            public string DateCreate { get; set; }
            public string ObisCode { get; set; }
            public string ParamNumber { get; set; }

            public Thinggh(string Description, string Value, string datecreate, string obisCode, string ParamNumber, string Info1)
            {
                this.Description = Description;
                this.Value = Value;
                this.DateCreate = datecreate;
                this.ObisCode = obisCode;
                this.ParamNumber = ParamNumber;
                this.Info1 = Info1;
            }
        }

        private void SliderArch_DragCompleted(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int h = (int) Math.Round((((Slider)sender).Value));

            SliderArch_DragCompleted(h);

        }
        public void SliderArch_DragCompleted(int h)
        {

            dataGridArchives.Items.Clear();
            

            if(myData.Count>0)
            try
            {
                    var temp = myData.ElementAt(h);
                   
                    for (int i = 0; i < temp.Count; i++)
                {
                    var el = temp.ElementAt(i);
                    var d = new Thinggh(el.Description, el.value.ToString(), el.DateCreate.ToString(), el.obisCode.ToString(),
                        el.ParameterNumber.ToString(), el.Info1);

                    dataGridArchives.Items.Add(d);
                }
            }
            catch (Exception ex)
            {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Error in SeeArchive :" + ex.Message);
                    //   var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
        }

    }


}


