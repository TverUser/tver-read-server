﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO.Ports;
using Server;

namespace Mercury_230
{
    public class Mercury230 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        public ManualResetEvent allDone = new ManualResetEvent(false);

        private byte NetworkNddress;
        private byte[] Initiaze_pak;

        byte[] Feature_pak;

        private byte[] Voltage3_pak;
        private byte[] Voltage2_pak;
        private byte[] Voltage1_pak;

        private byte[] ElCurrent3_pak;
        private byte[] ElCurrent2_pak;
        private byte[] ElCurrent1_pak;

        private byte[] Pover3_pak;
        private byte[] Pover2_pak;
        private byte[] Pover1_pak;

        public Mercury230(StationEquipmentData Params) : base(Params)
        {
            StationEquipment = new StationEquipmentData(Params);
            MakeCRC();

            if (StationEquipment.ConnectionType == 1)
            {
                StationEquipment.DeviseAdress = 0;
                MakeCRC();
                Thread mercury = new Thread(StartAsyncTcp);
                mercury.Start();
                ColoredConsole.WriteLine("Tcp Start Mercury");
            }
        }

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Setting COM-port settings
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;
            port.DtrEnable = true;
            port.RtsEnable = true;

            try
            {
                if (!port.IsOpen) port.Open(); // Opening COM-port if it is closed

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                ColoredConsole.WriteLine("Begin to reading data from mercury...");
                List<double> readedList = new List<double>();

                byte[] Initiaze_recive = { };
                byte[] Feature_recive = { };

                byte[] Voltage3_recive = { };
                byte[] Voltage2_recive = { };
                byte[] Voltage1_recive = { };

                byte[] ElCurrent3_recive = { };
                byte[] ElCurrent2_recive = { };
                byte[] ElCurrent1_recive = { };

                byte[] Pover3_recive = { };
                byte[] Pover2_recive = { };
                byte[] Pover1_recive = { };

                Initiaze_recive = ReadProperties(Initiaze_pak, port);

                Feature_recive = ReadProperties(Feature_pak, port);
                string s1 = Feature_recive[1].ToString(); if (s1.Length == 1) s1 = "0" + s1;
                string s2 = Feature_recive[2].ToString(); if (s2.Length == 1) s2 = "0" + s2;
                string s3 = Feature_recive[3].ToString(); if (s3.Length == 1) s3 = "0" + s3;
                string s4 = Feature_recive[4].ToString(); if (s4.Length == 1) s4 = "0" + s4;
                uint feature = Convert.ToUInt32(s1 + s2 + s3 + s4);

                Voltage3_recive = ReadProperties(Voltage3_pak, port); double Voltage3 = Get_CurrentVoltage(Voltage3_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 97, 2, Voltage3, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage3);

                Voltage2_recive = ReadProperties(Voltage2_pak, port); double Voltage2 = Get_CurrentVoltage(Voltage2_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 97, 1, Voltage2, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage2);

                Voltage1_recive = ReadProperties(Voltage1_pak, port); double Voltage1 = Get_CurrentVoltage(Voltage1_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 97, 0, Voltage1, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage1);

                ElCurrent3_recive = ReadProperties(ElCurrent3_pak, port); double ElCurrent3 = Get_CurrentVoltage(ElCurrent3_recive) / 1000;
                Data.Add(new DataParameter(feature.ToString(), 3, 2, ElCurrent3, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent3);

                ElCurrent2_recive = ReadProperties(ElCurrent2_pak, port); double ElCurrent2 = Get_CurrentVoltage(ElCurrent2_recive) / 1000;
                Data.Add(new DataParameter(feature.ToString(), 3, 1, ElCurrent2, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent3); readedList.Add(ElCurrent2);

                ElCurrent1_recive = ReadProperties(ElCurrent1_pak, port); double ElCurrent1 = Get_CurrentVoltage(ElCurrent1_recive) / 1000;
                Data.Add(new DataParameter(feature.ToString(), 3, 0, ElCurrent1, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent1);

                Pover3_recive = ReadProperties(Pover3_pak, port); double Pover3 = Get_Pover(Pover3_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 2, 2, Pover3, StationEquipment.EquipmentTypeId)); readedList.Add(Pover3);

                Pover2_recive = ReadProperties(Pover2_pak, port); double Pover2 = Get_Pover(Pover2_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 2, 1, Pover2, StationEquipment.EquipmentTypeId)); readedList.Add(Pover2);

                Pover1_recive = ReadProperties(Pover1_pak, port); double Pover1 = Get_Pover(Pover1_recive) / 100;
                Data.Add(new DataParameter(feature.ToString(), 2, 0, Pover1, StationEquipment.EquipmentTypeId)); readedList.Add(Pover1);

                CSD_End(port);
                port.Close();
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));

                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);
                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    ColoredConsole.WriteLine("Waiting for a connection mercury... " + StationEquipment.ServerPort);  // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing.
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("mercury : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState;  // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begin to reading data from mercury...");
                    List<double> readedList = new List<double>();

                    byte[] Initiaze_recive = { };

                    byte[] Feature_recive = { };

                    byte[] Voltage3_recive = { };
                    byte[] Voltage2_recive = { };
                    byte[] Voltage1_recive = { };

                    byte[] ElCurrent3_recive = { };
                    byte[] ElCurrent2_recive = { };
                    byte[] ElCurrent1_recive = { };

                    byte[] Power3_recive = { };
                    byte[] Power2_recive = { };
                    byte[] Power1_recive = { };

                    ColoredConsole.WriteLine("Start");
                    Initiaze_recive = ReadProperties(Initiaze_pak, handler);

                    Feature_recive = ReadProperties(Feature_pak, handler);
                    string s1 = Feature_recive[1].ToString(); if (s1.Length == 1) s1 = "0" + s1;
                    string s2 = Feature_recive[2].ToString(); if (s2.Length == 1) s2 = "0" + s2;
                    string s3 = Feature_recive[3].ToString(); if (s3.Length == 1) s3 = "0" + s3;
                    string s4 = Feature_recive[4].ToString(); if (s4.Length == 1) s4 = "0" + s4;
                    uint feature = Convert.ToUInt32(s1 + s2 + s3 + s4);

                    ColoredConsole.WriteLine(feature);
                    Voltage3_recive = ReadProperties(Voltage3_pak, handler); double Voltage3 = Get_CurrentVoltage(Voltage3_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 97, 2, Voltage3, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage3);

                    ColoredConsole.WriteLine(Voltage3_recive);
                    Voltage2_recive = ReadProperties(Voltage2_pak, handler); double Voltage2 = Get_CurrentVoltage(Voltage2_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 97, 1, Voltage2, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage2);

                    Voltage1_recive = ReadProperties(Voltage1_pak, handler); double Voltage1 = Get_CurrentVoltage(Voltage1_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 97, 0, Voltage1, StationEquipment.EquipmentTypeId)); readedList.Add(Voltage1);

                    ElCurrent3_recive = ReadProperties(ElCurrent3_pak, handler); double ElCurrent3 = Get_CurrentVoltage(ElCurrent3_recive) / 1000;
                    Data.Add(new DataParameter(feature.ToString(), 3, 2, ElCurrent3, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent3);

                    ElCurrent2_recive = ReadProperties(ElCurrent2_pak, handler); double ElCurrent2 = Get_CurrentVoltage(ElCurrent2_recive) / 1000;
                    Data.Add(new DataParameter(feature.ToString(), 3, 1, ElCurrent2, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent3); readedList.Add(ElCurrent2);

                    ElCurrent1_recive = ReadProperties(ElCurrent1_pak, handler); double ElCurrent1 = Get_CurrentVoltage(ElCurrent1_recive) / 1000;
                    Data.Add(new DataParameter(feature.ToString(), 3, 0, ElCurrent1, StationEquipment.EquipmentTypeId)); readedList.Add(ElCurrent1);

                    Power3_recive = ReadProperties(Pover3_pak, handler); double Power3 = Get_Pover(Power3_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 2, 2, Power3, StationEquipment.EquipmentTypeId)); readedList.Add(Power3);

                    Power2_recive = ReadProperties(Pover2_pak, handler); double Power2 = Get_Pover(Power2_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 2, 1, Power2, StationEquipment.EquipmentTypeId)); readedList.Add(Power2);

                    Power1_recive = ReadProperties(Pover1_pak, handler); double Power1 = Get_Pover(Power1_recive) / 100;
                    Data.Add(new DataParameter(feature.ToString(), 2, 0, Power1, StationEquipment.EquipmentTypeId)); readedList.Add(Power1);

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(2000);
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("mercury : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); handler.Disconnect(true); handler.Close(); return; }
        }

        private double Get_CurrentVoltage(byte[] Voltage)
        {
            return ((int)Voltage[1] << 16 | (int)Voltage[3] << 8 | (int)Voltage[2]);
        }

        private double Get_Pover(byte[] Pover)
        {
            int a = 1;
            if (((int)Pover[1] & (-128)) != 0) a = -1;

            return ((((int)Pover[1] & (127)) << 16) | (int)Pover[3] << 8 | (int)Pover[2]) * a;
        }

        private byte[] ReadProperties(byte[] TheParameters, Socket port)
        {
            WritePacket(port, TheParameters);
            return ReadPacket(port, TheParameters);
        }

        private byte[] ReadProperties(byte[] TheParameters, SerialPort port)
        {
            WritePacket(port, TheParameters); Thread.Sleep(200);
            return ReadPacket(port, TheParameters);
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

        private void MakeCRC()
        {
            NetworkNddress = 0;

            Initiaze_pak = new byte[] { NetworkNddress, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00 };

            Feature_pak = new byte[] { NetworkNddress, 0x08, 0x00, 0x00, 0x00 };

            Voltage3_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x13, 0x00, 0x00 };
            Voltage2_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x12, 0x00, 0x00 };
            Voltage1_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x11, 0x00, 0x00 };

            ElCurrent3_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x23, 0x00, 0x00 };
            ElCurrent2_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x22, 0x00, 0x00 };
            ElCurrent1_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x21, 0x00, 0x00 };

            Pover3_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x03, 0x00, 0x00 };
            Pover2_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x02, 0x00, 0x00 };
            Pover1_pak = new byte[] { NetworkNddress, 0x08, 0x11, 0x01, 0x00, 0x00 };

            CRC16(Initiaze_pak, Initiaze_pak.Length);
            CRC16(Feature_pak, Feature_pak.Length);

            CRC16(Voltage3_pak, Voltage3_pak.Length);
            CRC16(Voltage2_pak, Voltage2_pak.Length);
            CRC16(Voltage1_pak, Voltage1_pak.Length);

            CRC16(ElCurrent3_pak, ElCurrent3_pak.Length);
            CRC16(ElCurrent2_pak, ElCurrent2_pak.Length);
            CRC16(ElCurrent1_pak, ElCurrent1_pak.Length);

            CRC16(Pover3_pak, Pover3_pak.Length);
            CRC16(Pover2_pak, Pover2_pak.Length);
            CRC16(Pover1_pak, Pover1_pak.Length);
        }

        public byte[] ReadPacket(SerialPort port, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 10)
                {
                    int byteCount = port.BytesToRead;
                    byte[] retBytes = new byte[byteCount];
                    port.Read(retBytes, 0, byteCount);

                    if (byteCount < 2)
                    {
                        WritePacket(port, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        count++;
                        WritePacket(port, lastSend);
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        public byte[] ReadPacket(Socket socket, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 10)
                {
                    int byteCount = socket.ReceiveBufferSize;
                    byte[] retBytes = new byte[100];
                    int bytesRead = socket.Receive(retBytes);

                    retBytes = retBytes.Take(bytesRead).ToArray();

                    if (byteCount < 2)
                    {
                        WritePacket(socket, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        WritePacket(socket, lastSend);
                        count++;
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }
    }
}
