﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.IO.Ports;
using System.Net.Sockets;
using System.Net;
using Modbus.Device;
using Server;

namespace namespaceTV7
{


    public class TV7 : Driver
    {
        private List<Parameter> ParametersListCurrentToRead = new List<Parameter>();
        private int[] register = { 0x00, 0x0dd7, 0x0e31, 0x0d57, 0x0d87 };
        private int[] sizes = { 0x07, 0x50, 0x10, 0x30, 0x24 };

        public TV7(StationEquipmentData param) : base(param)
        {

            for (int i = 0; i < register.Length; i++)
            {
                ParametersListCurrentToRead.Add(new Parameter(register[i], sizes[i]));
            }

        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {

        }


        public override string GetSerialNumber()
        {

            comm.CreateRtu();
            byte[] receiveser = MakeByteArray(comm.master.ReadHoldingRegisters(Convert.ToByte(Config.DeviseAdress), 0, 7));

            var serial = ReadProperties(receiveser);

            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true;
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }

        public override bool ConfigBeforeReadArchive()
        {

            //Считывание даты минимально возможной записи
            try
            {

                var FromDATEtemp = (comm.master.ReadHoldingRegisters(Convert.ToByte(Config.DeviseAdress), 2676, 12));
                var FromDATE = MakeByteArrayreverse2(FromDATEtemp);


                var date1 = getDATE(FromDATE, 0);
                var date2 = getDATE(FromDATE, 6);
                var date3 = getDATE(FromDATE, 12);
                var date4 = getDATE(FromDATE, 18);
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Минимальная дата записи часового архива : " + date1);
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Минимальная дата записи суточного архива : " + date2);
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Минимальная дата записи месячного архива : " + date3);
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Минимальная дата записи итогового архива : " + date4);

            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать :Минимальные даты записи архивов");
                return false;
            }


            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            ArcHiveReadedPack ArchivesList = new ArcHiveReadedPack();

            int arcType = 0;
            if (Config.ArchivesType == 1) arcType = 0;
            if (Config.ArchivesType == 2) arcType = 1;
            if (Config.ArchivesType == 3) arcType = 2;

            List<DataParameter> ArchivesData = ArchivesReturnedPack(Config.DriverFeatures, 6, comm.master, arcType, month, day, year, hour);

            ArchivesList.Data.Add(ArchivesData);
            ArchivesList.Status = STATUS_GOD;
            return ArchivesList;
        }
        #endregion

        public class Parameter
        {
            public int Register = 0;
            public int size = 0;

            public Parameter(int register, int size)
            {
                this.Register = register;
                this.size = size;
            }

            public Parameter(Parameter Par)
            {
                this.Register = Par.Register;
                this.size = Par.size;
            }
        }


        private void ReadData(ModbusSerialMaster master)
        {
            int serialNumber = 0;
            List<DataParameter> retdATA = new List<DataParameter>();
            for (int i = 0; i < ParametersListCurrentToRead.Count; i++)
            {
                byte[] receive = MakeByteArray(master.ReadHoldingRegisters(Convert.ToByte(Config.DeviseAdress), Convert.ToUInt16(ParametersListCurrentToRead[i].Register), Convert.ToUInt16(ParametersListCurrentToRead[i].size)));

                switch (i)
                {
                    case 0:
                        {
                            serialNumber = ReadProperties(receive);
                            if (serialNumber == 0)
                            {
                                ColoredConsole.WriteLine(3, "Не удалось считать данные с ТВ-7 : " + Config.PhoneDialing);
                                return;
                            }
                            else ColoredConsole.WriteLine(3, "ТВ-7 Считано серийный номер : " + serialNumber);
                        }
                        break;
                    case 1:
                        {
                            int count = 0;
                            int obisCode = 0;
                            int parameterNumber = 0;

                            for (int j = 0; j < 6; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        {
                                            obisCode = 95;
                                            parameterNumber = 0;
                                        }
                                        break;
                                    case 1:
                                        {
                                            obisCode = 1;
                                            parameterNumber = 0;
                                        }
                                        break;
                                    case 2:
                                        {
                                            obisCode = 94;
                                            parameterNumber = 0;
                                        }
                                        break;
                                    case 3:
                                        {
                                            obisCode = 93;
                                            parameterNumber = 0;
                                        }
                                        break;
                                    case 4:
                                        {
                                            obisCode = 88;
                                            parameterNumber = 0;
                                        }
                                        break;
                                    case 5:
                                        {
                                            obisCode = 87;
                                            parameterNumber = 0;
                                        }
                                        break;
                                }
                                byte[] data = receive.Skip(count).Take(24).ToArray();
                                ParametersAdd(data, Convert.ToInt32(Config.DriverFeatures), obisCode, parameterNumber, 4, 6);
                                count += 24;
                            }

                            for (int j = 0; j < 2; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        {
                                            obisCode = 88;
                                            parameterNumber = 6;
                                        }
                                        break;
                                    case 1:
                                        {
                                            obisCode = 87;
                                            parameterNumber = 6;
                                        }
                                        break;
                                }
                                byte[] data = receive.Skip(count).Take(8).ToArray();
                                ParametersAdd(data, Convert.ToInt32(Config.DriverFeatures), obisCode, parameterNumber, 4, 2);
                                count += 8;
                            }
                            ColoredConsole.WriteLine(3, "ТВ-7 : " + serialNumber + " - Считано 20%");
                        }
                        break;
                    case 2:
                        {
                            // 32 bytes
                            int count = 0;
                            int obisCode = 0;
                            int parameterNumber = 0;

                            for (int j = 0; j < 4; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        {
                                            obisCode = 95;
                                            parameterNumber = 6;
                                        }
                                        break;
                                    case 1:
                                        {
                                            obisCode = 1;
                                            parameterNumber = 6;
                                        }
                                        break;
                                    case 2:
                                        {
                                            obisCode = 95;
                                            parameterNumber = 8;
                                        }
                                        break;
                                    case 3:
                                        {
                                            obisCode = 95;
                                            parameterNumber = 10;
                                        }
                                        break;
                                }
                                byte[] data = receive.Skip(count).Take(8).ToArray();
                                ParametersAdd(data, serialNumber, obisCode, parameterNumber, 4, 2);
                                count += 8;
                            }
                            ColoredConsole.WriteLine(3, "ТВ-7 : " + serialNumber + " - Считано 42%");
                        }
                        break;
                    case 3:
                        {
                            // 96 bytes
                            int count = 0;
                            int obisCode = 0;
                            int parameterNumber = 0;

                            for (int j = 0; j < 6; j++)
                            {
                                byte[] data = receive.Skip(count).Take(16).ToArray();
                                for (int k = 0; k < 2; k++)
                                {
                                    switch (k)
                                    {
                                        case 0:
                                            {
                                                obisCode = 91;
                                                parameterNumber = j;
                                                ParametersAdd(data.Skip(0).Take(8).ToArray(), serialNumber, obisCode, parameterNumber, 8, 1);
                                            }
                                            break;
                                        case 1:
                                            {
                                                obisCode = 90;
                                                parameterNumber = j;
                                                ParametersAdd(data.Skip(8).Take(8).ToArray(), serialNumber, obisCode, parameterNumber, 8, 1);
                                            }
                                            break;
                                    }
                                }
                                count += 16;
                            }
                            ColoredConsole.WriteLine(3, "ТВ-7 : " + serialNumber + " - Считано 65%");
                        }
                        break;
                    case 4:
                        {
                            // 64 bytes
                            int count = 0;

                            for (int j = 0; j < 2; j++)
                            {
                                byte[] data = receive.Skip(count).Take(32).ToArray();
                                ParametersAdd(data.Take(8).ToArray(), serialNumber, 90, 6, 8, 1);

                                count += 8;

                                for (int k = 0; k < 1; k++)
                                {
                                    data = receive.Skip(count).Take(24).ToArray();
                                    ParametersAdd(data, serialNumber, 13, j * 3, 8, 3);
                                    count += 24;
                                }

                                data = receive.Skip(count).Take(2).ToArray();
                                retdATA.Add(new DataParameter(serialNumber.ToString(), 10, j, Convert.ToDouble(BitConverter.ToUInt16(data, 0)), Config.EquipmentTypeId));
                                count += 2;


                                data = receive.Skip(count).Take(2).ToArray();
                                retdATA.Add(new DataParameter(serialNumber.ToString(), 51, j, Convert.ToDouble(BitConverter.ToUInt16(data, 0)), Config.EquipmentTypeId));
                                count += 2;
                            }
                            ColoredConsole.WriteLine(3, "ТВ-7 : " + serialNumber + " - Считано 100%");
                        }
                        break;
                    default:
                        {
                            ColoredConsole.WriteLine("Unbelivable");
                        }
                        break;
                }
            }
        }

        public int[] getNums(ushort[] at)
        {
            int[] ret = new int[2];
            ret[1] = (at[0] & 0xff00) >> 8;
            ret[0] = at[0] & 0x00ff;

            return ret;
        }

        public int[] getNums(ushort at)
        {
            int[] ret = new int[2];
            ret[1] = (at & 0xff00) >> 8;
            ret[0] = at & 0x00ff;

            return ret;
        }

        public string getDATE(byte[] ar, int index)
        {
            int day = ar[index + 0];
            int month = ar[index + 1];
            int year = ar[index + 2];
            int hour = ar[index + 3];
            int minute = ar[index + 4];
            int seconds = ar[index + 5];
            string ret = day.ToString() + "." + month.ToString() + "."
                + (year + 2000).ToString() + "/" + hour.ToString() + ":"
                + minute.ToString() + "." + seconds.ToString() + "";

            return ret;
        }

        public ushort[] ReadHoldingRegistersMy(ushort[] ALL, byte DeviceAdress, int adress, int bytes)
        {
            ushort[] ret = new ushort[bytes];
            int count = 0;
            for (int i = adress - 2740; i < adress - 2740 + bytes; i++)
            {
                ret[count++] = ALL[i];
            }
            return ret;
        }

        public List<DataParameter> ArchivesReturnedPack(string serial, int EquipType, ModbusSerialMaster master, int ArchiveType, int month, int day, int year, int hour)
        {

            List<DataParameter> tempD = new List<DataParameter>();


            //Month | day
            ushort dayMonth = Convert.ToUInt16((month << 8) | day);

            // Hour Year
            ushort yearHour = Convert.ToUInt16((hour << 8) | (year - 2000));
            ushort minuteSecond = Convert.ToUInt16(0);

            master.WriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 99, new ushort[] { dayMonth, yearHour, 0, (ushort)ArchiveType });

            var ALL = (master.ReadHoldingRegisters(Convert.ToByte(Config.DeviseAdress), 2740, 103));



            byte[] rec_2740 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2740, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2740[1], EquipType, DateTime.Now, "Дата: день"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2740[0], EquipType, DateTime.Now, "Дата: месяц"));

            byte[] rec_2741 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2741, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2741[1], EquipType, DateTime.Now, "Дата: год-2000"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2741[0], EquipType, DateTime.Now, "Время: час"));

            DateTime FromWhere = new DateTime((int)rec_2741[1] + 2000, (int)rec_2740[0], (int)rec_2740[1], (int)rec_2741[0], 0, 0);


            // Значения по трубам
            int bazeTryba = 2742;
            for (int i = 0; i < 6; i++)
            {
                ushort bazeTryba2 = (ushort)(bazeTryba + i * 8);
                string nameParam = "ТВ" + (i / 3 + 1) + " Труба " + (i % 3 + i / 3 + 1) + " ";

                float rec_t = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 95, i, rec_t, EquipType, FromWhere, nameParam + "t"));
                bazeTryba2 += 2;

                float rec_P = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 1, i, rec_P, EquipType, FromWhere, nameParam + "P"));
                bazeTryba2 += 2;

                float rec_V = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 91, i, rec_V, EquipType, FromWhere, nameParam + "V"));
                bazeTryba2 += 2;

                float rec_M = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 90, i, rec_M, EquipType, FromWhere, nameParam + "M"));
                bazeTryba2 += 2;
            }

            // Значения по тепловым вводам
            int bazeTrybatep = 2790;
            for (int i = 0; i < 2; i++)
            {
                ushort bazeTryba2 = (ushort)(bazeTrybatep + i * 18);
                string nameParam = "ТВ" + (i / 3 + 1) + " ";

                float rec_tнв = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 95, 10 + i, rec_tнв, EquipType, FromWhere, nameParam + "tнв"));
                bazeTryba2 += 2;

                float rec_tх = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 95, 8 + i, rec_tх, EquipType, FromWhere, nameParam + "tх"));
                bazeTryba2 += 2;

                float rec_Pх = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 1, 6 + i, rec_Pх, EquipType, FromWhere, nameParam + "Pх"));
                bazeTryba2 += 2;

                float rec_dt = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 95, 6 + i, rec_dt, EquipType, FromWhere, nameParam + "dt"));
                bazeTryba2 += 2;

                float rec_dM = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 0, 1, rec_dM, EquipType, FromWhere, nameParam + "dM"));
                bazeTryba2 += 2;

                float rec_Qтв = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 13, 0, rec_Qтв, EquipType, FromWhere, nameParam + "Qтв"));
                bazeTryba2 += 2;

                float rec_Q12 = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 13, 1, rec_Q12, EquipType, FromWhere, nameParam + "Q12"));
                bazeTryba2 += 2;

                float rec_Qг = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 4)));
                tempD.Add(new DataParameter(serial, 13, 2, rec_Qг, EquipType, FromWhere, nameParam + "Qг"));
                bazeTryba2 += 2;

                ushort[] rec_ВНР = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 2));
                tempD.Add(new DataParameter(serial, 10, i, (double)rec_ВНР[0], EquipType, FromWhere, "ВНР"));
                bazeTryba2 += 1;

                ushort[] rec_ВОС = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), bazeTryba2, 2));
                tempD.Add(new DataParameter(serial, 51, i, (double)rec_ВОС[0], EquipType, FromWhere, "ВОС"));
            }

            //-- Значение по ДП
            float rec_2826 = FloatConvert(MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2826, 4)));
            tempD.Add(new DataParameter(serial, 0, 1, rec_2826, EquipType, FromWhere, "Значение по ДП"));



            byte[] rec_2828 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2828, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2828[1], EquipType, FromWhere, "НС по ТВ1 Труба 1"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2828[0], EquipType, FromWhere, "НС по ТВ1 Труба 2"));

            byte[] rec_2829 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2829, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2829[1], EquipType, FromWhere, "НС по ТВ1 Труба 3"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2829[0], EquipType, FromWhere, "НС по ТВ2 Труба 1"));

            byte[] rec_2830 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2830, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2830[1], EquipType, FromWhere, "НС по ТВ2 Труба 2"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2830[0], EquipType, FromWhere, "НС по ТВ2 Труба 3"));

            ushort[] rec_2831 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2831, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2831[0], EquipType, FromWhere, "НС по ТВ1"));

            ushort[] rec_2832 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2832, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2832[0], EquipType, FromWhere, "НС по ТВ2"));

            byte[] rec_2833 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2833, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2833[1], EquipType, FromWhere, "НС по ДП"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2833[0], EquipType, FromWhere, "Резерв 1"));

            ushort[] rec_2834 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2834, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2834[0], EquipType, FromWhere, "Признаки событий"));

            ushort[] rec_2835 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2835, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2835[0], EquipType, FromWhere, "Резерв 2"));

            ushort[] rec_2836 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2836, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2836[0], EquipType, FromWhere, "Длит.работы по сети"));

            ushort[] rec_2837 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2837, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2837[0], EquipType, FromWhere, "Длит.работы дисплея"));

            ushort[] rec_2838 = (ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2838, 2));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2838[0], EquipType, FromWhere, "Длит.отсут.сет.питания"));

            byte[] rec_2839 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2839, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2839[0], EquipType, FromWhere, "Активная БД"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2839[1], EquipType, FromWhere, "СИ ТВ1"));

            byte[] rec_2840 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2840, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2840[1], EquipType, FromWhere, "КТ3 ТВ1"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2840[0], EquipType, FromWhere, "ФРТ ТВ1"));

            byte[] rec_2841 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2841, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2841[1], EquipType, FromWhere, "Активная БД"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2841[0], EquipType, FromWhere, "СИ ТВ2"));

            byte[] rec_2842 = MakeByteArray(ReadHoldingRegistersMy(ALL, Convert.ToByte(Config.DeviseAdress), 2842, 1));
            tempD.Add(new DataParameter(serial, 0, 1, (double)rec_2842[1], EquipType, FromWhere, "КТ3 ТВ2"));
            tempD.Add(new DataParameter(serial, 0, 2, (double)rec_2842[0], EquipType, FromWhere, "ФРТ ТВ2"));






            return tempD;
        }

        private int ReadProperties(byte[] receive) // Функция чтения свойств
        {
            string programVersion = receive[2] + "." + receive[3]; // Программная версия
            string hardwareVersion = receive[4] + "." + receive[5]; // Аппаратная версия
            string PO_CRC = BitConverter.ToString(new byte[] { receive[6], receive[7] }).Replace("-", ""); // CRC для ПО
            byte deviceModel = receive[8]; // Модель прибора
            byte deviceReserv = receive[9]; //Какой-то резерв
            byte[] deviceNumber = receive.Skip(10).Take(4).ToArray(); //Массив хранящий байты для преобразования в серийный номер

            ColoredConsole.WriteLine("Programme version : " + programVersion);
            ColoredConsole.WriteLine("Hardware version : " + hardwareVersion);
            ColoredConsole.WriteLine("PO-CRC : " + PO_CRC);
            ColoredConsole.WriteLine("Device Model : " + getDeviceModel(deviceModel));
            ColoredConsole.WriteLine("Device reserve : " + deviceReserv);
            ColoredConsole.WriteLine("Serial Number : " + LongConvert(deviceNumber));

            return LongConvert(deviceNumber); // Возвращение серийного номера
        }

        public void ParametersAdd(byte[] receive, int serialNumber, int obisCode, int parameterNumber, int size, int count)
        {
            List<DataParameter> retData = new List<DataParameter>();
            double value = 0;
            for (int i = 0; i < count; i++)
            {
                if (size == 4) value = Convert.ToDouble(FloatConvert(receive.Skip(i * 4).Take(4).ToArray()));
                if (size == 8) value = DoubleConvert(receive.Skip(i * 8).Take(8).ToArray());
                if (Double.IsNaN(value)) value = 0;
                retData.Add(new DataParameter(serialNumber.ToString(), obisCode, parameterNumber, value, Config.EquipmentTypeId));
                parameterNumber++;
            }
        }

        private string getDeviceModel(byte data) // Функция получения модели прибора по присланному байту из ответа
        {
            switch (data) // байт определяющий модель прибора
            {
                case 0: return "ТВ7-01";
                case 1: return "ТВ7-4.1";
                case 2: return "ТВ7-03";
                case 3: return "ТВ7-04";
                default: return "Model is not identificated"; // неправильно переданно значение байта
            }
        }

        private int LongConvert(byte[] data)
        {
            return ((int)data[2] << 24) | (int)(data[3] << 16) | (int)(data[0] << 8) | (int)data[1]; // Преобразование байтов в целое число (32 бита)
        }

        private float FloatConvert(byte[] data)
        {
            byte[] value = new byte[4];
            float retValue = 0;
            value[0] = data[1]; // Преобразование   
            value[1] = data[0]; // байтов
            value[2] = data[3]; // согласно
            value[3] = data[2]; // стандарту
            retValue = BitConverter.ToSingle(value, 0);
            return retValue;
        }

        private double DoubleConvert(byte[] data) // Функция преобразования по стандарту в double
        {
            byte[] value = new byte[8]; // Массив для байтов значения
            double retValue = 0;  // Возвращаемое значение
            value[0] = data[1]; // Перестановка
            value[1] = data[0]; // байтов
            value[2] = data[3]; // по 
            value[3] = data[2]; // стандарту
            value[4] = data[5]; // протокола
            value[5] = data[4]; // в double
            value[6] = data[7];
            value[7] = data[6];
            retValue = BitConverter.ToDouble(value, 0); // Массив в double
            return retValue; // вернуть значение параметра
        }

        private byte[] MakeByteArray(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];

            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j + 1] = Convert.ToByte(data[i] & 0xFF);
            }

            return resArray;
        }

        private byte[] MakeByteArrayreverse2(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];

            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j + 1] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j] = Convert.ToByte(data[i] & 0xFF);
            }

            return resArray;
        }


    }


    /*
    public void ReadArchives(ModbusSerialMaster master, SerialPort port, byte day, byte month, byte year, byte hour, byte minute, byte second, ushort archiveType, int serialNumber)
    {
        byte[] receive;
        ushort dayMonth = Convert.ToUInt16(day << 8 | month);
        ushort yearHour = Convert.ToUInt16(year << 8 | hour);
        ushort minuteSecond = Convert.ToUInt16(minute << 8 | second);

        try
        {
            master.WriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 99, new ushort[] { dayMonth });
            master.WriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 100, new ushort[] { yearHour });
            master.WriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 101, new ushort[] { minuteSecond });
            master.WriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 102, new ushort[] { archiveType });

            //receive = MakeByteArray(master.ReadWriteMultipleRegisters(Convert.ToByte(Config.DeviseAdress), 2740, 86, 99, new ushort[] { dayMonth, yearHour, minuteSecond, archiveType }));
            receive = MakeByteArray(master.ReadHoldingRegisters(Convert.ToByte(Config.DeviseAdress), 2740, 1));

            Console.WriteLine("TV7 : " + serialNumber + " try to read arhive for : " + day + "-" + month + "-" + year + "" + hour + "-" + minute + "-" + second);
            for (int i = 0; i < receive.Length; i++)
                Console.Write("{0:X} ", receive[i]);
            Console.WriteLine();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }*/

    /*
    public override List<DataParameter> StartCom()
    {
        int serialNumber = 0;

        try
        {
            ModbusSerialMaster master = ModbusSerialMaster.CreateAscii(port);

            ReadData(master);

            CSD_End(port);
            if (port.IsOpen) port.Close();
            //Thread.Sleep(TimeSpan.FromSeconds(30));
            if (!(Data.Any()))
                ColoredConsole.WriteLine(3, "Не удалось считать данные с ТВ-7 : " + serialNumber);

            ColoredConsole.WriteLine(3, "Считывание данных с ТВ-7 : " + Config.PhoneDialing + " завершено.");
            return Data;
        }
        catch (Exception ex)
        {
            ColoredConsole.WriteLine(ex.Message);
            ColoredConsole.WriteLine(3, "Не удалось считать данные с ТВ-7 : " + serialNumber);
            CSD_End(port);
            if (port.IsOpen) port.Close();
            return Data;
        }
    }*/

    /*
    public override List<DataParameter> Read()
    {
        List<DataParameter> par = new List<DataParameter>(Data);
        if (par.Any())
        {
            Data.Clear();
            return par;
        }
        else
        {
            return Data;
        }
    }
    */

}