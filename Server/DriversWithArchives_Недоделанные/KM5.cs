﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO.Ports;
using Server;

namespace KM_5
{
    public class KM5 : Driver
    {
        private readonly byte[] NETWORKADDRESSREQUEST = { 0x51, 0x52, 0x53, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x4a };

        private const int FIRST_DATA_FRAME = 0x7B;
        private const int SECOND_DATA_FRAME = 0x5F;

        private const int DB_HEADER_REQUEST = 50;
        private const int DB_STRING_REQUEST = 52;
        private const int DB_DATA_REQUEST = 65;

        private const int FRAME_READ_DELAY = 2000;

        public KM5(StationEquipmentData param) : base(param)
        {

        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {

        }

        byte[] NetworkAddres_global;
        public override string GetSerialNumber()
        {
            uint serial = 0;
            try
            {
                byte[] receive = comm.MessagebyCRC(getNetworkAddress, CommunicateCurrent.CRCtype_fromKM5);

                NetworkAddres_global = receive.Skip(5).Take(4).ToArray(); // Convert Received Serial Number
                serial = BitConverter.ToUInt32(NetworkAddres_global, 0); // Получение серийного номера прибора
            }
            catch (Exception)
            {
                throw;
            }


            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }


        int ServerVersion;
        public override bool ConfigBeforeReadArchive()
        {
            try
            {


            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            ArcHiveReadedPack ret = new ArcHiveReadedPack();
            var temp = new List<List<DataParameter>>();
            temp.Add(ReadArchiveForHour(new DateTime(year, month, day, hour, 0, 0)));
          
            ret.Data = temp;

            //TODO set nodatearchive

            return ret;
        }

        #endregion

        private byte[] getNetworkAddress = { 0x51, 0x52, 0x53, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x4a };
        private byte[] getCurrentValuesOne = { 0x00, 0x00, 0x00, 0x00, 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private byte[] getCurrentValuesTwo = { 0x00, 0x00, 0x00, 0x00, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        private void FirstStageWritingData(byte[] data, uint serial)
        {
            List<DataParameter> Data = new List<DataParameter>();
            int i = 5, j = 0;

            for (j = 0; j < 3; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 93, j, BitConverter.ToSingle(converted, 0), Config.EquipmentTypeId));
                i += 4;
            }

            for (j = 0; j < 4; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(converted, 0), Config.EquipmentTypeId));
                i += 4;
            }

            for (j = 0; j < 3; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 1, j, BitConverter.ToSingle(converted, 0), Config.EquipmentTypeId));
                i += 4;
            }

            Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 12, 0, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), Config.EquipmentTypeId));
            i += 4;

            for (j = 4; j < 7; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(converted, 0), Config.EquipmentTypeId));
                i += 4;
            }

            Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 12, 1, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), Config.EquipmentTypeId));
            i += 4;

            Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), Config.EquipmentTypeId));
            i += 4;
        }

        private void SecondStageWrintingData(byte[] data, uint serial)
        {
            List<DataParameter> Data = new List<DataParameter>();

            int i = 13;
            byte[] val;

            for (int j = 0; j < 2; j++)
            {
                val = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 90, j, Math.Round(BitConverter.ToSingle(val, 0), 3), Config.EquipmentTypeId));
                i += 4;
            }

            for (int j = 0; j < 3; j++)
            {
                val = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 91, j, Math.Round(BitConverter.ToSingle(val, 0), 3), Config.EquipmentTypeId));
                i += 4;
            }

            val = data.Skip(i).Take(4).ToArray();
            Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 13, 0, Math.Round(BitConverter.ToSingle(val, 0) * 1.163, 3), Config.EquipmentTypeId));

            i += 4;
            val = data.Skip(i).Take(4).ToArray();
            Data.Add(new DataParameter(Config.StationEquipmentId.ToString(), 10, 0, Math.Round(BitConverter.ToSingle(val, 0), 3), Config.EquipmentTypeId));
        }

        #region READ_ARCHIVES_VALUES
        public List<DataParameter> ReadArchiveForHour(DateTime data)
        {
            /*byte[] ReadDbHeader = { 0, 0, 0, 0, DB_HEADER_REQUEST, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            CalculateCRC(ReadDbHeader, NetworkAddress);

            WritePacket(port, ReadDbHeader);
            byte[] DbHeaderResponse = ReadPacket(port);
            byte[] DbCrystallAddres = { DbHeaderResponse[5], DbHeaderResponse[6] };*/

            byte[] RequestStringByDate = { 0, 0, 0, 0, DB_STRING_REQUEST, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            RequestStringByDate[6] = Convert.ToByte(data.Day);
            RequestStringByDate[7] = Convert.ToByte(data.Month);
            RequestStringByDate[8] = Convert.ToByte(data.Year - 2000);
            RequestStringByDate[9] = Convert.ToByte(data.Hour);
            CalculateCRC(RequestStringByDate, NetworkAddres_global);

            byte[] StringResponse = comm.MessagebyCRC(RequestStringByDate, CommunicateCurrent.CRCtype_CRC16);
            byte SpecialInfo = StringResponse[0];

            if (!((SpecialInfo & (1 << 6)) != 0))
                return new List<DataParameter>();

            byte[] ArchiveStringNumber = { StringResponse[1], StringResponse[2] };

            byte[] RequestArchiveString = { 0, 0, 0, 0, DB_DATA_REQUEST, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            RequestArchiveString[6] = ArchiveStringNumber[0];
            RequestArchiveString[7] = ArchiveStringNumber[1];
            CalculateCRC(RequestArchiveString, NetworkAddres_global);

            byte[] ArchiveHourlyData = comm.MessagebyCRC(RequestStringByDate, CommunicateCurrent.CRCtype_CRC16);

            return ParseArchiveBuffer(ArchiveHourlyData);
        }

        private List<DataParameter> ParseArchiveBuffer(byte[] ArchiveBuffer)
        {
            try
            {
                List<DataParameter> ArchiveData = new List<DataParameter>();

                String Serial = Config.StationEquipmentId.ToString();
                int EquipmentID = Config.EquipmentTypeId;

                DateTime archiveTime = new DateTime(ArchiveBuffer[3], ArchiveBuffer[2], ArchiveBuffer[1], ArchiveBuffer[5], ArchiveBuffer[6], ArchiveBuffer[7]);

                ArchiveBuffer = ArchiveBuffer.Skip(8).ToArray();

                ArchiveData.Add(new DataParameter(Serial, 95, 3, BitConverter.ToSingle(ArchiveBuffer.Skip(0).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 1, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(4).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 1, 1, BitConverter.ToSingle(ArchiveBuffer.Skip(8).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 1, 2, BitConverter.ToSingle(ArchiveBuffer.Skip(12).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 95, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(16).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 95, 1, BitConverter.ToSingle(ArchiveBuffer.Skip(20).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 95, 2, BitConverter.ToSingle(ArchiveBuffer.Skip(24).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 91, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(28).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 91, 1, BitConverter.ToSingle(ArchiveBuffer.Skip(32).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 90, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(36).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 90, 1, BitConverter.ToSingle(ArchiveBuffer.Skip(40).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 90, 2, BitConverter.ToSingle(ArchiveBuffer.Skip(44).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 13, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(48).Take(4).ToArray(), 0), EquipmentID, archiveTime));
                ArchiveData.Add(new DataParameter(Serial, 10, 0, BitConverter.ToSingle(ArchiveBuffer.Skip(52).Take(4).ToArray(), 0), EquipmentID, archiveTime));

                return ArchiveData;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " KM-5 ParseAHourlyArchive");
                return new List<DataParameter>();
            }
        }
        #endregion

        private uint GetSerial(byte[] data)
        {
            byte[] reg = new byte[4];
            reg = data.Reverse().ToArray();
            string serNum = BitConverter.ToString(reg).Replace("-", string.Empty);
            return Convert.ToUInt32(serNum);
        }

        private void SetAddress(byte[] input, byte[] NetworkAddress)
        {
            for (int i = 0; i < 4; i++)
            {
                input[i] = NetworkAddress[i];
                input[i] = NetworkAddress[i];
            }
        }

        private void CalculateCRC(byte[] input, byte[] NetworkAddress)
        {
            SetAddress(input, NetworkAddress);
            CRC.KM5_CRC16(input, input.Length);
        }

    }
}