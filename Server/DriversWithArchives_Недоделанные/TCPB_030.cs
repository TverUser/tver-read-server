﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using Modbus.Device;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO.Ports;
using Server;

namespace Vzlyot_TCPB_030
{
    public class TCPB_030 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // Список параметров передающийся на сервер
        private StationEquipmentData StationEquipment; // Параметры для инициализации
        public ManualResetEvent allDone = new ManualResetEvent(false);

        public TCPB_030(StationEquipmentData param) : base(param)
        {
            StatusLogNum = 24;
            StationEquipment = new StationEquipmentData(param);
            StartLogMessage = "Readed TCPB_030" + StationEquipment.PhoneDialing;

            if (StationEquipment.ConnectionType == 1)
            {
                Thread adi = new Thread(StartAsyncTcp);
                adi.Start();
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Tcp Start");
            }
        }

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Begining to read data from TCPB_030...");

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(port);
                GetData(master);

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Data is reading succesfully...");

                CSD_End(port);
                port.Close();
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Waiting for a connection TCPB_030... " + StationEquipment.ServerPort); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ColoredConsole.TYPE_DEBUG, "TCPB_030 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ColoredConsole.TYPE_DEBUG, ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            TcpClient client = new TcpClient();
            client.Client = handler;
            ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(client);

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Begining to read data from TCPB_030...");

                    GetData(master);

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // задержка до следующего опроса
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_DEBUG, "VZLET TCPB-030 : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Disconnect(true);
                handler.Close();
                return;
            }
        }

        private void GetData(ModbusSerialMaster master)
        {
            ushort[] answer;
            StLogDob = 0;

            answer = master.ReadHoldingRegisters(1, 0x8000, 2);
            uint serial = LongConvert(MakeByteArray(answer));
            ColoredConsole.WriteLine("Serial number: " + serial);
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32768, 2);
            double W1 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.0002388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 96, 0, W1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленное количество тепловой энергии W1: " + W1 + " Гкал");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32770, 2);
            double W2 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.0002388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 96, 1, W2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленное количество тепловой энергии W2: " + W2 + " Гкал");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32772, 2);
            double W3 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.0002388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 96, 2, W3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленное количество тепловой энергии W3: " + W3 + " Гкал");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32774, 2);
            double m1 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 0, m1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленная масса теплоносителя 1: " + m1 + " кг");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32776, 2);
            double m2 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 1, m2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленная масса теплоносителя 2: " + m2 + " кг");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32778, 2);
            double m3 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 2, m3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленная масса теплоносителя 3: " + m3 + " кг");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32780, 2);
            double V1 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, 0, V1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленный объем теплоносителя 1: " + V1 + " м3");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32782, 2);
            double V2 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, 1, V2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленный объем теплоносителя 2: " + V2 + " м3");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32784, 2);
            double V3 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.001;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, 2, V3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Накопленный объем теплоносителя 3: " + V3 + " м3");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49152, 2);
            double t1 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 0, t1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Температура в расчетном канале 1: " + t1 + "°С");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49154, 2);
            double t2 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 1, t2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Температура в расчетном канале 2: " + t2 + "°С");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49156, 2);
            double t3 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 2, t3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Температура в расчетном канале 3: " + t3 + "°С");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49164, 2);
            double E1 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.2388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 88, 0, E1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Тепловая мощность E1: " + E1 + "ГДж/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49166, 2);
            double E2 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.2388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 88, 1, E2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Тепловая мощность E2: " + E2 + "ГДж/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49168, 2);
            double E3 = Convert.ToDouble(LongConvert(MakeByteArray(answer))) * 0.2388458966275;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 88, 2, E3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Тепловая мощность E3: " + E3 + "ГДж/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49170, 2);
            double Qm1 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, 0, Qm1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Массовый расход в расчетном канале 1: " + Qm1 + "т/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49172, 2);
            double Qm2 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, 1, Qm2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Массовый расход в расчетном канале 2: " + Qm2 + "т/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49174, 2);
            double Qm3 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, 2, Qm3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Массовый расход в расчетном канале 3: " + Qm3 + "т/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49176, 2);
            double Qv1 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, 0, Qv1, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Средний объемный расход в точке 1: " + Qv1 + "м3/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49178, 2);
            double Qv2 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, 1, Qv2, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Средний объемный расход в точке 2: " + Qv2 + "м3/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 49180, 2);
            double Qv3 = Convert.ToDouble(LongConvert(MakeByteArray(answer)));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, 2, Qm3, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Средний объемный расход в точке 3: " + Qv3 + "м3/ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32786, 2);
            double Tnar = Convert.ToDouble(LongConvert(MakeByteArray(answer))) / 3600;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 10, 0, Tnar, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Время наработки: " + Tnar + "ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

            answer = master.ReadInputRegisters(1, 32788, 2);
            double Tpr = Convert.ToDouble(LongConvert(MakeByteArray(answer))) / 3600;
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 77, 0, Tpr, StationEquipment.EquipmentTypeId));
            ColoredConsole.WriteLine("Время простоя: " + Tpr + "ч");
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, serial.ToString(), StartLogMessage + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

        }

        private byte[] MakeByteArray(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];
            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j + 1] = Convert.ToByte(data[i] & 0xFF);
            }
            return resArray;
        }

        private uint LongConvert(byte[] data)
        {
            return Convert.ToUInt32((uint)(data[0] << 24) | (uint)(data[1] << 16) | (uint)(data[2] << 8) | (uint)data[3]); // Преобразование байтов в целое число (32 бита)
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

    }
}
