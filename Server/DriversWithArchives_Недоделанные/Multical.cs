﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.IO.Ports;
using Server;
using System.Threading;

namespace Multical
{
    public class Multical_602 : Driver
    {
        public const int START_BYTE = 0x80;
        public const int STOP_BYTE = 0x0D;
        public const int RESPONSE_START_BYTE = 0x40;
        public const int DESTINATON_ADDRESS = 0x3F;
        public const int ARCHIVE_DESTINATION_ADDRESS = 0x7F;

        public const int GET_TYPE = 0x01;
        public const int GET_SERIAL_NUMBER = 0x02;
        public const int GET_REGISTER = 0x10;
        public const int READ_ARCHIVE_COMMAND = 0x63;

        private readonly byte[] GET_TYPE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_TYPE, 0x00, 0x00, STOP_BYTE };

        #region CURRENT DATA REQUESTS     
        // Frame ->           StartByte , DestinationAddress, CommandID, DeviceAddress, Register{2}, CRC{2}, StopByte
        private readonly byte[] SERIAL_NUMBER_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x03, 0xE9, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] HEAT_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x3C, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] FIRST_VOLUME_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x44, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] VOLUME_AT_HIGHT_RESOLUTION = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0xEF, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] VOLUME_AT_REVERSE_FLOW = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0xF3, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] SECOND_VOLUME_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x45, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] CONTROL_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x5E, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] COOLING_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x3F, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] FLOW_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x3D, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] REVERSE_FLOW_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x3E, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] TAP_WATER_ENERGY_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x5F, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] HEAT_ENERGY_Y_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x60, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] ENERGY_REGISTER_8 = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x61, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] ENERGY_REGISTER_9 = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x6E, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] WEIGHT_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x48, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] REVERSE_WEIGHT_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x49, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] TEMPERATURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x56, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] REVERSE_TEMPERATURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x57, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] ADDITIONAL_TEMPERATURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x58, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] ADDITIONAL_2_TEMPERATURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x7A, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] DELTA_TEMPERATURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x59, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] PRESSURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x5B, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] REVERSE_PRESSURE_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x5C, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] FLOW_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x4A, 0x00, 0x00, STOP_BYTE };
        private readonly byte[] REVERSE_FLOW_REQUEST = { START_BYTE, DESTINATON_ADDRESS, GET_REGISTER, 0x01, 0x00, 0x4B, 0x00, 0x00, STOP_BYTE };
        #endregion

        private const int ARCHIVE_MODULE_DESTINATION_ADDRESS = 0x7B;
        private const int LOG_READ_BY_TIME_COMMAND = 0xA0;
        private const int NUMBER_OF_REGISTERS = 20;
        private const int NUMBER_OF_RECORDS = 1;
        private const int LOG_ID = 0x01;
        private const int MAX_APL_LENGTH = 0xFF;

        #region  Units
        public readonly string[] MEASHUREMENT_UNITS =  { "","Wh","kWh","MWh","GWh","j","kj","Mj", "Gj","Cal","kCal","Mcal","Gcal","varh",
                                                        "kvarh","Mvarh","Gvarh", "VAh","kVAh","MVAh","GVAh","kW","kW","MW","GW","kvar",
                                                        "kvar","Mvar", "Gvar","VA","kVA","MVA","GVA","V","A","kV","kA","C","K","l","m3",
                                                        "l/h","m3/h","m3xC","ton","ton/h","h","hh:mm:ss","yy:mm:dd","yyyy:mm:dd", "mm:dd",
                                                        "","bar","RTC","ASCII","m3 x 10","ton xr 10","GJ x 10","minutes","Bitfield", "s",
                                                        "ms","days","RTC-Q","Datetime" };
        #endregion

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {
            comm.CreateRtu();
        }

        public override string GetSerialNumber()
        {
            try
            {
                byte[] Response = SendPreparedRequest(comm.port, SERIAL_NUMBER_REQUEST);
                return DecodeResponse(Response).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }

        public override bool ConfigBeforeReadArchive()
        {
            try
            {


            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            ArcHiveReadedPack ArchivesList = new ArcHiveReadedPack();
            List<List<DataParameter>> ret = new List<List<DataParameter>>
            {
                //ReadArchive(comm.port, hour, day, month, year)
            };
            return ArchivesList;
        }

        #endregion


        public Multical_602(StationEquipmentData param)
            : base(param)
        {
            Config = new StationEquipmentData(param);

            SERIAL_NUMBER_REQUEST[3] = (byte)Config.DeviseAdress;
            HEAT_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            FIRST_VOLUME_REQUEST[3] = (byte)Config.DeviseAdress;
            SECOND_VOLUME_REQUEST[3] = (byte)Config.DeviseAdress;
            CONTROL_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            COOLING_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            FLOW_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            REVERSE_FLOW_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            TAP_WATER_ENERGY_REQUEST[3] = (byte)Config.DeviseAdress;
            HEAT_ENERGY_Y_REQUEST[3] = (byte)Config.DeviseAdress;
            WEIGHT_REQUEST[3] = (byte)Config.DeviseAdress;
            REVERSE_WEIGHT_REQUEST[3] = (byte)Config.DeviseAdress;
            TEMPERATURE_REQUEST[3] = (byte)Config.DeviseAdress;
            REVERSE_TEMPERATURE_REQUEST[3] = (byte)Config.DeviseAdress;
            ADDITIONAL_TEMPERATURE_REQUEST[3] = (byte)Config.DeviseAdress;
            ADDITIONAL_2_TEMPERATURE_REQUEST[3] = (byte)Config.DeviseAdress;
            DELTA_TEMPERATURE_REQUEST[3] = (byte)Config.DeviseAdress;
            PRESSURE_REQUEST[3] = (byte)Config.DeviseAdress;
            REVERSE_PRESSURE_REQUEST[3] = (byte)Config.DeviseAdress;
            FLOW_REQUEST[3] = (byte)Config.DeviseAdress;
            REVERSE_FLOW_REQUEST[3] = (byte)Config.DeviseAdress;
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            List<DataParameter> Data = new List<DataParameter>();

            SerialPort port = new SerialPort(Config.Com_Name, Convert.ToInt32(Config.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;

            String Identifier = Config.StationEquipmentId.ToString();
            int EquipmentID = Config.EquipmentTypeId;

            try
            {
                if (!(port.IsOpen))
                    port.Open(); // Port opening

                CSD.CSD_async(port, Config.PhoneDialing, Config.CSDType); // Start CSD Connections
                byte[] Response = SendPreparedRequest(port, SERIAL_NUMBER_REQUEST);
                double serial = DecodeResponse(Response);

                Response = SendPreparedRequest(port, HEAT_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, CONTROL_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 1, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, COOLING_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 2, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, FLOW_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 3, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, REVERSE_FLOW_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 4, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, TAP_WATER_ENERGY_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 5, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, HEAT_ENERGY_Y_REQUEST);
                Data.Add(new DataParameter(Identifier, 14, 6, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, FIRST_VOLUME_REQUEST);
                Data.Add(new DataParameter(Identifier, 91, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, SECOND_VOLUME_REQUEST);
                Data.Add(new DataParameter(Identifier, 91, 1, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, WEIGHT_REQUEST);
                Data.Add(new DataParameter(Identifier, 90, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, REVERSE_WEIGHT_REQUEST);
                Data.Add(new DataParameter(Identifier, 90, 1, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, TEMPERATURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 95, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, REVERSE_TEMPERATURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 95, 1, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, ADDITIONAL_TEMPERATURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 95, 2, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, ADDITIONAL_2_TEMPERATURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 95, 3, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, DELTA_TEMPERATURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 95, 4, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, PRESSURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 1, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, REVERSE_PRESSURE_REQUEST);
                Data.Add(new DataParameter(Identifier, 1, 1, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, FLOW_REQUEST);
                Data.Add(new DataParameter(Identifier, 88, 0, DecodeResponse(Response), EquipmentID));

                Response = SendPreparedRequest(port, REVERSE_FLOW_REQUEST);
                Data.Add(new DataParameter(Identifier, 88, 1, DecodeResponse(Response), EquipmentID));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                CSD.CSD_End(port); // End CSD Connection
                if (port.IsOpen)
                    port.Close();
            }
            return Data;
        }

        public List<DataParameter> ReadArchive(SerialPort port, int Hour, int Day, int Month, int Year)
        {
            List<DataParameter> Data = new List<DataParameter>();

            String Identifier = Config.StationEquipmentId.ToString();
            int EquipmentID = Config.EquipmentTypeId;

            byte[][] Registers =
            {
                new byte[4] {0x00, 0x56, 95, 0}, // T1 Current flow temperature
                new byte[4] {0x00, 0x5b, 1, 0},  // P1 Pressure in flow
                new byte[4] {0x00, 0x44, 90, 0}, // V1 Volume register V1
                new byte[4] {0x00, 0x48, 91, 0}, // M1 Mass register V1
                new byte[4] {0x00, 0x57, 95, 1}, // T2 Current return flow temperature
                new byte[4] {0x00, 0x5c, 1, 1},  // P2 Pressure in return flow
                new byte[4] {0x00, 0x45, 90, 1}, // V2 Volume register V2
                new byte[4] {0x00, 0x49, 91, 1}  // M2 Mass register V2
            };

            foreach (byte[] RequestData in Registers)
            {
                byte[] Request = { START_BYTE, ARCHIVE_DESTINATION_ADDRESS, READ_ARCHIVE_COMMAND, RequestData[0], RequestData[1], (byte)(Year % 100), (byte)Month, (byte)(Day - 1), 0x00, 0x00, 0x00, STOP_BYTE };
                byte[] Response = SendPreparedRequest(port, Request);

                byte[] ApplicationData = Response.Skip(9).Take(Response[6] * 24).ToArray();

                for (int i = 0; i < 24; i++)
                {
                    double value = GetValue(ApplicationData.Skip(i * Response[6]).Take(Response[6]).ToArray(), Response[7], Response[6]);
                    Data.Add(new DataParameter(Identifier, RequestData[2], RequestData[3], value, EquipmentID, new DateTime(Hour, Month, Day, (23 - i), 0, 0)));
                }
            }

            return Data;

            //[A0h] [Log ID] [Number of registers] (Reg ID 1) (Reg ID 2) (Reg ID n) [Number of records] [Max APL length] (Time)
            /*byte[] ArchiveByDateRequest = { START_BYTE, ARCHIVE_MODULE_DESTINATION_ADDRESS, LOG_READ_BY_TIME_COMMAND, LOG_ID, NUMBER_OF_REGISTERS,
                                            0x00, 0x3C, 0x00, 0x44, 0x00, 0x45, 0x00, 0x5E, 0x00, 0x3F, 0x00, 0x3D, 0x00, 0x3E,
                                            0x00, 0x5F, 0x00, 0x60, 0x00, 0x48, 0x00, 0x49, 0x00, 0x56, 0x00, 0x57, 0x00, 0x58,
                                            0x00, 0x7A, 0x00, 0x59, 0x00, 0x5B, 0x00, 0x5c, 0x00, 0x4A, 0x00, 0x4B,
                                            NUMBER_OF_RECORDS, MAX_APL_LENGTH, 0x00, 0x00,  0x00, 0x00, (byte)Hour, (byte)Day, (byte)Month, (byte)(Year - 2000), 0x00, 0x00, STOP_BYTE};*/
            /*byte[] ArchiveByDateRequest = { LOG_READ_BY_TIME_COMMAND, LOG_ID, NUMBER_OF_REGISTERS,
                                            0x00, 0x3C, 0x00, 0x44, 0x00, 0x45, 0x00, 0x5E, 0x00, 0x3F, 0x00, 0x3D, 0x00, 0x3E,
                                            0x00, 0x5F, 0x00, 0x60, 0x00, 0x48, 0x00, 0x49, 0x00, 0x56, 0x00, 0x57, 0x00, 0x58,
                                            0x00, 0x7A, 0x00, 0x59, 0x00, 0x5B, 0x00, 0x5c, 0x00, 0x4A, 0x00, 0x4B,
                                            NUMBER_OF_RECORDS, MAX_APL_LENGTH, 0x00, 0x00,  0x00, 0x00, (byte)Hour, (byte)Day, (byte)Month, (byte)(Year - 2000)};*/
        }

        private List<DataParameter> ParseArchiveData(byte[] ArchiveBuffer, DateTime ArchivationTime)
        {
            if (ArchiveBuffer[ArchiveBuffer.Length - 1] != 0)
                return new List<DataParameter>();

            byte[] RecordData = ArchiveBuffer.Skip(5).Take(ArchiveBuffer.Length - 3).ToArray();

            for (int i = 0; i < NUMBER_OF_REGISTERS; i++)
            {
                byte[] RegisterData = RecordData.Skip(i * 9).Take(9).ToArray();
                double Value = GetValueFromApplicationLayer(RegisterData.Skip(3).Take(RegisterData.Length - 3).ToArray());
                Console.WriteLine(ArchivationTime + " -> Archive value : " + Value);
            }

            return new List<DataParameter>();
        }

        private byte[] SendPreparedRequest(SerialPort port, byte[] packet)
        {
            byte[] Response = new byte[1024];
            byte[] Request = Stuffing(packet);

            Communicate.WritePacket(port, Request);
            Thread.Sleep(5000);
            int bytesRead = port.Read(Response, 0, Response.Length);

            return Destuffing(Response.Take(bytesRead).ToArray());
        }

        public double DecodeResponse(byte[] ResponseMessage)
        {
            byte[] toConvert = new byte[ResponseMessage.Length - 1];
            for (int j = 1; j < ResponseMessage.Length; j++)
            {
                toConvert[j - 1] = ResponseMessage[j];
            }

            // skip if message is not valid
            if (toConvert[0] != 0x3F || toConvert[1] != 0x10)
            {
                return -1;
            }
            // decode the mantissa
            long x = 0;
            int i = 0;
            for (i = 0; i < toConvert[5]; i++)
            {
                x <<= 8;
                x |= toConvert[i + 7];
            }
            i = toConvert[6] & 0x3F;
            if ((toConvert[6] & 0x40) > 0)
            {
                i = -i;
            };
            double ifl = Math.Pow(10, i);
            if ((toConvert[6] & 0x80) > 0)
            {
                ifl = -ifl;
            }
            return x * ifl;
        }

        private byte[] Stuffing(byte[] Request)
        {
            List<byte> StuffedData = new List<byte>
            {
                Request[0]
            };

            for (int i = 1; i < Request.Length - 1; i++)
            {
                if (Request[i] == 0x80
                    || Request[i] == 0x40
                    || Request[i] == 0x0D
                    || Request[i] == 0x06
                    || Request[i] == 0x1B)
                {
                    StuffedData.Add(0x1B);
                    StuffedData.Add((byte)(~Request[i]));
                }
                else
                    StuffedData.Add(Request[i]);
            }
            StuffedData.Add(Request[Request.Length - 1]);
            CRC16(StuffedData.ToArray(), StuffedData.ToArray().Length);

            return StuffedData.ToArray();
        }

        private byte[] Destuffing(byte[] Response)
        {
            List<byte> Result = new List<byte>
            {
                Response[0]
            };

            for (int i = 1; i < Response.Length - 3; i++)
            {
                if (Response[i] == 0x1B)
                {
                    if (Response[i + 1] == 0x7F
                       || Response[i + 1] == 0xBF
                       || Response[i + 1] == 0xF2
                       || Response[i + 1] == 0xF9
                       || Response[i + 1] == 0xE4)
                    {
                        Result.Add((byte)(~Response[i + 1]));
                        i += 1;
                    }
                }
                else
                {
                    Result.Add(Response[i]);
                }
            }
            Result.Add(Response[Response.Length - 3]);
            Result.Add(Response[Response.Length - 2]);
            Result.Add(Response[Response.Length - 1]);

            return Result.ToArray();
        }

        private double GetValueFromApplicationLayer(byte[] ApplicationLayerData) // 6 bytes of Value data
        {
            if (ApplicationLayerData.Length != 2 + ApplicationLayerData[0])
                return -1;

            // Second Byte in Array in Mantissa
            int Ifl = ApplicationLayerData[1] & 0x3F;
            if ((ApplicationLayerData[1] & 0x40) > 0)
                Ifl = -Ifl;

            double ifl = Math.Pow(10, Ifl);
            if ((ApplicationLayerData[1] & 0x80) > 0)
                ifl = -ifl;

            long x = 0;
            // First byte in Array is Data Length
            ApplicationLayerData = ApplicationLayerData.Skip(2).Take(ApplicationLayerData[0]).ToArray();
            for (int i = 0; i < ApplicationLayerData.Length; i++)
            {
                x <<= 8;
                x |= ApplicationLayerData[i];
            }

            return x * ifl;
        }

        private double GetValue(byte[] Data, byte mantisa, byte size) // 6 bytes of Value data
        {
            // Second Byte in Array in Mantissa
            int Ifl = mantisa & 0x3F;
            if ((mantisa & 0x40) > 0)
                Ifl = -Ifl;

            double ifl = Math.Pow(10, Ifl);
            if ((mantisa & 0x80) > 0)
                ifl = -ifl;

            long x = 0;
            // First byte in Array is Data Length
            for (int i = 0; i < size; i++)
            {
                x <<= 8;
                x |= Data[i];
            }

            return x * ifl;
        }

        public static ushort CRC16(byte[] c, int nByte)
        {
            ushort Polynominal = 0x1021;
            ushort InitValue = 0x0;

            ushort i = 1, j, index = 1;
            ushort CRC = InitValue;
            ushort Remainder, tmp, short_c;
            for (i = 0; i < nByte - 4; i++)
            {
                short_c = (ushort)(0x00ff & (ushort)c[index]);
                tmp = (ushort)((CRC >> 8) ^ short_c);
                Remainder = (ushort)(tmp << 8);
                for (j = 0; j < 8; j++)
                {
                    if ((Remainder & 0x8000) != 0)
                    {
                        Remainder = (ushort)((Remainder << 1) ^ Polynominal);
                    }
                    else
                    {
                        Remainder = (ushort)(Remainder << 1);
                    }
                }
                CRC = (ushort)((CRC << 8) ^ Remainder);
                index++;
            }

            c[c.Length - 3] = Convert.ToByte(CRC >> 8);
            c[c.Length - 2] = Convert.ToByte(CRC & 0xFF);
            return CRC;
        }
    }
}
