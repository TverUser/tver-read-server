﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MasterParent;
using System.Net;
using System.Net.Sockets;
using Server;

namespace TEM104n
{
    public class TEM104 : Driver
    {
        public TEM104(StationEquipmentData param) : base(param)
        {

        }
        byte[] Initiaze_pak = new byte[] { 0x55, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00 };
        byte[] Feature_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x00, 0x7c, 0x04, 0x00 };

        byte[] Memo2K_firstff_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x00, 0x00, 0xff, 0x00 };
        byte[] Memo2K_sysint__pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x00, 0xff, 0x00 };
        byte[] Memo2K_syscon__pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x06, 0x00, 0xff, 0x00 };

        byte[] OP_Memo_pak = new byte[] { 0x55, 0x00, 0xff, 0x0c, 0x01, 0x03, 0x22, 0x00, 0xff, 0x00 };
        byte[] Timer128b_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x02, 0x02, 0, 0x1a, 0 };

        List<byte[]> OP_MemoLisInit = new List<byte[]>();

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x00, 0x0f, 0x00 }); // Temperature
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x10, 0x0f, 0x00 }); // Pressure
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x20, 0x0f, 0x00 }); // DensityCoolant
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x30, 0x0f, 0x00 }); // Enthalpy
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x40, 0x0f, 0x00 }); // VolumeFlow
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x50, 0x0f, 0x00 }); // MassFlow
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x60, 0x0f, 0x00 }); // EnergyValues
            MakeCRC((byte)Config.DeviseAdress);
        }

        public override string GetSerialNumber()
        {
            uint serial = 0;
            try
            {
                byte[] Initiaze_recive = { };

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);
                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");
                }
                else
                {
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + Config.PhoneDialing);
                    return "";
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                byte[] Feature_recive = comm.MessagebyCRC(Feature_pak, CommunicateCurrent.CRCtype_fromTEM);
                serial = asBitConverter.ToUInt32_HL(Feature_recive, 7);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("Ошибка считывания серийника: " + ex.Message);
            }
            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return ConfigBeforeRead(); ; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }

        public override bool ConfigBeforeReadArchive()
        {
            return ConfigBeforeRead();
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            List<List<DataParameter>> ret = new List<List<DataParameter>>();

            for (int i = 0; i < 3; i++)//3 tryys
            {
                byte[] GetIndexByDate_pak = new byte[] { 0x55, 0x00, 0xff, 0x0d, 0x11, 0x05, 0, Get_BCD((byte)hour), Get_BCD((byte)day), Get_BCD((byte)month), Get_BCD((byte)(year - 2000)), 0 };
                CRC.CRC_fromTEM(GetIndexByDate_pak);

                byte[] GetIndexByDate_rez = comm.MessagebyCRC(GetIndexByDate_pak, CommunicateCurrent.CRCtype_fromTEM);
                if (GetIndexByDate_rez.Length != 9 || GetIndexByDate_rez == null)
                {
                    //Re-try
                    Thread.Sleep(2000);
                    comm.port.DiscardInBuffer();
                    continue;
                }
                if (GetIndexByDate_rez[6] == 0xff && GetIndexByDate_rez[7] == 0xff)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Запись за дату не найдена:" + new DateTime(year, month, day, hour, 0, 0).ToString());
                }
                else
                {
                    List<DataParameter> Data = new List<DataParameter>();

                    byte[] GetFlash_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x03, 0x05, 0xff, 0, GetIndexByDate_rez[6], GetIndexByDate_rez[7], 0, 0 };
                    CRC.CRC_fromTEM(GetFlash_pak);
                    byte[] GetFlash_rez = comm.MessagebyCRC(GetFlash_pak, CommunicateCurrent.CRCtype_fromTEM);

                    if (GetFlash_rez[6] == 0 || GetFlash_rez.Length != 262 || GetFlash_rez == null)
                    {
                        //Re-try
                        Thread.Sleep(2000);
                        comm.port.DiscardInBuffer();
                        continue;
                    }

                    /*
                    Memo2kFirstff;
                    SysCon;
                    */

                    SysIntStruct SysInt = GetSysInt(GetFlash_rez, 6);
                    int temp = 0;
                    /*
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V1_fraction + V1_whole, 7, when, "V", "м3"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V2_fraction + V2_whole, 7, when, "V", "м3"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V3_fraction + V3_whole, 7, when, "V", "м3"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V4_fraction + V4_whole, 7, when, "V", "м3"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M1_fraction + M1_whole, 7, when, "M", "т"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M2_fraction + M2_whole, 7, when, "M", "т"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M3_fraction + M3_whole, 7, when, "M", "т"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M4_fraction + M4_whole, 7, when, "M", "т"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E1_fraction + E1_whole, 7, when, "E", "МВт"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E2_fraction + E2_whole, 7, when, "E", "МВт"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E3_fraction + E3_whole, 7, when, "E", "МВт"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E4_fraction + E4_whole, 7, when, "E", "МВт"));


                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t1_1, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t1_2, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t1_3, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t1_4, 7, when, "t", "°C"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t2_1, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t2_2, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t2_3, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t2_4, 7, when, "t", "°C"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t3_1, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t3_2, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t3_3, 7, when, "t", "°C"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, t3_4, 7, when, "t", "°C"));


                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p1_1, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p1_2, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p1_3, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p1_4, 7, when, "p", "МПа"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p2_1, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p2_2, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p2_3, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p2_4, 7, when, "p", "МПа"));

                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p3_1, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p3_2, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p3_3, 7, when, "p", "МПа"));
                    Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, p3_4, 7, when, "p", "МПа"));


                    DataParameter param1 = new DataParameter(Config.DriverFeatures, 999, 0, 1, Config.EquipmentTypeId, when);//зАПИСЬ СЧИТАНА
                    Data.Add(param1);
                    */
                    ret.Add(Data);
                }
            }
            return new ArcHiveReadedPack();
        }

        #endregion
        public class Memo2kFirstffStruct
        {
            /// <summary>
            /// число систем
            /// </summary>
            public int systems;

            /// <summary>
            /// тип датчиков расхода:	0 - частотные	1 - импульсные
            /// </summary>
            public int type_g;

            /// <summary>
            /// тип единиц энергии:	0 - Вт* ч	1 - Кал	2 - Джоули
            /// </summary>
            public int type_q;

            /// <summary>
            /// тип температур в статистике:	0 - среднеарифметиче-ские	1 - средневзвешенные
            /// </summary>
            public int type_t;

            /// <summary>
            /// номер прибора в сети
            /// </summary>
            public UInt32 net_num;

            /// <summary>
            /// заводской номер прибора
            /// </summary>
            public UInt32 number;

            /// <summary>
            /// Диаметр условного прохода по каналам
            /// </summary>
            public Int32[] diam;

            /// <summary>
            /// Максимальное значение расхода по системам(Gmax1)*
            /// </summary>
            public float[] g_max;

            /// <summary>
            /// Установленное значение Gуmax в процентах от (*).
            /// Значение Gmax рассчитывается как
            /// Gmax = Gmax1* Gуmax * 0.01 
            /// </summary>
            public int[] g_pcnt_max;

            /// <summary>
            /// Установленное значение Gуmin в процентах от (*).
            /// Значение Gmin рассчитывается как
            /// Gmin = Gmax1* Gуmin * 0.005 
            /// </summary>
            public int[] g_pcnt_min;

            /// <summary>
            /// Максимальная частота
            /// </summary>
            public float[] f_max;

            /// <summary>
            /// Вес импульса
            /// </summary>
            public float[] weight;

            /// <summary>
            /// Адрес следующей часовой записи
            /// </summary>
            public UInt32 next_hour;

            /// <summary>
            /// Адрес следующей суточной записи
            /// </summary>
            public UInt32 next_day;

            /// <summary>
            /// Адрес следующей записи на отчетную дату
            /// </summary>
            public UInt32 next_month;


            public Memo2kFirstffStruct()
            {
                systems = -1;
                type_g = -1;
                type_q = -1;
                type_t = -1;
                net_num = 0;
                number = 0;
                diam = new Int32[4];
                g_max = new float[4];
                g_pcnt_max = new int[4];
                g_pcnt_min = new int[4];
                f_max = new float[2];
                weight = new float[2];
                next_hour = 0;
                next_day = 0;
                next_month = 0;
            }
        }
        public Memo2kFirstffStruct GetMemo2kFirstff(byte[] arr, int index)
        {
            Memo2kFirstffStruct Memo2kFirstff = new Memo2kFirstffStruct();
            Memo2kFirstff.systems = arr[index + 0x00];
            Memo2kFirstff.type_g = arr[index + 0x01];
            Memo2kFirstff.type_q = arr[index + 0x07];
            Memo2kFirstff.type_t = arr[index + 0x0c];
            Memo2kFirstff.net_num = asBitConverter.ToUInt32_HL(arr, index + 0x78);
            Memo2kFirstff.number = asBitConverter.ToUInt32_HL(arr, index + 0x7c);

            Memo2kFirstff.diam[0] = asBitConverter.ToInt16_HL(arr, index + 0xc4);
            Memo2kFirstff.diam[1] = asBitConverter.ToInt16_HL(arr, index + 0xc6);
            Memo2kFirstff.diam[2] = asBitConverter.ToInt16_HL(arr, index + 0xc8);
            Memo2kFirstff.diam[3] = asBitConverter.ToInt16_HL(arr, index + 0xca);

            Memo2kFirstff.g_max[0] = asBitConverter.ToFloat32_HL(arr, index + 0xcc);
            Memo2kFirstff.g_max[1] = asBitConverter.ToFloat32_HL(arr, index + 0xd0);
            Memo2kFirstff.g_max[2] = asBitConverter.ToFloat32_HL(arr, index + 0xd4);
            Memo2kFirstff.g_max[3] = asBitConverter.ToFloat32_HL(arr, index + 0xd8);

            Memo2kFirstff.g_pcnt_max[0] = arr[index + 0xdc];
            Memo2kFirstff.g_pcnt_max[1] = arr[index + 0xdd];
            Memo2kFirstff.g_pcnt_max[2] = arr[index + 0xde];
            Memo2kFirstff.g_pcnt_max[3] = arr[index + 0xdf];

            Memo2kFirstff.g_pcnt_min[0] = arr[index + 0xe0];
            Memo2kFirstff.g_pcnt_min[1] = arr[index + 0xe1];
            Memo2kFirstff.g_pcnt_min[2] = arr[index + 0xe2];
            Memo2kFirstff.g_pcnt_min[3] = arr[index + 0xe3];

            Memo2kFirstff.f_max[0] = asBitConverter.ToFloat32_HL(arr, index + 0xe4);
            Memo2kFirstff.f_max[1] = asBitConverter.ToFloat32_HL(arr, index + 0xe8);

            Memo2kFirstff.weight[0] = asBitConverter.ToFloat32_HL(arr, index + 0xec);
            Memo2kFirstff.weight[1] = asBitConverter.ToFloat32_HL(arr, index + 0xf0);

            Memo2kFirstff.next_hour = asBitConverter.ToUInt32_HL(arr, index + 0xf4);
            Memo2kFirstff.next_day = asBitConverter.ToUInt32_HL(arr, index + 0xf8);
            Memo2kFirstff.next_month = asBitConverter.ToUInt32_HL(arr, index + 0xfc);
            return Memo2kFirstff;
        }


        public class SysConStruct
        {
            /// <summary>
            /// тип системы (01…0C)
            /// возможные значения типов систем:
            /// 00 - Расходомер V
            /// 01 - Расходомер M
            /// 02 - Магистраль
            /// 03 - Подача 
            /// 04 - Обратка
            /// 05 - Тупиковая ГВС
            /// 06 - Подпитка НСО
            /// 07 - Подпитка источника
            /// 08 - Подача + Р
            /// 0A - Открытая
            /// 0B - ГВС с рециркуляцией
            /// 0C - Источник
            /// </summary>
            public int SysType;

            /// <summary>
            /// Расход по каналам: 0 – измеряемый; 1 …100 – программируемый в% от Gmax
            /// </summary>
            public int[] Gprog;

            /// <summary>
            /// Используемые каналы расхо-да
            /// </summary>
            public int[] Gchan;

            /// <summary>
            /// Температура по каналам: 0 – измеряемая; 1 …150 – программируемая в C
            /// </summary>
            public int[] Tprog;

            /// <summary>
            /// Используемые каналы темпе-ратуры
            /// </summary>
            public int[] Tchan;

            /// <summary>
            /// Давление по каналам: 0 – измеряемое; 1 …16 – программируемая в МПа/10
            /// </summary>
            public int[] Pprog;

            /// <summary>
            /// Используемые каналы давле-ния
            /// </summary>
            public int[] Pchan;

            public SysConStruct()
            {
                SysType = -1;
                Gprog = new int[4];
                Gchan = new int[4];
                Tprog = new int[4];
                Tchan = new int[4];
                Pprog = new int[4];
                Pchan = new int[4];
            }
        }
        public SysConStruct GetSysCon(byte[] arr, int index)
        {
            SysConStruct SysCon = new SysConStruct();

            SysCon.SysType = arr[index + 0];

            SysCon.Gprog[0] = arr[index + 0x00];
            SysCon.Gprog[1] = arr[index + 0x02];
            SysCon.Gprog[2] = arr[index + 0x03];
            SysCon.Gprog[3] = arr[index + 0x04];

            SysCon.Gchan[0] = arr[index + 0x05];
            SysCon.Gchan[1] = arr[index + 0x06];
            SysCon.Gchan[2] = arr[index + 0x07];
            SysCon.Gchan[3] = arr[index + 0x08];

            SysCon.Tprog[0] = arr[index + 0x09];
            SysCon.Tprog[1] = arr[index + 0x0a];
            SysCon.Tprog[2] = arr[index + 0x0b];
            SysCon.Tprog[3] = arr[index + 0x0c];

            SysCon.Tchan[0] = arr[index + 0x0d];
            SysCon.Tchan[1] = arr[index + 0x0e];
            SysCon.Tchan[2] = arr[index + 0x0f];
            SysCon.Tchan[3] = arr[index + 0x10];

            SysCon.Pprog[0] = arr[index + 0x11];
            SysCon.Pprog[1] = arr[index + 0x12];
            SysCon.Pprog[2] = arr[index + 0x13];
            SysCon.Pprog[3] = arr[index + 0x14];

            SysCon.Pchan[0] = arr[index + 0x15];
            SysCon.Pchan[1] = arr[index + 0x16];
            SysCon.Pchan[2] = arr[index + 0x17];
            SysCon.Pchan[3] = arr[index + 0x18];
            return SysCon;
        }


        public class SysIntStruct
        {
            public DateTime current;
            public DateTime previous;
            public double[] V_fraction;
            public double[] M_fraction;
            public double[] E_fraction;
            public double[] V_whole;
            public double[] M_whole;
            public double[] E_whole;
            public double[] t1;
            public double[] t2;
            public double[] t3;
            public double[] t4;
            public double[] p1;
            public double[] p2;
            public double[] p3;
            public double[] p4;
            public double[] rshv;

            public SysIntStruct()
            {
                current = new DateTime();
                previous = new DateTime();
                V_fraction = new double[4];
                M_fraction = new double[4];
                E_fraction = new double[4];
                V_whole = new double[4];
                M_whole = new double[4];
                E_whole = new double[4];
                t1 = new double[3];
                t2 = new double[3];
                t3 = new double[3];
                t4 = new double[3];
                p1 = new double[3];
                p2 = new double[3];
                p3 = new double[3];
                p4 = new double[3];
                rshv = new double[4];
            }
        }
        public SysIntStruct GetSysInt(byte[] arr, int index)
        {
            SysIntStruct SysInt = new SysIntStruct();
            SysInt.current = new DateTime(Get_fromBCD(arr[index + 3]) + 2000, Get_fromBCD(arr[index + 2]), Get_fromBCD(arr[index + 1]), Get_fromBCD(arr[index + 0]), 0, 0);
            SysInt.previous = new DateTime(Get_fromBCD(arr[index + 7]) + 2000, Get_fromBCD(arr[index + 6]), Get_fromBCD(arr[index + 5]), Get_fromBCD(arr[index + 4]), 0, 0);

            SysInt.V_fraction[0] = asBitConverter.ToFloat32_HL(arr, index + 0x08);
            SysInt.V_fraction[1] = asBitConverter.ToFloat32_HL(arr, index + 0x08 + 4);
            SysInt.V_fraction[2] = asBitConverter.ToFloat32_HL(arr, index + 0x08 + 8);
            SysInt.V_fraction[3] = asBitConverter.ToFloat32_HL(arr, index + 0x08 + 12);

            SysInt.M_fraction[0] = asBitConverter.ToFloat32_HL(arr, index + 0x18);
            SysInt.M_fraction[1] = asBitConverter.ToFloat32_HL(arr, index + 0x18 + 4);
            SysInt.M_fraction[2] = asBitConverter.ToFloat32_HL(arr, index + 0x18 + 8);
            SysInt.M_fraction[3] = asBitConverter.ToFloat32_HL(arr, index + 0x18 + 12);

            SysInt.E_fraction[0] = asBitConverter.ToFloat32_HL(arr, index + 0x28);
            SysInt.E_fraction[1] = asBitConverter.ToFloat32_HL(arr, index + 0x28 + 4);
            SysInt.E_fraction[2] = asBitConverter.ToFloat32_HL(arr, index + 0x28 + 8);
            SysInt.E_fraction[3] = asBitConverter.ToFloat32_HL(arr, index + 0x28 + 12);

            SysInt.V_whole[0] = asBitConverter.ToUInt32_HL(arr, index + 0x38);
            SysInt.V_whole[1] = asBitConverter.ToUInt32_HL(arr, index + 0x38 + 4);
            SysInt.V_whole[2] = asBitConverter.ToUInt32_HL(arr, index + 0x38 + 8);
            SysInt.V_whole[3] = asBitConverter.ToUInt32_HL(arr, index + 0x38 + 12);

            SysInt.M_whole[0] = asBitConverter.ToUInt32_HL(arr, index + 0x48);
            SysInt.M_whole[1] = asBitConverter.ToUInt32_HL(arr, index + 0x48 + 4);
            SysInt.M_whole[2] = asBitConverter.ToUInt32_HL(arr, index + 0x48 + 8);
            SysInt.M_whole[3] = asBitConverter.ToUInt32_HL(arr, index + 0x48 + 12);

            SysInt.E_whole[0] = asBitConverter.ToUInt32_HL(arr, index + 0x58);
            SysInt.E_whole[1] = asBitConverter.ToUInt32_HL(arr, index + 0x58 + 4);
            SysInt.E_whole[2] = asBitConverter.ToUInt32_HL(arr, index + 0x58 + 8);
            SysInt.E_whole[3] = asBitConverter.ToUInt32_HL(arr, index + 0x58 + 12);


            SysInt.t1[0] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 00) / 100;
            SysInt.t1[1] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 02) / 100;
            SysInt.t1[2] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 04) / 100;

            SysInt.t2[0] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 08) / 100;
            SysInt.t2[1] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 10) / 100;
            SysInt.t2[2] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 12) / 100;

            SysInt.t3[0] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 16) / 100;
            SysInt.t3[1] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 18) / 100;
            SysInt.t3[2] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 20) / 100;

            SysInt.t4[0] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 16) / 100;
            SysInt.t4[1] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 18) / 100;
            SysInt.t4[2] = asBitConverter.ToInt16_HL(arr, index + 0xc8 + 20) / 100;


            SysInt.p1[0] = arr[index + 0xe0 + 00] / 100 * 10.197;
            SysInt.p1[1] = arr[index + 0xe0 + 01] / 100 * 10.197;
            SysInt.p1[2] = arr[index + 0xe0 + 02] / 100 * 10.197;

            SysInt.p2[0] = arr[index + 0xe0 + 04] / 100 * 10.197;
            SysInt.p2[1] = arr[index + 0xe0 + 05] / 100 * 10.197;
            SysInt.p2[2] = arr[index + 0xe0 + 07] / 100 * 10.197;

            SysInt.p3[0] = arr[index + 0xe0 + 08] / 100 * 10.197;
            SysInt.p3[1] = arr[index + 0xe0 + 09] / 100 * 10.197;
            SysInt.p3[2] = arr[index + 0xe0 + 10] / 100 * 10.197;

            SysInt.p4[0] = arr[index + 0xe0 + 08] / 100 * 10.197;
            SysInt.p4[1] = arr[index + 0xe0 + 09] / 100 * 10.197;
            SysInt.p4[2] = arr[index + 0xe0 + 10] / 100 * 10.197;

            return SysInt;
        }


        Memo2kFirstffStruct Memo2kFirstff;
        SysConStruct[] SysCon;
        public bool ConfigBeforeRead()
        {

            byte[] Memo2K_firstff_recive;
            byte[] Memo2K_sysint_recive;
            byte[] Memo2K_syscon_recive;

            byte[] Timer128b_rez;
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание Память таймера 1");
            try
            {
                Memo2K_firstff_recive = comm.MessagebyCRC(Memo2K_firstff_pak, CommunicateCurrent.CRCtype_fromTEM);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Считывание Память таймера 2К провалилось");
                return false;
            }

            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание Память таймера 2");
            try
            {
                Memo2K_sysint_recive = comm.MessagebyCRC(Memo2K_sysint__pak, CommunicateCurrent.CRCtype_fromTEM);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Считывание Память таймера 2К провалилось");
                return false;
            }

            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание Память таймера 3");
            try
            {
                Memo2K_syscon_recive = comm.MessagebyCRC(Memo2K_syscon__pak, CommunicateCurrent.CRCtype_fromTEM);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Считывание Память таймера 2К провалилось");
                return false;
            }
            ///
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание Память таймера 128 байт");
            try
            {
                Timer128b_rez = comm.MessagebyCRC(Timer128b_pak, CommunicateCurrent.CRCtype_fromTEM);
                DateTime d0 = new DateTime(Get_fromBCD(Timer128b_rez[31]) + 2000, Get_fromBCD(Timer128b_rez[30]), Get_fromBCD(Timer128b_rez[29])
                    , Get_fromBCD(Timer128b_rez[26]), Get_fromBCD(Timer128b_rez[24]), Get_fromBCD(Timer128b_rez[22]));
                if (Math.Abs(d0.Subtract(DateTime.Now).TotalMinutes) > 30)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Время в приборе(" + d0.ToString() + ")  отличается от времени на сервере более чем на пол часа");
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Считывание Память таймера 128 байт провалилось");
                return false;
            }

            try
            {
                Memo2kFirstff = GetMemo2kFirstff(Memo2K_firstff_recive, 6);
                SysCon = new SysConStruct[4];
                SysCon[0] = GetSysCon(Memo2K_syscon_recive, 6);
                SysCon[1] = GetSysCon(Memo2K_syscon_recive, 6 + 0x1a * 1);
                SysCon[2] = GetSysCon(Memo2K_syscon_recive, 6 + 0x1a * 2);
                SysCon[3] = GetSysCon(Memo2K_syscon_recive, 6 + 0x1a * 3);

            }
            catch (Exception)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось распарсить структуры");
                return false;
            }
            return true; //Успешно или нет
        }

        public static byte Get_BCD(byte bcd)
        {
            string str = Convert.ToString(bcd);
            int h = Convert.ToInt32(str, 16);
            return (byte)h;
        }

        public static byte Get_fromBCD(byte bcd)
        {
            string str = Convert.ToString(bcd.ToString("x"));
            int h = Convert.ToInt32(str);
            return (byte)h;
        }

        private void MakeCRC(byte NetworkAddress)
        {
            Initiaze_pak[1] = NetworkAddress;
            Feature_pak[1] = NetworkAddress;
            Memo2K_firstff_pak[1] = NetworkAddress;
            Memo2K_sysint__pak[1] = NetworkAddress;
            Memo2K_syscon__pak[1] = NetworkAddress;
            OP_Memo_pak[1] = NetworkAddress;
            Timer128b_pak[1] = NetworkAddress;

            Initiaze_pak[2] = (byte)(Initiaze_pak[2] - NetworkAddress);
            Feature_pak[2] = (byte)(Feature_pak[2] - NetworkAddress);
            Memo2K_firstff_pak[2] = (byte)(Memo2K_firstff_pak[2] - NetworkAddress);
            Memo2K_sysint__pak[2] = (byte)(Memo2K_sysint__pak[2] - NetworkAddress);
            Memo2K_syscon__pak[2] = (byte)(Memo2K_syscon__pak[2] - NetworkAddress);

            OP_Memo_pak[2] = (byte)(OP_Memo_pak[2] - NetworkAddress);
            Timer128b_pak[2] = (byte)(Timer128b_pak[2] - NetworkAddress);

            CRC.CRC_fromTEM(Initiaze_pak);
            CRC.CRC_fromTEM(Feature_pak);
            CRC.CRC_fromTEM(Memo2K_firstff_pak);
            CRC.CRC_fromTEM(Memo2K_sysint__pak);
            CRC.CRC_fromTEM(Memo2K_syscon__pak);
            CRC.CRC_fromTEM(OP_Memo_pak);
            CRC.CRC_fromTEM(Timer128b_pak);


            foreach (var f in OP_MemoLisInit)
            {
                CRC.CRC_fromTEM(f);
            }
        }

        public static long GetLong(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 1];
            newb[1] = arr[start + 0];
            newb[2] = arr[start + 3];
            newb[3] = arr[start + 2];

            return BitConverter.ToInt32(newb, 0);
        }

        public static float GetFloat(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToSingle(newb, 0);
        }

        public static long GetLongv2(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToInt32(newb, 0);
        }


    }
}