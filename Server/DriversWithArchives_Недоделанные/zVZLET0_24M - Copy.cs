﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using System.Threading;
using System.IO.Ports;
using Modbus.Device;
using System.Net;
using System.Net.Sockets;
using Server;

namespace VZLET_024M
{
    public class VZLET_TCPB_024M : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // Список параметров передающийся на сервер
        private StationEquipmentData StationEquipment; // Параметры для инициализации
        public ManualResetEvent allDone = new ManualResetEvent(false);
        public int ErrorRequest = 5;

        //private int[] VolumeQuantityAddresses = { 0xC05A, 0xC05C, 0xC05E, 0xC060, 0xC062, 0xC064, 0xC066, 0xC068, 0xC06A }; //DONE 2 BYTES
        
        private int[] TemperatureAddresses = { 0xC048, 0xC04A, 0xC04C, 0xC04E, 0xC050, 0xC052, 0xC4F6, 0xC4F8, 0xC4FA, 0xC6BE }; //DONE 2 BYTES
        private int[] PressureAddresses = { 0xC03C, 0xC03E, 0xC040, 0xC042, 0xC044, 0xC046, 0xC4FC, 0xC4FE, 0xC500 }; //DONE 2 BYTES

        private int[] VolumeChannelAddresses = { 0xC06C, 0xC070, 0xC074, 0xC078, 0xC07C, 0xC080, 0xC084, 0xC088, 0xC08C }; //DONE 2 BYTES

        //private int[] MassiveQuantityAddresses = { 0xC6B0, 0xC6B6, 0xC6BC }; //DONE 2 BYTES
        // private int[] EntalphyAddresses = { 0x8064, 0x8066, 0x8068 }; //DONE 4 BYTES

        private int[] HeatAddresses = { 0xC234, 0xC270, 0xC2AC, 0xC2E8, 0xC324, 0xC360, 0xC39C, 0xC3D8, 0xC414, 0xC450, 0xC48C, 0xC4C8, 0xC0AE, 0xC0BE, 0xC0C2, 0xC0D6, 0xC0DA, 0xC0EE0, 0xC0F2, 0xC69A, 0xC69C, 0xC6A0, 0xC6A2, 0xC6A6, 0xC6A8 };//DONE 2 BYTES
        private int[] WeightAddresses = { 0xC238, 0xC274, 0xC2B0, 0xC2EC, 0xC328, 0xC364, 0xC3A0, 0xC3DC, 0xC418, 0xC454, 0xC490, 0xC4CC, 0xC0B2, 0xC0C6, 0xC0DE, 0xC0F6, 0xC69E, 0xC6A4, 0xC6AA };//DONE 2 BYTES

        private int[] EnergyAddresses = { 0xC6AC, 0xC6AE, 0xC6B2, 0xC6B4, 0xC6B8, 0xC6BA }; //DONE 2 BYTES

        private int[] Time = { 0x8016, 0x8024, 0x8032 };
        private int[] NoTime = { 0x8018, 0x8026, 0x8034 };

        public VZLET_TCPB_024M(StationEquipmentData param)
            : base(param)
        {
            StationEquipment = new StationEquipmentData(param);

            if (StationEquipment.CommunicationChannel == 1)
            {
                StationEquipment.DeviseAdress = 1;
                Thread VZLT_TCRV_026M = new Thread(StartAsyncTcp);
                VZLT_TCRV_026M.Start();

                ColoredConsole.WriteLine("VZLET_TCPB_024M Tcp Start");
            }
        }

        public override ArcHiveReadedPack ReadArchive()
        {
            ArcHiveReadedPack Data = new ArcHiveReadedPack();

            /* Archive_Type
             * 1- Часовой
             * 2- Суточный
             * 3- Месячный
             * */

            int Archive_Type = StationEquipment.ArchivesType;
            Data.ReadedArchiveType = Archive_Type;

            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;
            
            uint serial = 0;

            ColoredConsole.WriteLine("Begining to read data from VZLET_TCPB_024M...");
            
            if (!(port.IsOpen))
                port.Open(); // Открытие порта

            ColoredConsole.WriteLine(3, "Звоним на ВЗЛЕТ-ТСРВ-024М : " + StationEquipment.PhoneDialing);
            CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

            ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(port);

            try
            {
                ushort[] reg = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, 0x8002, 2);
                serial = LongConvert(MakeByteArray(reg));
                ColoredConsole.WriteLine("ВЗЛЕТ-ТСРВ-024М серийник считало : " + serial);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("ВЗЛЕТ-ТСРВ-024М ошибка считывания серийника: " + ex.Message);
            }

            if (serial == 0)
            {
                CSD_End(port);
                if (port.IsOpen) port.Close();

                return Data;
            }
            
            string[] Eq = StationEquipment.ArchivesParameter.Split('|');
            DateTime d_from = DateTime.Parse(Eq[0]);
            DateTime d_to = DateTime.Parse(Eq[1]);
             
            string Serial = StationEquipment.StationEquipmentId.ToString();

            while (d_from < d_to)
            {
                int month1 = 0;
                int day1 = 0;
                int year1 = 0;
                int hour1 = 0;
                day1 = d_from.Day;
                month1 = d_from.Month;
                year1 = d_from.Year;
                hour1 = d_from.Hour;
                
                for (int ii1 = 0; ii1 < 2; ii1++)
                {
                    byte archIndex = 0;

                    switch (Archive_Type)
                    {
                        case 1:
                            archIndex = 0; break;
                        case 2:
                            archIndex = 1; break;
                        case 3:
                            archIndex = 2; break;
                        default: archIndex = 255; break;
                    }
                    archIndex += (byte)(ii1 * 3);
                    
                    int readNumber = 1;

                    {
                    M20: byte[] arch =
                 //Device_Adress  Function   archIndex_Little archIndex_Big (archIndex)  WhatRead(0-ByIndex 1-ByTime)
                 // Seconds Minuter Hours Days Month Year  CRC1 СRС2
                 { (byte)StationEquipment.DeviseAdress, 0x41, 0, archIndex, 0, BitConverter.GetBytes(readNumber)[0], 1,
                            (byte)1,  (byte)1,  (byte)hour1,  (byte)day1,  (byte)month1,  (byte)(year1%100), 0, 0 };
                        /*
                        CRC16(arch, arch.Count());

                        Thread.Sleep(3000);

                        WritePacket(port, arch);
                        Thread.Sleep(10000);                    
                        byte[] receive = ReadPacket(port);
                        //  goto M20;
*/      
    
        byte[] receive;

                        try
                        {
                            // receive = executeAsyncMessage(port, arch, 177, 4000).Result;
                             receive = executeMessage(port, arch);
                            
                        }
                        catch (Exception ex)
                        {
                            ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, ex.Message);
                            continue;
                        }

                        int otstup = 3;

                        for (int ij = 0; ij < readNumber; ij++)
                        {
                            otstup += ij * 172;
                            str: try
                            {
                                int io = 0;

                                List<DataParameter> tData = new List<DataParameter>();
                                
                                byte[] rec = receive;
                                DateTime when = new DateTime();
                                when = d_from;

                                /*
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, 0, 18, when, "Предполагаемо номер теплосистемы", (ii1 + 1).ToString()));
                                
                                ulong _0TimeArchive = asBitConverter.ToUInt32_HL(rec, 0 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _0TimeArchive, 18, when, "Время архивирования", "Дата с 01.01.1970"));
                                
                                int _4 = asBitConverter.ToUInt16_HL(rec, 4 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 10, ii1, _4, 18, when, "Чистое время работы ТС в штатном режиме", "мин"));
                                
                                int _6 = asBitConverter.ToUInt16_HL(rec, 6 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _6, 18, when, "Чистое время работы ТС при пропадании питания", "мин"));
                                
                                int _8 = asBitConverter.ToUInt16_HL(rec, 8 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _8, 18, when, "Время простоя ТС из-за отказа датчиков", "мин"));
                                
                                int _10 = asBitConverter.ToUInt16_HL(rec, 10 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 51, ii1, _10, 18, when, "Общее время простоя из-за действия НС", "мин"));
                                
                                int _12 = asBitConverter.ToUInt16_HL(rec, 12 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _12, 18, when, "Время простоя ТС при выходе из режима «Работа»", "мин"));
                                
                                for (int i = 0; i < 32; i++)
                                {
                                    int _14_76 = asBitConverter.ToUInt16_HL(rec, 14 + i * 2 + otstup);
                                    tData.Add(new DataParameter(Serial.ToString(), 0, i, _14_76, 18, when, "Счетчики времени действия по отдельной НС [" + i + "]", "мин"));
                                }
                                
                                int _78 = asBitConverter.ToUInt16_HL(rec, 78 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _78, 18, when, "Текущий алгоритм учета (схема см.п.6 потребления, статус ТС)", "-"));
                                
                                ulong _80 = asBitConverter.ToUInt32_HL(rec, 80 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _80, 18, when, "Набор флагов нештатных ситуаций", "-"));
                                
                                double _84 = asBitConverter.ToFloat32_HL(rec, 84 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _84, 18, when, "Общее тепло, потребленное абонентом", "Гкал"));
                                
                                double _88 = asBitConverter.ToFloat32_HL(rec, 88 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _88, 18, when, "Тепло, потребленное с водоразбором", "Гкал"));
                                
                                double _92 = asBitConverter.ToFloat32_HL(rec, 92 + otstup);
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _92, 18, when, "Общая потребленная масса", "т"));
                                */

                                for (int i = 0; i < 4; i++)
                                {
                                    double _96_108 = asBitConverter.ToFloat32_HL(rec, 96 + i * 4 + otstup);
                                    tData.Add(new DataParameter(Serial.ToString(), 13, ii1 * 4 + i, _96_108, 18, when, "Тепловая энергия, перенесенное по каждому трубопроводу [" + i + "]", "Гкал"));
                                }
                                
                                for (int i = 0; i < 4; i++)
                                {
                                    double _112_124 = asBitConverter.ToFloat32_HL(rec, 112 + i * 4 + otstup);
                                    tData.Add(new DataParameter(Serial.ToString(), 90, ii1 * 4 + i, _112_124, 18, when, "Масса, перенесенная по каждому трубопроводу [" + i + "]", "т"));
                                }
                                
                                for (int i = 0; i < 4; i++)
                                {
                                    double _128_140 = asBitConverter.ToFloat32_HL(rec, 128 + i * 4 + otstup);
                                    tData.Add(new DataParameter(Serial.ToString(), 91, ii1 * 4 + i, _128_140, 18, when, "Объем, перенесенный по каждому трубопроводу [" + i + "]", "м^3"));
                                }

                                /*
                                for (int i = 0; i < 4; i++)
                                {
                                    double _144_150 = asBitConverter.ToUInt16_HL(rec, 144 + i * 2 + otstup);
                                    _144_150= _144_150* 0.01;
                                    tData.Add(new DataParameter(Serial.ToString(), 0, i, _144_150, 18, when, "Средневзвешенная температура по каждому трубопроводу [" + i + "]", " °С"));
                                }
                                */

                                for (int i = 0; i < 4; i++)
                                {
                                    double _152_158 = asBitConverter.ToUInt16_HL(rec, 152 + i * 2 + otstup);
                                    _152_158 = _152_158 * 0.01;
                                    tData.Add(new DataParameter(Serial.ToString(), 95, ii1 * 4 + i, _152_158, 18, when, "Средняя температура по каждому трубопроводу [" + i + "]", " °С"));
                                }
                                
                                for (int i = 0; i < 4; i++)
                                {
                                    double _160_166 = asBitConverter.ToUInt16_HL(rec, 160 + i * 2 + otstup);
                                    _160_166 = _160_166 * 0.001;
                                    tData.Add(new DataParameter(Serial.ToString(), 1, ii1 * 4 + i, _160_166, 18, when, "Среднее давление по каждому трубопроводу [" + i + "]", " МПа"));
                                }

                                double _168 = asBitConverter.ToUInt16_HL(rec, 168 + otstup);
                                _168 = _168 * 0.01;
                                tData.Add(new DataParameter(Serial.ToString(), 0, 0, _168, 18, when, "Температура холодной воды", " °С"));

                                Data.Data.Add(tData);
                               // Thread.Sleep(1000);

                                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Успешно считано за " + d_from + " TC("+(ii1+1)+")");
                            }
                            catch (Exception ex)
                            {
                                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать за: " + d_from + " TC(" + (ii1 +1) + ")"  + " Ошибка:(" + ex.Message + ") /n");
                            } 
                        }
                    }
                }

                if (StationEquipment.ArchivesType == 1) d_from = d_from.AddHours(1);
                if (StationEquipment.ArchivesType == 2) d_from = d_from.AddDays(1);
                if (StationEquipment.ArchivesType == 3) d_from = d_from.AddMonths(1);
            }
            ENDRead:
            if (port.IsOpen)
            {
                CSD_End(port);
                if (port.IsOpen) port.Close();
            }
            return Data;
        }
        

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            uint serial = 0;

            ColoredConsole.WriteLine("Begining to read data from VZLET_TCPB_024M...");

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                ColoredConsole.WriteLine(3, "Звоним на ВЗЛЕТ-ТСРВ-024М : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(port);

                try
                {
                    ushort[] reg = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, 0x8002, 2);
                    serial = LongConvert(MakeByteArray(reg));
                    ColoredConsole.WriteLine(serial);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                if (serial == 0)
                {
                    CSD_End(port);
                    if (port.IsOpen) port.Close();

                    ColoredConsole.WriteLine(3, "Не удалось считать даные с ВЗЛЕТ-ТСРВ-024М : " + StationEquipment.PhoneDialing);
                    return new List<DataParameter>();
                }
                else ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано серийный номер : " + serial);

                try
                {
                    ReadData(VolumeChannelAddresses, 4, serial, 91, master);

                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о обьёме :  16%");
                    ReadData(TemperatureAddresses, 2, serial, 95, master);

                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о температуре :  33%");
                    //ReadData(VolumeQuantityAddresses, 2, serial, 91, master);
                    ReadData(PressureAddresses, 2, serial, 1, master);

                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    //ReadData(MassiveQuantityAddresses, 2, serial, s93, master);
                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о давлении :  49%");
                    
                    //ReadData(EntalphyAddresses, 2, serial, 87, master);
                    //ReadData(DensityAddresses, 2, serial, 11, master);
                    //ReadData(ThermalPowerAddresses, 2, serial, 12, master);

                    ReadData(HeatAddresses, 4, serial, 13, master);

                    // if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о тепловой енергии :  64%");
                    ReadData(WeightAddresses, 4, serial, 90, master);

                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    //ReadData(VolumeAddresses, 4, serial, 91, master);
                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о массе :  80%");
                    ReadData(EnergyAddresses, 2, serial, 14, master);

                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ReadData(Time, 2, serial, 10, master);
                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ReadData(NoTime, 2, serial, 51, master);
                    if (ErrorRequest < 0) throw new Exception("Bad Quality");

                    ColoredConsole.WriteLine(3, "ВЗЛЕТ-ТСРВ-024М Считано информацию о Энергии :  100%");
                    ColoredConsole.WriteLine(3, " Считывание даных завершено ВЗЛЕТ-ТСРВ-024М");

                    CSD_End(port);
                    if (port.IsOpen) port.Close();
                    return Data;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ColoredConsole.WriteLine(3, "Не удалось считать даные с ВЗЛЕТ-ТСРВ-024М : " + StationEquipment.PhoneDialing);
                    CSD_End(port);
                    if (port.IsOpen) port.Close();
                    return Data;
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state

                    ColoredConsole.WriteLine("Waiting for a connection VZLET_TCPB_024M... " + StationEquipment.ServerPort); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("VZLET_TCPB_024M : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 2000;
            handler.SendTimeout = 2000;

            TcpClient client = new TcpClient();
            client.Client = handler;

            ModbusSerialMaster master = ModbusSerialMaster.CreateAscii(client);

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begining to read data from VZLET_TCPB_024M...");

                    uint serial = getSerialNumber(0x8002, master);
                    ColoredConsole.WriteLine(serial);

                    if (serial == 0)
                    {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Disconnect(true);
                        handler.Close();
                        return;
                    }

                    ReadData(VolumeChannelAddresses, 4, serial, 94, master);
                    ReadData(TemperatureAddresses, 2, serial, 95, master);
                    // ReadData(VolumeQuantityAddresses, 2, serial, 91, master);
                    ReadData(PressureAddresses, 2, serial, 1, master);
                    // ReadData(MassiveQuantityAddresses, 2, serial, 93, master);
                    //ReadData(EntalphyAddresses, 2, serial, 87, master);
                    //ReadData(DensityAddresses, 2, serial, 11, master);
                    //ReadData(ThermalPowerAddresses, 2, serial, 12, master);
                    ReadData(HeatAddresses, 4, serial, 96, master);
                    ReadData(WeightAddresses, 4, serial, 90, master);
                    //ReadData(VolumeAddresses, 4, serial, 91, master);
                    ReadData(EnergyAddresses, 2, serial, 14, master);

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // задержка до следующего опроса
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("VZLET_TCPB_024M : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Disconnect(true);
                handler.Close();
                return;
            }
        }

        private void ReadData(int[] data, int size, uint serial, int obisCode, ModbusSerialMaster master)
        {

            try
            {
                int i = 0;
                foreach (var a in data)
                {
                    if (size == 2) Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), obisCode, i, Convert.ToDouble(getFloatValue(Convert.ToUInt16(a), master)), StationEquipment.EquipmentTypeId));
                    if (size == 4) Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), obisCode, i, Convert.ToDouble(getLongFloatValue(Convert.ToUInt16(a), master)), StationEquipment.EquipmentTypeId));
                    i++;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private uint getSerialNumber(ushort address, ModbusSerialMaster master, int count = 0)
        {
            uint val = 0;
            ushort[] reg = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, address, 2);
            return LongConvert(MakeByteArray(reg));
        }
        
        private double getLongFloatValue(ushort address, ModbusSerialMaster master, int count = 0)
        {
            try
            {
                double val = 0;
                ushort[] reg = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, address, 4);
                byte[] receive = MakeByteArray(reg);
                long longVal = LongConvert(receive.Take(4).ToArray());
                float floatVal = asBitConverter.ToFloat32_HL(receive, 4);
                val = longVal + floatVal;
                ColoredConsole.WriteLine("Address - " + address + " : " + val);
                return val;
            }
            catch (Exception ex)
            {
                ErrorRequest--;
                return 0;
            }
        }

        private double getFloatValue(ushort address, ModbusSerialMaster master, int count = 0)
        {
            try
            {

                float val = 0;
                ushort[] reg = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, address, 2);
                val = asBitConverter.ToFloat32_HL(MakeByteArray(reg),0);
                ColoredConsole.WriteLine("Address - " + address + " " + val);
                return val;
            }
            catch (Exception ex)
            {
                ErrorRequest--;
                return 0;
            }
        } 

        private byte[] MakeByteArray(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];
            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j + 1] = Convert.ToByte(data[i] & 0xFF);
            }
            return resArray;
        }

        private uint LongConvert(byte[] data)
        {
            return Convert.ToUInt32((uint)(data[0] << 24) | (uint)(data[1] << 16) | (uint)(data[2] << 8) | (uint)data[3]); // Преобразование байтов в целое число (32 бита)
        }
        
        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else return Data;
        }
    }
}
