﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using System.Threading;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using Modbus.Device;
using Server;
using Modbus.Message;
using System.Globalization;
using Modbus.Data;

namespace vct9
{
    public class Parameter
    {
        public int Register = 0;
        public int size = 0;

        public Parameter(int register, int size)
        {
            this.Register = register;
            this.size = size;
        }

        public Parameter(Parameter Par)
        {
            this.Register = Par.Register;
            this.size = Par.size;
        }
    }

    public class VCT9 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // Список параметров передающийся на сервер
        private List<Parameter> Parameters = new List<Parameter>();
        private StationEquipmentData StationEquipment; // Параметры для инициализации
        public ManualResetEvent allDone = new ManualResetEvent(false);
        public int RequestError = 5;

        ushort[] register = { 0, 41, 74, 118, 215, 242, 339 }; // Адресса регистров начиая с которого считывать
        ushort[] size = { 2, 13, 18, 78, 9, 78, 9 };  // Количество регистров для считывания ( 1 регистр - 2 байта)

        public VCT9(StationEquipmentData param)
            : base(param)
        {
            StationEquipment = new StationEquipmentData(param);

            for (int i = 0; i < register.Length; i++)
            {
                Parameters.Add(new Parameter(register[i], size[i]));
            }

            if (StationEquipment.CommunicationChannel == 1)
            {
                // StationEquipment.DeviseAdress = 0;

                Thread vkt9 = new Thread(StartAsyncTcp);
                vkt9.Start();
                ColoredConsole.WriteLine("Tcp Start");
            }
        }

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            ColoredConsole.WriteLine("Begining to read data from VKT9...");

            Data.Clear();

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                ColoredConsole.WriteLine(3, "Звоним на ВКТ-9 : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(port);

                AddParameters(master);

                /*
                foreach (var a in Data)
                {
                    ColoredConsole.WriteLine(a.value);
                }
                */
                if (!(Data.Any())) ColoredConsole.WriteLine(3, "Не удалось считать данные с ВКТ-9 : " + StationEquipment.PhoneDialing);
                else ColoredConsole.WriteLine(3, "Считывание данных с ВКТ-9 : " + StationEquipment.PhoneDialing + " заверщено.");

                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                ColoredConsole.WriteLine(3, "Не удалось считать данные с ВКТ-9 : " + StationEquipment.PhoneDialing);
                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state

                    ColoredConsole.WriteLine("Waiting for a connection VKT9... " + StationEquipment.ServerPort); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("VKT9 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            TcpClient client = new TcpClient();
            client.Client = handler;

            ModbusSerialMaster master = ModbusSerialMaster.CreateRtu(client);

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begining to read data from vkt9...");

                    AddParameters(master);

                    foreach (var a in Data)
                    {
                        ColoredConsole.WriteLine(a.value);
                    }

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // задержка до следующего опроса
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("vkt9 : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Disconnect(true);
                handler.Close();
                return;
            }
        }

        private void AddParameters(ModbusSerialMaster master)
        {
            uint serialNumber = 0;
            ushort[] receive;

            try
            {
                receive = master.ReadHoldingRegisters(Convert.ToByte(StationEquipment.DeviseAdress), Convert.ToUInt16(Parameters[0].Register), Convert.ToUInt16(Parameters[0].size));
                byte[] data = MakeByteArray(receive);

                serialNumber = UlongValue(data);
                ColoredConsole.WriteLine(serialNumber);

                if (serialNumber == 0)
                {
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ВКТ-9 : " + StationEquipment.PhoneDialing);
                    return;
                }
                else ColoredConsole.WriteLine(3, "ВКТ-9 Считано серийный номер : " + serialNumber);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                return;
            }

            for (int i = 1; i < Parameters.Count; i++)
            {
                try
                {
                    if (RequestError < 0) return;
                    receive = master.ReadInputRegisters(Convert.ToByte(StationEquipment.DeviseAdress), Convert.ToUInt16(Parameters[i].Register), Convert.ToUInt16(Parameters[i].size));
                    byte[] data = MakeByteArray(receive);

                    switch (i)
                    {
                        case 1:
                            {
                                ColoredConsole.WriteLine("Array 1");
                                ConsoleOutput(data);
                                FirstArrayConversion(data, serialNumber);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 16.6%");
                                if (RequestError < 0) return;
                            }
                            break;
                        case 2:
                            {
                                ColoredConsole.WriteLine("Array 2");
                                ConsoleOutput(data);
                                SecondArrayConversion(data, serialNumber, 12, 6);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 33.2%");
                                if (RequestError < 0) return;
                            }
                            break;
                        case 3:
                            {
                                ColoredConsole.WriteLine("Array 3");
                                ConsoleOutput(data);
                                ThirdArrayConversion(data, serialNumber, 1);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 49.8%");
                                if (RequestError < 0) return;
                            }
                            break;
                        case 4:
                            {
                                ColoredConsole.WriteLine("Array 4");
                                ConsoleOutput(data);
                                FourthArrayConversion(data, serialNumber, 1);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 67.4%");
                                if (RequestError < 0) return;
                            }
                            break;
                        case 5:
                            {
                                ColoredConsole.WriteLine("Array 5");
                                ConsoleOutput(data);
                                ThirdArrayConversion(data, serialNumber, 2);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 84.0%");
                                if (RequestError < 0) return;
                            }
                            break;
                        case 6:
                            {
                                ColoredConsole.WriteLine("Array 6");
                                ConsoleOutput(data);
                                FourthArrayConversion(data, serialNumber, 2);
                                ColoredConsole.WriteLine(3, "ВКТ-9 : " + serialNumber + " - считано 100.0%");
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ex.Message);
                    RequestError -= 2;
                    continue;
                }
            }
        }

        private void FirstArrayConversion(byte[] data, uint serialNumber)
        {
            byte[] value;

            double Q_Summary = 0;
            double W_Summary = 0;
            double Summary_Time = 0;
            double Summary_No_Time = 0;
            double Cold_Water_Temperature = 0;
            double Cold_Water_Pressure = 0;
            double Air_Temperature = 0;

            int count = 0;

            value = data.Skip(count).Take(4).ToArray();
            ulong first = UlongValue(value);
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            float second = FloatConvert(value);
            Q_Summary = first + second;
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            W_Summary = Convert.ToDouble(FloatConvert(value));
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            Summary_Time = Convert.ToDouble(UlongValue(value));
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            Summary_No_Time = Convert.ToDouble(UlongValue(value));
            count += 4;

            value = data.Skip(count).Take(2).ToArray();
            Cold_Water_Temperature = Convert.ToDouble(ShortValue(value) / 100.0); // градусы цельсия
            count += 2;

            value = data.Skip(count).Take(2).ToArray();
            Cold_Water_Pressure = Convert.ToDouble(UshortValue(value) / 10000.0); // Мпа
            count += 2;

            value = data.Skip(count).Take(2).ToArray();
            Air_Temperature = Convert.ToDouble(ShortValue(value) / 100.0); // градусы цельсия
            count += 2;

            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 13, 0, Q_Summary, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 12, 0, W_Summary, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 10, 0, Summary_Time, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 51, 0, Summary_No_Time, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 0, Cold_Water_Temperature, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, 0, Cold_Water_Pressure, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 1, Air_Temperature, StationEquipment.EquipmentTypeId));
        }

        private void SecondArrayConversion(byte[] data, uint serialNumber, int parameterNumberVolume, int parameterNumberQuantity)
        {
            byte[] value;

            double V7 = 0, V8 = 0, V9 = 0;
            double G7 = 0, G8 = 0, G9 = 0;

            int count = 0;
            uint first = 0;
            float second = 0;

            value = data.Skip(count).Take(4).ToArray();
            first = UlongValue(value);
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            second = FloatConvert(value);
            count += 4;

            V7 = first + second; // V7 параметр

            value = data.Skip(count).Take(4).ToArray();
            first = UlongValue(value);
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            second = FloatConvert(value);
            count += 4;

            V8 = first + second; // V8 параметр

            value = data.Skip(count).Take(4).ToArray();
            first = UlongValue(value);
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            second = FloatConvert(value);
            count += 4;

            V9 = first + second; // V9 параметр

            value = data.Skip(count).Take(4).ToArray();
            G7 = FloatConvert(value);  // G7 параметр
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            G8 = FloatConvert(value); // G8 параметр
            count += 4;

            value = data.Skip(count).Take(4).ToArray();
            G9 = FloatConvert(value); // G9 параметр
            count += 4;

            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, parameterNumberVolume, V7, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, parameterNumberVolume + 1, V8, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, parameterNumberVolume + 2, V9, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, parameterNumberQuantity, G7, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, parameterNumberQuantity + 1, G8, StationEquipment.EquipmentTypeId));
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, parameterNumberQuantity + 2, G9, StationEquipment.EquipmentTypeId));
        }

        private void ThirdArrayConversion(byte[] data, uint serialNumber, int TvCount)
        {
            int count = 0;
            int obisCode = 0, parameterNumber = 0;

            for (int i = 0; i < 15; i++)
            {
                byte[] value = data.Skip(count).Take(8).ToArray();

                if (i == 0)
                {
                    if (TvCount == 1) parameterNumber = 1;
                    else parameterNumber = 3;
                    obisCode = 13;
                }
                if (i == 2)
                {
                    if (TvCount == 1) parameterNumber = 0;
                    else parameterNumber = 7;
                    obisCode = 90;
                }
                if (i == 9)
                {
                    if (TvCount == 1) parameterNumber = 0;
                    else parameterNumber = 6;
                    obisCode = 91;
                }

                ConvertLongFloatValue(value, serialNumber, obisCode, parameterNumber++);

                count += 8;
            }

            for (int i = 0; i < 9; i++)
            {
                byte[] value = data.Skip(count).Take(4).ToArray();
                if (i == 0)
                {
                    obisCode = 12;
                    if (TvCount == 1) parameterNumber = 1;
                    else parameterNumber = 3;
                }
                if (i == 2)
                {
                    obisCode = 93;
                    if (TvCount == 1) parameterNumber = 0;
                    else parameterNumber = 4;
                }
                if (i == 6)
                {
                    obisCode = 94;
                    if (TvCount == 1) parameterNumber = 0;
                    else parameterNumber = 3;
                }
                ConvertFloatValue(data, serialNumber, obisCode, parameterNumber++);
                count += 4;
            }
        }

        private void FourthArrayConversion(byte[] data, uint serialNumber, int TvCount)
        {
            int parameterNumber = 0, count = 0;
            byte[] value;

            if (TvCount == 1) parameterNumber = 2;
            else parameterNumber = 8;

            //Read Temperatures by selected TV Count
            for (int i = 0; i < 6; i++)
            {
                value = data.Skip(count).Take(2).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, parameterNumber, ShortValue(value) / 100.0, StationEquipment.EquipmentTypeId));
                parameterNumber++;
                count += 2;
            }

            if (TvCount == 1) parameterNumber = 1;
            else parameterNumber = 4;

            //Read Pressures by selected TV Count
            for (int i = 0; i < 3; i++)
            {
                value = data.Skip(count).Take(2).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, parameterNumber, UshortValue(value) / 10000.0, StationEquipment.EquipmentTypeId));
                parameterNumber++;
                count += 2;
            }
        }

        private void SeventhArrayConversion(byte[] data, uint serialNumber)
        {
            int count = 0, parameterNumber = 0;

            // Read Quantities V
            for (int i = 0; i < 9; i++)
            {
                byte[] value = data.Skip(count).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, parameterNumber++, FloatConvert(value), StationEquipment.EquipmentTypeId));
                count += 4;
            }
        }

        private void EightArrayConversion(byte[] data, uint serialNumber)
        {
            int count = 0, parameterNumber = 0;

            // Read Temperatures T
            for (int i = 0; i < 8; i++)
            {
                byte[] value = data.Skip(count).Take(2).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, parameterNumber++, ShortValue(value) / 100.0, StationEquipment.EquipmentTypeId));
                count += 2;
            }
        }

        private void NinethArrrayConversion(byte[] data, uint serialNumber)
        {
            int count = 0, parameterNumber = 0;

            // Read Pressures P
            for (int i = 0; i < 6; i++)
            {
                byte[] value = data.Skip(count).Take(2).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, parameterNumber++, ShortValue(value) / 10000.0, StationEquipment.EquipmentTypeId));
                count += 2;
            }
        }

        private void ConvertLongFloatValue(byte[] data, uint serialNumber, int obisCode, int parameterNumber)
        {
            uint first = UlongValue(data);
            float second = FloatConvert(data);
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), obisCode, parameterNumber, Convert.ToDouble(first + second), StationEquipment.EquipmentTypeId));
        }

        private void ConvertFloatValue(byte[] data, uint serialNumber, int obisCode, int parameterNumber)
        {
            float value = FloatConvert(data);
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), obisCode, parameterNumber, Convert.ToDouble(value), StationEquipment.EquipmentTypeId));
        }

        private float FloatValue(byte[] data)
        {
            return FloatConvert(data);
        }

        private ushort UshortValue(byte[] data)
        {
            return Convert.ToUInt16((ushort)(data[0] << 8) | (ushort)data[1]);
        }
        private ushort UshortValue(byte[] data, int start)
        {
            return (ushort)((data[start + 7] << 8) + (data[start + 8] & 0xff));
        }

        private short ShortValue(byte[] data)
        {
            try
            {
                short tmp = Convert.ToInt16(data[0] << 8 | data[1]);
                return tmp;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                return 0;
            }
        }
        private short ShortValue(byte[] data, int start)
        {
            try
            {
                short tmp = (short)((data[start + 7] << 8) + (data[start + 8] & 0xff));
                return tmp;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                return 0;
            }
        }

        private uint UlongValue(byte[] data)
        {
            return Convert.ToUInt32((uint)(data[0] << 24) | (uint)(data[1] << 16) | (uint)(data[2] << 8) | (uint)data[3]);
        }

        private byte[] MakeByteArray(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];
            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j + 1] = Convert.ToByte(data[i] & 0xFF);
            }
            ColoredConsole.WriteLine();
            return resArray;
        }

        private int LongConvert(byte[] data)
        {
            return Convert.ToInt32((int)(data[0] << 24) | (int)(data[1] << 16) | (int)(data[2] << 8) | (int)data[3]); // Преобразование байтов в целое число (32 бита)
        }

        private float FloatConvert(byte[] data)
        {
            byte[] value = new byte[4];
            float retValue = 0;
            value = data.Reverse().ToArray();
            retValue = BitConverter.ToSingle(value, 0);
            return retValue;
        }

        private void ConsoleOutput(byte[] data)
        {
            foreach (var a in data)
            {
                Console.Write("{0:X} ", a);
            }
            ColoredConsole.WriteLine();
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

        /********СЕКЦИЯ ДЛЯ АРХИВОВ start*******************/
        public override ArcHiveReadedPack ReadArchive()
        {
            ArcHiveReadedPack forRet = new ArcHiveReadedPack();

            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;

            ColoredConsole.WriteLine(3, "Звоним на VKT9 : " + StationEquipment.PhoneDialing);
            ModbusSerialMaster master;
            int serialNumber = 0;
            string serial = "";
            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                //master = ModbusSerialMaster.CreateRtu(port);

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);
                 
            }
            catch (Exception ex)
            {
                CSD_End(port);
                if (port.IsOpen) port.Close();
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка считывания архивов : " + ex.Message);
                return forRet;
            }

            ModbusSerialMaster master1 = ModbusSerialMaster.CreateRtu(port);

            var receive = master1.ReadHoldingRegisters(Convert.ToByte(StationEquipment.DeviseAdress), Convert.ToUInt16(Parameters[0].Register), Convert.ToUInt16(Parameters[0].size));
            byte[] data = MakeByteArray(receive);

            var serialNumber_0 = UlongValue(data);
            ColoredConsole.WriteLine(serialNumber_0);
            
            serial = StationEquipment.StationEquipmentId.ToString();

            int arcType = 0;
            if (StationEquipment.ArchivesType == 1) arcType = 0;
            if (StationEquipment.ArchivesType == 2) arcType = 1;
            if (StationEquipment.ArchivesType == 3) arcType = 2;
            
            forRet.ReadedArchiveType = StationEquipment.ArchivesType;

            int month = 0;
            int day = 0;

            string[] Eq = StationEquipment.ArchivesParameter.Split('|');
            DateTime d_from = DateTime.Parse(Eq[0]);
            DateTime d_to = DateTime.Parse(Eq[1]);

            //Проверяем на версию. Версия прибора для разбора была 1.1 
            //Поэтому и код  был написан для нее, для других версий прошивок нужно дописывать код
            byte[] versionreq = { 1, 17, 0, 0 };
            var version = executeMessage(port, versionreq);

            if (!(version[9] == 1 && version[10] == 1))
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка, версия прошивки прибора не 1.1. Архивы считаны не будут!");
                return forRet;
            }
            

            int EquipType = StationEquipment.EquipmentTypeId;
            { 
                while (d_from < d_to)
                {

                    int month1 = 0;
                    int day1 = 0;
                    int year1 = 0;
                    int hour1 = 0;
                    day1 = d_from.Day;
                    month1 = d_from.Month;
                    year1 = d_from.Year;
                    hour1 = d_from.Hour;

                    ColoredConsole.WriteLine(3, "Попытка считать архив за  : " + d_from.ToString());
                     
                    List<List<DataParameter>> readed = new List<List<DataParameter>>();

                    try
                    {
                        readed = RetPack(port, serial, EquipType, arcType, month1, day1, year1, hour1);
                        foreach(var  a in readed)
                        {
                            forRet.Data.Add(a);

                        }
                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать данные за:" + d_from.ToString());
                    }

                    //Перейти к следующей дате
                    if (StationEquipment.ArchivesType == 1) d_from = d_from.AddDays(1); //хоть запрос считать за час, мне удобней считать за весь день!!!
                    if (StationEquipment.ArchivesType == 2) d_from = d_from.AddDays(1);
                    if (StationEquipment.ArchivesType == 3) d_from = d_from.AddMonths(1);

                }
            }
             
            if (port.IsOpen) CSD_End(port);
            if (port.IsOpen) port.Close();

            return forRet;
        }
        
        
        public List<DataParameter> GetTS(DateTime date, int EquipType, string serial, string nameParam, byte[] dat, int start, int TSNum)
        { 
            List<DataParameter> ret = new List<DataParameter>();
            try
            { 
                ret.Add(new DataParameter(serial, 13, 1+((TSNum-1)*2),       asBitConverter.ToFloat32_LH(dat, start + 0), EquipType, date, nameParam + "Qо"));
                ret.Add(new DataParameter(serial, 13, 2+((TSNum-1)*2),       asBitConverter.ToFloat32_LH(dat,start + 4 ), EquipType, date, nameParam + "Qгвс"));
                ret.Add(new DataParameter(serial, 90, 0+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 8 ), EquipType, date, nameParam + "M1"));
                ret.Add(new DataParameter(serial, 90, 1+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 12), EquipType, date, nameParam + "M2"));
                ret.Add(new DataParameter(serial, 90, 2+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 16), EquipType, date, nameParam + "M3"));
                ret.Add(new DataParameter(serial, 90, 3+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 20), EquipType, date, nameParam + "M1R"));
                ret.Add(new DataParameter(serial, 90, 4+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 24), EquipType, date, nameParam + "M2R"));
                ret.Add(new DataParameter(serial, 90, 5+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 28), EquipType, date, nameParam + "M3R"));
                ret.Add(new DataParameter(serial, 90, 6+((TSNum-1)*7),       asBitConverter.ToFloat32_LH(dat,start + 32), EquipType, date, nameParam + "dM"));
                ret.Add(new DataParameter(serial, 91, 0+((TSNum-1)*6),       asBitConverter.ToFloat32_LH(dat,start + 36), EquipType, date, nameParam + "V1"));
                ret.Add(new DataParameter(serial, 91, 1+((TSNum-1)*6),       asBitConverter.ToFloat32_LH(dat,start + 40), EquipType, date, nameParam + "V2"));
                ret.Add(new DataParameter(serial, 91, 2+((TSNum-1)*6),       asBitConverter.ToFloat32_LH(dat,start + 44), EquipType, date, nameParam + "V3"));
                ret.Add(new DataParameter(serial, 91, 3+((TSNum-1)*6),       asBitConverter.ToFloat32_LH(dat,start + 48), EquipType, date, nameParam + "V1R"));
                ret.Add(new DataParameter(serial, 91, 4+((TSNum-1)*6),       asBitConverter.ToFloat32_LH(dat,start + 52), EquipType, date, nameParam + "V2R"));
                ret.Add(new DataParameter(serial, 91, 5 + ((TSNum - 1) * 6), asBitConverter.ToFloat32_LH(dat, start + 56), EquipType, date, nameParam + "V3R"));

                ret.Add(new DataParameter(serial, 95, 2 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 60)/100.0, EquipType, date, nameParam + "t1"));
                ret.Add(new DataParameter(serial, 95, 3 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 62)/100.0, EquipType, date, nameParam + "t2"));
                ret.Add(new DataParameter(serial, 95, 4 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 64)/100.0, EquipType, date, nameParam + "t3"));
                ret.Add(new DataParameter(serial, 0, 1,                      asBitConverter.ToInt16_LH(dat, start + 66)/100.0, EquipType, date, nameParam + "t1св"));
                ret.Add(new DataParameter(serial, 0, 1,                      asBitConverter.ToInt16_LH(dat, start + 68)/100.0, EquipType, date, nameParam + "t2св"));
                ret.Add(new DataParameter(serial, 0, 1,                      asBitConverter.ToInt16_LH(dat, start + 70)/100.0, EquipType, date, nameParam + "t3св"));
                ret.Add(new DataParameter(serial, 95, 5 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 72)/100.0, EquipType, date, nameParam + "dt1"));
                ret.Add(new DataParameter(serial, 95, 6 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 74)/100.0, EquipType, date, nameParam + "dt2"));
                ret.Add(new DataParameter(serial, 95, 7 + ((TSNum - 1) * 6), asBitConverter.ToInt16_LH(dat, start + 76)/100.0, EquipType, date, nameParam + "dt3"));

                ret.Add(new DataParameter(serial, 1, 1 + ((TSNum - 1) * 3), asBitConverter.ToUInt16_LH(dat, start + 78)/10000.0, EquipType, date, nameParam + "P1 "));
                ret.Add(new DataParameter(serial, 1, 2 + ((TSNum - 1) * 3), asBitConverter.ToUInt16_LH(dat, start + 80)/10000.0, EquipType, date, nameParam + "P2"));
                ret.Add(new DataParameter(serial, 1, 3 + ((TSNum - 1) * 3), asBitConverter.ToUInt16_LH(dat, start + 82)/10000.0, EquipType, date, nameParam + "P3"));

                ret.Add(new DataParameter(serial, 0, 1,                     asBitConverter.ToUInt16_LH(dat, start + 84), EquipType, date, nameParam + "Траб.ТС "));
                ret.Add(new DataParameter(serial, 0, 1,                     asBitConverter.ToUInt16_LH(dat, start + 86), EquipType, date, nameParam + "Тост.ТС "));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH(dat.Skip(start+88).Take(4).Reverse().ToArray(), 0), EquipType, date, nameParam + "Кан.НС "));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt16_LH(dat, start + 92), EquipType, date, nameParam + "НС ТС "));
                ret.Add(new DataParameter(serial, 0, 1, (short)dat[start + 94], EquipType, date, nameParam + "Схема "));
                
            }
            catch (Exception ex)
            {


            }
            return ret;
        }
         
        public List<DataParameter> GetOtherInf(DateTime date, int EquipType,string serial, string nameParam, byte[] dat, int start)
        {
              
            List<DataParameter> ret = new List<DataParameter>();
            try
            {
                ret.Clear();
                 
                ret.Add(new DataParameter(serial, 0, 1, (double)dat[start + 0], EquipType, date, nameParam + "Год"));
                ret.Add(new DataParameter(serial, 0, 1, (double)dat[start + 1], EquipType, date, nameParam + "Месяц"));
                ret.Add(new DataParameter(serial, 0, 1, (double)dat[start + 2], EquipType, date, nameParam + "День"));
                ret.Add(new DataParameter(serial, 0, 1, (double)dat[start + 3], EquipType, date, nameParam + "Час"));

                ret.Add(new DataParameter(serial, 13, 0, asBitConverter.ToFloat32_LH(dat, start + 4), EquipType, date, nameParam + "Qобщ"));
                ret.Add(new DataParameter(serial, 0, 0, asBitConverter.ToInt16_LH(dat, start+8) /100.0 ,EquipType, date, nameParam + "tхв"));
                ret.Add(new DataParameter(serial, 1, 0, asBitConverter.ToUInt32_LH(dat, start+10)/1000.0 ,EquipType, date, nameParam + "Pхв"));
                ret.Add(new DataParameter(serial, 95, 1,asBitConverter.ToUInt32_LH    (dat, start+12)/100.0 ,EquipType, date, nameParam + "tвозд"));
                ret.Add(new DataParameter(serial, 10, 0,asBitConverter.ToUInt32_LH    (dat, start+14) ,EquipType, date, nameParam + "Твкл"));
                ret.Add(new DataParameter(serial, 51, 1,asBitConverter.ToUInt32_LH    (dat, start+16) ,EquipType, date, nameParam + "Твыкл"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH    (dat, start+18) ,EquipType, date, nameParam + "Аппаратные НС"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH(dat, start + 20) , EquipType, date, nameParam + "Общие НС"));
                ret.Add(new DataParameter(serial, 91, 12, asBitConverter.ToFloat32_LH(dat, (start + 22)), EquipType, date, nameParam + "V7 (E7)"));
                ret.Add(new DataParameter(serial, 91, 13, asBitConverter.ToFloat32_LH(dat, (start + 26)), EquipType, date, nameParam + "V8 (E8)"));
                ret.Add(new DataParameter(serial, 91, 14, asBitConverter.ToFloat32_LH(dat, (start + 30)), EquipType, date, nameParam + "V9 (E9)"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH     (dat, start+34), EquipType, date, nameParam + "Траб.V7"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH     (dat, start+36), EquipType, date, nameParam + "Траб.V8"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH     (dat, start+38), EquipType, date, nameParam + "Траб.V9"));
                ret.Add(new DataParameter(serial, 0, 1, asBitConverter.ToUInt32_LH(dat, start + 40), EquipType, date, nameParam + "Доп.НС"));

            }
            catch (Exception ex)
            {

            } 

            return ret;
        }

        public List<List<DataParameter>> RetPack(SerialPort port, string serial, int EquipType, int ArchiveType, int month, int day, int year, int hour)
        {

            List<List<DataParameter>> tempD = new List<List<DataParameter>>();
            try
            {                
               start:
                byte[] pagenumreq = { 1, 66, (byte)ArchiveType, (byte)(year -2000), (byte)month, (byte)day, 0, 0 };
                var pagenum = executeMessage(port, pagenumreq);
                  
                int PageIndex = asBitConverter.ToInt16_LH(pagenum, 6);

                for (int i0 = 0; i0 < 24; i0++)
                {
                    List<DataParameter> tempD_0 = new List<DataParameter>();
 

                    var p0 = PageIndex & 0x00ff;
                    var p1 = (PageIndex & 0xff00) >> 8;


                    byte[] pagereqAll = { 1, 65, (byte)ArchiveType, 56, (byte)p0, (byte)p1, 1, 0, 0 }; //ReadAll
                    var pageAll = executeMessage(port, pagereqAll); //53

                    
                    int startOtherInf = 7;
                    int startTS1 = 51;
                    int startTS2 = 7;

                    if ((pageAll[3] & 8 )!= 0) //если есть общие данные
                    {
                        DateTime date;
                        if (pageAll[startOtherInf + 3] == 24)//next day???
                        {
                            date = new DateTime(pageAll[startOtherInf + 0] + 2000, pageAll[startOtherInf + 1], pageAll[startOtherInf + 2], pageAll[startOtherInf + 3] - 1, 0, 0);
                            date = date.AddHours(1);
                        }
                        else
                            date = new DateTime(pageAll[startOtherInf + 0] + 2000, pageAll[startOtherInf + 1], pageAll[startOtherInf + 2], pageAll[startOtherInf + 3], 0, 0);

                        var d1 = new List<DataParameter>();
                        var d2 = new List<DataParameter>();
                        var d3 = new List<DataParameter>();

                        if ((pageAll[3] & 8) != 0) //Если есть Общие
                            d1 = GetOtherInf(date, EquipType, serial, "Общее:", pageAll, startOtherInf);

                        if ((pageAll[3] & 16) != 0) //Если есть TC1
                            d2 = GetTS(date, EquipType, serial, "TC1:", pageAll, startTS1, 1);

                        if ((pageAll[3] & 32) != 0) //Если есть TC1
                            d3 = GetTS(date, EquipType, serial, "TC2:", pageAll, startTS2, 2);

                        foreach (var a in d1)
                        {
                            tempD_0.Add(a);
                        }
                        foreach (var a in d2)
                        {
                            tempD_0.Add(a);
                        }
                        foreach (var a in d3)
                        {
                            tempD_0.Add(a);
                        }
                    }
                    tempD.Add(tempD_0);
                   PageIndex++;
                }



            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка считывания, вероятнее всего неверно указанная дата. или:"+ ex.Message);

            }
            //goto h2;
            /****************/


                    
            return tempD;
            /******************/
        }



        /********СЕКЦИЯ ДЛЯ АРХИВОВ end*********************/

    }               
}
