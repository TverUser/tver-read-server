﻿using MasterParent;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class TEM106 : Driver
    {

        public TEM106(StationEquipmentData param) : base(param)
        {

        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {

        }

        public override string GetSerialNumber()
        {

            uint serial = 0;
            try
            {
                byte[] Initiaze_recive = { };

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);
                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");
                }
                else
                {
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + Config.PhoneDialing);
                    return "";
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                byte[] Feature_recive = comm.MessagebyCRC(Feature_pak, CommunicateCurrent.CRCtype_fromTEM);
                serial = asBitConverter.ToUInt32_HL(Feature_recive, 7);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("Ошибка считывания серийника: " + ex.Message);
            }
            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }


        public override bool ConfigBeforeReadArchive()
        {
            try
            {

            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override List<List<DataParameter>> ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            List<List<DataParameter>> ret = new List<List<DataParameter>>();


            byte[] Feature_recive = { };
            byte[] Memo2K_recive = { };
            byte[] OP_Memo_recive = { };


            byte[] GetIndexByDate_pak = new byte[] { 0x55, 0x00, 0xff, 0x0d, 0x11, 0x05, 0, Get_BCD((byte)hour), Get_BCD((byte)day), Get_BCD((byte)month), Get_BCD((byte)(year - 2000)), 0 };
            CRC.CRC_fromTEM(GetIndexByDate_pak);

            byte[] GetIndexByDate_rez = comm.MessagebyCRC(GetIndexByDate_pak, CommunicateCurrent.CRCtype_fromTEM);

            if (GetIndexByDate_rez[6] == 0xff && GetIndexByDate_rez[7] == 0xff)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Запись за дату не найдена:" + new DateTime(year, month, day, hour, 0, 0).ToString());
            }
            else
            {
                List<DataParameter> Data = new List<DataParameter>();

                byte[] GetFlash_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x03, 0x05, 0xff, 0, GetIndexByDate_rez[6], GetIndexByDate_rez[7], 0, 0 };
                CRC.CRC_fromTEM(GetFlash_pak);
                byte[] GetFlash_rez = comm.MessagebyCRC(GetFlash_pak, CommunicateCurrent.CRCtype_fromTEM);

                int hour_rez = Get_fromBCD(GetFlash_rez[6]);
                int day_rez = Get_fromBCD(GetFlash_rez[7]);
                int month_rez = Get_fromBCD(GetFlash_rez[8]);
                int year_rez = Get_fromBCD(GetFlash_rez[9]) + 2000;

                DateTime when = new DateTime(year_rez, month_rez, day_rez, hour_rez, 0, 0);
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, 0, 18, when, "Время записи", "Время"));
            }

            return ret;
        }

        #endregion


        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        public ManualResetEvent allDone = new ManualResetEvent(false);
        private byte NetworkNddress;

        byte[] Initiaze_pak = new byte[] { 0x55, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00 };
        byte[] Feature_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x00, 0x7c, 0x04, 0x00 };



        //
        List<byte[]> Memo2k = new List<byte[]>();



        //

        byte[] OP_Memo_pak = new byte[] { 0x55, 0x00, 0xff, 0x0c, 0x01, 0x03, 0x22, 0x00, 0xff, 0x00 };

        public TEM106(StationEquipmentData Params) : base(Params)
        {
            StatusLogNum = 12;

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xfa, 6 * 0x04, 0 }); //comma

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x00, 7 * 0x04, 0 }); //t_n
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x34, 7 * 0x04, 0 }); //p_n

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x88, 6 * 0x04, 0 }); //rashod_v
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xa0, 6 * 0x04, 0 }); //rashod_m
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xd0, 6 * 0x04, 0 }); //freqan_v

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x18, 6 * 0x04, 0 }); //volume
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x48, 6 * 0x04, 0 }); //mass
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x78, 6 * 0x04, 0 }); //energy

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x00, 6 * 0x04, 0 }); //lvolume
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x30, 6 * 0x04, 0 }); //lmass
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x60, 6 * 0x04, 0 }); //lenergy


            /*
            double rashod_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x88);
           double rashod_m = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xa0);
            double freqan_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xd0);
             double volume = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x18);
           double mass = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x30);
             double energy = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x78);
            double comma = GetChar(Memo2K_recive2, i * 0x04 + ups + 0xfa);
            Data.Add(new DataParameter(feature, 95, i, comma, StationEquipment.EquipmentTypeId));

        }

        double t_n = GetFloat(Memo2K_recive2, i * 0x04 + ups);
        double p_n = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x34);
        */
            StationEquipment = new StationEquipmentData(Params);
            MakeCRC();

            if (StationEquipment.ConnectionType == 1)
            {
                Thread tem_106 = new Thread(StartAsyncTcp);
                tem_106.Start();
                ColoredConsole.WriteLine("Tcp Start");
            }
        }



        public override List<DataParameter> StartCom()
        {
            StLogDob = 0;
            SerialPort port;

            port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 2000;
            port.ReadTimeout = 2000;

            try
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Звоним на ТЭМ-106 : " + StationEquipment.PhoneDialing);

                if (!(port.IsOpen)) port.Open(); // Opening port

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                byte[] Initiaze_recive = { };
                byte[] Feature_recive = { };
                byte[] OP_Memo_recive = { };

                /**/
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = ReadProperties(Initiaze_pak, port);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_WARNING, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = ReadProperties(Initiaze_pak, port);

                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");

                }
                else
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Попытка считать TEM-106 завершилась неудачно");

                    return Data;
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                Feature_recive = ReadProperties(Feature_pak, port);

                /**/
                // Feature_recive = ReadProperties(Feature_pak, port);
                // OP_Memo_recive = ReadProperties(OP_Memo_pak, port);


                List<byte[]> Memo2k_read = new List<byte[]>();

                string feature = Convert.ToString(GetLongv2(Feature_recive, 6));

                foreach (var f in Memo2k)
                {
                    byte[] bReaded = ReadProperties(f, port);

                    if (bReaded == null)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_WARNING, "Повторная (одна) попытка считать тот же набор данных" + (StLogDob));
                        bReaded = ReadProperties(f, port);
                    }

                    Memo2k_read.Add(bReaded);

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, StartLogMessage + " TEM-106 " + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

                }

                AddToDataFunction(Memo2k_read, feature);

                CSD_End(port);
                port.Close();

                ColoredConsole.WriteLine("Data is reading succesfully  from TEM-106 " + StationEquipment.PhoneDialing);
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                CSD_End(port); if (port.IsOpen)
                    port.Close();
                return Data;
            }
        }



        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));

                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);
                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    ColoredConsole.WriteLine("Waiting for a connection tem-106... " + StationEquipment.ServerPort);  // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing.
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("tem-106 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message);
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            /*
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begin to reading data from tem-106...");

                    byte[] Initiaze_recive = { };
                    byte[] Feature_recive = { };
                    byte[] Memo2K_recive1 = { };
                    byte[] Memo2K_recive2 = { };
                    byte[] Memo2K_recive3 = { };
                    byte[] Memo2K_recive4 = { };

                    byte[] OP_Memo_recive = { };

                    Initiaze_recive = ReadProperties(Initiaze_pak, handler);
                    Feature_recive = ReadProperties(Feature_pak, handler);
                    OP_Memo_recive = ReadProperties(OP_Memo_pak, handler);

                    for (int i = 0; i < 5; i++)
                    {
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - try " + i + " read Memo2K_recive1");

                        Memo2K_recive1 = ReadProperties(Memo2K_pak1, handler);

                        if (Memo2K_recive1 != null)
                        {
                            ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - success read Memo2K_recive1");
                            break;
                        }
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - fail read Memo2K_recive1");
                    }
                    ////
                    for (int i = 0; i < 5; i++)
                    {
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - try " + i + " read Memo2K_recive2");

                        Memo2K_recive2 = ReadProperties(Memo2K_pak1, handler);

                        if (Memo2K_recive2 != null)
                        {
                            ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - success read Memo2K_recive2");
                            break;
                        }
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - fail read Memo2K_recive2");
                    }
                    ////
                    for (int i = 0; i < 5; i++)
                    {
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - try " + i + " read Memo2K_recive3");

                        Memo2K_recive3 = ReadProperties(Memo2K_pak1, handler);

                        if (Memo2K_recive3 != null)
                        {
                            ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - success read Memo2K_recive3");
                            break;
                        }
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - fail read Memo2K_recive4");
                    }
                    ////
                    for (int i = 0; i < 5; i++)
                    {
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - try " + i + " read Memo2K_recive4");

                        Memo2K_recive4 = ReadProperties(Memo2K_pak1, handler);

                        if (Memo2K_recive4 != null)
                        {
                            ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - success read Memo2K_recive4");
                            break;
                        }
                        ColoredConsole.WriteLine("TEM-106 - " + StationEquipment.PhoneDialing + " - fail read Memo2K_recive4");
                        ////
                    }


                 //   AddToDataFunction(Feature_recive, OP_Memo_recive, Memo2K_recive1, Memo2K_recive2, Memo2K_recive3, Memo2K_recive4);

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime)));
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("tem-106 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); handler.Disconnect(true); handler.Close(); return;
            }*/
        }


        private void AddToDataFunction(List<byte[]> Memo2k_read, string feature)
        {
            int ups = 6;


            //  while (true)
            {
                ups = 6;
                try
                {
                    //string feature = "1323602"; 


                    List<int> commaS = new List<int>();

                    List<long> ar_volume = new List<long>();
                    List<long> ar_mass = new List<long>();
                    List<long> ar_energy = new List<long>();

                    List<double> ar_lvolume = new List<double>();
                    List<double> ar_lmass = new List<double>();
                    List<double> ar_lenergy = new List<double>();


                    for (int i = 0; i < Memo2k_read.Count; i++)
                    {
                        try
                        {

                        }
                        catch (Exception)
                        {

                        }
                        byte[] red = Memo2k_read.ElementAt(i);

                        try
                        {
                            if (i == 0)
                                for (int j = 0; j < 6; j++)
                                {
                                    int comma = GetChar(red, j * 0x04 + ups);
                                    commaS.Add(comma);
                                }
                        }
                        catch (Exception)
                        {

                        }


                        try
                        {
                            if (i == 1)
                                for (int j = 0; j < 7; j++)
                                {
                                    double t_n = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, j, t_n, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {

                        }

                        try
                        {
                            if (i == 2)
                                for (int j = 0; j < 7; j++)
                                {
                                    double p_n = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, j, p_n, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 3)
                                for (int j = 0; j < 6; j++)
                                {
                                    double rashod_v = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, j, rashod_v, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 4)
                                for (int j = 0; j < 6; j++)
                                {
                                    double rashod_m = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, j, rashod_m, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 5)
                                for (int j = 0; j < 6; j++)
                                {
                                    double freqan_v = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 8, j, freqan_v, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 6)
                                for (int j = 0; j < 6; j++)
                                {
                                    long volume = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_volume.Add(volume);
                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 7)
                                for (int j = 0; j < 6; j++)
                                {
                                    long mass = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_mass.Add(mass);

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 8)
                                for (int j = 0; j < 6; j++)
                                {
                                    long energy = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_energy.Add(energy);

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 9)
                                for (int j = 0; j < 6; j++)
                                {
                                    double volume = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lvolume.Add(volume);
                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 10)
                                for (int j = 0; j < 6; j++)
                                {
                                    double mass = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lmass.Add(mass);

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 11)
                                for (int j = 0; j < 6; j++)
                                {
                                    double energy = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lenergy.Add(energy);

                                }
                        }
                        catch (Exception)
                        {


                        }


                    }

                    for (int i = 0; i < Memo2k_read.Count; i++)
                    {
                        if (commaS[i] == 6) commaS[i] = 100000;
                        else
                        if (commaS[i] == 5) commaS[i] = 10000;
                        else
                        if (commaS[i] == 4) commaS[i] = 1000;
                        else
                        if (commaS[i] == 3) commaS[i] = 100;
                        else
                        if (commaS[i] == 2) commaS[i] = 10; else commaS[i] = 1;

                        double volume = (ar_volume[i] + ar_lvolume[i]) / commaS[i];
                        double mass = (ar_mass[i] + ar_lmass[i]) / commaS[i];
                        double energy = (ar_energy[i] + ar_lenergy[i]) / commaS[i];

                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, i, volume, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, i, mass, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 92, i, energy, StationEquipment.EquipmentTypeId));


                    }

                    for (int i = 0; i < Data.Count; i++)
                    {
                        ColoredConsole.WriteLine(Data.ElementAt(i).value + "    " + Data.ElementAt(i).obisCode + "    ");
                    }

                    /*
                    if (feature == 0) return;

                    int ups = 5;
                    for (int i = 0; i < 6; i++)
                    {
                        ups = 5;
                        if (i < 7)
                        {
                            double rashod_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x88);
                            Data.Add(new DataParameter(feature, 95, i, rashod_v, StationEquipment.EquipmentTypeId));

                            double rashod_m = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xa0);
                            Data.Add(new DataParameter(feature, 95, i, rashod_m, StationEquipment.EquipmentTypeId));

                            double freqan_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xd0);
                            Data.Add(new DataParameter(feature, 95, i, freqan_v, StationEquipment.EquipmentTypeId));

                            double volume = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x18);
                            Data.Add(new DataParameter(feature, 95, i, volume, StationEquipment.EquipmentTypeId));

                            double mass = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x30);
                            Data.Add(new DataParameter(feature, 95, i, mass, StationEquipment.EquipmentTypeId));
                            //
                            double energy = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x78);
                            Data.Add(new DataParameter(feature, 95, i, energy, StationEquipment.EquipmentTypeId));


                            double comma = GetChar(Memo2K_recive2, i * 0x04 + ups + 0xfa);
                            Data.Add(new DataParameter(feature, 95, i, comma, StationEquipment.EquipmentTypeId));

                        }

                        double t_n = GetFloat(Memo2K_recive2, i * 0x04 + ups);
                        Data.Add(new DataParameter(feature, 95, i, t_n, StationEquipment.EquipmentTypeId));

                        double p_n = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x34);
                        Data.Add(new DataParameter(feature, 95, i, p_n, StationEquipment.EquipmentTypeId));



                        ColoredConsole.WriteLine("START TRYS");
                        for (int j = 0; j < Memo2K_recive2.Length - 5; j++)
                        {
                            double trry = GetLong(Memo2K_recive2, j);
                            if (trry>1 && trry<10000000000000000000)
                            {
                                ColoredConsole.WriteLine(trry +" | "+j);

                            }
                        }
                        ColoredConsole.WriteLine("ENDENDENDENFED TRYS");
                        */

                }


                catch (Exception ex)
                {

                }
            }
        }

        private byte[] ReadProperties(byte[] TheParameters, Socket port)
        {
            WritePacket(port, TheParameters);
            Thread.Sleep(200);
            return ReadPacket(port, TheParameters);
        }

        private byte[] ReadProperties(byte[] TheParameters, SerialPort port)
        {
            WritePacket(port, TheParameters);
            Thread.Sleep(200);
            return ReadPacket(port, TheParameters);
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

        private void MakeCRC()
        {
            NetworkNddress = 0;

            CRC(Initiaze_pak);
            CRC(Feature_pak);

            foreach (var f in Memo2k)
            {
                CRC(f);
            }
        }

        public static long GetLong(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 1];
            newb[1] = arr[start + 0];
            newb[2] = arr[start + 3];
            newb[3] = arr[start + 2];

            return BitConverter.ToInt32(newb, 0);
        }

        public static float GetFloat(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToSingle(newb, 0);
        }

        public static int GetChar(byte[] arr, int start)
        {
            int cg = (int)arr[start];
            return cg;
        }

        public static long GetLongv2(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToInt32(newb, 0);
        }

        public byte[] ReadPacket(SerialPort port, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 1)
                {
                    try
                    {

                        int byteCount = port.BytesToRead;
                        byte[] retBytes = new byte[byteCount];
                        port.Read(retBytes, 0, byteCount);

                        if (byteCount < 2)
                        {
                            WritePacket(port, lastSend);
                            count++;
                            continue;
                        }

                        int crc = Convert.ToInt32(retBytes[retBytes.Length - 1]);
                        int crc1 = CRC(retBytes);

                        if (crc == crc1)
                        {
                            foreach (var a in retBytes) Console.Write("{0:X} ", a);
                            ColoredConsole.WriteLine();
                            return retBytes;
                        }
                        else
                        {
                            count++;
                            WritePacket(port, lastSend);
                        }
                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ex.Message);
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                return null;
            }
            return null;
        }

        public byte[] ReadPacket(Socket socket, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 3)
                {
                    int byteCount = socket.ReceiveBufferSize;
                    byte[] retBytes = new byte[100];
                    int bytesRead = socket.Receive(retBytes);

                    retBytes = retBytes.Take(bytesRead).ToArray();
                    if (byteCount < 2)
                    {
                        WritePacket(socket, lastSend);
                        count++;
                        continue;
                    }

                    int crc = Convert.ToInt32(retBytes[retBytes.Length - 1]);
                    int crc1 = CRC(retBytes);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        count++;
                        WritePacket(socket, lastSend);
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        private static int CRC(byte[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length - 1; i++) sum += array[i];
            sum = ~sum;
            byte[] a = BitConverter.GetBytes(sum);
            byte Crc = a[0];
            //byte sum1 = Convert.ToByte(sum);//***
            //ColoredConsole.WriteLine("{0:X} ", Crc);
            array[array.Length - 1] = Crc;



            return Crc;
        }
    }

}
