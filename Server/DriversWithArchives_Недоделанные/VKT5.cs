﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.Net.Sockets;
using System.IO.Ports;
using System.Net;
using Server;

namespace VKT_5
{
    public class VKT5 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        public ManualResetEvent allDone = new ManualResetEvent(false);

        public class TV
        {
            public List<Trumple> Trumples = new List<Trumple>();
            public float Weight;
            public float Heat;
            public float HeatWithoutHWS;
            public float HeatWithHWS;
            public int number;

            public TV()
            {
                this.Trumples.Clear();
                this.Weight = 0;
                this.Heat = 0;
                this.HeatWithoutHWS = 0;
                this.HeatWithHWS = 0;
                this.number = 0;
            }

            public TV(TV tv)
            {
                this.Trumples = new List<Trumple>(tv.Trumples);
                this.Weight = tv.Weight;
                this.Heat = tv.Heat;
                this.HeatWithoutHWS = tv.HeatWithoutHWS;
                this.HeatWithHWS = tv.HeatWithHWS;
                this.number = tv.number;
            }
        }

        public class Trumple
        {
            public float Temperature = 0;
            public float Pressure = 0;
            public float Weight = 0;
            public int number;
            public string appointment = String.Empty;

            public Trumple()
            {
                this.Temperature = 0;
                this.Weight = 0;
                this.Pressure = 0;
                this.number = 0;
                this.appointment = String.Empty;
            }
        }

        private List<TV> TV_s = new List<TV>();
        private byte[] ReadConf = { 0x01, 0x03, 0x0A, 0x00, 0x00, 0x28, 0x00 };
        private byte[] Recvisites = { 0x01, 0x03, 0x0F, 0x00, 0x00, 0x00, 0x00 };
        private byte[] CurrentDate = { 0x01, 0x03, 0x0B, 0x00, 0x00, 0x0A, 0x00, 0x00 };
        private byte[] Trumples_Values = { 0x01, 0x03, 0x01, 0x06, 0x00, 0x06, 0x00, 0x00 };
        private byte[] TV_Values = { 0x01, 0x03, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00 };
        private byte[] OtherValues = { 0x01, 0x03, 0x03, 0x00, 0x00, 0x06, 0x00, 0x00 };
        private byte[] Parameters = { 0x01, 0x03, 0x02, 0x10, 0x00, 0x10, 0x00, 0x00 };

        private void SetAddress()
        {
            ReadConf[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            Recvisites[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            CurrentDate[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            TV_Values[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            Trumples_Values[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            OtherValues[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            Parameters[0] = Convert.ToByte(StationEquipment.DeviseAdress);
            CRC16(ReadConf, ReadConf.Length);
            CRC16(Recvisites, Recvisites.Length);
            CRC16(CurrentDate, CurrentDate.Length);
            CRC16(TV_Values, TV_Values.Length);
            CRC16(Trumples_Values, Trumples_Values.Length);
            CRC16(OtherValues, OtherValues.Length);
            CRC16(Parameters, Parameters.Length);
        }

        public VKT5(StationEquipmentData param) : base(param)
        {
            StationEquipment = new StationEquipmentData(param);
            SetAddress();

            if (StationEquipment.ConnectionType == 1)
            {
                StationEquipment.DeviseAdress = 0;
                SetAddress();

                Thread vkt5 = new Thread(StartAsyncTcp);
                vkt5.Start();
                Server.ColoredConsole.WriteLine("Tcp Start");
            }
        }

        private void StartAsyncTcp() // Begin async server that listen VKT5 devices by TCP
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));

                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100); // Max Count of connected device 100
                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    ColoredConsole.WriteLine("Waiting for a connection VKT5... " + StationEquipment.ServerPort);
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener); // Start an asynchronous socket to listen for connections.

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("VKT5 : " + StationEquipment.ServerPort + " - " + ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar) // Callback function calls when device connects to server
        {
            ColoredConsole.WriteLine("Connected");
            byte[] receive;
            allDone.Set(); // Set Main Thread to continue

            Socket listener = (Socket)ar.AsyncState;  // Get the socket that
            Socket handler = listener.EndAccept(ar);  // handles the client request.

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            ColoredConsole.WriteLine("Begining to read data from VKT5...");
            ColoredConsole.WriteLine("Read Configuration...");

            try
            {
                WritePacket(handler, ReadConf); // Write request for read configuration of device
                receive = ReadPacket(handler, ReadConf);  // Read responce from device

                List<TV> HeatInputs = ReadConfiguration(receive); // Fill structure by read configuration

                WritePacket(handler, Recvisites);
                receive = ReadPacket(handler, Recvisites);
                String serialNumber = ReadRecvisites(receive);

                while (true) // Communication with device (reading current parameters values)
                {
                    HeatInputs = SetTrumplesValues(handler, HeatInputs); // Trumples values to structure
                    HeatInputs = SetTV_s_Values(handler, HeatInputs); // Heat inputs values to structure
                    DataConfirm(HeatInputs, serialNumber); // Convert structure to Parameters that server can correctly read

                    ColoredConsole.WriteLine("Read Other Values...");
                    WritePacket(handler, OtherValues); // Request for additional parameters
                    receive = ReadPacket(handler, OtherValues); // Responce from device
                    ConvertOtherValues(receive, serialNumber); // Convert additional Parameters to structure that server can correctly read

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // Sleep to next interview
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("vkt5 : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both); // Shutdown socket
                handler.Disconnect(true); // Dissconnect socket
                handler.Close(); // and close socket if something bad happens
                return; // End thread - Device must connect in several minutes
            }
        }

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Setting COM-port settings
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;

            try
            {
                byte[] receive;
                if (!port.IsOpen) port.Open(); // Opening COM-port if it is closed

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);
                ColoredConsole.WriteLine("Read Configuration...");

                WritePacket(port, Recvisites);
                receive = ReadPacket(port, Recvisites);
                String serialNumber = ReadRecvisites(receive);

                WritePacket(port, ReadConf);
                receive = ReadPacket(port, ReadConf);

                List<TV> HeatInputs = ReadConfiguration(receive);
                HeatInputs = SetTrumplesValues(port, HeatInputs);
                HeatInputs = SetTV_s_Values(port, HeatInputs);
                DataConfirm(HeatInputs, serialNumber);

                ColoredConsole.WriteLine("Read Other Values...");
                WritePacket(port, OtherValues);
                receive = ReadPacket(port, OtherValues);
                ConvertOtherValues(receive, serialNumber);

                CSD_End(port);
                port.Close();
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        private void DataConfirm(List<TV> HeatInputs, string serial) // Function that convert structure to parameters that server can correctly read
        {
            int TVParamCoeficient = 0, TrumpleParamCoeficient = 0; // identifier and parameterNumbers for parameters
            for (int i = 0; i < HeatInputs.Count; i++) // Loop for all parameters in one Heat Input
            {
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, i, HeatInputs[i].Weight, StationEquipment.EquipmentTypeId)); // Add Weight to Structure
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 89, TVParamCoeficient++, HeatInputs[i].Heat, StationEquipment.EquipmentTypeId)); // Add Heat to Structure
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 89, TVParamCoeficient++, HeatInputs[i].HeatWithHWS, StationEquipment.EquipmentTypeId));  // Add HeatWithHWS to Structure
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 89, TVParamCoeficient++, HeatInputs[i].HeatWithoutHWS, StationEquipment.EquipmentTypeId));  // Add HeatWithHWS to Structure
                for (int j = 0; j < HeatInputs[i].Trumples.Count; j++) // Loop for all parameters in all Trumples that contains to this Heat Input
                {
                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, TrumpleParamCoeficient, HeatInputs[i].Trumples[j].Pressure, StationEquipment.EquipmentTypeId)); // Add Pressure to Structure
                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, TrumpleParamCoeficient, HeatInputs[i].Trumples[j].Temperature, StationEquipment.EquipmentTypeId)); // Add Temperature to Structure
                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, TrumpleParamCoeficient++, HeatInputs[i].Trumples[j].Weight, StationEquipment.EquipmentTypeId)); // Add Quantity to Structure
                }
            }
        }

        private string ReadRecvisites(byte[] data)
        {
            string serial = String.Empty;
            for (int i = 3; i < 15; i++) serial += Convert.ToChar(data[i]);
            return serial;
        }

        private void ConvertOtherValues(byte[] data, string serialNumber) // Function that convert additional parameters to parameters that server can correctly read 
        {
            try
            {
                byte[] value;
                int paramNumber = 8; // (parameterNumber + 1) of last Tempature that was read before it
                for (int i = 3; i < data.Length - 19; i += 4) // Loop by all additional parameters
                {
                    value = data.Skip(i).Take(4).ToArray(); // parameter is byte[4]
                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, paramNumber++, FloatConvert(value), StationEquipment.EquipmentTypeId)); // Add to Data (parameters that pass to server)
                }

                value = data.Skip(19).Take(4).ToArray(); // parameter is byte[4]
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, 9, FloatConvert(value), StationEquipment.EquipmentTypeId));

                value = data.Skip(23).Take(4).ToArray(); // parameter is byte[4]
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, paramNumber, FloatConvert(value), StationEquipment.EquipmentTypeId));
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return; }
        }

        private List<TV> SetTrumplesValues(SerialPort port, List<TV> HeatInputs) //Function adds trumple values to the structure for serial port
        {
            Trumple[] TrumpleParameters = new Trumple[8]; // Max count of trumples in device
            for (int t = 0; t < 8; t++) TrumpleParameters[t] = new Trumple(); // Create object for all elements in array
            byte[] receive;

            for (int j = 1; j <= 3; j++) // 3 Parameters (1 - Temperature, 2 - Pressure, 3 - Quantity)
            {
                Parameters[3] = Convert.ToByte(j * 16); // Change request for necessary parameter
                CRC16(Parameters, Parameters.Length); // Find CRC
                WritePacket(port, Parameters); // Send request for this parameter
                receive = ReadPacket(port, Parameters); // Read responce

                for (int i = 3, p = 0; i < receive.Length - 2; i += 4, p++) // Parsing responce
                {
                    byte[] res = receive.Skip(i).Take(4).ToArray(); // Read one value ( value is byte[4] )
                    TrumpleParameters[p].number = p + 1; // Count of current trumple
                    if (j == 1) TrumpleParameters[p].Temperature = FloatConvert(res); // if parameter is Temperature
                    if (j == 2) TrumpleParameters[p].Pressure = FloatConvert(res); // if parameter is Pressure
                    if (j == 3) TrumpleParameters[p].Weight = FloatConvert(res); // if parameter is Quantity
                }
            }

            for (int k = 0; k < HeatInputs.Count; k++) // Loop that adds converting parameters to the structure
            {
                for (int l = 0; l < HeatInputs[k].Trumples.Count; l++) // for all Heat Inputs
                {
                    if (HeatInputs[k].Trumples[l].number == TrumpleParameters[l].number) // if (heat input trumple number = trumple number
                    {
                        HeatInputs[k].Trumples[l].Pressure = TrumpleParameters[l].Pressure; // add Temperature to the structure
                        HeatInputs[k].Trumples[l].Temperature = TrumpleParameters[l].Temperature; // add Temperature to the structure
                        HeatInputs[k].Trumples[l].Weight = TrumpleParameters[l].Weight; // add Quantity to the structure
                    }
                }
            }
            return HeatInputs; // return filled structure
        }

        private List<TV> SetTrumplesValues(Socket socket, List<TV> HeatInputs) //Function adds trumple values to the structure for socket
        {
            Trumple[] TrumpleParameters = new Trumple[8]; // Max count of trumples in device
            for (int t = 0; t < 8; t++) TrumpleParameters[t] = new Trumple(); // Create object for all elements in array
            byte[] receive;

            for (int j = 1; j <= 3; j++) // 3 Parameters (1 - Temperature, 2 - Pressure, 3 - Quantity)
            {
                Parameters[3] = Convert.ToByte(j * 16); // Change request for necessary parameter
                CRC16(Parameters, Parameters.Length); // Find CRC
                WritePacket(socket, Parameters); // Send request for this parameter
                receive = ReadPacket(socket, Parameters); // Read responce

                for (int i = 3, p = 0; i < receive.Length - 2; i += 4, p++) // Parsing responce
                {
                    byte[] res = receive.Skip(i).Take(4).ToArray(); // Read one value ( value is byte[4] )
                    TrumpleParameters[p].number = p + 1; // Count of current trumple
                    if (j == 1) TrumpleParameters[p].Temperature = FloatConvert(res); // if parameter is Temperature
                    if (j == 2) TrumpleParameters[p].Pressure = FloatConvert(res); // if parameter is Pressure
                    if (j == 3) TrumpleParameters[p].Weight = FloatConvert(res); // if parameter is Quantity
                }
            }

            for (int k = 0; k < HeatInputs.Count; k++) // Loop that adds converting parameters to the structure
            {
                for (int l = 0; l < HeatInputs[k].Trumples.Count; l++) // for all Heat Inputs
                {
                    if (HeatInputs[k].Trumples[l].number == TrumpleParameters[l].number) // if (heat input trumple number = trumple number
                    {
                        HeatInputs[k].Trumples[l].Pressure = TrumpleParameters[l].Pressure; // add Pressure to the structure
                        HeatInputs[k].Trumples[l].Temperature = TrumpleParameters[l].Temperature; // add Temperature to the structure
                        HeatInputs[k].Trumples[l].Weight = TrumpleParameters[l].Weight; // add Quantity to the structure
                    }
                }
            }
            return HeatInputs; // return filled structure
        }

        private List<TV> SetTV_s_Values(SerialPort port, List<TV> HeatInputs) //Function adds heat input values to the structure for serial port
        {
            TV[] TvParameters = new TV[8]; // Max count of heat inputs in device
            for (int t = 0; t < 8; t++) TvParameters[t] = new TV(); // Create objects for array
            byte[] receive;

            for (int j = 1, p = 0; j <= 8; j++, p++) // Loop by all heat inputs
            {
                TV_Values[3] = Convert.ToByte(j * 16); // Change request for current heat input
                CRC16(TV_Values, TV_Values.Length); // Find CRC for request
                WritePacket(port, TV_Values); // Send request 
                receive = ReadPacket(port, TV_Values); // Read responce

                for (int i = 3, k = 0; i < receive.Length - 2; i += 4, k++) // Parsing receiving responce
                {
                    byte[] res = receive.Skip(i).Take(4).ToArray();  // Read one value ( value is byte[4] )
                    if (k == 0) TvParameters[p].Weight = FloatConvert(res); // if parameter is Weight
                    if (k == 1) TvParameters[p].Heat = FloatConvert(res); // if parameter is Heat
                    if (k == 2) TvParameters[p].HeatWithoutHWS = FloatConvert(res); // if parameter is HeatWithoutHWS
                    if (k == 3) TvParameters[p].HeatWithHWS = FloatConvert(res); // if parameter is HeatWithHWS
                    TvParameters[p].number = p + 1; // Current number of heat input
                }
            }

            for (int k = 0; k < HeatInputs.Count; k++) // Loop that adds converting parameters to the structure
            {
                if (HeatInputs[k].number == TvParameters[k].number) // if (heat input  number = TV structure number)
                {
                    HeatInputs[k].Weight = TvParameters[k].Weight;  // add Weight to the structure
                    HeatInputs[k].Heat = TvParameters[k].Heat;  // add Heat to the structure
                    HeatInputs[k].HeatWithHWS = TvParameters[k].HeatWithHWS;  // add HeatWithHWS to the structure
                    HeatInputs[k].HeatWithoutHWS = TvParameters[k].HeatWithoutHWS;  // add HeatWithoutHWS to the structure
                }
            }
            return HeatInputs; // return structure
        }

        private List<TV> SetTV_s_Values(Socket socket, List<TV> HeatInputs) //Function adds heat input values to the structure for socket
        {
            TV[] TvParameters = new TV[8]; // Max count of heat inputs in device
            for (int t = 0; t < 8; t++) TvParameters[t] = new TV();  // Create objects for array
            byte[] receive;

            for (int j = 1, p = 0; j <= 8; j++, p++) // Loop by all heat inputs
            {
                TV_Values[3] = Convert.ToByte(j * 16); // Change request for current heat input
                CRC16(TV_Values, TV_Values.Length); // Find CRC for request
                WritePacket(socket, TV_Values); // Send request 
                receive = ReadPacket(socket, TV_Values); // Read responce

                for (int i = 3, k = 0; i < receive.Length - 2; i += 4, k++) // Parsing receiving responce
                {
                    byte[] res = receive.Skip(i).Take(4).ToArray(); // Read one value ( value is byte[4] )
                    if (k == 0) TvParameters[p].Weight = FloatConvert(res); // if parameter is Weight
                    if (k == 1) TvParameters[p].Heat = FloatConvert(res); // if parameter is Heat
                    if (k == 2) TvParameters[p].HeatWithoutHWS = FloatConvert(res); // if parameter is HeatWithoutHWS
                    if (k == 3) TvParameters[p].HeatWithHWS = FloatConvert(res); // if parameter is HeatWithHWS
                    TvParameters[p].number = p + 1; // Current number of heat input
                }
            }

            for (int k = 0; k < HeatInputs.Count; k++) // Loop that adds converting parameters to the structure
            {
                if (HeatInputs[k].number == TvParameters[k].number) // if (heat input  number = TV structure number)
                {
                    HeatInputs[k].Weight = TvParameters[k].Weight; // add Weight to the structure
                    HeatInputs[k].Heat = TvParameters[k].Heat; // add Heat to the structure
                    HeatInputs[k].HeatWithHWS = TvParameters[k].HeatWithHWS; // add HeatWithHWS to the structure
                    HeatInputs[k].HeatWithoutHWS = TvParameters[k].HeatWithoutHWS; // add HeatWithoutHWS to the structure
                }
            }
            return HeatInputs; // return structure
        }

        private List<TV> ReadConfiguration(byte[] data) // Function that Convert responce array of configurations
        {
            List<TV> HeatInputs = new List<TV>(); // Create a structure
            int number = 1; // Trumple number
            for (int i = 1; i <= 8; i++) // Loop by heat inputs
            {
                TV Tv = new TV(); // Create heat input object
                List<Trumple> Trumples = new List<Trumple>(); // Crate list of trumple objects
                for (int j = 3; j < data.Length - 6; j += 7) // Parsing responce
                {
                    if (data[j] == i) // data[j] contains heat input number
                    {
                        switch (data[j + 1]) // data[j+1] contains trumple appoinment
                        {
                            case 0:
                                Trumples.Add(SetTrumpleBaseSettings("подающая", number++));
                                break;
                            case 1:
                                Trumples.Add(SetTrumpleBaseSettings("обратная", number++));
                                break;
                            case 2:
                                Trumples.Add(SetTrumpleBaseSettings("ГВС", number++));
                                break;
                            case 3:
                                Trumples.Add(SetTrumpleBaseSettings("подпитка", number++));
                                break;
                            case 4:
                                Trumples.Add(SetTrumpleBaseSettings("электросчетчик", number++));
                                break;
                            case 5:
                                Trumples.Add(SetTrumpleBaseSettings("холодная вода", number++));
                                break;
                        }
                    }
                }
                if (Trumples.Count > 0)
                {
                    Tv.Trumples = new List<Trumple>(Trumples);  // create trumples list
                    Tv.number = i;  // number of heat input
                    HeatInputs.Add(Tv); // add heat input to structure
                }
            }
            return HeatInputs; // return structure
        }

        private Trumple SetTrumpleBaseSettings(string appoitment, int number) // Function that fill trumple appointment
        {
            Trumple tr = new Trumple(); // create trumple object
            tr.appointment = appoitment; // set trumple appoinment
            tr.number = number; // set trumple number
            return tr; // return trumple object
        }

        private string GetPOVersion(byte[] data) // Function that convert PO number ( wasting time for writing it)
        {
            byte first = Convert.ToByte(data[4] >> 4);
            byte second = Convert.ToByte(data[4] & 0x0f);
            return first + "." + second; // return PO number
        }

        private DateTime DateConvert(byte[] data) // Function that convert Device DateTime
        {
            int year = (data[3] << 8) | data[4];
            int month = (data[5] << 8) | data[6];
            int day = (data[7] << 8) | data[8];
            int hour = (data[9] << 8) | data[10];
            int minute = (data[11] << 8) | data[12];
            return new DateTime(year, month, day, hour, minute, DateTime.UtcNow.Second); // return DateTime
        }

        private float FloatConvert(byte[] data) // Function that convert byte[] to float
        {
            return BitConverter.ToSingle(data.Reverse().ToArray(), 0); // return converting value
        }

        public byte[] ReadPacket(SerialPort port, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 10)
                {
                    int byteCount = port.BytesToRead;
                    byte[] retBytes = new byte[byteCount];
                    port.Read(retBytes, 0, byteCount);

                    if (byteCount < 2)
                    {
                        WritePacket(port, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        count++;
                        WritePacket(port, lastSend);
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        public byte[] ReadPacket(Socket socket, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 10)
                {
                    int byteCount = socket.ReceiveBufferSize;
                    byte[] retBytes = new byte[100];
                    int bytesRead = socket.Receive(retBytes);

                    retBytes = retBytes.Take(bytesRead).ToArray();

                    if (byteCount < 2)
                    {
                        WritePacket(socket, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        WritePacket(socket, lastSend);
                        count++;
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        public override List<DataParameter> Read() // overrided function that confirm structure sended to server
        {
            List<DataParameter> par = new List<DataParameter>(Data); // create temp list of data and copy all parameters to it 
            if (par.Any()) // if if is no empty
            {
                Data.Clear(); // Cleat Main Data
                return par; // Send temp data
            }
            else
            {
                return Data; // Send empty data
            }
        }
    }
}
