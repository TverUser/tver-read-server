﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO.Ports;
using Server;

namespace KM_5
{
    public class KM5 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // Structure sending to Server
        private StationEquipmentData StationEquipment; // Parameters for initialization
        public ManualResetEvent allDone = new ManualResetEvent(false);

        private byte[] getNetworkAddress = { 0x51, 0x52, 0x53, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x4a };
        private byte[] getCurrentValuesOne = { 0x00, 0x00, 0x00, 0x00, 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private byte[] getCurrentValuesTwo = { 0x00, 0x00, 0x00, 0x00, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        public KM5(StationEquipmentData param) : base(param)
        {
            StationEquipment = new StationEquipmentData(param);

            if (StationEquipment.ConnectionType == 1)
            {
                Thread KM5 = new Thread(StartAsyncTcp);
                KM5.Start();
                ColoredConsole.WriteLine("Tcp Start");
            }
        }

        public override List<DataParameter> StartCom()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            byte[] receive;
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;
            port.DtrEnable = true;
            port.RtsEnable = true;

            Data.Clear();
            ColoredConsole.WriteLine("Begininsg to read data from KM5...");

            try
            {
                if (!(port.IsOpen)) port.Open(); // Port opening


                ColoredConsole.WriteLine(3, "Звоним на КМ-5 : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType); // Start CSD Connections

                ColoredConsole.WriteLine("Called");

                WritePacket(port, getNetworkAddress); // Get serial number for reading device parameters
                receive = ReadPacket(port, getNetworkAddress);

                byte[] NetworkAddress = receive.Skip(5).Take(4).ToArray(); // Convert Received Serial Number

                CalculateCRC(NetworkAddress); // Calculating CRC for other request's
                ColoredConsole.WriteLine(3, "КМ-5 - Считано серийный номер : " + BitConverter.ToUInt32(NetworkAddress, 0));

                ColoredConsole.WriteLine("NetworkAddress : " + BitConverter.ToUInt32(NetworkAddress, 0));

                WritePacket(port, getCurrentValuesOne); // Request for current values
                receive = ReadPacket(port, getCurrentValuesOne); // Get Responce

                FirstStageWritingData(receive, GetSerial(NetworkAddress));
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "КМ-5 " + BitConverter.ToUInt32(NetworkAddress, 0) + " считано : 50 %");

                WritePacket(port, getCurrentValuesTwo);
                receive = ReadPacket(port, getCurrentValuesTwo);

                SecondStageWrintingData(receive, GetSerial(NetworkAddress));
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "КМ-5 " + BitConverter.ToUInt32(NetworkAddress, 0) + " считано : 100%");

                ColoredConsole.WriteLine("Data is reading succesfully...");
                ColoredConsole.WriteLine(3, "Считывание данных с КМ-5 : " + StationEquipment.PhoneDialing + " завершено.");

                CSD_End(port); // End CSD Connection
                port.Close();
                // Thread.Sleep(TimeSpan.FromSeconds(30));
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                ColoredConsole.WriteLine(3, "Не удалось считать данные с КМ-5 : " + StationEquipment.PhoneDialing);
                CSD_End(port);
                if (port.IsOpen) port.Close();
                return Data;
            }
        }

        public override ArcHiveReadedPack ReadArchive()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;

            port.Handshake = Handshake.None;
            port.WriteTimeout = 2000;
            port.ReadTimeout = 2000;

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                ColoredConsole.WriteLine(3, "Звоним на KM-5 : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                WritePacket(port, getNetworkAddress); // Get serial number for reading device parameters
                byte[] receive = ReadPacket(port, getNetworkAddress);

                byte[] NetworkAddress = receive.Skip(5).Take(4).ToArray(); // Convert Received Serial Number

                ColoredConsole.WriteLine(3, "КМ-5 - Считано серийный номер : " + BitConverter.ToUInt32(NetworkAddress, 0));

                string[] Eq = StationEquipment.ArchivesParameter.Split('|');
                DateTime d_from = DateTime.Parse(Eq[0]);
                DateTime d_to = DateTime.Parse(Eq[1]);

                while (d_from < d_to)
                {
                    int month1 = 0;
                    int day1 = 0;
                    int year1 = 0;
                    int hour1 = 0;
                    day1 = d_from.Day;
                    month1 = d_from.Month;
                    year1 = d_from.Year;
                    hour1 = d_from.Hour;

                    ColoredConsole.WriteLine(3, "Попытка считать архив за  : " + d_from.ToString());

                    List<List<DataParameter>> readed = new List<List<DataParameter>>();

                    for (int i = 0; i < 24; i++)
                    {
                        readed.Add(ReadArchiveForHour(port, NetworkAddress, d_from));
                        d_from.AddHours(1);
                    }

                    if (StationEquipment.ArchivesType == 1) d_from = d_from.AddDays(1); //хоть запрос считать за час, мне удобней считать за весь день!!!
                    if (StationEquipment.ArchivesType == 2) d_from = d_from.AddDays(1);
                    if (StationEquipment.ArchivesType == 3) d_from = d_from.AddMonths(1);
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                ColoredConsole.WriteLine(3, "Не удалось считать данные с КМ-5 : " + StationEquipment.PhoneDialing);
            }

            CSD_End(port);
            if (port.IsOpen) port.Close();
            return base.ReadArchive();
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state

                    ColoredConsole.WriteLine("Waiting for a connection KM5... " + StationEquipment.ServerPort); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("KM5 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.
            byte[] receive;

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            try
            {
                ColoredConsole.WriteLine("Begining to read data from KM5...");

                while (true)
                {
                    WritePacket(handler, getNetworkAddress); // Get serial number for reading device parameters
                    receive = ReadPacket(handler, getNetworkAddress);

                    byte[] NetworkAddress = receive.Skip(5).Take(4).ToArray(); // Convert Received Serial Number

                    CalculateCRC(NetworkAddress); // Calculating CRC for other request's

                    WritePacket(handler, getCurrentValuesOne); // Request for current values
                    receive = ReadPacket(handler, getCurrentValuesOne); // Get Responce

                    FirstStageWritingData(receive, GetSerial(NetworkAddress));

                    WritePacket(handler, getCurrentValuesTwo);
                    receive = ReadPacket(handler, getCurrentValuesTwo);

                    SecondStageWrintingData(receive, GetSerial(NetworkAddress));

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // задержка до следующего опроса
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("KM5 : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Disconnect(true);
                handler.Close();
                return;
            }
        }

        private void FirstStageWritingData(byte[] data, uint serial)
        {
            int i = 5, j = 0;

            for (j = 0; j < 3; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, j, BitConverter.ToSingle(converted, 0), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            for (j = 0; j < 4; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(converted, 0), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            for (j = 0; j < 3; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, j, BitConverter.ToSingle(converted, 0), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 12, 0, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId));
            i += 4;

            for (j = 4; j < 7; j++)
            {
                byte[] converted = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(converted, 0), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 12, 1, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId));
            i += 4;

            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, j, BitConverter.ToSingle(data.Skip(i).Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId));
            i += 4;
        }

        private void SecondStageWrintingData(byte[] data, uint serial)
        {
            int i = 13;
            byte[] val;

            for (int j = 0; j < 2; j++)
            {
                val = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, j, Math.Round(BitConverter.ToSingle(val, 0), 3), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            for (int j = 0; j < 3; j++)
            {
                val = data.Skip(i).Take(4).ToArray();
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, j, Math.Round(BitConverter.ToSingle(val, 0), 3), StationEquipment.EquipmentTypeId));
                i += 4;
            }

            val = data.Skip(i).Take(4).ToArray();
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 13, 0, Math.Round(BitConverter.ToSingle(val, 0) * 1.163, 3), StationEquipment.EquipmentTypeId));

            i += 4;
            val = data.Skip(i).Take(4).ToArray();
            Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 10, 0, Math.Round(BitConverter.ToSingle(val, 0), 3), StationEquipment.EquipmentTypeId));
        }

        public List<DataParameter> ReadArchiveForHour(SerialPort port, byte[] NetworkAddress, DateTime data)
        {
            byte[] cmd = { 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < 4; i++)
                cmd[i] = NetworkAddress[i];

            CRC16(cmd, cmd.Length);

            WritePacket(port, cmd);
            byte[] receive = ReadPacket(port);

            receive = receive.Skip(5).Take(8).ToArray();
            byte[] crystallAddres = { receive[0], receive[1] };

            cmd[4] = 52;
            cmd[6] = Convert.ToByte(data.Day);
            cmd[7] = Convert.ToByte(data.Month);
            cmd[8] = Convert.ToByte(data.Year - 2000);
            cmd[9] = Convert.ToByte(data.Hour);
            CRC16(cmd, cmd.Length);

            WritePacket(port, cmd);
            receive = ReadPacket(port);

            receive = receive.Skip(5).Take(23).ToArray();

            byte specialInfo = receive[0];

            if (!((specialInfo & (1 << 6)) != 0))
            {
                return new List<DataParameter>();
            }

            byte[] archivePageNumber = { receive[2], receive[1] };

            cmd[4] = 64;
            cmd[5] = crystallAddres[0];
            cmd[6] = crystallAddres[1];
            cmd[7] = archivePageNumber[0];
            cmd[8] = archivePageNumber[1];
            CRC16(cmd, cmd.Length);

            WritePacket(port, cmd);
            receive = ReadPacket(port);

            return ParseAHourlyArchive(receive);
        }

        public List<DataParameter> ParseAHourlyArchive(byte[] receive)
        {
            List<DataParameter> parsingData = new List<DataParameter>();

            try
            {
                DateTime archiveTime;

                byte[] date = receive.Skip(5).Take(8).ToArray();
                receive = receive.Skip(8).ToArray();

                try
                { 
                    archiveTime = new DateTime(2000 + date[3], date[2], date[1], date[5], date[6], date[7]);
                }
                catch (Exception ex)
                {
                    archiveTime = new DateTime(0, 0, 0, 0, 0, 0);
                }
                
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 3, BitConverter.ToSingle(receive.Skip(0).Take(4).Reverse().ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, 0, BitConverter.ToSingle(receive.Skip(4).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, 1, BitConverter.ToSingle(receive.Skip(8).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, 2, BitConverter.ToSingle(receive.Skip(12).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 0, BitConverter.ToSingle(receive.Skip(16).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 1, BitConverter.ToSingle(receive.Skip(20).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, 2, BitConverter.ToSingle(receive.Skip(24).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, 0, BitConverter.ToSingle(receive.Skip(28).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, 1, BitConverter.ToSingle(receive.Skip(32).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 0, BitConverter.ToSingle(receive.Skip(36).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 1, BitConverter.ToSingle(receive.Skip(40).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, 2, BitConverter.ToSingle(receive.Skip(44).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 13, 0, BitConverter.ToSingle(receive.Skip(48).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
                Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 10, 0, BitConverter.ToSingle(receive.Skip(52).Reverse().Take(4).ToArray(), 0), StationEquipment.EquipmentTypeId, archiveTime));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " KM-5 ParseAHourlyArchive");
                return new List<DataParameter>();
            }

            return parsingData;
        }

        private uint GetSerial(byte[] data)
        {
            byte[] reg = new byte[4];
            reg = data.Reverse().ToArray();
            string serNum = BitConverter.ToString(reg).Replace("-", string.Empty);
            return Convert.ToUInt32(serNum);
        }

        private void SetAddress(byte[] NetworkAddress)
        {
            for (int i = 0; i < 4; i++)
            {
                getCurrentValuesOne[i] = NetworkAddress[i];
                getCurrentValuesTwo[i] = NetworkAddress[i];
            }
        }

        private void CalculateCRC(byte[] NetworkAddress)
        {
            SetAddress(NetworkAddress);
            CRC16(getCurrentValuesOne, getCurrentValuesOne.Length);
            CRC16(getCurrentValuesTwo, getCurrentValuesTwo.Length);
        }

        public override Int16 CRC16(byte[] data, int size)
        {
            byte crc1 = data[0];
            byte crc2 = data[0];
            int length = size - 2;
            for (int j = 1; j < length; j++)
            {
                crc1 ^= data[j];
                crc2 += data[j];
            }
            crc2 = Convert.ToByte(crc2 % 256);
            data[data.Length - 2] = crc1;
            data[data.Length - 1] = Convert.ToByte(crc2);
            return BitConverter.ToInt16(new byte[] { data[data.Length - 2], data[data.Length - 1] }, 0);
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

        public byte[] ReadPacket(SerialPort port, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 2)
                {
                    Thread.Sleep(2000);
                    int byteCount = port.BytesToRead;
                    byte[] retBytes = new byte[byteCount];
                    port.Read(retBytes, 0, byteCount);

                    if (byteCount < 2)
                    {
                        Thread.Sleep(2000);
                        WritePacket(port, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        count++;
                        WritePacket(port, lastSend);
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        public byte[] ReadPacket(Socket socket, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 2)
                {
                    Thread.Sleep(2000);
                    int byteCount = socket.ReceiveBufferSize;
                    byte[] retBytes = new byte[100];
                    int bytesRead = socket.Receive(retBytes);
                    ColoredConsole.WriteLine("Return bytes : " + bytesRead);
                    retBytes = retBytes.Take(bytesRead).ToArray();

                    if (byteCount < 2)
                    {
                        Thread.Sleep(2000);
                        WritePacket(socket, lastSend);
                        count++;
                        continue;
                    }

                    int crc = BitConverter.ToInt16(new byte[2] { retBytes[retBytes.Length - 2], retBytes[retBytes.Length - 1] }, 0);
                    int crc1 = CRC16(retBytes, retBytes.Length);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                    else
                    {
                        WritePacket(socket, lastSend);
                        count++;
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }
    }
}
