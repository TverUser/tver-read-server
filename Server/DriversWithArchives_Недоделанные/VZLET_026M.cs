﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterParent;
using System.Threading;
using System.IO.Ports;
using Modbus.Device;
using Server;

namespace VZLET_TSRV_026M
{
    public class VZLET_026M : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>();
        private StationEquipmentData StationEquipment;
        public ManualResetEvent allDone = new ManualResetEvent(false);

        private const int VZLET_026_EQUIPMENT_TYPE_ID = 23;
        private const byte READ_ARCHIVE_COMMAND = 0x41;
        private const ushort SERIAL_NUMBER_REGISTER = 0x8002;
        private const int SIZE_2 = 2;
        private const int SIZE_4 = 4;

        private readonly ushort[] VOLUME_QUANTITY_ADDRESSES = { 0xC01E, 0xC020, 0xC022, 0xC024, 0xC026, 0xC028 };
        private readonly ushort[] MASSIVE_QUANTITY_ADDRESES = { 0xC098, 0xC09A, 0xC09C, 0xC09E, 0xC136, 0xC138 };
        private readonly ushort[] TEMPERATURE_ADDRESSSES = { 0xC02A, 0xC02C, 0xC02E, 0xC030, 0xC032, 0xC100, 0xC13A };
        private readonly ushort[] PRESSURE_ADDRESSES = { 0xC034, 0xC036, 0xC038, 0xC03A, 0xC140 };
        private readonly ushort[] HEAT_ADDRESSES = { 0xC0B8, 0xC0BC, 0xC0C0, 0xC0C4, 0xC068, 0xC06C };
        private readonly ushort[] WEIGHT_ADDRESSES = { 0xC0C8, 0xC0CC, 0xC0D0, 0xC0D4, 0xC070, 0xC074 };
        private readonly ushort[] VOLUME_ADDRESSES = { 0xC0D8, 0xC0DC, 0xC0E0, 0xC0E4 };
        private readonly ushort[] ENERGY_ADDRESSES = { 0xC132, 0xC134, 0xC168 };
        private readonly ushort[] REALISING_TIME_ADDRESSES = { 0x8004, 0x8006, 0x8008, 0x800A, 0x800C };

        private uint EquipmentSerialNumber;
        private ModbusSerialMaster master;

        public VZLET_026M(StationEquipmentData param) : base(param)
        {
            StationEquipment = new StationEquipmentData(param);
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            SerialPort Port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One)
            {
                WriteTimeout = 2000,
                ReadTimeout = 2000
            }; // Инициализация ком порта;

            try
            {
                if (!(Port.IsOpen))
                    Port.Open();

                CSD.CSD_async(Port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                master = ModbusSerialMaster.CreateRtu(Port);

                ushort[] ModbusRegister = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, SERIAL_NUMBER_REGISTER, SIZE_2);
                EquipmentSerialNumber = GetLong(ShortBufferToByteBuffer(ModbusRegister));

                if (EquipmentSerialNumber == 0)
                {
                    throw new Exception("Read device serial number error");
                }

                ReadData(TEMPERATURE_ADDRESSSES, 2, 95);
                //ReadData(VOLUME_QUANTITY_ADDRESSES, 2, 94);
                ReadData(PRESSURE_ADDRESSES, 2, 1);
                //ReadData(MASSIVE_QUANTITY_ADDRESES, 2, 93);
                ReadData(HEAT_ADDRESSES, 4, 13);
                ReadData(WEIGHT_ADDRESSES, 4, 90);
                ReadData(VOLUME_ADDRESSES, 4, 91);
                ReadData(ENERGY_ADDRESSES, 2, 13);
                ReadData(REALISING_TIME_ADDRESSES, 2, 51);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                CSD.CSD_End(Port);
                if (Port.IsOpen)
                    Port.Close();
            }
            return Data;
        }

        private void ReadData(ushort[] Registers, int FrameSize, int ObisCode)
        {
            String EquipmentID = StationEquipment.StationEquipmentId.ToString();
            int EquipmentTypeID = StationEquipment.EquipmentTypeId;

            for (int i = 0; i < Registers.Length; i++)
            {
                try
                {
                    if (FrameSize == 2)
                        Data.Add(new DataParameter(EquipmentID, ObisCode, i, ReadFloat(Registers[i]), EquipmentTypeID));
                    if (FrameSize == 4)
                        Data.Add(new DataParameter(EquipmentID, ObisCode, i, ReadLongFloat(Registers[i]), EquipmentTypeID));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private uint ReadSerialNumber(ushort Register)
        {
            ushort[] SerialRegisterResponse = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, Register, SIZE_2);
            return GetLong(ShortBufferToByteBuffer(SerialRegisterResponse)); ;
        }

        private double ReadLongFloat(ushort Register)
        {
            ushort[] RegistersResponse = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, Register, SIZE_4);

            byte[] Response = ShortBufferToByteBuffer(RegistersResponse);

            long longVal = GetLong(Response.Take(4).ToArray());
            float floatVal = GetFloat(Response.Skip(4).Take(4).ToArray());

            return longVal + floatVal; ;
        }

        private double ReadFloat(ushort Register)
        {
            ushort[] RegistersResponse = master.ReadInputRegisters((byte)StationEquipment.DeviseAdress, Register, 2);
            return GetFloat(ShortBufferToByteBuffer(RegistersResponse)); ;
        }

        private byte[] ShortBufferToByteBuffer(ushort[] DataBuffer)
        {
            byte[] ResultArray = new byte[DataBuffer.Length * 2];
            for (int i = 0, j = 0; i < DataBuffer.Length; i += 1, j += 2)
            {
                ResultArray[j] = Convert.ToByte((DataBuffer[i] >> 8) & 0xFF);
                ResultArray[j + 1] = Convert.ToByte(DataBuffer[i] & 0xFF);
            }
            return ResultArray;
        }

        private uint GetLong(byte[] DataBuffer)
        {
            return Convert.ToUInt32((uint)(DataBuffer[0] << 24) | (uint)(DataBuffer[1] << 16) | (uint)(DataBuffer[2] << 8) | (uint)DataBuffer[3]); // Преобразование байтов в целое число (32 бита)
        }

        private float GetFloat(byte[] data)
        {
            return Convert.ToSingle(Math.Round(BitConverter.ToSingle(data.Reverse().ToArray(), 0), 2));
        }

        private double GetLongFloat(byte[] Response)
        {
            long longVal = GetLong(Response.Take(4).ToArray());
            float floatVal = GetFloat(Response.Skip(4).Take(4).ToArray());
            return longVal + floatVal;
        }

        public List<List<DataParameter>> ReadArchive(int Hour, int Day, int Month, int Year)
        {
            List<List<DataParameter>> Result = new List<List<DataParameter>>();

            //Device_Adress  Function   archIndex_Little archIndex_Big (archIndex)  WhatRead(0-ByIndex 1-ByTime)
            // Seconds Minuter Hours Days Month Year  CRC1 СRС2
            byte[] ArchiveRequest = { (byte)Config.DeviseAdress, READ_ARCHIVE_COMMAND, 0, (byte)(Config.ArchivesType - 1), 0, 1, 1,
                                            0,  0, (byte)Hour, (byte)Day, (byte)Month, (byte)(Year % 100), 0, 0 };

            try
            {
                byte[] ArchiveBuffer = comm.MessagebyCRC(ArchiveRequest, CommunicateCurrent.CRCtype_CRC16, timeout: 5000);
                Result.Add(ParseArchiveBuffer(ArchiveBuffer.Skip(3).Take(ArchiveBuffer.Length - 5).ToArray()));
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, ex.Message);
                return new List<List<DataParameter>>();
            }
            return Result;
        }

        private List<DataParameter> ParseArchiveBuffer(byte[] ArchiveBuffer)
        {
            List<DataParameter> ArchiveData = new List<DataParameter>();

            float[] HeatSystems = new float[4];
            float[] WeightSystems = new float[4];
            float[] VolumeSystems = new float[4];
            float[] TemperatureSystems = new float[4];
            float[] AverageTemperatureSystems = new float[4];
            float[] PressureSystems = new float[4];

            DateTime ArchiveTime = new DateTime(1970, 1, 1);
            ArchiveTime.AddSeconds(GetLong(ArchiveBuffer.Take(4).ToArray()));

            int WorkingTime = BitConverter.ToInt16(ArchiveBuffer.Skip(4).Take(2).ToArray(), 0);   // Чистое время работы ТС в штатном ре-жиме
            int NoPowerTime = BitConverter.ToInt16(ArchiveBuffer.Skip(6).Take(2).ToArray(), 0);   // Время простоя ТС при пропадании питания
            int NoSensorTime = BitConverter.ToInt16(ArchiveBuffer.Skip(8).Take(2).ToArray(), 0);  // Время простоя ТС из-за отказа датчиков
            int TotalNoTime = BitConverter.ToInt16(ArchiveBuffer.Skip(10).Take(2).ToArray(), 0);  // Общее время простоя из-за действия НС
            int WorkNoTime = BitConverter.ToInt16(ArchiveBuffer.Skip(12).Take(2).ToArray(), 0);   // Время простоя ТС при выходе из режима «Работа»
            int HeatNoTime = BitConverter.ToInt16(ArchiveBuffer.Skip(14).Take(2).ToArray(), 0);   // Время простоя системы отопления
            int GVS_NoTime = BitConverter.ToInt16(ArchiveBuffer.Skip(16).Take(2).ToArray(), 0);   // Время простоя системы ГВС
            double TotalHeat = GetLongFloat(ArchiveBuffer.Skip(40).Take(8).ToArray());            // Общее тепло, потребленное абонентом, нарастающим итогом
            double TotalWeight = GetLongFloat(ArchiveBuffer.Skip(48).Take(8).ToArray());          // Общая масса на отопление, потребленное абонентом, нарастающим итогом
            double HeatWithWaterProof = GetLongFloat(ArchiveBuffer.Skip(56).Take(8).ToArray());   // Тепло, потребленное с водоразбором, нарастающим итогом
            double WeightWithWaterProof = GetLongFloat(ArchiveBuffer.Skip(64).Take(8).ToArray());   // Масса, потребленная с водоразбором, нарастающим итогом

            for (int i = 0; i < 4; i++)
            {
                ArchiveData.Add(new DataParameter(Config.DriverFeatures, 13, i, BitConverter.ToInt16(ArchiveBuffer.Skip(72 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));
                ArchiveData.Add(new DataParameter(Config.DriverFeatures, 91, i, BitConverter.ToInt16(ArchiveBuffer.Skip(88 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));
                ArchiveData.Add(new DataParameter(Config.DriverFeatures, 90, i, BitConverter.ToInt16(ArchiveBuffer.Skip(104 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));
                //ArchiveData.Add(new DataParameter(Config.DriverFeatures, 95, i, BitConverter.ToInt16(ArchiveBuffer.Skip(120 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));
                ArchiveData.Add(new DataParameter(Config.DriverFeatures, 95, i, BitConverter.ToInt16(ArchiveBuffer.Skip(128 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));
                ArchiveData.Add(new DataParameter(Config.DriverFeatures, 1, i, BitConverter.ToInt16(ArchiveBuffer.Skip(136 + (i * 4)).Take(4).ToArray(), 0), VZLET_026_EQUIPMENT_TYPE_ID, ArchiveTime));

                /*HeatSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(72 + (i * 4)).Take(4).ToArray());
                WeightSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(88 + (i * 4)).Take(4).ToArray());
                VolumeSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(104 + (i * 4)).Take(4).ToArray());
                TemperatureSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(120 + (i * 4)).Take(4).ToArray());
                AverageTemperatureSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(128 + (i * 4)).Take(4).ToArray());
                PressureSystems[i] = BitConverter.ToInt16(ArchiveBuffer.Skip(136 + (i * 4)).Take(4).ToArray());*/
            }

            return ArchiveData;
        }
    }
}
