﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MasterParent;
using System.Net;
using System.Net.Sockets;
using Server;

namespace TEM104n
{
    public class TEM104 : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        public ManualResetEvent allDone = new ManualResetEvent(false);
        private byte NetworkNddress = 0;

        byte[] Initiaze_pak = new byte[] { 0x55, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00 };
        byte[] Feature_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x00, 0x7c, 0x04, 0x00 };
        byte[] Memo2K_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x00, 0xff, 0x00 };
        byte[] OP_Memo_pak = new byte[] { 0x55, 0x00, 0xff, 0x0c, 0x01, 0x03, 0x22, 0x00, 0xff, 0x00 };



        /// <summary>
        List<byte[]> OP_MemoLisInit = new List<byte[]>();

        public TEM104(StationEquipmentData Params) : base(Params)
        {
            StatusLogNum = 8;

            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x00, 0x0f, 0x00 }); // Temperature
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x10, 0x0f, 0x00 }); // Pressure
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x20, 0x0f, 0x00 }); // DensityCoolant
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x30, 0x0f, 0x00 }); // Enthalpy
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x40, 0x0f, 0x00 }); // VolumeFlow
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x50, 0x0f, 0x00 }); // MassFlow
            OP_MemoLisInit.Add(new byte[] { 0x55, 0x00, 0xff, 0x0C, 0x01, 0x03, 0x22, 0x60, 0x0f, 0x00 }); // EnergyValues


            StationEquipment = new StationEquipmentData(Params);
            MakeCRC((byte)StationEquipment.DeviseAdress);

            if (StationEquipment.ConnectionType == 1)
            {
                MakeCRC(0);
                Thread tem_104 = new Thread(StartAsyncTcp);
                tem_104.Start();
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Tcp соединение запущено: ");

            }
        }

        public override List<DataParameter> StartCom()
        {
            StLogDob = 0;
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            try
            {
                if (!(port.IsOpen)) port.Open(); // Opening port

                ColoredConsole.WriteLine(3, "Звоним на ТЭМ-104 : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                byte[] Initiaze_recive = { };
                byte[] Feature_recive = { };
                byte[] Memo2K_recive = { };
                byte[] OP_Memo_recive = { };

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = ReadProperties(Initiaze_pak, port);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = ReadProperties(Initiaze_pak, port);

                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");

                }
                else
                {
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + StationEquipment.PhoneDialing);
                    return Data;
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                Feature_recive = ReadProperties(Feature_pak, port);

                //  OP_Memo_recive = ReadProperties(OP_Memo_pak, port);
                try
                {
                    string feature = Convert.ToString(GetLongv2(Feature_recive, 6));

                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось преобразовать серийник, устройство ответило неверно или не ответило вообще.");
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + StationEquipment.PhoneDialing);
                    return Data;
                }


                List<byte[]> OP_MemoLisRead = new List<byte[]>();
                foreach (var f in OP_MemoLisInit)
                {

                    byte[] bReaded = ReadProperties(f, port);
                    OP_MemoLisRead.Add(bReaded);

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "ТЭМ-104 " + StationEquipment.PhoneDialing + " : Считано " + (StLogDob += (100.0 / StatusLogNum)));

                }
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "ТЭМ-104 " + StationEquipment.PhoneDialing + " : Считано 100% ");


                AddToDataFunction(Feature_recive, Memo2K_recive, OP_MemoLisRead, OP_Memo_recive);

                //CSD_End(port);
                port.Close();

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание данных с TЭM-104 завершено." + StationEquipment.PhoneDialing);
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + StationEquipment.PhoneDialing);
                ColoredConsole.WriteLine(ex.Message); CSD_End(port); if (port.IsOpen) port.Close(); return Data;
            }
        }


        public override ArcHiveReadedPack ReadArchive()
        {
            string[] Eq0 = StationEquipment.ArchivesParameter.Split('|');
            DateTime d_from0 = DateTime.Parse(Eq0[0]);
            DateTime d_to0 = DateTime.Parse(Eq0[1]);

            /**/
            ArcHiveReadedPack forRet = new ArcHiveReadedPack();


            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.ReadTimeout = 5000;
            port.WriteTimeout = 5000;

            try
            {
                ColoredConsole.WriteLine(3, "Звоним на TEM-104 : " + StationEquipment.PhoneDialing);
                int serverVersion = 0;
                string serial = StationEquipment.PhoneDialing.ToString();
                try
                {
                    if (!(port.IsOpen)) port.Open(); // Открытие порта

                    CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                    byte[] Initiaze_recive = { };
                    byte[] Feature_recive = { };
                    byte[] Memo2K_recive = { };
                    byte[] OP_Memo_recive = { };

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                    Initiaze_recive = ReadProperties(Initiaze_pak, port);

                    if (Initiaze_recive == null)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 2/2 ");
                        Initiaze_recive = ReadProperties(Initiaze_pak, port);
                    }

                    if (Initiaze_recive != null)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");
                    }
                    else
                    {
                        ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + StationEquipment.PhoneDialing);
                        return forRet;
                    }

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");
                    /*
                    Feature_recive = ReadProperties(Feature_pak, port);*/
                }
                catch (Exception ex)
                {
                    CSD_End(port);
                    if (port.IsOpen) port.Close();

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка считывания архивов : " + ex.Message);
                    return forRet;
                }

                forRet.ReadedArchiveType = StationEquipment.ArchivesType;
                int month = 0;
                int day = 0;
                /**/
                /**/
                string[] Eq = StationEquipment.ArchivesParameter.Split('|');
                DateTime d_from = DateTime.Parse(Eq[0]);
                DateTime d_to = DateTime.Parse(Eq[1]);
                //day

                //Узнать время на приборе

                byte[] Timer128b_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x02, 0x02, 0, 0x1a, 0 };
                CRC(Timer128b_pak);

                byte[] Timer128b_rez = ReadProperties(Timer128b_pak, port);


                DateTime d0 = new DateTime(Get_fromBCD(Timer128b_rez[31]) + 2000, Get_fromBCD(Timer128b_rez[30]), Get_fromBCD(Timer128b_rez[29])
                    , Get_fromBCD(Timer128b_rez[26]), Get_fromBCD(Timer128b_rez[24]), Get_fromBCD(Timer128b_rez[22]));
                if (Math.Abs(d0.Subtract(DateTime.Now).TotalMinutes) > 30)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Время в приборе(" + d0.ToString() + ")  отличается от времени на сервере более чем на пол часа");
                }


                while (d_from <= d_to)
                {

                    int month1 = 0;
                    int day1 = 0;
                    int year1 = 0;
                    int hour1 = 0;
                    day1 = d_from.Day;
                    month1 = d_from.Month;
                    year1 = d_from.Year;
                    hour1 = d_from.Hour;

                    ColoredConsole.WriteLine(3, "Попытка считать архив за  : " + d_from.ToString());


                    if (StationEquipment.ArchivesType == 1) d_from = d_from.AddHours(1);
                    if (StationEquipment.ArchivesType == 2) d_from = d_from.AddDays(1);
                    if (StationEquipment.ArchivesType == 3) d_from = d_from.AddMonths(1);


                    byte[] GetIndexByDate_pak = new byte[] { 0x55, 0x00, 0xff, 0x0d, 0x11, 0x05, 0, Get_BCD((byte)hour1), Get_BCD((byte)day1), Get_BCD((byte)month1), Get_BCD((byte)(year1 - 2000)), 0 };
                    CRC(GetIndexByDate_pak);

                    byte[] GetIndexByDate_rez = ReadProperties(GetIndexByDate_pak, port);

                    if (GetIndexByDate_rez[6] == 0xff && GetIndexByDate_rez[7] == 0xff)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Запись за дату не найдена:" + d_from.ToString());
                    }
                    else
                    {
                        List<DataParameter> Data = new List<DataParameter>();

                        byte[] GetFlash_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x03, 0x05, 0xff, 0, GetIndexByDate_rez[6], GetIndexByDate_rez[7], 0, 0 };
                        CRC(GetFlash_pak);
                        byte[] GetFlash_rez = ReadProperties(GetFlash_pak, port);

                        int hour_rez = Get_fromBCD(GetFlash_rez[6]);
                        int day_rez = Get_fromBCD(GetFlash_rez[7]);
                        int month_rez = Get_fromBCD(GetFlash_rez[8]);
                        int year_rez = Get_fromBCD(GetFlash_rez[9]) + 2000;

                        DateTime when = new DateTime(year_rez, month_rez, day_rez, hour_rez, 0, 0);

                        double V1_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x08);
                        double V2_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x08 + 4);
                        double V3_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x08 + 8);
                        double V4_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x08 + 12);

                        double M1_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x18);
                        double M2_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x18 + 4);
                        double M3_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x18 + 8);
                        double M4_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x18 + 12);

                        double E1_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x28);
                        double E2_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x28 + 4);
                        double E3_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x28 + 8);
                        double E4_fraction = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x28 + 12);

                        double V1_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x38);
                        double V2_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x38 + 4);
                        double V3_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x38 + 8);
                        double V4_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x38 + 12);

                        double M1_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x48);
                        double M2_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x48 + 4);
                        double M3_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x48 + 8);
                        double M4_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x48 + 12);

                        double E1_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x58);
                        double E2_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x58 + 4);
                        double E3_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x58 + 8);
                        double E4_whole = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x58 + 12);

                        Data.Add(new DataParameter(serial, 0, 0, 0, 18, when, "Время записи", "Время"));

                        Data.Add(new DataParameter(serial, 0, 0, V1_fraction + V1_whole, 7, when, "V", "м3"));
                        Data.Add(new DataParameter(serial, 0, 0, V2_fraction + V2_whole, 7, when, "V", "м3"));
                        Data.Add(new DataParameter(serial, 0, 0, V3_fraction + V3_whole, 7, when, "V", "м3"));
                        Data.Add(new DataParameter(serial, 0, 0, V4_fraction + V4_whole, 7, when, "V", "м3"));

                        Data.Add(new DataParameter(serial, 0, 0, M1_fraction + M1_whole, 7, when, "M", "т"));
                        Data.Add(new DataParameter(serial, 0, 0, M2_fraction + M2_whole, 7, when, "M", "т"));
                        Data.Add(new DataParameter(serial, 0, 0, M3_fraction + M3_whole, 7, when, "M", "т"));
                        Data.Add(new DataParameter(serial, 0, 0, M4_fraction + M4_whole, 7, when, "M", "т"));

                        Data.Add(new DataParameter(serial, 0, 0, E1_fraction + E1_whole, 7, when, "E", "МВт"));
                        Data.Add(new DataParameter(serial, 0, 0, E2_fraction + E2_whole, 7, when, "E", "МВт"));
                        Data.Add(new DataParameter(serial, 0, 0, E3_fraction + E3_whole, 7, when, "E", "МВт"));
                        Data.Add(new DataParameter(serial, 0, 0, E4_fraction + E4_whole, 7, when, "E", "МВт"));


                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 0), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 2), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 4), 7, when, "t", "°C"));

                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 6), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 8), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 10), 7, when, "t", "°C"));

                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 12), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 14), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 16), 7, when, "t", "°C"));

                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 18), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 20), 7, when, "t", "°C"));
                        Data.Add(new DataParameter(serial, 0, 0, asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0xc8 + 22), 7, when, "t", "°C"));

                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 0], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 1], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 2], 7, when, "p", "МПа"));

                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 3], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 4], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 5], 7, when, "p", "МПа"));

                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 6], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 7], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 8], 7, when, "p", "МПа"));

                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 9], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 10], 7, when, "p", "МПа"));
                        Data.Add(new DataParameter(serial, 0, 0, GetFlash_rez[6 + 0xc8 + 11], 7, when, "p", "МПа"));



                    }



                }




                if (port.IsOpen) CSD_End(port);
                if (port.IsOpen) port.Close();
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка в считывании TEM-104: " + ex.Message);
                try
                {
                    if (port.IsOpen) CSD_End(port);
                }
                catch (Exception)
                {
                }
                if (port.IsOpen) port.Close();
            }
            return forRet;
        }

        public static byte Get_BCD(byte bcd)
        {
            string str = Convert.ToString(bcd);
            int h = Convert.ToInt32(str, 16);
            return (byte)h;
        }

        public static byte Get_fromBCD(byte bcd)
        {
            string str = Convert.ToString(bcd.ToString("x"));
            int h = Convert.ToInt32(str);
            return (byte)h;
        }

        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));

                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);
                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    ColoredConsole.WriteLine("Waiting for a connection tem-104... " + StationEquipment.ServerPort);  // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing.
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("tem-104 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            /*
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);
            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begin to reading data from tem-104...");

                    byte[] Initiaze_recive = { };
                    byte[] Feature_recive = { };
                    byte[] Memo2K_recive = { };
                    byte[] OP_Memo_recive = { };

                    Initiaze_recive = ReadProperties(Initiaze_pak, handler);
                    Feature_recive = ReadProperties(Feature_pak, handler);
                    OP_Memo_recive = ReadProperties(OP_Memo_pak, handler);

                    AddToDataFunction(Feature_recive, OP_Memo_recive, Memo2K_recive);

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime)));
                }
            }*/
            //catch (Exception ex) { ColoredConsole.WriteLine("tem-104 : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); handler.Disconnect(true); handler.Close(); return; }
        }

        private void AddToDataFunction(byte[] Feature_recive, byte[] Memo2K_recive, List<byte[]> OP_MemoLisRead, byte[] OP_Memo_recive)
        {
        dada: string feature = Convert.ToString(GetLongv2(Feature_recive, 6));
            if (feature == "0") return;

            /////
            /*

            uint feature2 = Convert.ToUInt32(GetLongv2(Feature_recive, 6));
            if (feature2 == 0) return;

            double Temperature1 = GetFloat(OP_Memo_recive, 0x00 + 6);
            Data.Add(new DataParameter(feature, 95, 0, Temperature1, StationEquipment.EquipmentTypeId));
            double Temperature2 = GetFloat(OP_Memo_recive, 0x04 + 6);
            Data.Add(new DataParameter(feature, 95, 1, Temperature2, StationEquipment.EquipmentTypeId));
            double Temperature3 = GetFloat(OP_Memo_recive, 0x08 + 6);
            Data.Add(new DataParameter(feature, 95, 2, Temperature3, StationEquipment.EquipmentTypeId));
            double Temperature4 = GetFloat(OP_Memo_recive, 0x0c + 6);
            Data.Add(new DataParameter(feature, 95, 3, Temperature4, StationEquipment.EquipmentTypeId));

            double Pressure1 = GetFloat(OP_Memo_recive, 0x10 + 6);
            Data.Add(new DataParameter(feature, 1, 0, Pressure1, StationEquipment.EquipmentTypeId));
            double Pressure2 = GetFloat(OP_Memo_recive, 0x14 + 6);
            Data.Add(new DataParameter(feature, 1, 1, Pressure2, StationEquipment.EquipmentTypeId));
            double Pressure3 = GetFloat(OP_Memo_recive, 0x18 + 6);
            Data.Add(new DataParameter(feature, 1, 2, Pressure3, StationEquipment.EquipmentTypeId));
            double Pressure4 = GetFloat(OP_Memo_recive, 0x1c + 6);
            Data.Add(new DataParameter(feature, 1, 3, Pressure4, StationEquipment.EquipmentTypeId));

            double DensityCoolant1 = GetFloat(OP_Memo_recive, 0x20 + 6);
            Data.Add(new DataParameter(feature, 11, 0, DensityCoolant1, StationEquipment.EquipmentTypeId));
            double DensityCoolant2 = GetFloat(OP_Memo_recive, 0x24 + 6);
            Data.Add(new DataParameter(feature, 11, 1, DensityCoolant2, StationEquipment.EquipmentTypeId));
            double DensityCoolant3 = GetFloat(OP_Memo_recive, 0x28 + 6);
            Data.Add(new DataParameter(feature, 11, 2, DensityCoolant3, StationEquipment.EquipmentTypeId));
            double DensityCoolant4 = GetFloat(OP_Memo_recive, 0x2c + 6);
            Data.Add(new DataParameter(feature, 11, 3, DensityCoolant4, StationEquipment.EquipmentTypeId));

            double Enthalpy1 = GetFloat(OP_Memo_recive, 0x30 + 6);
            Data.Add(new DataParameter(feature, 87, 0, Enthalpy1, StationEquipment.EquipmentTypeId));
            double Enthalpy2 = GetFloat(OP_Memo_recive, 0x34 + 6);
            Data.Add(new DataParameter(feature, 87, 1, Enthalpy2, StationEquipment.EquipmentTypeId));
            double Enthalpy3 = GetFloat(OP_Memo_recive, 0x38 + 6);
            Data.Add(new DataParameter(feature, 87, 2, Enthalpy3, StationEquipment.EquipmentTypeId));
            double Enthalpy4 = GetFloat(OP_Memo_recive, 0x3c + 6);
            Data.Add(new DataParameter(feature, 87, 3, Enthalpy4, StationEquipment.EquipmentTypeId));

            double VolumeFlow1 = GetFloat(OP_Memo_recive, 0x40 + 6);
            Data.Add(new DataParameter(feature, 94, 0, VolumeFlow1, StationEquipment.EquipmentTypeId));
            double VolumeFlow2 = GetFloat(OP_Memo_recive, 0x44 + 6);
            Data.Add(new DataParameter(feature, 94, 1, VolumeFlow2, StationEquipment.EquipmentTypeId));
            double VolumeFlow3 = GetFloat(OP_Memo_recive, 0x48 + 6);
            Data.Add(new DataParameter(feature, 94, 2, VolumeFlow3, StationEquipment.EquipmentTypeId));
            double VolumeFlow4 = GetFloat(OP_Memo_recive, 0x4c + 6);
            Data.Add(new DataParameter(feature, 94, 3, VolumeFlow4, StationEquipment.EquipmentTypeId));

            double MassFlow1 = GetFloat(OP_Memo_recive, 0x50 + 6);
            Data.Add(new DataParameter(feature, 93, 0, MassFlow1, StationEquipment.EquipmentTypeId));
            double MassFlow2 = GetFloat(OP_Memo_recive, 0x54 + 6);
            Data.Add(new DataParameter(feature, 93, 1, MassFlow2, StationEquipment.EquipmentTypeId));
            double MassFlow3 = GetFloat(OP_Memo_recive, 0x58 + 6);
            Data.Add(new DataParameter(feature, 93, 2, MassFlow3, StationEquipment.EquipmentTypeId));
            double MassFlow4 = GetFloat(OP_Memo_recive, 0x5c + 6);
            Data.Add(new DataParameter(feature, 93, 3, MassFlow4, StationEquipment.EquipmentTypeId));

            double EnergyValues1 = GetFloat(OP_Memo_recive, 0x60 + 6);
            Data.Add(new DataParameter(feature, 13, 0, EnergyValues1, StationEquipment.EquipmentTypeId));
            double EnergyValues2 = GetFloat(OP_Memo_recive, 0x64 + 6);
            Data.Add(new DataParameter(feature, 13, 1, EnergyValues2, StationEquipment.EquipmentTypeId));
            double EnergyValues3 = GetFloat(OP_Memo_recive, 0x68 + 6);
            Data.Add(new DataParameter(feature, 13, 2, EnergyValues3, StationEquipment.EquipmentTypeId));
            double EnergyValues4 = GetFloat(OP_Memo_recive, 0x6c + 6);
            Data.Add(new DataParameter(feature, 13, 3, EnergyValues4, StationEquipment.EquipmentTypeId));
            */
            ////

            try
            {

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Преобразование принятой информации в нужные данные   " + StationEquipment.PhoneDialing);

                for (int i = 0; i < 4; i++)
                {
                    try
                    {
                        double Temperature = GetFloat(OP_MemoLisRead[0], 0 + i * 0x04 + 6);
                        double Pressure = GetFloat(OP_MemoLisRead[1], 0 + i * 0x04 + 6);
                        double DensityCoolant = GetFloat(OP_MemoLisRead[2], 0 + i * 0x04 + 6);
                        double Enthalpy = GetFloat(OP_MemoLisRead[3], 0 + i * 0x04 + 6);
                        double VolumeFlow = GetFloat(OP_MemoLisRead[4], 0 + i * 0x04 + 6);
                        double MassFlow = GetFloat(OP_MemoLisRead[5], 0 + i * 0x04 + 6);
                        double EnergyValues = GetFloat(OP_MemoLisRead[6], 0 + i * 0x04 + 6);

                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, i, Temperature, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, i, Pressure, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 86, i, DensityCoolant, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 87, i, Enthalpy, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, i, VolumeFlow, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, i, MassFlow, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 92, i, EnergyValues, StationEquipment.EquipmentTypeId));
                    }
                    catch (Exception)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Преобразование некоторых данных не удалось   " + StationEquipment.PhoneDialing);

                    }

                }



            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_DEBUG, ex.Message + " " + StationEquipment.PhoneDialing);

            }

            // goto dada;
        }

        private byte[] ReadProperties(byte[] TheParameters, Socket port)
        {
            WritePacket(port, TheParameters);
            Thread.Sleep(200);
            return ReadPacket(port, TheParameters);
        }

        private byte[] ReadProperties(byte[] TheParameters, SerialPort port)
        {
            WritePacket(port, TheParameters);
            Thread.Sleep(2000);
            return ReadPacket(port, TheParameters);
        }

        public override List<DataParameter> Read()
        {
            List<DataParameter> par = new List<DataParameter>(Data);
            if (par.Any())
            {
                Data.Clear();
                return par;
            }
            else
            {
                return Data;
            }
        }

        private void MakeCRC(byte NetworkAddress)
        {
            Initiaze_pak[1] = NetworkAddress;
            Feature_pak[1] = NetworkAddress;
            Memo2K_pak[1] = NetworkAddress;
            OP_Memo_pak[1] = NetworkAddress;

            CRC(Initiaze_pak);
            CRC(Feature_pak);
            CRC(Memo2K_pak);
            CRC(OP_Memo_pak);


            foreach (var f in OP_MemoLisInit)
            {
                CRC(f);
            }
        }

        public static long GetLong(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 1];
            newb[1] = arr[start + 0];
            newb[2] = arr[start + 3];
            newb[3] = arr[start + 2];

            return BitConverter.ToInt32(newb, 0);
        }

        public static float GetFloat(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToSingle(newb, 0);
        }

        public static long GetLongv2(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToInt32(newb, 0);
        }

        public byte[] ReadPacket(SerialPort port, byte[] lastSend)
        {
            try
            {
                byte count = 0;


                byte[] retBytes = ReadPacket(port);
                int crc = Convert.ToInt32(retBytes[retBytes.Length - 1]);
                int crc1 = CRC(retBytes);
                string s = Encoding.ASCII.GetString(retBytes);

                if (crc == crc1)
                {

                    // foreach (var a in retBytes) Console.Write("{0:X} ", a);
                    // ColoredConsole.WriteLine(); 
                    return retBytes;
                }
                return null;
            }
            catch (Exception ex) { ColoredConsole.WriteLine(ex.Message); return null; }
        }

        public byte[] ReadPacket(Socket socket, byte[] lastSend)
        {
            try
            {
                byte count = 0;

                while (count < 1)
                {
                    int byteCount = socket.ReceiveBufferSize;
                    byte[] retBytes = new byte[100];
                    int bytesRead = socket.Receive(retBytes);

                    retBytes = retBytes.Take(bytesRead).ToArray();

                    if (byteCount < 2)
                    {
                        WritePacket(socket, lastSend);
                        count++;
                        continue;
                    }

                    int crc = Convert.ToInt32(retBytes[retBytes.Length - 1]);
                    int crc1 = CRC(retBytes);

                    if (crc == crc1)
                    {
                        foreach (var a in retBytes) Console.Write("{0:X} ", a);
                        ColoredConsole.WriteLine();
                        return retBytes;
                    }
                }
                throw new TimeoutException();
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message); return null;
            }
        }

        private static int CRC(byte[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length - 1; i++) sum += array[i];
            sum = ~sum;
            byte[] a = BitConverter.GetBytes(sum);
            byte Crc = a[0];
            //byte sum1 = Convert.ToByte(sum);//***
            //ColoredConsole.WriteLine("{0:X} ", Crc);
            array[array.Length - 1] = Crc;
            return Crc;
        }
    }
}




/*
                //FIND INDEX

                byte[] Memo2k_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0, 0xf4, 0xff, 0 };
                CRC(Memo2k_pak);

                byte[] Memo2k_rez = ReadProperties(Memo2k_pak, port);
                Int32 index = asBitConverter.ToInt32_HL(Memo2k_rez, 6) - 0x200000-0x100; //текущая запись

                index = 0;

            sft:
                try
                {
                    byte[] index_byte = BitConverter.GetBytes(index).Reverse().ToArray();
                    byte[] GetFlash_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x03, 0x05, 0xff, index_byte[0], index_byte[1], index_byte[2], index_byte[3], 0 };
                    CRC(GetFlash_pak);
                    byte[] GetFlash_rez = ReadProperties(GetFlash_pak, port);

                    int hour_rez = Get_fromBCD(GetFlash_rez[6]);
                    int day_rez = Get_fromBCD(GetFlash_rez[7]);
                    int month_rez = Get_fromBCD(GetFlash_rez[8]);
                    int year_rez = Get_fromBCD(GetFlash_rez[9]) + 2000;

                    DateTime d = new DateTime(year_rez, month_rez, day_rez, hour_rez, 0, 0);*/
/*
int total_diff = (int)d.Subtract(d_from).TotalHours;
index = index - (total_diff * 0x100);

if(index <0) index = 0;*/

/*
Console.WriteLine(d.ToString() + " " + index / 0x100);
index += 0x100;

}
catch (Exception ex)
{
ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, ex.Message);
index += 0x100;
}

goto sft;
*/
//END FIND INDEX