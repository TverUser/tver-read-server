﻿using MasterParent;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MasterParent;
using Server;

namespace TEM106n
{
    public class TEM106 : Driver
    {

        public TEM106(StationEquipmentData param) : base(param)
        {

        }

        private void MakeCRC(byte NetworkAddress)
        {
            Initiaze_pak[1] = NetworkAddress;
            Feature_pak[1] = NetworkAddress;
            OP_Memo_pak[1] = NetworkAddress;

            Initiaze_pak[2] = (byte)(Initiaze_pak[2] - NetworkAddress);
            Feature_pak[2] = (byte)(Feature_pak[2] - NetworkAddress);
            OP_Memo_pak[2] = (byte)(OP_Memo_pak[2] - NetworkAddress);

            CRC.CRC_fromTEM(Initiaze_pak);
            CRC.CRC_fromTEM(Feature_pak);
            CRC.CRC_fromTEM(OP_Memo_pak);


        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {
            MakeCRC((byte)Config.DeviseAdress);
        }

        public override string GetSerialNumber()
        {

            uint serial = 0;
            try
            {
                byte[] Initiaze_recive = { };

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = comm.MessagebyCRC(Initiaze_pak, CommunicateCurrent.CRCtype_fromTEM);
                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");
                }
                else
                {
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с ТЭМ-104 : " + Config.PhoneDialing);
                    return "";
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                byte[] Feature_recive = comm.MessagebyCRC(Feature_pak, CommunicateCurrent.CRCtype_fromTEM);
                serial = asBitConverter.ToUInt32_HL(Feature_recive, 6);

            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("Ошибка считывания серийника: " + ex.Message);
            }
            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }


        public override bool ConfigBeforeReadArchive()
        {
            try
            {

            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            List<List<DataParameter>> ret = new List<List<DataParameter>>();


            byte[] Feature_recive = { };
            byte[] Memo2K_recive = { };
            byte[] OP_Memo_recive = { };


            byte[] GetIndexByDate_pak = new byte[] { 0x55, 0x00, 0xff, 0x0d, 0x11, 0x05, 0, Get_BCD((byte)hour), Get_BCD((byte)day), Get_BCD((byte)month), Get_BCD((byte)(year - 2000)), 0 };
            CRC.CRC_fromTEM(GetIndexByDate_pak);

            byte[] GetIndexByDate_rez = comm.MessagebyCRC(GetIndexByDate_pak, CommunicateCurrent.CRCtype_fromTEM);

            if (GetIndexByDate_rez[6] == 0xff && GetIndexByDate_rez[7] == 0xff)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Запись за дату не найдена:" + new DateTime(year, month, day, hour, 0, 0).ToString());
            }
            else
            {
                List<DataParameter> Data = new List<DataParameter>();

                byte[] GetFlash_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x03, 0x05, 0xff, 0, 0, GetIndexByDate_rez[6], GetIndexByDate_rez[7], 0 };
                CRC.CRC_fromTEM(GetFlash_pak);
                byte[] GetFlash_rez = comm.MessagebyCRC(GetFlash_pak, CommunicateCurrent.CRCtype_fromTEM);

                int hour_rez = Get_fromBCD(GetFlash_rez[6]);
                int day_rez = Get_fromBCD(GetFlash_rez[7]);
                int month_rez = Get_fromBCD(GetFlash_rez[8]);
                int year_rez = Get_fromBCD(GetFlash_rez[9]) + 2000;

                DateTime when = new DateTime(year_rez, month_rez, day_rez, hour_rez, 0, 0);
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, 0, 18, when, "Время записи", "Время"));

                
                double V1_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x04);
                double V2_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x08);
                double V3_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x0c);
                double V4_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x10);
                double V5_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x14);
                double V6_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x18);

                double V1__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x1c);
                double V2__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x20);
                double V3__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x24);
                double V4__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x28);
                double V5__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x2c);
                double V6__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x30);



                double M1_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x34);
                double M2_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x38);
                double M3_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x3c);
                double M4_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x40);
                double M5_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x44);
                double M6_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x48);

                double M1__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x4c);
                double M2__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x50);
                double M3__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x54);
                double M4__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x58);
                double M5__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x5c);
                double M6__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x60);



                double E1_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x64);
                double E2_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x68);
                double E3_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x6c);
                double E4_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x70);
                double E5_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x74);
                double E6_p = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x78);

                double E1__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x7c);
                double E2__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x80);
                double E3__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x84);
                double E4__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x88);
                double E5__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x8c);
                double E6__ = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x90);


                double E6_po = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x94);
                double E1__o = asBitConverter.ToUInt32_HL(GetFlash_rez, 6 + 0x98);




                double T1 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x11e);
                double T2 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x122);
                double T3 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x126);
                double T4 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x12a);
                double T5 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x12e);
                double T6 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x132);
                double T7 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x136);

                 
                double P1 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x13a);
                double P2 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x13e);
                double P3 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x142);
                double P4 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x146);
                double P5 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x14a);
                double P6 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x14e);

                double R1 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x152);
                double R2 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x156);
                double R3 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x15a);
                double R4 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x15e);
                double R5 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x162);
                double R6 = asBitConverter.ToFloat32_HL(GetFlash_rez, 6 + 0x164);



                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V1_p, 7, when, "Промежуточный объем 1", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V2_p, 7, when, "Промежуточный объем 2", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V3_p, 7, when, "Промежуточный объем 3", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V4_p, 7, when, "Промежуточный объем 4", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V5_p, 7, when, "Промежуточный объем 5", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V6_p, 7, when, "Промежуточный объем 6", "м3"));
                                                                                                            
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V1__, 7, when, "Объем 1", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V2__, 7, when, "Объем 2", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V3__, 7, when, "Объем 3", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V4__, 7, when, "Объем 4", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V5__, 7, when, "Объем 5", "м3"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, V6__, 7, when, "Объем 6", "м3"));




                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M1_p, 7, when, "Промежуточная масса 1", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M2_p, 7, when, "Промежуточная масса 2", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M3_p, 7, when, "Промежуточная масса 3", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M4_p, 7, when, "Промежуточная масса 4", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M5_p, 7, when, "Промежуточная масса 5", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M6_p, 7, when, "Промежуточная масса 6", "т"));

                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M1__, 7, when, "Масса 1", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M2__, 7, when, "Масса 2", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M3__, 7, when, "Масса 3", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M4__, 7, when, "Масса 4", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M5__, 7, when, "Масса 5", "т"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M6__, 7, when, "Масса 6", "т"));


                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E1_p, 7, when, "Промежуточная энергия 1", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E2_p, 7, when, "Промежуточная энергия 2", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E3_p, 7, when, "Промежуточная энергия 3", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E4_p, 7, when, "Промежуточная энергия 4", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E5_p, 7, when, "Промежуточная энергия 5", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E6_p, 7, when, "Промежуточная энергия 6", "МВт*ч"));

                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E1__, 7, when, "Потребленная энергия 1", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E2__, 7, when, "Потребленная энергия 2", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E3__, 7, when, "Потребленная энергия 3", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E4__, 7, when, "Потребленная энергия 4", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E5__, 7, when, "Потребленная энергия 5", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, E6__, 7, when, "Потребленная энергия 6", "МВт*ч"));
                

                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M6_p, 7, when, "Общая промежуточная энергия ", "МВт*ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, M1__, 7, when, "Общая потребленная энергия ", "МВт*ч"));



                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T1, 7, when, "Температура 1", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T2, 7, when, "Температура 2", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T3, 7, when, "Температура 3", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T4, 7, when, "Температура 4", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T5, 7, when, "Температура 5", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T6, 7, when, "Температура 6", "ºC"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, T7, 7, when, "Температура 6", "ºC"));
                

                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P1, 7, when, "Давление 1", "МПа"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P2, 7, when, "Давление 2", "МПа"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P3, 7, when, "Давление 3", "МПа"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P4, 7, when, "Давление 4", "МПа"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P5, 7, when, "Давление 5", "МПа"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, P6, 7, when, "Давление 6", "МПа"));
                
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R1, 7, when, "Расход 1", "т/ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R2, 7, when, "Расход 2", "т/ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R3, 7, when, "Расход 3", "т/ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R4, 7, when, "Расход 4", "т/ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R5, 7, when, "Расход 5", "т/ч"));
                Data.Add(new DataParameter(Config.DriverFeatures, 0, 0, R6, 7, when, "Расход 6", "т/ч"));

            }

            return new ArcHiveReadedPack();
        }

        #endregion









        public static byte Get_BCD(byte bcd)
        {
            string str = Convert.ToString(bcd);
            int h = Convert.ToInt32(str, 16);
            return (byte)h;
        }

        public static byte Get_fromBCD(byte bcd)
        {
            string str = Convert.ToString(bcd.ToString("x"));
            int h = Convert.ToInt32(str);
            return (byte)h;
        }


        private List<DataParameter> Data = new List<DataParameter>(); // List that return's from .dll for writing to DB
        private StationEquipmentData StationEquipment; // for intializition
        public ManualResetEvent allDone = new ManualResetEvent(false);
        private byte NetworkNddress;

        byte[] Initiaze_pak = new byte[] { 0x55, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00 };
        byte[] Feature_pak = new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x01, 0x52, 0x04, 0x00 };

        //
        List<byte[]> Memo2k = new List<byte[]>();



        //

        byte[] OP_Memo_pak = new byte[] { 0x55, 0x00, 0xff, 0x0c, 0x01, 0x03, 0x22, 0x00, 0xff, 0x00 };
        /*
        public TEM106(StationEquipmentData Params) : base(Params)
        {
            StatusLogNum = 12;

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xfa, 6 * 0x04, 0 }); //comma

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x00, 7 * 0x04, 0 }); //t_n
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x34, 7 * 0x04, 0 }); //p_n

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x88, 6 * 0x04, 0 }); //rashod_v
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xa0, 6 * 0x04, 0 }); //rashod_m
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0xd0, 6 * 0x04, 0 }); //freqan_v

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x18, 6 * 0x04, 0 }); //volume
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x48, 6 * 0x04, 0 }); //mass
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x78, 6 * 0x04, 0 }); //energy

            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x00, 6 * 0x04, 0 }); //lvolume
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x03, 0x30, 6 * 0x04, 0 }); //lmass
            Memo2k.Add(new byte[] { 0x55, 0x00, 0xff, 0x0f, 0x01, 0x03, 0x02, 0x60, 6 * 0x04, 0 }); //lenergy


            
        //   double rashod_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x88);
        //  double rashod_m = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xa0);
        //   double freqan_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xd0);
        //    double volume = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x18);
        //  double mass = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x30);
        //    double energy = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x78);
        //   double comma = GetChar(Memo2K_recive2, i * 0x04 + ups + 0xfa);
        //   Data.Add(new DataParameter(feature, 95, i, comma, StationEquipment.EquipmentTypeId));
        //
        // }
        //
        //   double t_n = GetFloat(Memo2K_recive2, i * 0x04 + ups);
        //    double p_n = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x34);
        
            StationEquipment = new StationEquipmentData(Params);
            MakeCRC();

            if (StationEquipment.ConnectionType == 1)
            {
                Thread tem_106 = new Thread(StartAsyncTcp);
                tem_106.Start();
                ColoredConsole.WriteLine("Tcp Start");
            }
        }



        public override List<DataParameter> StartCom()
        {
            StLogDob = 0;
            SerialPort port;

            port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;
            port.WriteTimeout = 2000;
            port.ReadTimeout = 2000;

            try
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Звоним на ТЭМ-106 : " + StationEquipment.PhoneDialing);

                if (!(port.IsOpen)) port.Open(); // Opening port

                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                byte[] Initiaze_recive = { };
                byte[] Feature_recive = { };
                byte[] OP_Memo_recive = { };

               
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка соединения с устройством 1/2 ");
                Initiaze_recive = ReadProperties(Initiaze_pak, port);

                if (Initiaze_recive == null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_WARNING, "Попытка соединения с устройством 2/2 ");
                    Initiaze_recive = ReadProperties(Initiaze_pak, port);

                }

                if (Initiaze_recive != null)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Как минимум устройство ответило ");

                }
                else
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Попытка считать TEM-106 завершилась неудачно");

                    return Data;
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Попытка считать серийник ");

                Feature_recive = ReadProperties(Feature_pak, port);

                
                // Feature_recive = ReadProperties(Feature_pak, port);
                // OP_Memo_recive = ReadProperties(OP_Memo_pak, port);


                List<byte[]> Memo2k_read = new List<byte[]>();

                string feature = Convert.ToString(GetLongv2(Feature_recive, 6));

                foreach (var f in Memo2k)
                {
                    byte[] bReaded = ReadProperties(f, port);

                    if (bReaded == null)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_WARNING, "Повторная (одна) попытка считать тот же набор данных" + (StLogDob));
                        bReaded = ReadProperties(f, port);
                    }

                    Memo2k_read.Add(bReaded);

                    ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, StartLogMessage + " TEM-106 " + (StLogDob += (100.0 / StatusLogNum)) + EndLogMessage);

                }

                AddToDataFunction(Memo2k_read, feature);

                CSD_End(port);
                port.Close();

                ColoredConsole.WriteLine("Data is reading succesfully  from TEM-106 " + StationEquipment.PhoneDialing);
                return Data;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                CSD_End(port); if (port.IsOpen)
                    port.Close();
                return Data;
            }
        }


        */



        private void AddToDataFunction(List<byte[]> Memo2k_read, string feature)
        {
            int ups = 6;


            //  while (true)
            {
                ups = 6;
                try
                {
                    //string feature = "1323602"; 


                    List<int> commaS = new List<int>();

                    List<long> ar_volume = new List<long>();
                    List<long> ar_mass = new List<long>();
                    List<long> ar_energy = new List<long>();

                    List<double> ar_lvolume = new List<double>();
                    List<double> ar_lmass = new List<double>();
                    List<double> ar_lenergy = new List<double>();


                    for (int i = 0; i < Memo2k_read.Count; i++)
                    {
                        try
                        {

                        }
                        catch (Exception)
                        {

                        }
                        byte[] red = Memo2k_read.ElementAt(i);

                        try
                        {
                            if (i == 0)
                                for (int j = 0; j < 6; j++)
                                {
                                    int comma = GetChar(red, j * 0x04 + ups);
                                    commaS.Add(comma);
                                }
                        }
                        catch (Exception)
                        {

                        }


                        try
                        {
                            if (i == 1)
                                for (int j = 0; j < 7; j++)
                                {
                                    double t_n = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 95, j, t_n, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {

                        }

                        try
                        {
                            if (i == 2)
                                for (int j = 0; j < 7; j++)
                                {
                                    double p_n = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 1, j, p_n, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 3)
                                for (int j = 0; j < 6; j++)
                                {
                                    double rashod_v = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 94, j, rashod_v, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 4)
                                for (int j = 0; j < 6; j++)
                                {
                                    double rashod_m = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 93, j, rashod_m, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 5)
                                for (int j = 0; j < 6; j++)
                                {
                                    double freqan_v = GetFloat(red, j * 0x04 + ups);
                                    Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 8, j, freqan_v, StationEquipment.EquipmentTypeId));

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 6)
                                for (int j = 0; j < 6; j++)
                                {
                                    long volume = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_volume.Add(volume);
                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 7)
                                for (int j = 0; j < 6; j++)
                                {
                                    long mass = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_mass.Add(mass);

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 8)
                                for (int j = 0; j < 6; j++)
                                {
                                    long energy = GetLong(red, j * 0x04 + ups) / 0xffff;
                                    ar_energy.Add(energy);

                                }
                        }
                        catch (Exception)
                        {


                        }


                        try
                        {
                            if (i == 9)
                                for (int j = 0; j < 6; j++)
                                {
                                    double volume = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lvolume.Add(volume);
                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 10)
                                for (int j = 0; j < 6; j++)
                                {
                                    double mass = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lmass.Add(mass);

                                }
                        }
                        catch (Exception)
                        {


                        }

                        try
                        {
                            if (i == 11)
                                for (int j = 0; j < 6; j++)
                                {
                                    double energy = GetFloat(red, j * 0x04 + ups) / 0xffff;
                                    ar_lenergy.Add(energy);

                                }
                        }
                        catch (Exception)
                        {


                        }


                    }

                    for (int i = 0; i < Memo2k_read.Count; i++)
                    {
                        if (commaS[i] == 6) commaS[i] = 100000;
                        else
                        if (commaS[i] == 5) commaS[i] = 10000;
                        else
                        if (commaS[i] == 4) commaS[i] = 1000;
                        else
                        if (commaS[i] == 3) commaS[i] = 100;
                        else
                        if (commaS[i] == 2) commaS[i] = 10; else commaS[i] = 1;

                        double volume = (ar_volume[i] + ar_lvolume[i]) / commaS[i];
                        double mass = (ar_mass[i] + ar_lmass[i]) / commaS[i];
                        double energy = (ar_energy[i] + ar_lenergy[i]) / commaS[i];

                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 91, i, volume, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 90, i, mass, StationEquipment.EquipmentTypeId));
                        Data.Add(new DataParameter(StationEquipment.StationEquipmentId.ToString(), 92, i, energy, StationEquipment.EquipmentTypeId));


                    }

                    for (int i = 0; i < Data.Count; i++)
                    {
                        ColoredConsole.WriteLine(Data.ElementAt(i).value + "    " + Data.ElementAt(i).obisCode + "    ");
                    }

                    /*
                    if (feature == 0) return;

                    int ups = 5;
                    for (int i = 0; i < 6; i++)
                    {
                        ups = 5;
                        if (i < 7)
                        {
                            double rashod_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x88);
                            Data.Add(new DataParameter(feature, 95, i, rashod_v, StationEquipment.EquipmentTypeId));

                            double rashod_m = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xa0);
                            Data.Add(new DataParameter(feature, 95, i, rashod_m, StationEquipment.EquipmentTypeId));

                            double freqan_v = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0xd0);
                            Data.Add(new DataParameter(feature, 95, i, freqan_v, StationEquipment.EquipmentTypeId));

                            double volume = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x18);
                            Data.Add(new DataParameter(feature, 95, i, volume, StationEquipment.EquipmentTypeId));

                            double mass = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x30);
                            Data.Add(new DataParameter(feature, 95, i, mass, StationEquipment.EquipmentTypeId));
                            //
                            double energy = GetLong(Memo2K_recive3, i * 0x04 + ups + 0x78);
                            Data.Add(new DataParameter(feature, 95, i, energy, StationEquipment.EquipmentTypeId));


                            double comma = GetChar(Memo2K_recive2, i * 0x04 + ups + 0xfa);
                            Data.Add(new DataParameter(feature, 95, i, comma, StationEquipment.EquipmentTypeId));

                        }

                        double t_n = GetFloat(Memo2K_recive2, i * 0x04 + ups);
                        Data.Add(new DataParameter(feature, 95, i, t_n, StationEquipment.EquipmentTypeId));

                        double p_n = GetFloat(Memo2K_recive2, i * 0x04 + ups + 0x34);
                        Data.Add(new DataParameter(feature, 95, i, p_n, StationEquipment.EquipmentTypeId));



                        ColoredConsole.WriteLine("START TRYS");
                        for (int j = 0; j < Memo2K_recive2.Length - 5; j++)
                        {
                            double trry = GetLong(Memo2K_recive2, j);
                            if (trry>1 && trry<10000000000000000000)
                            {
                                ColoredConsole.WriteLine(trry +" | "+j);

                            }
                        }
                        ColoredConsole.WriteLine("ENDENDENDENFED TRYS");
                        */

                }


                catch (Exception ex)
                {

                }
            }
        }

        private void MakeCRC()
        {
            NetworkNddress = 0;

            CRC.CRC_fromTEM(Initiaze_pak);
            CRC.CRC_fromTEM(Feature_pak);

            foreach (var f in Memo2k)
            {
                CRC.CRC_fromTEM(f);
            }
        }

        public static long GetLong(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 1];
            newb[1] = arr[start + 0];
            newb[2] = arr[start + 3];
            newb[3] = arr[start + 2];

            return BitConverter.ToInt32(newb, 0);
        }

        public static float GetFloat(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToSingle(newb, 0);
        }

        public static int GetChar(byte[] arr, int start)
        {
            int cg = (int)arr[start];
            return cg;
        }

        public static long GetLongv2(byte[] arr, int start)
        {
            byte[] newb = new byte[4];
            newb[0] = arr[start + 3];
            newb[1] = arr[start + 2];
            newb[2] = arr[start + 1];
            newb[3] = arr[start + 0];

            return BitConverter.ToInt32(newb, 0);
        }

    }

}
