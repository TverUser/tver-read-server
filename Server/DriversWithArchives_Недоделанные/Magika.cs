﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO.Ports;

namespace Server
{
    public class Magika : Driver
    {
        private List<DataParameter> Data = new List<DataParameter>(); // Список параметров передающийся на сервер
        private StationEquipmentData StationEquipment; // Параметры для инициализации
        public ManualResetEvent allDone = new ManualResetEvent(false);

        byte[] getSerial =    { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, 0x01, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00 };
        byte[] getAllFirst =  { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, 0x01, 0x04, 0x00, 100, 0x00, 88, 0x00, 0x00 };
        byte[] getAllSecond = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, 0x01, 0x04, 0x01, 0x91, 0x00, 40, 0x00, 0x00 };

        #region trash
        string[] baseTemplates =
        {
            "DATE",
            "HOUR",
            "M1,",
            "M2,",
            "M3,",
            "M4,",
            "M5,",
            "M6,",
            "Q1,,",
            "Q2,",
            "Q13",
            "T1",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "P1",
            "P2",
            "P3",
            "P4",
            "P5",
            "P6",
            "uM1",
            "uM2",
            "uM3",
            "pM1",
            "pM2",
            "pM3",
            "TNORM1,",
            "TNORM2,",
            "TNORM3,",
            "TMIN1",
            "TMIN2",
            "TMIN3",
            "TMAX1",
            "TMAX2",
            "TMAX3",
            "TDELTAT1",
            "TDELTAT2",
            "TDELTAT3",
            "ERRORCODE1",
            "ERRORCODE2",
            "ERRORCODE3",
            "TNORM",
            "TMIN",
            "TMAX",
            "TDELTAT"
        };
        #endregion

        // 122 registers = 244 bytes
        // all = 256 bytes

        public Magika(StationEquipmentData param)
            : base(param)
        {
            StatusLogNum = 8;
            StationEquipment = new StationEquipmentData(param);
            CalculateCRC();

            if (StationEquipment.CommunicationChannel == 1)
            {
                Thread Magica = new Thread(StartAsyncTcp);
                Magica.Start();
                ColoredConsole.WriteLine("Tcp Start");
            }
        }

        public override List<DataParameter> StartCom()
        {
            StLogDob = 0;

            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;

            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;

            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                ColoredConsole.WriteLine(3, "Звоним на Магика : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);

                string serialNumber = ReadSeriaNumber(port);

                if (serialNumber == String.Empty)
                    throw new Exception("Magika serial number not readed");

                WritePacket(port, getAllFirst);
                byte[] firstPacket = ReadPacket(port, getAllFirst);

                WritePacket(port, getAllSecond);
                byte[] secondPacket = ReadPacket(port, getAllSecond);

                if (firstPacket.Length > 0 && secondPacket.Length > 0)
                    ConvertData(serialNumber.ToString(), firstPacket, secondPacket);

                if (!(Data.Any()))
                    ColoredConsole.WriteLine(3, "Не удалось считать данные с Магика : " + StationEquipment.PhoneDialing);
                else
                    ColoredConsole.WriteLine(3, "Считывание данных с Магика : " + StationEquipment.PhoneDialing + " завершено.");
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(3, "Не удалось считать данные с Магика : " + StationEquipment.PhoneDialing);
            }

            CSD_End(port);
            port.Close();
            return Data;
        }

        public void ConvertData(String serialNumber, byte[] firstPacket, byte[] secondPacket)
        {
            try
            {
                byte[] VolumeQuantity = firstPacket.Skip(6).Take(40).ToArray();
                byte[] MassiveQuantity = firstPacket.Skip(46).Take(40).ToArray();
                byte[] Temperature = firstPacket.Skip(86).Take(40).ToArray();
                byte[] HeatPower = firstPacket.Skip(126).Take(16).ToArray();
                byte[] Pressure = firstPacket.Skip(142).Take(40).ToArray();
                byte[] Weight = secondPacket.Skip(6).Take(40).ToArray();
                byte[] Energy = secondPacket.Skip(46).Take(16).ToArray();
                byte[] Time = secondPacket.Skip(62).Take(24).ToArray();

                for (int i = 0; i < 10; i++)
                {
                    byte[] volumeVal = VolumeQuantity.Skip(i * 4).Take(4).Reverse().ToArray();
                    byte[] massiveVal = MassiveQuantity.Skip(i * 4).Take(4).Reverse().ToArray();
                    byte[] pressureVal = Pressure.Skip(i * 4).Take(4).Reverse().ToArray();
                    byte[] temperatureVal = Temperature.Skip(i * 4).Take(4).Reverse().ToArray();
                    byte[] weightVal = Weight.Skip(i * 4).Take(4).Reverse().ToArray();

                    Data.Add(new DataParameter(serialNumber.ToString(), 94, i, Convert.ToDouble(BitConverter.ToSingle(massiveVal, 0)), StationEquipment.EquipmentTypeId));
                    Data.Add(new DataParameter(serialNumber.ToString(), 93, i, Convert.ToDouble(BitConverter.ToSingle(volumeVal, 0)), StationEquipment.EquipmentTypeId));
                    Data.Add(new DataParameter(serialNumber.ToString(), 95, i, Convert.ToDouble(BitConverter.ToSingle(temperatureVal, 0)), StationEquipment.EquipmentTypeId));
                    Data.Add(new DataParameter(serialNumber.ToString(), 1, i, Convert.ToDouble(BitConverter.ToSingle(pressureVal, 0)), StationEquipment.EquipmentTypeId));
                    Data.Add(new DataParameter(serialNumber.ToString(), 90, i, Convert.ToDouble(BitConverter.ToSingle(weightVal, 0)), StationEquipment.EquipmentTypeId));
                }

                for (int i = 0; i < 4; i++)
                {
                    byte[] heatVal = HeatPower.Skip(i * 4).Take(4).Reverse().ToArray();
                    byte[] energyVal = Energy.Skip(i * 4).Take(4).Reverse().ToArray();
                    Data.Add(new DataParameter(serialNumber.ToString(), 12, i, Convert.ToDouble(BitConverter.ToSingle(heatVal, 0)), StationEquipment.EquipmentTypeId));
                    Data.Add(new DataParameter(serialNumber.ToString(), 13, i, Convert.ToDouble(BitConverter.ToSingle(energyVal, 0)), StationEquipment.EquipmentTypeId));
                }

                for (int i = 0; i < 6; i++)
                {

                    byte[] timeVal = Time.Skip(i * 4).Take(4).Reverse().ToArray();
                    Data.Add(new DataParameter(serialNumber.ToString(), 10, i, Convert.ToDouble(BitConverter.ToSingle(timeVal, 0)), StationEquipment.EquipmentTypeId));
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }


        public override ArcHiveReadedPack ReadArchive()
        {
            SerialPort port = new SerialPort(StationEquipment.Com_Name, Convert.ToInt32(StationEquipment.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;

            port.DtrEnable = true; // Setting handshake
            port.RtsEnable = true;

            port.WriteTimeout = 5000;
            port.ReadTimeout = 5000;

            ColoredConsole.WriteLine("Begining to read data from Magika...");

            try
            {
                if (!(port.IsOpen)) port.Open(); // Открытие порта

                ColoredConsole.WriteLine(3, "Звоним на Магика : " + StationEquipment.PhoneDialing);
                CSD(port, StationEquipment.PhoneDialing, StationEquipment.CSDType);
                
                string serialNumber = ReadSeriaNumber(port);

                if (serialNumber == String.Empty)
                    throw new Exception("Magika serial number not readed");

                CRC16(getAllFirst, getAllFirst.Length);
                WritePacket(port, getAllFirst);
                byte[] firstPacket = ReadPacket(port, getAllFirst);

                CRC16(getAllSecond, getAllSecond.Length);
                WritePacket(port, getAllSecond);
                byte[] secondPacket = ReadPacket(port, getAllSecond);

                string[] Eq = StationEquipment.ArchivesParameter.Split('|');
                DateTime d_from = DateTime.Parse(Eq[0]);
                DateTime d_to = DateTime.Parse(Eq[1]);

                while (d_from < d_to)
                {
                    int month1 = 0;
                    int day1 = 0;
                    int year1 = 0;
                    int hour1 = 0;
                    day1 = d_from.Day;
                    month1 = d_from.Month;
                    year1 = d_from.Year;
                    hour1 = d_from.Hour;

                    ColoredConsole.WriteLine(3, "Попытка считать архив за  : " + d_from.ToString());

                    List<List<DataParameter>> readed = new List<List<DataParameter>>();

                    
                    ReadArchivesHourly(port, serialNumber, day1, month1, year1, 0);
                    //readed = ReadHourlyArchive(port, serialNumber, day1, month1, year1, 0);

                    if (StationEquipment.ArchivesType == 1) d_from = d_from.AddDays(1); //хоть запрос считать за час, мне удобней считать за весь день!!!
                    if (StationEquipment.ArchivesType == 2) d_from = d_from.AddDays(1);
                    if (StationEquipment.ArchivesType == 3) d_from = d_from.AddMonths(1);
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message);
                ColoredConsole.WriteLine(3, "Не удалось считать данные с Магика : " + StationEquipment.PhoneDialing);
            }

            CSD_End(port);
            if (port.IsOpen) port.Close();
            return base.ReadArchive();
        }


        public void ReadArchivesDaily(SerialPort port, String serialNumber, int day, int month, int year)
        {
            byte[] receive;

            // Step 1
            // request = { ‘$’,’#’,длина пакета MODBUS в байтах без CRC, Адрес устройства, 0x06, регистр старший байт, регистр младший байт, 0, 1, CRC1, CRC2 }
            byte[] archiveRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 1, 0x00, 0x00 };
            byte[] requestForTemplateLength = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };

            CRC16(archiveRequest, archiveRequest.Length);
            CRC16(requestForTemplateLength, requestForTemplateLength.Length);

            WritePacket(port, archiveRequest);
            receive = ReadPacket(port);

            WritePacket(port, requestForTemplateLength);
            receive = ReadPacket(port);

            short length = Convert.ToInt16(receive[6] << 8 | receive[7]);
            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            byte high = Convert.ToByte((length & 0xFF00) >> 8);
            byte low = Convert.ToByte(length & 0x00FF);

            byte[] readTemplateRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
            CRC16(readTemplateRequest, readTemplateRequest.Length);

            if (length > 250)
            {
                byte[] readTemplateOddRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x04, 0x67, high, low, 0x00, 0x00 };
                CRC16(readTemplateOddRequest, readTemplateOddRequest.Length);

                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port);

                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port);
            }
            else
            {
                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port);

                String template = Encoding.ASCII.GetString(receive);
            }

            // Step 2
            byte[] findArchiveStringRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 10, 0x00, 0x00 };

            byte[] readArchiveStringRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xEA, Convert.ToByte(day), Convert.ToByte(month), 0x00, 0x00 };

            CRC16(findArchiveStringRequest, findArchiveStringRequest.Length);
            CRC16(readArchiveStringRequest, readArchiveStringRequest.Length);

            WritePacket(port, findArchiveStringRequest);
            receive = ReadPacket(port);

            int status = 0;
            while (status == 0)
            {
                byte[] readStatusRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };
                CRC16(readStatusRequest, readStatusRequest.Length);

                WritePacket(port, readStatusRequest);
                receive = ReadPacket(port);

                status = receive[5];

                switch (status)
                {
                    case 1:
                        status = 0;
                        break;
                    default:
                        Console.WriteLine("Error in finding archive string");
                        break;
                }
            }

            byte[] readArchiveStringInitRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 2, 0x00, 0x00 };
            byte[] readArchiveStringLengthRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };

            CRC16(readArchiveStringInitRequest, readArchiveStringInitRequest.Length);
            CRC16(readArchiveStringLengthRequest, readArchiveStringLengthRequest.Length);

            WritePacket(port, readArchiveStringInitRequest);
            receive = ReadPacket(port);

            WritePacket(port, readArchiveStringLengthRequest);
            receive = ReadPacket(port);

            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            high = Convert.ToByte((length & 0xFF00) >> 8);
            low = Convert.ToByte(length & 0x00FF);

            if (length == 0)
                return;

            byte[] readArchiveString = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
            CRC16(readArchiveString, readArchiveString.Length);

            List<byte> dataBytes = new List<byte>();

            if (length > 250)
            {
                WritePacket(port, readArchiveString);
                receive = ReadPacket(port);

                foreach (var a in receive)
                    dataBytes.Add(a);

                length -= 250;
            }

            // Step 3
            byte[] endReadArchive = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 4, 0x00, 0x00 };
            CRC16(endReadArchive, endReadArchive.Length);

            WritePacket(port, endReadArchive);
            receive = ReadPacket(port);
        }

        public string[] ReadTemlate(SerialPort port)
        {
            byte[] receive;

            //Step 1
            byte[] archiveRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 5, 0x00, 0x00 };
            byte[] archiveTemplateLengthRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };

            CRC16(archiveRequest, archiveRequest.Length);
            CRC16(archiveTemplateLengthRequest, archiveTemplateLengthRequest.Length);

            WritePacket(port, archiveRequest);
            receive = ReadPacket(port, archiveRequest);

            WritePacket(port, archiveTemplateLengthRequest);
            receive = ReadPacket(port, archiveTemplateLengthRequest);

            short length = Convert.ToInt16(receive[6] << 8 | receive[7]);
            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            byte high = Convert.ToByte((length & 0xFF00) >> 8);
            byte low = Convert.ToByte(length & 0x00FF);

            byte[] readTemplateRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
            CRC16(readTemplateRequest, readTemplateRequest.Length);

            String[] templates;

            if (length > 250)
            {
                byte[] readTemplateOddRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x04, 0x67, high, low, 0x00, 0x00 };
                CRC16(readTemplateOddRequest, readTemplateOddRequest.Length);

                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port, readTemplateRequest);
                String template = Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());

                WritePacket(port, readTemplateOddRequest);
                receive = ReadPacket(port, readTemplateOddRequest);
                template += Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());
                templates = template.Split(' ');
            }
            else
            {
                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port, readTemplateRequest);

                String template = Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());
                templates = template.Split(' ');
            }

            return templates;
        }

        public int findArchiveStringString(SerialPort port, int day, int month, int year, int hour)
        {
            byte[] receive;
            //Step 2
            byte[] findStringRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 11, 0x00, 0x00 };
            CRC16(findStringRequest, findStringRequest.Length);

            WritePacket(port, findStringRequest);
            receive = ReadPacket(port);

            byte[] monthDayrequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xEA, Convert.ToByte(day), Convert.ToByte(month), 0x00, 0x00 };
            byte[] hourYear = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xEA, Convert.ToByte(year - 2000), Convert.ToByte(hour), 0x00, 0x00 };

            CRC16(monthDayrequest, monthDayrequest.Length);
            CRC16(hourYear, hourYear.Length);

            WritePacket(port, monthDayrequest);
            receive = ReadPacket(port);

            WritePacket(port, hourYear);
            receive = ReadPacket(port);

            int status = 0;
            while (status == 0)
            {
                byte[] readStatusRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };
                CRC16(readStatusRequest, readStatusRequest.Length);

                WritePacket(port, readStatusRequest);
                receive = ReadPacket(port);

                status = receive[5];

                switch (status)
                {
                    case 1:
                        status = 0;
                        return 1;
                    case 2:
                        //status = 0;
                        break;
                    default:
                        Console.WriteLine("Error in finding archive string");
                        break;
                        //// return;
                }
            }

            return status;
        }

        public List<DataParameter> ReadArchiveString(SerialPort port, String serialNumber, String[] templates, int year)
        {
            byte[] receive;
            int length = 0;
            byte high = 0, low = 0;

            byte[] readInitRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 6, 0x00, 0x00 };
            CRC16(readInitRequest, readInitRequest.Length);

            WritePacket(port, readInitRequest);
            receive = ReadPacket(port);

            byte[] readArchiveStringLength = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };
            CRC16(readArchiveStringLength, readArchiveStringLength.Length);

            WritePacket(port, readArchiveStringLength);
            receive = ReadPacket(port);

            length = Convert.ToInt16(receive[6] << 8 | receive[7]);
            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            List<byte> dataBytes = new List<byte>();

            while (length > 0)
            {
                if (length > 250)
                {
                    int tmpLengt = 250;

                    high = Convert.ToByte((tmpLengt & 0xFF00) >> 8);
                    low = Convert.ToByte(tmpLengt & 0x00FF);

                    byte[] readArchiveString = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
                    CRC16(readArchiveString, readArchiveString.Length);

                    WritePacket(port, readArchiveString);
                    receive = ReadPacket(port);

                    foreach (var a in receive)
                        dataBytes.Add(a);

                    length -= 250;
                }
                else
                {
                    int tmpLengt = length;

                    high = Convert.ToByte((tmpLengt & 0xFF00) >> 8);
                    low = Convert.ToByte(tmpLengt & 0x00FF);

                    byte[] readArchiveString = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
                    CRC16(readArchiveString, readArchiveString.Length);

                    WritePacket(port, readArchiveString);
                    receive = ReadPacket(port);

                    foreach (var a in receive)
                        dataBytes.Add(a);

                    length = 0;
                }
            }

            return ConvertArchives(templates, dataBytes.Skip(6).Take(dataBytes.Count - 8).ToArray(), serialNumber, year);
        }

        public void EndRead(SerialPort port)
        {
            byte[] receive;

            // Step 3
            byte[] endReadArchive = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 8, 0x00, 0x00 };
            CRC16(endReadArchive, endReadArchive.Length);

            WritePacket(port, endReadArchive);
            receive = ReadPacket(port);
        }

        public List<List<DataParameter>> ReadHourlyArchive(SerialPort port, String serialNumber, int day, int month, int year, int hour)
        {
            string[] templates = ReadTemlate(port);

            int status = findArchiveStringString(port, day, month, year, hour);
            List<List<DataParameter>> listOfList = new List<List<DataParameter>>();

            if (status == 2)
            {

                for (int i = 0; i < 24; i++)
                {
                    try
                    {
                        listOfList.Add(ReadArchiveString(port, serialNumber, templates, year));
                        ColoredConsole.WriteLine(3, "Магика " + serialNumber + " считано архив за " + listOfList[i][0].DateCreate);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            else
            {
                ColoredConsole.WriteLine(3, "Магика " + StationEquipment.DriverFeatures + " не удалось найти архив за указаную дату");
                return null;
            }

            EndRead(port);
            return listOfList;
        }

        public void ReadArchivesHourly(SerialPort port, String serialNumber, int day, int month, int year, int hour)
        {
            byte[] receive;

            //Step 1
            byte[] archiveRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 5, 0x00, 0x00 };
            byte[] archiveTemplateLengthRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };

            CRC16(archiveRequest, archiveRequest.Length);
            CRC16(archiveTemplateLengthRequest, archiveTemplateLengthRequest.Length);

            WritePacket(port, archiveRequest);
            receive = ReadPacket(port, archiveRequest);

            WritePacket(port, archiveTemplateLengthRequest);
            receive = ReadPacket(port, archiveTemplateLengthRequest);

            short length = Convert.ToInt16(receive[6] << 8 | receive[7]);
            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            byte high = Convert.ToByte((length & 0xFF00) >> 8);
            byte low = Convert.ToByte(length & 0x00FF);

            byte[] readTemplateRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
            CRC16(readTemplateRequest, readTemplateRequest.Length);

            String[] templates;

            if (length > 250)
            {
                byte[] readTemplateOddRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x04, 0x67, high, low, 0x00, 0x00 };
                CRC16(readTemplateOddRequest, readTemplateOddRequest.Length);

                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port, readTemplateRequest);
                String template = Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());

                WritePacket(port, readTemplateOddRequest);
                receive = ReadPacket(port, readTemplateOddRequest);
                template += Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());
                templates = template.Split(' ');
            }
            else
            {
                WritePacket(port, readTemplateRequest);
                receive = ReadPacket(port, readTemplateRequest);

                String template = Encoding.ASCII.GetString(receive.Skip(6).Take(receive.Length - 8).ToArray());
                templates = template.Split(' ');
            }

            //Step 2
            byte[] findStringRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 11, 0x00, 0x00 };
            CRC16(findStringRequest, findStringRequest.Length);

            WritePacket(port, findStringRequest);
            receive = ReadPacket(port);

            byte[] monthDayrequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xEA, Convert.ToByte(day), Convert.ToByte(month), 0x00, 0x00 };
            byte[] hourYear = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xEA, Convert.ToByte(year - 2000), Convert.ToByte(hour), 0x00, 0x00 };

            CRC16(monthDayrequest, monthDayrequest.Length);
            CRC16(hourYear, hourYear.Length);

            WritePacket(port, monthDayrequest);
            receive = ReadPacket(port);

            WritePacket(port, hourYear);
            receive = ReadPacket(port);

            int status = 0;
            while (status == 0)
            {
                byte[] readStatusRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };
                CRC16(readStatusRequest, readStatusRequest.Length);

                WritePacket(port, readStatusRequest);
                receive = ReadPacket(port);

                status = receive[5];

                switch (status)
                {
                    case 1:
                        status = 0;
                        return;
                    case 2:
                        //status = 0;
                        break;
                    default:
                        Console.WriteLine("Error in finding archive string");
                        break;
                        //// return;
                }
            }

            byte[] readInitRequest = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 6, 0x00, 0x00 };
            CRC16(readInitRequest, readInitRequest.Length);

            WritePacket(port, readInitRequest);
            receive = ReadPacket(port);

            byte[] readArchiveStringLength = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xE9, 0, 1, 0x00, 0x00 };
            CRC16(readArchiveStringLength, readArchiveStringLength.Length);

            WritePacket(port, readArchiveStringLength);
            receive = ReadPacket(port);

            length = Convert.ToInt16(receive[6] << 8 | receive[7]);
            length = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(length) / 2));

            List<byte> dataBytes = new List<byte>();

            while (length > 0)
            {
                if (length > 250)
                {
                    int tmpLengt = 250;

                    high = Convert.ToByte((tmpLengt & 0xFF00) >> 8);
                    low = Convert.ToByte(tmpLengt & 0x00FF);

                    byte[] readArchiveString = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
                    CRC16(readArchiveString, readArchiveString.Length);

                    WritePacket(port, readArchiveString);
                    receive = ReadPacket(port);

                    foreach (var a in receive)
                        dataBytes.Add(a);

                    length -= 250;
                }
                else
                {
                    int tmpLengt = length;

                    high = Convert.ToByte((tmpLengt & 0xFF00) >> 8);
                    low = Convert.ToByte(tmpLengt & 0x00FF);

                    byte[] readArchiveString = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x04, 0x03, 0xEA, high, low, 0x00, 0x00 };
                    CRC16(readArchiveString, readArchiveString.Length);

                    WritePacket(port, readArchiveString);
                    receive = ReadPacket(port);

                    foreach (var a in receive)
                        dataBytes.Add(a);

                    length = 0;
                }
            }

            ConvertArchives(templates, dataBytes.Skip(6).Take(dataBytes.Count - 8).ToArray(), serialNumber, year);

            // Step 3
            byte[] endReadArchive = { Convert.ToByte('$'), Convert.ToByte('#'), 0x06, Convert.ToByte(StationEquipment.DeviseAdress), 0x06, 0x03, 0xE8, 0, 8, 0x00, 0x00 };
            CRC16(endReadArchive, endReadArchive.Length);

            WritePacket(port, endReadArchive);
            receive = ReadPacket(port);
        }

        public List<DataParameter> ConvertArchives(string[] template, byte[] content, String serialNumber, int year)
        {
            List<DataParameter> archive = new List<DataParameter>();
            int skipSize = 0;
            byte[] time = new byte[4];
            byte[] data;
            DateTime archiveTime = new DateTime();

            foreach (string item in template)
            {
                try
                {
                    switch (item.Trim())
                    {
                        case "DATE":
                            data = content.Skip(skipSize).Take(3).ToArray();
                            skipSize += 3;

                            time[0] = data[0];
                            time[1] = data[1];
                            time[2] = data[2];

                            break;
                        case "HOUR":
                            data = content.Skip(skipSize).Take(1).ToArray();
                            skipSize += 1;
                            time[3] = data[0];

                            try
                            {
                                archiveTime = new DateTime(year, time[1], time[2], time[3], 0, 0);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }

                            break;
                        case "M1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "M2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "M3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "M4":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 3, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "M5":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 4, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            skipSize += 4;
                            break;
                        case "M6":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 90, 5, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "Q1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 13, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "Q2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 13, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "Q3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 13, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T4":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 3, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T5":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 4, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T6":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 5, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "T7":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 95, 6, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P4":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 3, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P5":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 4, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "P6":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1, 5, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "uM1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1112, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "uM2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1112, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "uM3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1112, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "pM1":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1111, 0, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "pM2":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1111, 1, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "pM3":
                            data = content.Skip(skipSize).Take(4).Reverse().ToArray();
                            skipSize += 4;
                            archive.Add(new DataParameter(serialNumber.ToString(), 1111, 2, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TNORM1,":
                            data = content.Skip(skipSize).Take(2).Reverse().ToArray();
                            skipSize += 2;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 0, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TNORM2":
                            data = content.Skip(skipSize).Take(2).Reverse().ToArray();
                            skipSize += 2;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 1, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TNORM3":
                            data = content.Skip(skipSize).Take(2).Reverse().ToArray();
                            skipSize += 2;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 2, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMIN1,":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 3, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMIN2":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 4, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMIN3":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 5, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMAX1,":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 6, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMAX2":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 7, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMAX3":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 8, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TDELTAT1,":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 9, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TDELTAT2":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 10, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TDELTAT3":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(data[0]), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "ERRORCODE1":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "ERRORCODE2":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "ERRORCODE3":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TNORM":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMIN":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TMAX":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TDELTAT":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TSUHOST":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TAVARIA":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "TNKP":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                        case "MODES":
                            data = content.Skip(skipSize).Take(1).Reverse().ToArray();
                            skipSize += 1;
                            //archive.Add(new DataParameter(serialNumber.ToString(), 10, 11, Convert.ToDouble(BitConverter.ToSingle(data, 0)), StationEquipment.EquipmentTypeId, archiveTime));
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return archive;
        }


        private void StartAsyncTcp()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(StationEquipment.ServerPort));
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state

                    ColoredConsole.WriteLine("Waiting for a connection Magika... " + StationEquipment.ServerPort); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing
                }
            }
            catch (Exception ex) { ColoredConsole.WriteLine("Magika : " + StationEquipment.ServerPort); ColoredConsole.WriteLine(ex.Message); }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            ColoredConsole.WriteLine("Connected");
            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 5000;
            handler.SendTimeout = 5000;

            try
            {
                while (true)
                {
                    ColoredConsole.WriteLine("Begining to read data from Magika...");

                    string serialNumber = ReadSeriaNumber(handler);

                    if (serialNumber == String.Empty)
                        throw new Exception("Magika serial number not readed");

                    WritePacket(handler, getAllFirst);  
                    byte[] firstPacket = ReadPacket(handler, getAllFirst);

                    WritePacket(handler, getAllSecond);
                    byte[] secondPacket = ReadPacket(handler, getAllSecond);

                    if (firstPacket.Length > 0 && secondPacket.Length > 0)
                        ConvertData(serialNumber.ToString(), firstPacket, secondPacket);

                    if (!(Data.Any()))
                        ColoredConsole.WriteLine(3, "Не удалось считать данные с Магика : " + StationEquipment.PhoneDialing);
                    else
                        ColoredConsole.WriteLine(3, "Считывание данных с Магика : " + StationEquipment.PhoneDialing + " завершено.");

                    ColoredConsole.WriteLine("Data is reading succesfully...");
                    Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(StationEquipment.SurveyTime))); // задержка до следующего опроса
                }
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("Magika : " + ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                return;
            }
        }

        public String ReadSeriaNumber(SerialPort port)
        {
            String temp = String.Empty;

            try
            {
                
                try
                {
                    byte[] receive1 = ReadPacket(port, getSerial);
                    String s1 = Encoding.ASCII.GetString(receive1);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
                CRC16(getSerial, getSerial.Length);
                WritePacket(port, getSerial);
                byte[] receive = ReadPacket(port, getSerial);
                String s = Encoding.ASCII.GetString(receive);

                receive = receive.Skip(6).Take(8).ToArray();

                foreach (var a in receive)
                {
                    temp += Convert.ToChar(a);
                }
            }
            catch (Exception ex)
            {
                return String.Empty;
            }

            ColoredConsole.WriteLine(3, "Магика - Считано серийный номер : " + temp);
            return temp.Replace("M", "");
        }

        public String ReadSeriaNumber(Socket socket)
        {
            String temp = String.Empty;

            try
            {
                byte[] receive = new byte[1024];
                CRC16(getSerial, getSerial.Length);
                WritePacket(socket, getSerial);

                receive = ReadPacket(socket, getSerial);

                receive = receive.Skip(6).Take(8).ToArray();

                foreach (var a in receive)
                {
                    temp += Convert.ToChar(a);
                }
            }
            catch (Exception ex)
            {
                return String.Empty;
            }

            ColoredConsole.WriteLine(3, "Магика - Считано серийный номер : " + temp);
            return temp.Replace("M", "");
        }

        private void CalculateCRC()
        {
            getSerial[3] = Convert.ToByte(StationEquipment.DeviseAdress);
            getAllFirst[3] = Convert.ToByte(StationEquipment.DeviseAdress);
            getAllSecond[3] = Convert.ToByte(StationEquipment.DeviseAdress);
            CRC16(getAllFirst, getAllFirst.Length);
            CRC16(getAllSecond, getAllSecond.Length);
            CRC16(getSerial, getSerial.Length);
        }

        public bool CheckCRC(byte[] response)
        {
            byte[] template = response;

            CRC16(template, template.Length);

            if (template[template.Length - 1] == response[response.Length - 1] && template[template.Length - 2] == response[response.Length - 2])
                return true;
            else
                return false;
        }

        public override Int16 CRC16(byte[] data, int size) // Функция нахождения контрольной сумы
        {
            int Sum = 0;
            int byte_cnt = size - 5;
            int shift_cnt;
            Sum = (ushort)0xffff;
            int i = 3;
            for (; byte_cnt > 0; byte_cnt--)
            {
                Sum = (ushort)((Sum / 256) * 256 + ((Sum % 256) ^ data[i++]));
                for (shift_cnt = 0; shift_cnt < 8; shift_cnt++)
                {
                    if ((Sum & 0x1) == 1) Sum = ((Sum >> 1) ^ 0xa001);
                    else Sum >>= 1;
                }
            }
            byte[] byteArray = BitConverter.GetBytes((short)Sum);
            data[data.Length - 2] = byteArray[0]; // Запись первого байта контрольной сумы
            data[data.Length - 1] = byteArray[1]; // Запись второго байта контрольной сумы
            return BitConverter.ToInt16(new byte[] { data[data.Length - 2], data[data.Length - 1] }, 0);
        }

        public byte[] ReadPacket(SerialPort port, byte[] request)
        {
            byte[] receive = new byte[65535];
            int count = 0;

            while (count <= 2)
            {
                int bytesRead = port.Read(receive, 0, receive.Length);
                receive = receive.Take(bytesRead).ToArray();
                if (CheckCRC(receive))
                    return receive;
                else
                {
                    port.Write(request, 0, request.Length);
                    Thread.Sleep(2000);
                    count++;
                }
            }

            return new byte[0];
        }

        public byte[] ReadPacket(Socket socket, byte[] request)
        {
            byte[] receive = new byte[65535];
            int count = 0;

            while (count <= 2)
            {
                int bytesRead = socket.Receive(receive);
                receive = receive.Take(bytesRead).ToArray();
                if (CheckCRC(receive))
                    return receive;
                else
                {
                    socket.Send(request);
                    Thread.Sleep(2000);
                    count++;
                }
            }

            return new byte[0];
        }
    }
}
