﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using Modbus.Device;
using Server;

namespace MasterParent
{
    public static class SerialPortExtensions
    {
        public static void WritePacketWithoutSleep(SerialPort port, byte[] data)
        {
            port.DiscardInBuffer();
            port.Write(data, 0, data.Length);
        }

        public async static Task ReadAsync(this SerialPort serialPort, byte[] buffer, int offset, int count)
        {
            int quedanPorLeer = count;
            var temp = new byte[count];

            while (quedanPorLeer > 0)
            {
                int leidos = await serialPort.BaseStream.ReadAsync(temp, 0, quedanPorLeer);
                Array.Copy(temp, 0, buffer, offset + count - quedanPorLeer, leidos);
                quedanPorLeer -= leidos;
            }
        }

        public async static Task<byte[]> ReadAsync(this SerialPort serialPort, int count)
        {
            var datos = new byte[count];
            await serialPort.ReadAsync(datos, 0, count);
            return datos;
        }
    }

    public static class SerialPortAsync
    {
        public static async Task<byte[]> AsyncMessageNumber(SerialPort port, byte[] com, int recLength, int delayTime)
        {
            SerialPortExtensions.WritePacketWithoutSleep(port, com);
            Task<byte[]> task = SerialPortExtensions.ReadAsync(port, recLength);

            if (await Task.WhenAny(task, Task.Delay(delayTime)) == task)
            {
                return task.Result;
            }
            else
            {
                Console.WriteLine("AsyncMessageNumber Error");
                return task.Result;
            }
        }

        public static async Task<byte[]> AsyncMessageString(SerialPort port, byte[] com, string Message, int delayTime = 3000)
        {
            SerialPortExtensions.WritePacketWithoutSleep(port, com);
            Task<byte[]> task = SerialPortExtensions.ReadAsync(port, Message.Length);

            if (await Task.WhenAny(task, Task.Delay(delayTime)) == task)
            {
                var t1 = System.Text.Encoding.ASCII.GetString(task.Result);
                var t2 = Message.Trim();
                if (t1.Contains(t2))
                    return task.Result;
                else

                {
                    port.DiscardInBuffer();
                    return task.Result;
                }
            }
            else
            {
                Console.WriteLine("AsyncMessageString Error");
                return task.Result;
            }
        }

        public static bool Messageby_GetText(int timeout, SerialPort port, byte[] com, string WantSee, int tryes = 1, string DontWantSee = "")
        {
            byte[] receive = new byte[1];
            List<byte> recTemp = new List<byte>();
            bool first = true;
            int whatHaappend = 0;
            for (int i = 0; i < tryes; i++)
            {
                whatHaappend = 0;


                Communicate.WritePacketWithoutSleep(port, com);
                if (first) Thread.Sleep(100);
                else
                    Thread.Sleep(2000 + timeout);

                int trys = 0;
                int whenNothingRecive = 0;
                while (whenNothingRecive < 10)
                {
                    while (trys < 10)
                    {
                        Thread.Sleep(50);
                        receive = Communicate.ReadPacket(port);
                        if (first)
                        {
                            string c1 = ASCIIEncoding.ASCII.GetString(receive).ToString().Trim();
                            string c2 = WantSee.Trim();
                            if (c1.Contains(c2))
                            {
                                return true;
                            }
                            if (DontWantSee != "" && c1.Contains(DontWantSee.Trim()))
                            {
                                return false;
                            }


                            first = false;
                        }

                        if (receive.Length > 0)
                        {

                            trys = 0;

                            foreach (var a in receive)
                            {
                                recTemp.Add(a);
                            }
                        }
                        else
                            trys++;
                    }
                    trys = 0;
                    receive = recTemp.ToArray();
                    if (receive.Length == 0)
                    {
                        whatHaappend = 1;
                        Thread.Sleep(timeout / 5);
                        whenNothingRecive++;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                if (receive.Length != 0)
                {
                    bool isCRCgood = false;
                    string c1 = ASCIIEncoding.ASCII.GetString(receive).ToString().Trim();
                    string c2 = WantSee.Trim();
                    if (c1.Contains(c2))
                    {
                        return true;
                    }
                    else
                    {
                        if (DontWantSee != "" && c1.Contains(DontWantSee.Trim()))
                        {
                            return false;
                        }

                        Thread.Sleep(8000);
                    }
                }
            }
            if (whatHaappend == 0)
            {
                return false;
            }
            else
            {
                return false;
            }
        }



    }

    public static class Communicate //ТОлько методы
    {
        public static void WritePacket(SerialPort port, byte[] data)
        {
            port.Write(data, 0, data.Length);
            Thread.Sleep(2000);
        }
        public static void WritePacketWithoutSleep(SerialPort port, byte[] data)
        {
            port.DiscardInBuffer();
            port.Write(data, 0, data.Length);
        }

        public static byte[] ReadPacket(SerialPort port)
        {
            int byteCount = port.BytesToRead;
            byte[] retBytes = new byte[byteCount];
            port.Read(retBytes, 0, byteCount);
            return retBytes;
        }

        public static void WritePacket(Socket socket, byte[] data)
        {
            socket.Send(data, 0, data.Length, SocketFlags.None);
            Thread.Sleep(2000);
        }

        public static byte[] ReadPacket(Socket socket)
        {
            int byteCount = socket.ReceiveBufferSize;
            byte[] retBytes = new byte[byteCount];
            socket.Receive(retBytes);
            return retBytes;
        }

        public static byte[] executeMessageBetter(SerialPort port, byte[] com)
        {
            byte[] recive = new byte[1];

            List<byte> recTemp = new List<byte>();

            int whatHaappend = 0;

            for (int i = 0; i < 3; i++)
            {
                whatHaappend = 0;

                CRC.CRC16(com, com.Length);
                WritePacketWithoutSleep(port, com);
                int trys = 0;
                int whenNothingRecive = 0;
                while (whenNothingRecive < 3)
                {
                    while (trys < 25)
                    {
                        Thread.Sleep(50);
                        recive = ReadPacket(port);
                        if (recive.Length > 0)
                        {
                            trys = 0;

                            foreach (var a in recive)
                            {
                                recTemp.Add(a);
                            }
                        }
                        else
                            trys++;
                    }
                    recive = recTemp.ToArray();
                    if (recive.Length == 0)
                    {
                        whatHaappend = 1;
                        Thread.Sleep(6000);
                        whenNothingRecive++;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                if (recive.Length != 0)
                {
                    byte cr1 = recive[recive.Length - 1];
                    byte cr2 = recive[recive.Length - 2];
                    CRC.CRC16(recTemp.ToArray(), recive.Length);

                    if (recTemp[recTemp.Count - 1] == cr1 && cr2 == recTemp[recTemp.Count - 2])
                    {
                        return recive;
                    }
                    else
                    {
                        Thread.Sleep(8000);
                    }
                }
            }
            if (whatHaappend == 0)
            {
                throw new Exception("Не совпали контрольные суммы или ничего не пришло");
            }
            else
            {
                throw new Exception("Ничего не пришло");
            }
        }


        public static byte[] executeMessage(SerialPort port, byte[] com)
        {
            byte[] receive = new byte[1];
            int whatHaappend = 0;

            for (int i = 0; i < 3; i++)
            {
                CRC.CRC16(com, com.Length);
                WritePacket(port, com);
                Thread.Sleep(3000);

                receive = ReadPacket(port);
                if (receive.Length == 0)
                {
                    whatHaappend = 1;
                    continue;
                }

                byte cr1 = receive[receive.Length - 1];
                byte cr2 = receive[receive.Length - 2];

                CRC.CRC16(receive, receive.Length);

                if (receive[receive.Length - 1] == cr1 && cr2 == receive[receive.Length - 2])
                {
                    return receive;
                }
                else
                {
                    Thread.Sleep(2000);
                }
            }
            if (whatHaappend == 0)
            {
                throw new Exception("Не совпали контрольные суммы или ничего не пришло");
            }
            else
            {
                throw new Exception("Ничего не пришло");
            }

        }
    }

    public class CommunicateCurrent //Создать класс для драцвера и использовать считывание
    {

        public ModbusSerialMaster master;


        private int ConnectionType;

        //РЕАЛИЗОВАТЬ И ТСП а не только CSD 
        public SerialPort port;
        public const int CRCtype_CRC16 = 1;
        public const int CRCtype_fromTEM = 2;
        public const int CRCtype_fromKM5 = 3;

        public int DefaultReadByCRC__FirstSleep = 2000;
        public int DefaultReadByCRC__RetryCount = 1;
        public int DefaultReadByCRC__RetrySleep = 200;

        public CommunicateCurrent()
        {
        }

        public void SetSerialPort(SerialPort port)
        {
            ConnectionType = Driver.CONNECT_CSD;
            this.port = port;
            PortOpen(port);
        }

        public bool CreateRtu()
        {
            try
            {
                switch (ConnectionType)
                {
                    case Driver.CONNECT_CSD:

                        master = ModbusSerialMaster.CreateRtu(port);

                        break;
                    default:
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "no ConnectionType in CommunicateCurrent");
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool CreateAscii()
        {
            try
            {
                switch (ConnectionType)
                {
                    case Driver.CONNECT_CSD:

                        master = ModbusSerialMaster.CreateAscii(port);

                        break;
                    default:
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "no ConnectionType in CommunicateCurrent");
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public ushort[] ReadInputRegisters(int slaveAdress, ushort StartAdress, ushort numberOfPointsa)
        {
            return master.ReadInputRegisters((byte)slaveAdress, 0x8002, 2);
        }

        public bool PortOpen(SerialPort port)
        {
            if (!(port.IsOpen))
            {
                port.Open(); //Or will be exception Cant open port
                return true;
            }
            return true;

        }

        public void WritePacketWithSleep(byte[] data, int SleepTime = 2000)
        {
            switch (ConnectionType)
            {
                case Driver.CONNECT_CSD:

                    port.Write(data, 0, data.Length);
                    Thread.Sleep(SleepTime);

                    break;
                default:
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "no ConnectionType in CommunicateCurrent");
                    break;
            }

        }

        public void WritePacketWithoutSleep(byte[] data)
        {
            switch (ConnectionType)
            {
                case Driver.CONNECT_CSD:

                    port.DiscardInBuffer();
                    port.Write(data, 0, data.Length);

                    break;
                default:
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "no ConnectionType in CommunicateCurrent");
                    break;
            }

        }

        public byte[] MessagebyCRC(byte[] com, int CRC_Type, byte[] firstbyte = null, int timeout = 0)
        {
            switch (ConnectionType)
            {
                case Driver.CONNECT_CSD:

                    return MessagebyCRC(timeout, firstbyte, com, CRC_Type);

                default:
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "no ConnectionType in CommunicateCurrent");
                    throw new Exception("no ConnectionType in CommunicateCurrent");

            }
        }

        private byte[] MessagebyCRC(int timeout, byte[] firstbyte, byte[] com, int CRC_Type)
        {
            byte[] receive = new byte[1];
            List<byte> recTemp = new List<byte>();
            int whatHaappend = 0;
            for (int i = 0; i < 3; i++)
            {
                whatHaappend = 0;
                if (CRC_Type == CRCtype_CRC16)
                {
                    CRC.CRC16(com, com.Length);
                }
                if (CRC_Type == CRCtype_fromTEM)
                {
                    CRC.CRC_fromTEM(com);
                }
                if (CRC_Type == CRCtype_fromKM5)
                {
                    CRC.KM5_CRC16(com, com.Length);
                }

                if (firstbyte != null)
                    Communicate.WritePacketWithoutSleep(port, firstbyte);
                  
                Communicate.WritePacketWithoutSleep(port, com);
                Thread.Sleep(DefaultReadByCRC__FirstSleep + timeout);
                int trys = 0;
                int whenNothingRecive = 0;
                while (whenNothingRecive < 3)
                {
                    while (trys < DefaultReadByCRC__RetryCount)
                    {
                        Thread.Sleep(DefaultReadByCRC__RetrySleep);
                        receive = Communicate.ReadPacket(port);
                        if (receive.Length > 0)
                        {

                            trys = 0;

                            foreach (var a in receive)
                            {
                                recTemp.Add(a);
                            }
                        }
                        else
                            trys++;
                    }
                    trys = 0;
                    receive = recTemp.ToArray();
                    if (receive.Length == 0)
                    {
                        whatHaappend = 1;
                        Thread.Sleep(6000);
                        whenNothingRecive++;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                if (receive.Length != 0)
                {
                    bool isCRCgood = false;

                    if (CRC_Type == CRCtype_CRC16)
                    {
                        byte cr1 = receive[receive.Length - 1];
                        byte cr2 = receive[receive.Length - 2];
                        CRC.CRC16(receive, receive.Length);
                        if (receive[receive.Length - 1] == cr1 && cr2 == receive[receive.Length - 2])
                        {
                            isCRCgood = true;
                        }
                    }

                    if (CRC_Type == CRCtype_fromTEM)
                    {
                        byte cr1 = receive[receive.Length - 1];
                        CRC.CRC_fromTEM(receive);
                        if (receive[receive.Length - 1] == cr1)
                        {
                            isCRCgood = true;
                        }
                    }

                    if (CRC_Type == CRCtype_fromKM5)
                    {
                        short crc = BitConverter.ToInt16(new byte[] { receive[receive.Length - 2], receive[receive.Length - 1] }, 0);
                        short CalculatedCRC = CRC.KM5_CRC16(receive, receive.Length);
                        if (crc == CalculatedCRC)
                        {
                            isCRCgood = true;
                        }
                    }

                    if (isCRCgood)
                    {
                        return receive;
                    }
                    else
                    {
                        Thread.Sleep(8000);
                    }
                }
            }
            if (whatHaappend == 0)
            {
                throw new Exception("Не совпали контрольные суммы или ничего не пришло");
            }
            else
            {
                throw new Exception("Ничего не пришло");
            }
        }

    }

}


