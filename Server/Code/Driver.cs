﻿using System;
using System.Collections.Generic;
using Server;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;


namespace MasterParent
{
    public class Driver
    {
        //Для логов 
        public int StatusLogNum = 1;
        public double StLogDob = 0;
        public string StartLogMessage = "Считано ";
        public string EndLogMessage = "%";


        public StationEquipmentData Config;
        public int ConnectionType = 0; //Тыпы соединений что нужно реализовать. 0-нет соединения
        public const int CONNECT_CSD = 1;

        /// <summary>
        /// Использем методы все зависимости от типа соединения
        /// </summary>
        public CommunicateCurrent comm;

        public Driver(StationEquipmentData Config)
        {
            CantReadOneHour = false;
            this.Config = new StationEquipmentData(Config);
            if (Config.ConnectionType == 0)//РЕАЛИЗОВАТЬ CSD
            {
                this.Config.ConnectionType = CONNECT_CSD;

                this.Config.DriverFeatures = Config.StationEquipmentId.ToString(); //ДЛЯ CSD серийник не нужен
            }
            comm = new CommunicateCurrent();

            WorkInConstructor();
        }

        /// <summary>
        /// /Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно.  
        /// На данном етапе не гарантируется соединение с устройством
        /// </summary>
        public virtual void WorkInConstructor()
        {

        }

        /// <summary>
        /// Если нужно считать какую то дополнительную конфигурацию до считывания архивов.        
        /// </summary>
        /// <returns></returns>
        public virtual bool ConfigBeforeReadArchive()
        {
            return true; //Успешно или нет
        }

        /// <summary>
        /// Если нужно считать какую то дополнительную конфигурацию до считывания текущих.        
        /// </summary>
        /// <returns></returns>
        public virtual bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }


        /// <summary>
        /// Главная функция считывания тукущих
        /// </summary> 
        /// <returns></returns>
        public virtual List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }

        /// <summary>
        /// Получить текущие //РЕАЛИЗОВАТЬ
        /// </summary>
        /// <returns></returns>
        public List<DataParameter> ReadCurrent()
        {
            List<DataParameter> forRet = new List<DataParameter>();


            try
            {
                string isConnected = Connect();
                if (isConnected != "")
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не дозвонились");
                    Disconnect();
                    return forRet;
                }

                SerialNumber = GetSerialNumberWithtrycatch(); //Узнать серийник + проверка отвечает ли прибор
                if (SerialNumber == "")
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать серийник. Соединения с устройством нет");
                    Disconnect();
                    return forRet;
                }
                ColoredConsole.WriteLine(3, "Считано серийный номер : " + SerialNumber);

                //Определение то по чему будет происходить запись в базу
                switch (ConnectionType)
                {
                    case CONNECT_CSD:
                        Config.DriverFeatures = Config.StationEquipmentId.ToString(); //Для CSD договорились заполнять это поле StationEquipmentId. Чтоб не узнавать серийник
                        break;
                    default:
                        {
                            //сюда дойти не должно
                        }
                        break;
                }

                ConfigBeforeReadCurrent();

                ReadCurrentNow();

                Disconnect();
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Непредусмотренная ошибка при считывании архивов за  : " + ex.Message);
                Disconnect();
            }
            return forRet;
        }

        public string Connect()
        {
            string IsError = "";
            switch (Config.ConnectionType)
            {
                case CONNECT_CSD:


                    SerialPort port = new SerialPort(Config.Com_Name, Convert.ToInt32(Config.Com_Speed), Parity.None, 8, StopBits.One); // Инициализация ком порта;
                    port.DtrEnable = true; // Setting handshake
                    port.RtsEnable = true;
                    port.ReadTimeout = 5000;
                    port.WriteTimeout = 5000;
                    comm.SetSerialPort(port);
                    switch (Config.ModbussType)
                    {
                        case StationEquipmentData.MODBUSS_RTU:
                            comm.CreateRtu();
                            break;

                        case StationEquipmentData.MODBUSS_ASCII:
                            comm.CreateAscii();
                            break;

                        default:
                            break;
                    }

                    ColoredConsole.WriteLine(3, "Звоним : " + Config.PhoneDialing);

                    if (!CSD.CSD_async(comm.port, Config.PhoneDialing, Config.CSDType)) //Звоним
                    {
                        IsError = "Не дозвонились";
                    }
                    //CSD.CSDnoasynk(comm.port, Config.PhoneDialing, Config.CSDType);
                    break;

                default:
                    {
                        IsError = "Ошибка:Тип соединения не определен";
                    }
                    break;

            }
            return IsError;

        }

        public void Disconnect()
        {
            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Завершение опроса и запись в базу");
            switch (Config.ConnectionType)
            {
                case CONNECT_CSD:
                    if (comm.port != null)
                    {
                        if (comm.port.IsOpen)
                            CSD.CSD_End(comm.port);
                        if (comm.port.IsOpen)
                        {
                            comm.port.Close();
                            comm.port.Dispose();
                        }

                    }
                    break;
                default:
                    {
                        //сюда дойти не должно
                    }
                    break;
            }
        }

        /// <summary>
        /// Главная функция считывания
        /// 
        /// </summary> 
        /// <returns></returns>
        public virtual ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            return new ArcHiveReadedPack();
        }

        public ArcHiveReadedPack ReadArchiveByDateGuaranteed(int ArchivesType, int day, int month, int year, int hour)
        {
            for (int i = 0; i < 2; i++)
            {
                ArcHiveReadedPack ret = new ArcHiveReadedPack();
                try
                {
                    ret = ReadArchiveByDate(ArchivesType, day, month, year, hour);
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка при считывании архива за [" + new DateTime(year, month, day, hour, 0, 0).ToString() + "] :" + ex.Message);
                    ret.Status = STATUS_BAD;
                }

                if (ret.Status == STATUS_NOARCHIVESONTHISDATE || ret.Status == STATUS_GOD)
                {
                    return ret;
                }
                if (ret.Status == STATUS_BAD || ret.Status == STATUS_NOSTATUS || ret.Status == STATUS_LOSTCONNECTION)
                {
                    
                    string ifSerialNumber = GetSerialNumberWithtrycatch(); //Узнать серийник + проверка отвечает ли прибор
                    if (ifSerialNumber != "")
                    {
                        //if serial nubmber can read but  archive can`t re-try read archives
                        continue;
                    }
                    else
                    {
                        string connectRezult = ConnectWithGetSerialAndDisconnect();
                        if (connectRezult != "")
                        {
                            //reconnect fall 
                            ret.errorMessages = connectRezult;
                            ret.Status = STATUS_LOSTCONNECTION;
                            return ret;
                        }
                        else
                        {
                            //reconnect success continue reading
                            continue;
                        }
                    }
                }
            }
            ArcHiveReadedPack whathappend = new ArcHiveReadedPack();
            whathappend.Status = STATUS_BAD;

            return whathappend;
        }
        /// <summary>
        /// Обязательно к обпределению. Если нет серийника ститать хоть что-то. Пустая строка значит ошибка.
        /// Тут же делать пробуждение!! первый запрос к прибору
        /// </summary>
        /// <returns></returns>
        public virtual string GetSerialNumber()
        {
            return String.Empty;
        }

        //Пустая строка - все хорошо/  иначе ошибку
        public virtual string ConnectionStateRequest()
        {
            return GetSerialNumber();
        }

        public string GetSerialNumberWithtrycatch()
        {
            try
            {
                return GetSerialNumber();
            }
            catch (Exception)
            {

                return "";
            }
        }

        string SerialNumber;
        bool CantReadOneHour;
        /// <summary>
        /// Конструктор заполнил уже соединение. Тут только считываем
        /// Если прибор не может считать конкретный час а только все записи часовые за эти сутки 
        /// нужно выставить флаг CantReadOneHour
        /// </summary>
        /// <returns></returns>
        public ArcHiveReadedPack ReadArchive()
        {
            SerialNumber = "";
            ArcHiveReadedPack forRet = new ArcHiveReadedPack();
            try
            {
                string connectRezult = ConnectWithGetSerialAndDisconnect();
                if (connectRezult != "")
                {
                    forRet.errorMessages = connectRezult;
                    return forRet;
                }


                ColoredConsole.WriteLine(3, "Считано серийный номер : " + SerialNumber);

                //Определение то по чему будет происходить запись в базу
                switch (Config.ConnectionType)
                {
                    case CONNECT_CSD:
                        Config.DriverFeatures = Config.StationEquipmentId.ToString(); //Для CSD договорились заполнять это поле StationEquipmentId. Чтоб не узнавать серийник                        
                        break;
                    default:
                        {
                            //сюда дойти не должно
                        }
                        break;
                }

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Настройка перед считыванием записей за каждую с дат");
                if (!ConfigBeforeReadArchive())
                {
                    Disconnect();
                    forRet.errorMessages = "Не удалось настроить перед считыванием архивов";
                    return forRet;
                }

                forRet.ReadedArchiveType = Config.ArchivesType;
                string[] Eq = Config.ArchivesParameter.Split('|');
                DateTime d_from = DateTime.Parse(Eq[0]);
                DateTime d_to = DateTime.Parse(Eq[1]);

                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание архивов за период с: " + d_from.ToString() + " по " + d_to.ToString());

                ///Здвигаем рамки
                if (Config.ArchivesType == 1)
                    if (!CantReadOneHour)
                    {
                        d_to = d_to.AddDays(1);
                    }
                    else
                    {
                        d_to = d_to.AddDays(1);
                        //обнулить часы
                        d_to = new DateTime(d_to.Year, d_to.Month, d_to.Day);
                        d_from = new DateTime(d_from.Year, d_from.Month, d_from.Day);
                    }
                if (Config.ArchivesType == 2)
                {
                    d_to = d_to.AddDays(1);
                    //обнулить часы
                    d_to = new DateTime(d_to.Year, d_to.Month, d_to.Day);
                    d_from = new DateTime(d_from.Year, d_from.Month, d_from.Day);
                }
                if (Config.ArchivesType == 3)
                {
                    d_to = d_to.AddMonths(1);
                    //обнулить часы и дни
                    d_to = new DateTime(d_to.Year, d_to.Month, 0);
                    d_from = new DateTime(d_from.Year, d_from.Month, 0);
                }
                //границы включают вторую дату 
                //Точность даты час

                //ПРИМЕР
                // [02.03.17 - 03.03.17] //считает 48 запис
                // [02.03.17 - 03.03.17] //считает 2 записи
                // [02.03.17 - 03.03.17] //считает 1 запись

                while (d_from < d_to)
                {
                    //Не считывать за дату больше чем сейчас (- час)
                    if (d_from.CompareTo(DateTime.Now.Add(new TimeSpan(-1, 0, 0))) < 0)
                    {


                        //не реализовал для прибора чтот все сутки считает (СДЕЛАТЬ!!!) TODO
                        bool neddReadNow = true;


                        foreach (var a in Config.ArchiveReaded)
                        {
                            if (a.ReadDate == d_from)
                            {
                                neddReadNow = false;
                                break;
                            }
                        }
                        if (neddReadNow)
                        {
                            //d_from = new DateTime(2017, 07, 02,23, 0, 0);
                            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание архива за  : " + d_from.ToString());

                            ArcHiveReadedPack foradd = new ArcHiveReadedPack();
                            try
                            {

                                foradd = ReadArchiveByDateGuaranteed(Config.ArchivesType, d_from.Day, d_from.Month, d_from.Year, d_from.Hour);

                                if (foradd.Status == STATUS_BAD)
                                {
                                    //не знаю. и не считало. и не сказал что нет записи. и связь была. 
                                    //наверное надо ->  флаг запись считана будет поставлена в STATUS_BAD и больше не будет попыток считать

                                    Task taskA = Task.Run(() => Database.AddReadedStatusofArchive(Config.DriverFeatures, d_from, Config.ArchivesType,
                                        false, false, true));

                                }

                                if (foradd.Status == STATUS_NOARCHIVESONTHISDATE)
                                {
                                    //флаг запись считана будет поставлена в STATUS_NOARCHIVESONTHISDATE и больше не будет попыток считать
                                    Task taskA = Task.Run(() => Database.AddReadedStatusofArchive(Config.DriverFeatures, d_from, Config.ArchivesType,
                                        false, true, true));

                                }

                                if (foradd.Status == STATUS_LOSTCONNECTION)
                                {
                                    // понижаем приоритет команды не получается дозвониться
                                    if (Config.CurrentCommand.ID > 0)
                                    {
                                        Task taskA0 = Task.Run(() =>
                                        Database.UpradePrioritet(Config.CurrentCommand.ID, -167)
                                        );
                                    }
                                }

                                if (foradd.Status == STATUS_GOD)
                                {
                                    //флаг запись считана будет поставлена в считано

                                    Task taskA0 = Task.Run(() => Database.AddReadedStatusofArchive(Config.DriverFeatures, d_from, Config.ArchivesType,
                                        true, false, true));

                                    Task taskA = Task.Run(() => Database.WriteToDB(foradd.Data, Config.ArchivesType));
                                }
                            }
                            catch (Exception ex)
                            {
                                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Непредусмотренная ошибка (" + ex.Message + ") при считывании архива за  : " + d_from.ToString());
                            }

                            foreach (var a in foradd.Data)
                            {
                                forRet.Data.Add(a);
                            }
                        }
                        else
                        {
                            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Пропуск считывания (уже был считан) за  : " + d_from.ToString());
                        }
                    }

                    if (Config.ArchivesType == 1)
                        if (!CantReadOneHour)
                        {
                            d_from = d_from.AddHours(1);
                        }
                        else
                        {
                            d_from = d_from.AddDays(1);
                        }

                    if (Config.ArchivesType == 2) d_from = d_from.AddDays(1);
                    if (Config.ArchivesType == 3) d_from = d_from.AddMonths(1);

                }

                Disconnect();
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Непредусмотренная ошибка при считывании архивов за  : " + ex.Message);
                Disconnect();
            }
            return forRet;
        }

        //Connect -> 
        //  Good GetSerial 
        //      (Good -> return "") 
        //      (bad -> Disconnect return ErrorMessage )
        //  Bad   Disconnect return ErrorMessage 
        private string ConnectWithGetSerialAndDisconnect()
        {
            Disconnect();

            string isConnected = Connect();
            if (isConnected != "")
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не дозвонились");
                Disconnect();
                return isConnected;
            }

            ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Считывание с прибора серийника");
            SerialNumber = GetSerialNumberWithtrycatch(); //Узнать серийник + проверка отвечает ли прибор
            if (SerialNumber == "")
            {
                ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать серийник. Соединения с устройством нет");
                return "Не удалось считать серийник. Соединения с устройством нет";
            }
            return ""; //Connect Success

        }

        public const int STATUS_NOSTATUS = -1;
        public const int STATUS_GOD = 0;
        public const int STATUS_NOARCHIVESONTHISDATE = 1;
        public const int STATUS_BAD = 2; //dont know why
        public const int STATUS_LOSTCONNECTION = 3;

        public class ArcHiveReadedPack  //Структура возвращаемая архивами
        {
            public List<List<DataParameter>> Data;  //Список списков (для удобства) группировать по датам

            public int ReadedArchiveType;   //Тип архива 1 2 3 / час сутки месяц            
            public string errorMessages;    //Ее сообщение
            public int Status;

            public ArcHiveReadedPack()
            {
                errorMessages = "";
                Data = new List<List<DataParameter>>();
                ReadedArchiveType = 0;
                Status = STATUS_NOSTATUS;
            }
            public ArcHiveReadedPack(List<List<DataParameter>> Data, int ReadedArchiveType)
            {
                this.Data = Data;
                this.ReadedArchiveType = ReadedArchiveType;
                this.Status = STATUS_GOD;
            }

        }


    }


}
