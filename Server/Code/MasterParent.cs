﻿using Server;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using Server.DB;

namespace MasterParent
{

    public static class asBitConverter
    {
        /*INT*/
        public static Int16 ToInt16_HL(byte[] data, int index)
        {
            return (Int16)((data[index] << 8) | data[index + 1]);
        }

        public static Int16 ToInt16_LH(byte[] data, int index)
        {
            return (Int16)((data[index]) | data[index + 1] << 8);
        }

        public static Int32 ToInt32_HL(byte[] data, int index)
        {
            return (Int32)((data[index] << 24) | (data[index + 1] << 16) | (data[index + 2] << 8) | data[index + 3]);
        }

        public static Int32 ToInt32_LH(byte[] data, int index)
        {
            return (Int32)((data[index + 3] << 24) | (data[index + 2] << 16) | (data[index + 1] << 8) | data[index]);
        }

        public static UInt16 ToUInt16_HL(byte[] data, int index)
        {
            return (UInt16)((data[index] << 8) | data[index + 1]);
        }

        public static UInt16 ToUInt16_LH(byte[] data, int index)
        {
            return (UInt16)((data[index]) | data[index + 1] << 8);
        }

        public static UInt32 ToUInt32_HL(byte[] data, int index)
        {
            return (UInt32)((data[index] << 24) | (data[index + 1] << 16) | (data[index + 2] << 8) | data[index + 3]);
        }

        public static UInt32 ToUInt32_LH(byte[] data, int index)
        {
            return (UInt32)((data[index + 3] << 24) | (data[index + 2] << 16) | (data[index + 1] << 8) | data[index]);
        }

        public static float ToFloat32_HL(byte[] data, int from)
        {
            return ToFloat32_(data, from, false);
        }

        public static float ToFloat32_LH(byte[] data, int from)
        {
            return ToFloat32_(data, from, true);
        }

        private static float ToFloat32_(byte[] data, int from, bool isLittleEndian)
        {
            byte[] value = new byte[4];
            if (BitConverter.IsLittleEndian == isLittleEndian)
            {   // other-endian; reverse this portion of the data (4 bytes)

                value[0] = data[from];
                value[1] = data[from + 1];
                value[2] = data[from + 2];
                value[3] = data[from + 3];

            }
            else
            {
                value[0] = data[from + 3];
                value[1] = data[from + 2];
                value[2] = data[from + 1];
                value[3] = data[from];
            }

            // var h = BitConverter.ToSingle(value, 0);

            //goto start;

            return BitConverter.ToSingle(value, 0);
        }

        public static byte[] MakeByteArray(ushort[] data)
        {
            byte[] resArray = new byte[data.Length * 2];
            for (int i = 0, j = 0; i < data.Length; i += 1, j += 2)
            {
                resArray[j] = Convert.ToByte((data[i] >> 8) & 0xFF);
                resArray[j + 1] = Convert.ToByte(data[i] & 0xFF);
            }
            return resArray;
        }

    }

    [Serializable()]
    public class DataParameter
    {
        public byte quality { get; set; }
        public double value { get; set; }
        public string value_str { get; set; }
        public int obisCode { get; set; }
        public string DriverFeatures { get; set; }
        public int EquipmentTypeID { get; set; }
        public DateTime DateCreate { get; set; }
        public int ParameterNumber { get; set; }

        public string Description { get; set; }
        public string Info1 { get; set; }

        public DataParameter()
        {
            Description = "";
            Info1 = "";
            quality = new byte();
            value = new double();
            value_str = "";
            obisCode = new int();
            DriverFeatures = "";
            DateCreate = new DateTime(); DateCreate = DateTime.Now;
            ParameterNumber = new int(); ParameterNumber = 0;
            EquipmentTypeID = new int();
        }

        public DataParameter(string identifier, int obisCode, int parameterNumber, string value_str, int EquipmentTypeID)
        {
            this.DateCreate = DateTime.Now;
            this.DriverFeatures = identifier;
            this.obisCode = obisCode;
            this.ParameterNumber = parameterNumber;
            this.quality = 2;
            this.value_str = value_str;
            this.value = 0;
            this.EquipmentTypeID = EquipmentTypeID;
        }

        public DataParameter(string identifier, int obisCode, int parameterNumber, double value, int EquipmentTypeID)
        {
            this.DateCreate = DateTime.Now;
            this.DriverFeatures = identifier;
            this.obisCode = obisCode;
            this.ParameterNumber = parameterNumber;
            this.value_str = "-";
            this.quality = 2;
            this.value = value;
            this.EquipmentTypeID = EquipmentTypeID;
        }

        public DataParameter(string identifier, int obisCode, int parameterNumber, double value, int EquipmentTypeID, DateTime time)
        {
            this.DateCreate = time;
            this.DriverFeatures = identifier;
            this.obisCode = obisCode;
            this.ParameterNumber = parameterNumber;
            this.value_str = "-";
            this.quality = 2;
            this.value = value;
            this.EquipmentTypeID = EquipmentTypeID;
        }

        public DataParameter(string identifier, int obisCode, int parameterNumber, double value, int EquipmentTypeID, DateTime time, string Description, string Info1)
        {
            this.Description = Description;
            this.Info1 = Info1;
            this.DateCreate = time;
            this.DriverFeatures = identifier;
            this.obisCode = obisCode;
            this.ParameterNumber = parameterNumber;
            this.value_str = "-";
            this.quality = 2;
            this.value = value;
            this.EquipmentTypeID = EquipmentTypeID;
        }
        public DataParameter(string identifier, int obisCode, int parameterNumber, double value, int EquipmentTypeID, DateTime time, string Description)
        {
            this.Description = Description;
            this.Info1 = Description;
            this.DateCreate = time;
            this.DriverFeatures = identifier;
            this.obisCode = obisCode;
            this.ParameterNumber = parameterNumber;
            this.value_str = "-";
            this.quality = 2;
            this.value = value;
            this.EquipmentTypeID = EquipmentTypeID;
        }



    }

    public class StationEquipmentDataParameters
    {

        public int ObisCode { get; set; }
        public int ParameterNumber { get; set; }
        public int Register { get; set; }
        public int UnitID { get; set; }
        public double Raw_High { get; set; }
        public double Raw_Low { get; set; }
        public double Scaled_High { get; set; }
        public double Scaled_Low { get; set; }
        public int TransTo { get; set; }

        public StationEquipmentDataParameters()
        {
            ObisCode = new int();
            ParameterNumber = new int();
            Register = new int();
            UnitID = new int();
            Raw_High = new double();
            Raw_Low = new double();
            Scaled_High = new double();
            Scaled_Low = new double();
            TransTo = new int();
        }

    }


    public class StationEquipmentData
    {
        public const int MODBUSS_RTU = 1;
        public const int MODBUSS_ASCII = 2;

        public int ModbussType = -1;
        public bool NeedToReadArchives { get; set; }
        public int ArchivesType { get; set; }
        public string ArchivesParameter { get; set; }
        public string PointName { get; set; }

        public int StationEquipmentId { get; set; }
        public int EquipmentTypeId { get; set; }
        public int StationId { get; set; }
        public string DevicePort { get; set; }
        public string DeviceIp { get; set; }
        public int ConnectionType { get; set; }
        public int CommunicationChannel { get; set; }
        public string SurveyTime { get; set; }
        public string ServerPort { get; set; }
        public string PhoneDialing { get; set; }
        public string SurveyType { get; set; }

        public int SlaveNumber { get; set; }
        public int DeviseAdress { get; set; }
        public string Com_Name { get; set; }
        public string Com_Speed { get; set; }
        public string DriverFeatures { get; set; }
        public bool NeedToBeRead { get; set; }
        public List<StationEquipmentDataParameters> OptionParams { get; set; }

        public DateTime LastPollingTimeAgo { get; set; }
        public int BadPollsNumber { get; set; }
        public int CSDType { get; set; }

        public List<ReadedArchivesSuccess> ArchiveReaded;
        public string AdressCity;
        public bool OutOfTurn;

        public ServerCommandsPoolcustom CurrentCommand;
        public StationEquipmentData()
        {

            CurrentCommand = new ServerCommandsPoolcustom();
            CurrentCommand.ID = -1;


            ModbussType = -1;
            ArchiveReaded = new List<ReadedArchivesSuccess>();
            NeedToReadArchives = new bool();
            ArchivesType = new int();
            ArchivesParameter = new string('a', 0);
            PointName = "";

            NeedToBeRead = new bool();
            StationEquipmentId = new int();
            EquipmentTypeId = new int();
            StationId = new int();
            LastPollingTimeAgo = new DateTime();
            BadPollsNumber = new int();
            CSDType = new int();

            DevicePort = new string('a', 0);
            DeviceIp = new string('a', 0);
            ConnectionType = new int();
            CommunicationChannel = new int();
            SurveyTime = new string('a', 0);
            ServerPort = new string('a', 0);
            PhoneDialing = new string('a', 0);
            SurveyType = new string('a', 0);

            SlaveNumber = new int();
            DeviseAdress = new int();
            Com_Name = new string('a', 0);
            Com_Speed = new string('a', 0);
            DriverFeatures = new string('a', 0);
            AdressCity = "";
            OptionParams = new List<StationEquipmentDataParameters>();

            OutOfTurn = false;
        }

        public StationEquipmentData(StationEquipmentData par)
        {
            this.ModbussType = par.ModbussType;
            this.ArchiveReaded = par.ArchiveReaded;
            this.NeedToReadArchives = par.NeedToReadArchives;
            this.ArchivesType = par.ArchivesType;
            this.ArchivesParameter = par.ArchivesParameter;
            this.PointName = par.PointName;
            this.NeedToBeRead = par.NeedToBeRead;
            this.StationEquipmentId = par.StationEquipmentId;
            this.EquipmentTypeId = par.EquipmentTypeId;
            this.StationId = par.StationId;
            this.DevicePort = par.DevicePort;
            this.DeviceIp = par.DeviceIp;
            this.ConnectionType = par.ConnectionType;
            this.CommunicationChannel = par.CommunicationChannel;
            this.SurveyTime = par.SurveyTime;
            this.ServerPort = par.ServerPort;
            this.PhoneDialing = par.PhoneDialing;
            this.SurveyType = par.SurveyType;
            this.SlaveNumber = par.SlaveNumber;
            this.DeviseAdress = par.DeviseAdress;
            this.Com_Name = par.Com_Name.Trim();
            this.Com_Speed = par.Com_Speed;
            this.DriverFeatures = par.DriverFeatures;
            this.OptionParams = par.OptionParams;
            this.CSDType = par.CSDType;
            this.LastPollingTimeAgo = par.LastPollingTimeAgo;
            this.BadPollsNumber = par.BadPollsNumber;
            this.AdressCity = par.AdressCity;
            CurrentCommand = new ServerCommandsPoolcustom();
            CurrentCommand.ID = -1;
        }
    }
}