﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MasterParent;
using System.Threading;
using System.IO.Pipes;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using Server.DB;
using System.Windows.Controls;
using System.Windows.Input;
using Server;

namespace MasterParent
{
    class Database
    {

        //ArchiveType 0 is current else archives
        public static async Task WriteToDB(List<List<DataParameter>> list, int ArchiveType)
        {
            List<DataParameter> ret = new List<DataParameter>();
            foreach (var a in list)
            {
                foreach (var b in a)
                {
                    ret.Add(b);
                }
            }
            WriteToDB(ret, ArchiveType);
        }

        public static void WriteToDB(List<DataParameter> list, int ArchiveType)
        {
            string _connString = "data source=localhost\\LERS;initial catalog=SPOU;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;";
            SqlConnection _con = new SqlConnection(_connString);
            foreach (var toSave in list)
            {
                try
                {

                    if (toSave.value_str == "-")
                    {
                        DataTable query = new DataTable();

                        if (ArchiveType == 0)
                        {
                            using (SqlCommand cmd = new SqlCommand("ParametersToSet", _con))
                            {
                                if (Double.IsNaN(toSave.value)) toSave.value = -1;

                                String driverNumber = Convert.ToString(toSave.DriverFeatures);

                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = driverNumber;
                                cmd.Parameters.Add("@EquipmentTypeID", SqlDbType.Int).Value = toSave.EquipmentTypeID;
                                cmd.Parameters.Add("@ObisCode", SqlDbType.Int).Value = toSave.obisCode;
                                cmd.Parameters.Add("@ParameterNumber", SqlDbType.Int).Value = toSave.ParameterNumber;
                                cmd.Parameters.Add("@Value", SqlDbType.Float).Value = toSave.value;
                                cmd.Parameters.Add("@QualityId", SqlDbType.Int).Value = toSave.quality;
                                cmd.Parameters.Add("@DateToRead", SqlDbType.DateTime).Value = toSave.DateCreate;

                                try
                                {
                                    SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                                    _dap.Fill(query);
                                }
                                catch (Exception ex)
                                {
                                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                                }

                                foreach (DataRow par in query.Rows) { }
                            }
                        }
                        else
                        {
                            if (toSave.obisCode != 999)
                            {
                                using (SqlCommand cmd = new SqlCommand("ParametersToSet_Archive", _con))
                                {
                                    if (Double.IsNaN(toSave.value)) toSave.value = -1;

                                    String driverNumber = Convert.ToString(toSave.DriverFeatures);

                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = driverNumber;
                                    cmd.Parameters.Add("@EquipmentTypeID", SqlDbType.Int).Value = toSave.EquipmentTypeID;
                                    cmd.Parameters.Add("@ObisCode", SqlDbType.Int).Value = toSave.obisCode;
                                    cmd.Parameters.Add("@ParameterNumber", SqlDbType.Int).Value = toSave.ParameterNumber;
                                    cmd.Parameters.Add("@Value", SqlDbType.Float).Value = toSave.value;
                                    cmd.Parameters.Add("@QualityId", SqlDbType.Int).Value = toSave.quality;
                                    cmd.Parameters.Add("@DateToRead", SqlDbType.DateTime).Value = toSave.DateCreate;
                                    cmd.Parameters.Add("@ArchiveType", SqlDbType.Int).Value = ArchiveType;

                                    try
                                    {
                                        SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                                        _dap.Fill(query);

                                    }
                                    catch (Exception ex)
                                    {
                                        var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                                        //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                                    }

                                    foreach (DataRow par in query.Rows)
                                    {
                                    }

                                }
                            }
                            else
                            {
                                //was as addd param wits 99 obiscode
                            }
                        }




                    }
                }
                catch (Exception ex)
                {
                    ColoredConsole.WriteLine(ex.Message);
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    // MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }


        public static async Task AddReadedStatusofArchive(string DriverFeature, DateTime ReadDate,
            int ArchiveType, bool Status_GOOD, bool Status_NOARCHIVES, bool Status_BAD)
        {
            string _connString = "data source=localhost\\LERS;initial catalog=SPOU;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;";
            SqlConnection _con = new SqlConnection(_connString);
            DataTable query = new DataTable();
            using (SqlCommand cmd = new SqlCommand("ReadedArchivesSuccessAdd", _con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = DriverFeature;
                cmd.Parameters.Add("@ReadDate", SqlDbType.DateTime).Value = ReadDate;
                cmd.Parameters.Add("@ArchiveType", SqlDbType.Int).Value = ArchiveType;

                cmd.Parameters.Add("@Status_GOOD", SqlDbType.Int).Value = Status_GOOD;
                cmd.Parameters.Add("@Status_NOARCHIVES", SqlDbType.Int).Value = ArchiveType;
                cmd.Parameters.Add("@Status_BAD", SqlDbType.Int).Value = Status_BAD;

                try
                {

                    SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                    _dap.Fill(query);
                }
                catch (Exception ex)
                {
                    var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                    //MessageBox.Show(ex.Message + "\nLine Number: " + LineNumber, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                foreach (DataRow par in query.Rows)
                {
                }
            }
        }

        public static async Task UpradePrioritet(int ID, int Value)
        {
            DBEntities_f DB = new DBEntities_f();
            var me = DB.TServerCommandsPool.Where(x => x.ID == ID).ToList().First();
            me.Priority +=  Value;
            DB.SaveChanges();
        }
    }
}
