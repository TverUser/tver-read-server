﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterParent
{
    public class CRC
    {
        public static Int16 CRC16(byte[] data, int size) // Функция нахождения контрольной сумы
        {
            int Sum = 0;
            int byte_cnt = size - 2;
            int shift_cnt;
            Sum = (ushort)0xffff;
            int i = 0;
            for (; byte_cnt > 0; byte_cnt--)
            {
                Sum = (ushort)((Sum / 256) * 256 + ((Sum % 256) ^ data[i++]));
                for (shift_cnt = 0; shift_cnt < 8; shift_cnt++)
                {
                    if ((Sum & 0x1) == 1) Sum = ((Sum >> 1) ^ 0xa001);
                    else Sum >>= 1;
                }
            }
            byte[] byteArray = BitConverter.GetBytes((short)Sum);
            data[data.Length - 2] = byteArray[0]; // Запись первого байта контрольной сумы
            data[data.Length - 1] = byteArray[1]; // Запись второго байта контрольной сумы
            return BitConverter.ToInt16(new byte[] { data[data.Length - 2], data[data.Length - 1] }, 0);
        }

        //Контрольная сумма(дополнение до нуля)
        public static byte CRC_fromTEM(byte[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length - 1; i++) sum += array[i];
            sum = ~sum;
            byte[] a = BitConverter.GetBytes(sum);
            byte Crc = a[0];
            array[array.Length - 1] = Crc;
            return Crc;
        }

        public static Int16 KM5_CRC16(byte[] data, int size)
        {
            byte crc1 = data[0];
            byte crc2 = data[0];
            int length = size - 2;
            for (int j = 1; j < length; j++)
            {
                crc1 ^= data[j];
                crc2 += data[j];
            }
            crc2 = Convert.ToByte(crc2 % 256);
            data[data.Length - 2] = crc1;
            data[data.Length - 1] = Convert.ToByte(crc2);
            return BitConverter.ToInt16(new byte[] { data[data.Length - 2], data[data.Length - 1] }, 0);
        }

    }
}
