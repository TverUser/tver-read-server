﻿using System;
using System.Collections.Generic;
using Server;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace MasterParent
{
    public static class CSD
    {
        //IRZ MC52PU - Command for initialization
        private static byte[] CSDWake = { 0x2b, 0x2b, 0x2b };
        private static byte[] firstRequest = { 0x41, 0x54, 0x48, 0x0d };
        private static byte[] firstRequest1 = { 0x41, 0x54, 0x5A, 0x0d };
                 
        private static byte[] secondRequest = { 0x41, 0x54, 0x26, 0x46, 0x4c, 0x31, 0x45, 0x30, 0x56, 0x31, 0x26, 0x44, 0x32, 0x58, 0x34, 0x53,
                                           0x37, 0x3d, 0x31, 0x32, 0x30, 0x53, 0x31, 0x30, 0x3d, 0x39, 0x30, 0x56, 0x31, 0x45, 0x30, 0x0d };
                 
        private static  byte[] call1 = { 0x41, 0x54, 0x44, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x0a };
        private static  byte[] call2 = { 0x41, 0x54, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x0a };
                 
                 
                 
        //IRZ M static 52PU - Command for call
        private static byte[] CSD_COM_Speed = { 0x41, 0x54, 0x2b, 0x49, 0x50, 0x52, 0x3D, 0x39, 0x36, 0x30, 0x30, 0x0d };
        private static byte[] CSD_DTR_IGNORE = { 0x41, 0x54, 0x26, 0x44, 0x30, 0x0d };
        private static byte[] CSD_Disable_Eho = { 0x41, 0x54, 0x45, 0x30, 0x0d };
        private static byte[] CSD_Save = { 0x41, 0x54, 0x26, 0x57, 0x0d };
        private static byte[] CSD_Reboot = { 0x41, 0x54, 0x5E, 0x53, 0x4D, 0x53, 0x4F, 0x0d };
        private static byte[] CSD_Reset = { 0x41, 0x54, 0x5A, 0x0d };
                 
        private static byte[] CSD_ATZ = { 0x41, 0x54, 0x5a, 0x0d, 0x0a };
     
        public static bool CSD_async(SerialPort port, string number, int CSDType)
        {
            try
            {
                addPhoneNumber(number, 2);
                for (int i = 0; i < 3; i++)
                {
                    ColoredConsole.WriteLine("Init CSD try " + i + 1 + "/3");
                    byte[] receive_1 = SerialPortAsync.AsyncMessageString(port, CSD_ATZ, "  OK  ").Result;
                    if (receive_1 != null)
                    {
                        ColoredConsole.WriteLine("Init CSD OK");
                        ColoredConsole.WriteLine("Call " + number);
                        byte[] receive_2;
                        receive_2 = SerialPortAsync.AsyncMessageString(port, call2, "  CONNECT 9600 / RL", 30000).Result;
                        //receive_2 = SerialPortAsync.Messageby_GetText(50000, port, call2, "  CONNECT 9600 / RL", DontWantSee:"CAUSE");
                       

                        if (receive_2 != null && !Encoding.ASCII.GetString(receive_2).Trim().Contains("CAUSE") )
                        {
                            ColoredConsole.WriteLine("Called successfully " + number);
                            Thread.Sleep(15000);
                            port.DiscardInBuffer();
                            return true;
                        }

                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }
                port.Close();
                return false;
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("CSD Error" + ex.Message);
                port.Close();
                return false;
            }
        }


        #region OLD
        private static byte[] oldcall1 = { 0x41, 0x54, 0x44, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d };
        private static byte[] oldcall2 = { 0x41, 0x54, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d };


        public static void CSDnoasynk(SerialPort port, string number, int CSDType)
        {
            try
            {
                oldaddPhoneNumber(number.Trim(), CSDType);
                byte[] receive;
                ColoredConsole.WriteLine("CSD request 1 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(CSDWake));

               Communicate. WritePacket(port, CSDWake);
                receive = Communicate.ReadPacket(port);
                ColoredConsole.WriteLine("CSD responce 1 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(receive));
                ColoredConsole.WriteLine("CSD request 2 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(firstRequest));

                Communicate.WritePacket(port, firstRequest);
                receive = Communicate.ReadPacket(port);

                ColoredConsole.WriteLine("CSD responce 2 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(receive));
                /*WritePacket(port, secondRequest);
                receive = ReadPacket(port);*/
                if (CSDType == 1) ColoredConsole.WriteLine("CSD request 3 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(call1));
                else
                {
                    ColoredConsole.WriteLine("CSD request 3 " + port.PortName + " : " + call2.ToString());
                }

                if (CSDType == 1) Communicate.WritePacket(port, call1);
                else Communicate.WritePacket(port, call2);

                Thread.Sleep(TimeSpan.FromSeconds(5));
                receive = Communicate.ReadPacket(port);
                ColoredConsole.WriteLine("CSD responce 3 " + port.PortName + " : " + System.Text.Encoding.ASCII.GetString(receive));

                Thread.Sleep(TimeSpan.FromSeconds(30));
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine(ex.Message + " CSD()");
                port.Close();
            }
        }
        public static void oldaddPhoneNumber(string phone_number, int CSDType)
        {
            phone_number = phone_number.Trim();
            if (CSDType == 1)
            {
                for (int i = 4, j = 0; i < oldcall1.Length - 1; i++, j++)
                {
                    oldcall1[i] = Convert.ToByte(phone_number[j]);
                }
            }
            else
            {
                //   private static byte[] call2 = { 0x41, 0x54, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x0a };
                byte[] tmp = new byte[5 + phone_number.Length];

                tmp[0] = 0x41; //'A'
                tmp[1] = 0x54;//'T'
                tmp[2] = 0x44; // 'D'


                for (int i = 3, j = 0; i < 5 + phone_number.Length - 2; i++, j++)
                {
                    tmp[i] = Convert.ToByte(phone_number[j]);
                }
                tmp[tmp.Length - 2] = 0xd;
                tmp[tmp.Length - 1] = 0xa;
                oldcall2 = tmp;
            }
        }

        #endregion OLD

        public static void CSD_End(SerialPort port)
        {
            byte[] receive;
            Communicate.WritePacket(port, CSDWake);
            receive = Communicate.ReadPacket(port);

            Communicate.WritePacket(port, firstRequest);
            receive = Communicate.ReadPacket(port);
        }

        public static void addPhoneNumber(string phone_number, int CSDType)
        {
            phone_number = phone_number.Trim();
            if (CSDType == 1)
            {
                for (int i = 4, j = 0; i < call1.Length - 1; i++, j++)
                {
                    call1[i] = Convert.ToByte(phone_number[j]);
                }
            }
            else
            {
                //   private static byte[] call2 = { 0x41, 0x54, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x0a };
                byte[] tmp = new byte[5 + phone_number.Length];

                tmp[0] = 0x41; //'A'
                tmp[1] = 0x54;//'T'
                tmp[2] = 0x44; // 'D'


                for (int i = 3, j = 0; i < 5 + phone_number.Length - 2; i++, j++)
                {
                    tmp[i] = Convert.ToByte(phone_number[j]);
                }
                tmp[tmp.Length - 2] = 0xd;
                tmp[tmp.Length - 1] = 0xa;
                call2 = tmp;
            }
        }
    }
}
