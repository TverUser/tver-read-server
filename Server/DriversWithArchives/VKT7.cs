﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using MasterParent;
using System.Net.Sockets;
using System.Net;
using Server;

namespace VKT7
{
    public class VKT7_Properties
    {
        public string TtypeM { get; set; }
        public string GtypeM { get; set; }
        public string VtypeM { get; set; }
        public string MtypeM { get; set; }
        public string PtypeM { get; set; }
        public string dtTypeM { get; set; }
        public string tswTypeM { get; set; }
        public string taTypeM { get; set; }
        public string MgTypeM { get; set; }
        public string QoTypeM { get; set; }
        public string QgTypeM { get; set; }
        public string QntTypeHIM { get; set; }
        public string QntTypeM { get; set; }
        public int tTypeFractDigNum { get; set; }
        public int VTypeFractDigNum1 { get; set; }
        public int MTypeFractDigNum1 { get; set; }
        public int PTypeFractDigNum1 { get; set; }
        public int dtTypeFractDigNum1 { get; set; }
        public int tswTypeFractDigNum1 { get; set; }
        public int taTypeFractDigNum1 { get; set; }
        public int MgypeFractDigNum1 { get; set; }
        public int QoTypeFractDigNum1 { get; set; }
        public int tTypeFractDigNum2 { get; set; }
        public int VTypeFractDigNum2 { get; set; }
        public int MTypeFractDigNum2 { get; set; }
        public int PTypeFractDigNum2 { get; set; }
        public int dtTypeFractDigNum2 { get; set; }
        public int tswTypeFractDigNum2 { get; set; }
        public int taTypeFractDigNum2 { get; set; }
        public int MgTypeFractDigNum2 { get; set; }
        public int QoTypeFractDigNum2 { get; set; }
    }

    public struct Parameter
    {
        public string Name { get; set; }
        public string Unit { get; set; }
        public byte Quality { get; set; }
        public int Address { get; set; }
        public int NumberOfDigitsPassPoint { get; set; }
        public short ByteSize { get; set; }
        public byte[] toConvert;
        public float Value { get; set; }
        public int ObisCode { get; set; }
        public int ParameterNumber { get; set; }
    }

    public class VKT_7 : Driver
    {
        private VKT7_Properties DeviceProperties;
        private List<Parameter> DeviceParameters;
        private int DeviceServerVersion = 0;

        #region request's
        private byte[] startConnection = { 0x00, 0x10, 0x3F, 0xFF, 0x00, 0x00, 0xCC, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private byte[] writeProperties = { 0x00, 0x10, 0x3F, 0xFD, 0x00, 0x00, 0x02, 0x06, 0x00, 0x00, 0x00 };
        private byte[] writeCurrent = { 0x00, 0x10, 0x3F, 0xFD, 0x00, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00 };
        private byte[] writeSummary = { 0x00, 0x10, 0x3F, 0xFD, 0x00, 0x00, 0x02, 0x05, 0x00, 0x00, 0x00 };
        private byte[] writeArchives = { 0x00, 0x10, 0x3F, 0xFD, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00 };

        private byte[] writeActiveProperties =  { 0x00, 0x10, 0x3F, 0xFF, 0x00, 0x00, 0xBA, 0x2c, 0x00, 0x00, 0x40, 0x07, 0x00, 0x2d, 0x00,
                                                  0x00, 0x40, 0x07, 0x00, 0x2e, 0x00, 0x00, 0x40, 0x07, 0x00, 0x2f, 0x00, 0x00, 0x40, 0x07,
                                                  0x00, 0x30, 0x00, 0x00, 0x40, 0x07, 0x00, 0x31, 0x00, 0x00, 0x40, 0x07, 0x00, 0x32, 0x00,
                                                  0x00, 0x40, 0x07, 0x00, 0x33, 0x00, 0x00, 0x40, 0x07, 0x00, 0x34, 0x00, 0x00, 0x40, 0x07,
                                                  0x00, 0x35, 0x00, 0x00, 0x40, 0x07, 0x00, 0x36, 0x00, 0x00, 0x40, 0x07, 0x00, 0x37, 0x00,
                                                  0x00, 0x40, 0x07, 0x00, 0x38, 0x00, 0x00, 0x40, 0x07, 0x00, 0x39, 0x00, 0x00, 0x40, 0x01,
                                                  0x00, 0x3B, 0x00, 0x00, 0x40, 0x01, 0x00, 0x3C, 0x00, 0x00, 0x40, 0x01, 0x00, 0x3D, 0x00,
                                                  0x00, 0x40, 0x01, 0x00, 0x3E, 0x00, 0x00, 0x40, 0x01, 0x00, 0x3F, 0x00, 0x00, 0x40, 0x01,
                                                  0x00, 0x40, 0x00, 0x00, 0x40, 0x01, 0x00, 0x41, 0x00, 0x00, 0x40, 0x01, 0x00, 0x42, 0x00,
                                                  0x00, 0x40, 0x01, 0x00, 0x43, 0x00, 0x00, 0x40, 0x01, 0x00, 0x45, 0x00, 0x00, 0x40, 0x01,
                                                  0x00, 0x46, 0x00, 0x00, 0x40, 0x01, 0x00, 0x47, 0x00, 0x00, 0x40, 0x01, 0x00, 0x48, 0x00,
                                                  0x00, 0x40, 0x01, 0x00, 0x49, 0x00, 0x00, 0x40, 0x01, 0x00, 0x4A, 0x00, 0x00, 0x40, 0x01,
                                                  0x00, 0x4B, 0x00, 0x00, 0x40, 0x01, 0x00, 0x4C, 0x00, 0x00, 0x40, 0x01, 0x00, 0x00, 0x00 };

        private byte[] readActiveElements = { 0x00, 0x03, 0x3F, 0xFE, 0x00, 0x00, 0x00, 0x00 };
        private byte[] readEnumActiveElements = { 0x00, 0x03, 0x3F, 0xFC, 0x00, 0x00, 0x00, 0x00 };
        private byte[] readDate = { 0x00, 0x03, 0x3F, 0xFB, 0x00, 0x00, 0x00, 0x00 };
        private byte[] readUtilityData = { 0x00, 0x03, 0x3F, 0xF9, 0x00, 0x00, 0x00, 0x00 };
        private byte[] readActiveProperties = { 0x00, 0x03, 0x3F, 0xFE, 0x00, 0x00, 0x00, 0x00 };
        #endregion

        public VKT_7(StationEquipmentData param) : base(param)
        {

        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {

        }

        public override string GetSerialNumber()
        {
            DeviceServerVersion = WakeUp();   // Запрос на "пробуждение" устройства и считывания сетевых настроек
            int serial = GetDeviceSerialNumber(); // Получение серийного номера прибора

            if (serial != 0)
                return serial.ToString();

            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            ClearDataAndReadDeviceProperties();

            return true; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            ReadValues(DeviceProperties, 0);
            ReadValues(DeviceProperties, 1);

            return new List<DataParameter>();
        }

        public override bool ConfigBeforeReadArchive()
        {
            try
            {
                ClearDataAndReadDeviceProperties();

                if (Config.ArchivesType == 1) writeArchives[7] = 0;
                if (Config.ArchivesType == 2) writeArchives[7] = 1;
                if (Config.ArchivesType == 3) writeArchives[7] = 2;

                byte[] receive = comm.MessagebyCRC(writeArchives, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
                receive = comm.MessagebyCRC(readEnumActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff }, timeout:15000);

                DeviceParameters = ParseParameters(receive, DeviceProperties);

                byte[] WriteEnumEctiveElements = ConfirmRequest(DeviceParameters);
                ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "ВКТ-7 считана конфигурация ");

                receive = comm.MessagebyCRC(WriteEnumEctiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            ArcHiveReadedPack ArchivesList = new ArcHiveReadedPack();

            byte[] receive = WriteDate(ArchivesType, (byte)day, (byte)month, (byte)(year - 2000), (byte)hour);

            if (receive == null)
            {
                ArchivesList.Status = STATUS_NOARCHIVESONTHISDATE;
                return ArchivesList; // нет архивов за выбранную дату
            }

            if (receive.Length > 0)
            {
                ArchivesList.Status = STATUS_GOD;
                if (receive[2] == 5)
                {
                    ChangeShemeType();
                    return ReadArchiveByDate(ArchivesType, day, month, year, hour);
                }
                else
                {
                    GetValues(receive, DeviceParameters);
                    DeviceParameters = MakeList(DeviceParameters);
                    ArchivesList.Data.Add(ConfirmParametersList(DeviceParameters, Convert.ToInt32(Config.DriverFeatures), year, month, day, hour));
                }
            }
            else
            {
                ArchivesList.Status = STATUS_BAD;
            }
            return ArchivesList;
        }

        public override string ConnectionStateRequest()
        {
            int code = GetDeviceSerialNumber();
            return code == 0 ? "" : code.ToString();
        }
        #endregion

        public byte[] WriteDate(int ArchiveType, byte day, byte month, byte year, byte hour)
        {
            byte[] writeDate = { 0x00, 0x10, 0x3f, 0xfb, 0x00, 0x00, 0x04, day, month, year, hour, 0x00, 0x00 };

            if (ArchiveType == 2 || ArchiveType == 3)
            {
                writeDate[10] = 23;
            }

            CRC.CRC16(writeDate, writeDate.Length);

            byte[] receive = comm.MessagebyCRC(writeDate, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });

            if (receive[2] == 3)
                return null; // нет данных за эту дату (предложить считать другой тип архива с этой датой)

            receive = comm.MessagebyCRC(readActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff }, 2000);
            return receive;
        }

        public void ChangeShemeType()
        {
            byte[] receive = comm.MessagebyCRC(readEnumActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
            DeviceParameters = ParseParameters(receive, DeviceProperties);
            byte[] dataArray = ConfirmRequest(DeviceParameters);
            receive = comm.MessagebyCRC(dataArray, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xFF, 0xFF });
        }

        private List<DataParameter> ConfirmParametersList(List<Parameter> VKTParameters, int serial, int year, int month, int day, int hour) // Функция заполнения списка параметров передающихся на сервер
        {
            List<DataParameter> Data = new List<DataParameter>();
            DateTime RecordTime = new DateTime(year, month, day, hour, 0, 0);

            foreach (var VktParameter in VKTParameters)
            {
                DataParameter param = new DataParameter(Config.DriverFeatures,
                                                        VktParameter.ObisCode,
                                                        VktParameter.ParameterNumber,
                                                        Convert.ToDouble(VktParameter.Value),
                                                        Config.EquipmentTypeId,
                                                        RecordTime);
                Data.Add(param);
            }
            Data.Add(new DataParameter(Config.DriverFeatures, 999, 0, 1, Config.EquipmentTypeId, RecordTime)); //зАПИСЬ СЧИТАНА
            return Data;
        }

        public List<Parameter> MakeList(List<Parameter> VKTParameters)   // Функция Записи значений в сипсок параметров прибора
        {
            for (int i = 0; i < VKTParameters.Count; i++)
            {
                if (VKTParameters[i].Name.Contains("G")) VKTParameters[i] = SetValueG(VKTParameters[i]);  // Нахождение значения расхода
                else VKTParameters[i] = SetValue(VKTParameters[i]); // Нахождение значения других параметров
            }
            return VKTParameters; // вернуть список
        }

        public Parameter SetValue(Parameter par) // Функция конвертации байтов в значения 
        {
            if (par.ByteSize == 2) par.Value = BitConverter.ToInt16(par.toConvert, 0) / Convert.ToSingle(Math.Pow(10, par.NumberOfDigitsPassPoint));
            if (par.ByteSize == 4) par.Value = BitConverter.ToInt32(par.toConvert, 0) / Convert.ToSingle(Math.Pow(10, par.NumberOfDigitsPassPoint));
            return par;
        }

        public Parameter SetValueG(Parameter par) // Функция конвертации байтов в значения для расхода 
        {
            par.Value = BitConverter.ToSingle(par.toConvert, 0);
            return par; // Возврат значения
        }

        public void ClearDataAndReadDeviceProperties()
        {
            DeviceProperties = new VKT7_Properties();
            DeviceParameters = new List<Parameter>();
            DeviceProperties = ReadProperties();
        }

        private VKT7_Properties ReadProperties() // Функция чтения свойств прибора
        {
            List<string> properties = new List<string>();  //Создание списка строк для ед. измирения
            VKT7_Properties Properties = new VKT7_Properties();  // Структура хранящая свойства прибора
            int UnitSizeProperties = 13; // 13 свойств которые обозначают ед. измирения
            byte[] receive;

            receive = comm.MessagebyCRC(writeProperties, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });        // Запрос на установку считывания свойств
            receive = comm.MessagebyCRC(writeActiveProperties, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });  // Запрос на запись активных свойств прибора
            receive = comm.MessagebyCRC(readActiveProperties, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff }, 10000);   // Запрос на чтение запрошеных свойств в предыдущем запросе (читайте протокол обмена если не понятно все что выше

            int size = 0;
            if (DeviceServerVersion == 1) // если версия сервера 1
            {
                for (int i = 3; i < receive.Length - 2; i++)                // Прохождение принятого массива со свойствами
                {
                    try
                    {
                        while (UnitSizeProperties > 0)                      // Пока не распарсили все ед. измирения
                        {
                            string Unit = String.Empty;                     // Строка обозначающая ед. измирения
                            size = (receive[i + 1] << 8) | receive[i];      // Получить размер свойства в байтах
                            i += 2;

                            for (int j = i; j < i + size; j++)
                                Unit += (char)receive[j];                   // Прохождение байтов размером полученым выше

                            properties.Add(Unit);                           // Добавления свойства в список строк
                            i += size + 2;                                  // Переход к следующему свойству 
                            UnitSizeProperties--;                           //Минус одно свойство
                        }
                        size = receive[i];                                  //Свойства характеризирующие кол-во знаков после запятой
                    }
                    catch (Exception ex)
                    {
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Ошибка чтения свойст прибора : " + ex.Message);
                    }
                    properties.Add(Convert.ToString(size));  //Добавление в список
                    i += 2;                                  //перейти к след. свойству
                }
                Properties = FillProperties(properties);    //Парсинг свойтв в структуру
                UnitSizeProperties = 13;                    // заново инициализировать кол-во свойств характеризующие ед. измирения (уже нафиг не надо, но на всякий случай)
            }
            if (DeviceServerVersion == 0)                         //  если версия сервера 0
            {
                for (int i = 3; i < receive.Length - 2; i++) // Прохождение принятого массива со свойствами
                {
                    while (UnitSizeProperties > 0)          // Пока не распарсили все ед. измирения
                    {
                        string Unit = String.Empty;         // Строка обозначающая ед. измирения
                        for (int j = 0; j < 7; j++, i++)
                        {
                            Unit += receive[i];             // Запись свойтсва
                        }
                        properties.Add(Unit);               //Добавление в список
                        UnitSizeProperties--;               //перейти к след. свойству
                    }
                    size = receive[i];                      //Свойства характеризирующие кол-во знаков после запятой
                    properties.Add(Convert.ToString(size)); //Добавление в список
                    i += 2;                                 //перейти к след. свойству
                }
                Properties = FillProperties(properties);    //Парсинг свойтв в структуру
            }
            return Properties;                              // Возврат структуры
        }

        public List<Parameter> ReadValues(VKT7_Properties VKTProperties, int flag) // Функция Чтения Итоговых значений
        {
            List<Parameter> VKTParameters = new List<Parameter>();
            byte[] receive;

            if (flag == 0)
                receive = comm.MessagebyCRC(writeSummary, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
            else
                receive = comm.MessagebyCRC(writeCurrent, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });

            receive = comm.MessagebyCRC(readEnumActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
            receive = comm.MessagebyCRC(ConfirmRequest(VKTParameters), CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });
            receive = comm.MessagebyCRC(readActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });

            return MakeList(VKTParameters);
        }

        private void GetValues(byte[] data, List<Parameter> VKTParameters)
        {
            int i = 0, j = 3;

            while (j < data.Length - 2)
            {
                int k = j; int l = 0;
                int size = VKTParameters[i].ByteSize;
                byte[] result = new byte[size];

                while (k < size + j)
                    result[l++] = data[k++];

                VKTParameters[i] = CopyParameter(VKTParameters[i], data[k++], result);
                k++;
                i++;
                j = k;
            }
        }

        private Parameter CopyParameter(Parameter par, byte quality, byte[] data) // Функция копирования параметра
        {
            par.Quality = quality;
            par.toConvert = data;
            return par;
        }

        private List<Parameter> ParseParameters(byte[] receive, VKT7_Properties VKTProperties) // Функция заполнения параметров 
        {
            List<Parameter> VKTParameters = new List<Parameter>();

            for (int i = 3; i < receive.Length - 2; i += 6) // Проход массива
            {
                int address = BitConverter.ToInt32(receive.Skip(i).Take(4).ToArray(), 0);
                short size = Convert.ToInt16((receive[i + 5] << 8) | receive[i + 4]);
                switch (address) // Выбор по адресу
                {
                    case 1073741824:
                        VKTParameters.Add(CreateParameter(address, size, "t1Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 0));
                        break;
                    case 1073741825:
                        VKTParameters.Add(CreateParameter(address, size, "t2Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 1));
                        break;
                    case 1073741826:
                        VKTParameters.Add(CreateParameter(address, size, "t3Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 2));
                        break;
                    case 1073741833:
                        VKTParameters.Add(CreateParameter(address, size, "P1Type1", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 0));
                        break;
                    case 1073741834:
                        VKTParameters.Add(CreateParameter(address, size, "P2Type1", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 1));
                        break;
                    case 1073741838:
                        VKTParameters.Add(CreateParameter(address, size, "dtType1", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum1, 0, 95, 6));
                        break;
                    case 1073741839:
                        VKTParameters.Add(CreateParameter(address, size, "txType1", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum1, 0, 95, 8));
                        break;
                    case 1073741840:
                        VKTParameters.Add(CreateParameter(address, size, "taType1", VKTProperties.taTypeM, VKTProperties.taTypeFractDigNum1, 0, 95, 7));
                        break;
                    case 1073741843:
                        VKTParameters.Add(CreateParameter(address, size, "G1Type1", VKTProperties.GtypeM, 0, 0, 94, 0));
                        break;
                    case 1073741844:
                        VKTParameters.Add(CreateParameter(address, size, "G2Type1", VKTProperties.GtypeM, 0, 0, 94, 1));
                        break;
                    case 1073741845:
                        VKTParameters.Add(CreateParameter(address, size, "G3Type1", VKTProperties.GtypeM, 0, 0, 94, 2));
                        break;
                    case 1073741846:
                        VKTParameters.Add(CreateParameter(address, size, "t1Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 3));
                        break;
                    case 1073741847:
                        VKTParameters.Add(CreateParameter(address, size, "t2Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 4));
                        break;
                    case 1073741848:
                        VKTParameters.Add(CreateParameter(address, size, "t3Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 5));
                        break;
                    case 1073741855:
                        VKTParameters.Add(CreateParameter(address, size, "P1Type2", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum2, 0, 1, 2));
                        break;
                    case 1073741856:
                        VKTParameters.Add(CreateParameter(address, size, "P2Type2", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum2, 0, 1, 3));
                        break;
                    case 1073741860:
                        VKTParameters.Add(CreateParameter(address, size, "dtType2", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum2, 0, 95, 9));
                        break;
                    case 1073741861:
                        VKTParameters.Add(CreateParameter(address, size, "txType2", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum2, 0, 95, 11));
                        break;
                    case 1073741862:
                        VKTParameters.Add(CreateParameter(address, size, "taType2", VKTProperties.taTypeM, VKTProperties.taTypeFractDigNum2, 0, 95, 10));
                        break;
                    case 1073741865:
                        VKTParameters.Add(CreateParameter(address, size, "G1Type2", VKTProperties.GtypeM, 0, 0, 94, 3));
                        break;
                    case 1073741866:
                        VKTParameters.Add(CreateParameter(address, size, "G2Type2", VKTProperties.GtypeM, 0, 0, 94, 4));
                        break;
                    case 1073741867:
                        VKTParameters.Add(CreateParameter(address, size, "G3Type2", VKTProperties.GtypeM, 0, 0, 94, 5));
                        break;
                    case 1073741906:
                        VKTParameters.Add(CreateParameter(address, size, "P3Type", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 4));
                        break;
                    case 1073741827:
                        VKTParameters.Add(CreateParameter(address, size, "V1Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 0));
                        break;
                    case 1073741828:
                        VKTParameters.Add(CreateParameter(address, size, "V2Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 1));
                        break;
                    case 1073741829:
                        VKTParameters.Add(CreateParameter(address, size, "V3Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 2));
                        break;
                    case 1073741830:
                        VKTParameters.Add(CreateParameter(address, size, "M1Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 0));
                        break;
                    case 1073741831:
                        VKTParameters.Add(CreateParameter(address, size, "M2Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 1));
                        break;
                    case 1073741832:
                        VKTParameters.Add(CreateParameter(address, size, "M3Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 2));
                        break;
                    case 1073741835:
                        VKTParameters.Add(CreateParameter(address, size, "MgType1", VKTProperties.MgTypeM, VKTProperties.MgypeFractDigNum1, 0, 90, 6));
                        break;
                    case 1073741836:
                        VKTParameters.Add(CreateParameter(address, size, "QoType1", VKTProperties.QoTypeM, VKTProperties.QoTypeFractDigNum1, 0, 13, 0));
                        break;
                    case 1073741837:
                        VKTParameters.Add(CreateParameter(address, size, "QgType1", VKTProperties.QgTypeM, 3, 0, 13, 1));
                        break;
                    case 1073741849:
                        VKTParameters.Add(CreateParameter(address, size, "V1Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 3));
                        break;
                    case 1073741850:
                        VKTParameters.Add(CreateParameter(address, size, "V2Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 4));
                        break;
                    case 1073741851:
                        VKTParameters.Add(CreateParameter(address, size, "V3Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 5));
                        break;
                    case 1073741852:
                        VKTParameters.Add(CreateParameter(address, size, "M1Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 3));
                        break;
                    case 1073741853:
                        VKTParameters.Add(CreateParameter(address, size, "M2Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 4));
                        break;
                    case 1073741854:
                        VKTParameters.Add(CreateParameter(address, size, "M3Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 5));
                        break;
                    case 1073741857:
                        VKTParameters.Add(CreateParameter(address, size, "MgType2", VKTProperties.MgTypeM, VKTProperties.MgypeFractDigNum1, 0, 90, 7));
                        break;
                    case 1073741858:
                        VKTParameters.Add(CreateParameter(address, size, "QoType2", VKTProperties.QoTypeM, VKTProperties.QoTypeFractDigNum1, 0, 13, 2));
                        break;
                    case 1073741859:
                        VKTParameters.Add(CreateParameter(address, size, "QgType2", VKTProperties.QgTypeM, 3, 0, 13, 3));
                        break;
                    case 1073741841:
                        VKTParameters.Add(CreateParameter(address, size, "QntType_1HIP", VKTProperties.QntTypeHIM, 0, 0, 10, 0));
                        break;
                    case 1073741842:
                        VKTParameters.Add(CreateParameter(address, size, "QntType_1P", VKTProperties.QntTypeM, 0, 0, 51, 0));
                        break;
                    case 1073741863:
                        VKTParameters.Add(CreateParameter(address, size, "Qnt_2TypeHIP ", VKTProperties.QntTypeHIM, 0, 0, 10, 1));
                        break;
                    case 1073741864:
                        VKTParameters.Add(CreateParameter(address, size, "Qnt_2TypeP ", VKTProperties.QntTypeM, 0, 0, 51, 1));
                        break;
                }

            }
            return VKTParameters; //Возврат структуры
        }

        /* private List<Parameter> ParseParameters(byte[] receive, VKT7_Properties VKTProperties, int flag) // Функция заполнения параметров
         {
             List<Parameter> VKTParameters = new List<Parameter>();
             if (flag == 0) // флаг = 0, текущие значения
             {
                 for (int i = 3; i < receive.Length - 2; i += 6) // Проход массива
                 {
                     int address = BitConverter.ToInt32(new byte[] { receive[i], receive[i + 1], receive[i + 2], receive[i + 3] }, 0);
                     short size = Convert.ToInt16((receive[i + 5] << 8) | receive[i + 4]);
                     switch (address) // Выбор по адресу
                     {
                         case 1073741824:
                             VKTParameters.Add(CreateParameter(address, size, "t1Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 0));
                             break;
                         case 1073741825:
                             VKTParameters.Add(CreateParameter(address, size, "t2Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 1));
                             break;
                         case 1073741826:
                             VKTParameters.Add(CreateParameter(address, size, "t3Type1", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum, 0, 95, 2));
                             break;
                         case 1073741833:
                             VKTParameters.Add(CreateParameter(address, size, "P1Type1", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 0));
                             break;
                         case 1073741834:
                             VKTParameters.Add(CreateParameter(address, size, "P2Type1", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 1));
                             break;
                         case 1073741838:
                             VKTParameters.Add(CreateParameter(address, size, "dtType1", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum1, 0, 95, 6));
                             break;
                         case 1073741839:
                             VKTParameters.Add(CreateParameter(address, size, "txType1", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum1, 0, 95, 8));
                             break;
                         case 1073741840:
                             VKTParameters.Add(CreateParameter(address, size, "taType1", VKTProperties.taTypeM, VKTProperties.taTypeFractDigNum1, 0, 95, 7));
                             break;
                         case 1073741843:
                             VKTParameters.Add(CreateParameter(address, size, "G1Type1", VKTProperties.GtypeM, 0, 0, 94, 0));
                             break;
                         case 1073741844:
                             VKTParameters.Add(CreateParameter(address, size, "G2Type1", VKTProperties.GtypeM, 0, 0, 94, 1));
                             break;
                         case 1073741845:
                             VKTParameters.Add(CreateParameter(address, size, "G3Type1", VKTProperties.GtypeM, 0, 0, 94, 2));
                             break;
                         case 1073741846:
                             VKTParameters.Add(CreateParameter(address, size, "t1Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 3));
                             break;
                         case 1073741847:
                             VKTParameters.Add(CreateParameter(address, size, "t2Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 4));
                             break;
                         case 1073741848:
                             VKTParameters.Add(CreateParameter(address, size, "t3Type2", VKTProperties.TtypeM, VKTProperties.tTypeFractDigNum2, 0, 95, 5));
                             break;
                         case 1073741855:
                             VKTParameters.Add(CreateParameter(address, size, "P1Type2", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum2, 0, 1, 2));
                             break;
                         case 1073741856:
                             VKTParameters.Add(CreateParameter(address, size, "P2Type2", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum2, 0, 1, 3));
                             break;
                         case 1073741860:
                             VKTParameters.Add(CreateParameter(address, size, "dtType2", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum2, 0, 95, 9));
                             break;
                         case 1073741861:
                             VKTParameters.Add(CreateParameter(address, size, "txType2", VKTProperties.dtTypeM, VKTProperties.dtTypeFractDigNum2, 0, 95, 11));
                             break;
                         case 1073741862:
                             VKTParameters.Add(CreateParameter(address, size, "taType2", VKTProperties.taTypeM, VKTProperties.taTypeFractDigNum2, 0, 95, 10));
                             break;
                         case 1073741865:
                             VKTParameters.Add(CreateParameter(address, size, "G1Type2", VKTProperties.GtypeM, 0, 0, 94, 3));
                             break;
                         case 1073741866:
                             VKTParameters.Add(CreateParameter(address, size, "G2Type2", VKTProperties.GtypeM, 0, 0, 94, 4));
                             break;
                         case 1073741867:
                             VKTParameters.Add(CreateParameter(address, size, "G3Type2", VKTProperties.GtypeM, 0, 0, 94, 5));
                             break;
                         case 1073741906:
                             VKTParameters.Add(CreateParameter(address, size, "P3Type", VKTProperties.PtypeM, VKTProperties.PTypeFractDigNum1, 0, 1, 4));
                             break;
                     }
                 }
             }
             if (flag == 1) // флаг = 0, итоговые значения
             {
                 for (int i = 3; i < receive.Length - 2; i += 6)
                 {
                     int address = BitConverter.ToInt32(new byte[] { receive[i], receive[i + 1], receive[i + 2], receive[i + 3] }, 0);
                     short size = Convert.ToInt16((receive[i + 5] << 8) | receive[i + 4]);
                     switch (address)
                     {
                         case 1073741827:
                             VKTParameters.Add(CreateParameter(address, size, "V1Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 0));
                             break;
                         case 1073741828:
                             VKTParameters.Add(CreateParameter(address, size, "V2Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 1));
                             break;
                         case 1073741829:
                             VKTParameters.Add(CreateParameter(address, size, "V3Type1", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum1, 0, 91, 2));
                             break;
                         case 1073741830:
                             VKTParameters.Add(CreateParameter(address, size, "M1Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 0));
                             break;
                         case 1073741831:
                             VKTParameters.Add(CreateParameter(address, size, "M2Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 1));
                             break;
                         case 1073741832:
                             VKTParameters.Add(CreateParameter(address, size, "M3Type1", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum1, 0, 90, 2));
                             break;
                         case 1073741835:
                             VKTParameters.Add(CreateParameter(address, size, "MgType1", VKTProperties.MgTypeM, VKTProperties.MgypeFractDigNum1, 0, 90, 6));
                             break;
                         case 1073741836:
                             VKTParameters.Add(CreateParameter(address, size, "QoType1", VKTProperties.QoTypeM, VKTProperties.QoTypeFractDigNum1, 0, 13, 0));
                             break;
                         case 1073741837:
                             VKTParameters.Add(CreateParameter(address, size, "QgType1", VKTProperties.QgTypeM, 3, 0, 13, 1));
                             break;
                         case 1073741849:
                             VKTParameters.Add(CreateParameter(address, size, "V1Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 3));
                             break;
                         case 1073741850:
                             VKTParameters.Add(CreateParameter(address, size, "V2Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 4));
                             break;
                         case 1073741851:
                             VKTParameters.Add(CreateParameter(address, size, "V3Type2", VKTProperties.VtypeM, VKTProperties.VTypeFractDigNum2, 0, 91, 5));
                             break;
                         case 1073741852:
                             VKTParameters.Add(CreateParameter(address, size, "M1Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 3));
                             break;
                         case 1073741853:
                             VKTParameters.Add(CreateParameter(address, size, "M2Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 4));
                             break;
                         case 1073741854:
                             VKTParameters.Add(CreateParameter(address, size, "M3Type2", VKTProperties.MtypeM, VKTProperties.MTypeFractDigNum2, 0, 90, 5));
                             break;
                         case 1073741857:
                             VKTParameters.Add(CreateParameter(address, size, "MgType2", VKTProperties.MgTypeM, VKTProperties.MgypeFractDigNum1, 0, 90, 7));
                             break;
                         case 1073741858:
                             VKTParameters.Add(CreateParameter(address, size, "QoType2", VKTProperties.QoTypeM, VKTProperties.QoTypeFractDigNum1, 0, 13, 2));
                             break;
                         case 1073741859:
                             VKTParameters.Add(CreateParameter(address, size, "QgType2", VKTProperties.QgTypeM, 3, 0, 13, 3));
                             break;
                         case 1073741841:
                             VKTParameters.Add(CreateParameter(address, size, "QntType_1HIP", VKTProperties.QntTypeHIM, 0, 0, 10, 0));
                             break;
                         case 1073741842:
                             VKTParameters.Add(CreateParameter(address, size, "QntType_1P", VKTProperties.QntTypeM, 0, 0, 51, 0));
                             break;
                         case 1073741863:
                             VKTParameters.Add(CreateParameter(address, size, "Qnt_2TypeHIP ", VKTProperties.QntTypeHIM, 0, 0, 10, 1));
                             break;
                         case 1073741864:
                             VKTParameters.Add(CreateParameter(address, size, "Qnt_2TypeP ", VKTProperties.QntTypeM, 0, 0, 51, 1));
                             break;
                     }
                 }
             }
             return VKTParameters; //Возврат структуры
         }*/

        private byte[] ConfirmRequest(List<Parameter> Parameters)
        {
            byte[] request = new byte[(Parameters.Count * 6) + 9];
            request[0] = 0x00; // Convert.ToByte(Config.DeviseAdress);
            request[1] = 0x10;
            request[2] = 0x3F;
            request[3] = 0xFF;
            request[4] = 0x00;
            request[5] = 0x00;
            request[6] = Convert.ToByte(Parameters.Count * 6);
            for (int j = 7, i = 0; j < request.Length - 2; i++)
            {
                byte[] array = BitConverter.GetBytes(Parameters[i].Address);
                byte[] size = BitConverter.GetBytes(Parameters[i].ByteSize);
                for (int k = 0; k < array.Length; k++, j++) request[j] = array[k];
                request[j++] = size[0];
                request[j++] = size[1];
            }
            CRC.CRC16(request, request.Length);
            return request;
        }

        private Parameter CreateParameter(int address, short size, string name, string Unit, int num, float value, int obisCode, int parameterNumber)
        {
            Parameter par = new Parameter()
            {
                Address = address,
                ByteSize = size,
                Name = name,
                Unit = Unit,
                ObisCode = obisCode,
                NumberOfDigitsPassPoint = num,
                Value = value,
                ParameterNumber = parameterNumber
            };
            return par;
        }

        private VKT7_Properties FillProperties(List<string> prop)
        {
            VKT7_Properties VKTProperties = new VKT7_Properties()
            {
                TtypeM = prop[0],
                GtypeM = prop[1],
                VtypeM = prop[2],
                MtypeM = prop[3],
                PtypeM = prop[4],
                dtTypeM = prop[5],
                tswTypeM = prop[6],
                taTypeM = prop[7],
                MgTypeM = prop[8],
                QoTypeM = prop[9],
                QgTypeM = prop[10],
                QntTypeHIM = prop[11],
                QntTypeM = prop[12],
                tTypeFractDigNum = Convert.ToInt32(prop[13]),
                VTypeFractDigNum1 = Convert.ToInt32(prop[14]),
                MTypeFractDigNum1 = Convert.ToInt32(prop[15]),
                PTypeFractDigNum1 = Convert.ToInt32(prop[16]),
                dtTypeFractDigNum1 = Convert.ToInt32(prop[17]),
                tswTypeFractDigNum1 = Convert.ToInt32(prop[18]),
                taTypeFractDigNum1 = Convert.ToInt32(prop[19]),
                MgypeFractDigNum1 = Convert.ToInt32(prop[20]),
                QoTypeFractDigNum1 = Convert.ToInt32(prop[21]),
                tTypeFractDigNum2 = Convert.ToInt32(prop[22]),
                VTypeFractDigNum2 = Convert.ToInt32(prop[23]),
                MTypeFractDigNum2 = Convert.ToInt32(prop[24]),
                PTypeFractDigNum2 = Convert.ToInt32(prop[25]),
                dtTypeFractDigNum2 = Convert.ToInt32(prop[26]),
                tswTypeFractDigNum2 = Convert.ToInt32(prop[27]),
                taTypeFractDigNum2 = Convert.ToInt32(prop[28]),
                MgTypeFractDigNum2 = Convert.ToInt32(prop[29]),
                QoTypeFractDigNum2 = Convert.ToInt32(prop[30])
            };
            return VKTProperties;
        }

        private int WakeUp()
        {
            comm.MessagebyCRC(startConnection, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xFF, 0xFF });
            byte[] receive = comm.MessagebyCRC(readActiveElements, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xFF, 0xFF }, timeout: 2000);
            return receive[64] == 0 ? 0 : 1;
        }

        private int GetDeviceSerialNumber()
        {
            try
            {
                byte[] receive = comm.MessagebyCRC(readUtilityData, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });

                String serial = String.Empty;

                for (int i = 10; i <= 15; i++)
                    serial += Convert.ToChar(receive[i]);

                return Convert.ToInt32(serial);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        private DateTime ReadDate()
        {
            byte[] dateData = comm.MessagebyCRC(readDate, CommunicateCurrent.CRCtype_CRC16, new byte[2] { 0xff, 0xff });

            int day = dateData[3];
            int month = dateData[4];
            int year = dateData[5];
            int hour = dateData[6];
            int minute = dateData[7];
            int second = dateData[8];
            return new DateTime(2000 + year, month, day, hour, minute, second);
        }
    }
}