﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterParent;
using System.Threading;
using System.IO.Ports;
using Modbus.Device;
using System.Net;
using System.Net.Sockets;
using Server;

namespace VZLET_024M
{
    public class VZLET_TCPB_024M : Driver
    {
        public VZLET_TCPB_024M(StationEquipmentData param) : base(param)
        {

        }

        #region Overrides
        public override void WorkInConstructor()//Что то делаем в конструкторе. Например считаем CRC глобальные. Необязательно. На данном етапе не гарантируется соединение с устройством
        {
            Config.ModbussType = StationEquipmentData.MODBUSS_RTU;
        }

        public override string GetSerialNumber()
        {
            uint serial = 0;
            try
            {
                ushort[] reg = comm.ReadInputRegisters((byte)Config.DeviseAdress, 0x8006, 2);
                serial = asBitConverter.ToUInt32_HL((asBitConverter.MakeByteArray(reg)), 0);
                ColoredConsole.WriteLine("ВЗЛЕТ-ТСРВ-024М серийник считало : " + serial);
            }
            catch (Exception ex)
            {
                ColoredConsole.WriteLine("ВЗЛЕТ-ТСРВ-024М ошибка считывания серийника: " + ex.Message);
            }
            if (serial != 0)
                return serial.ToString();
            else return "";
        }

        public override bool ConfigBeforeReadCurrent()
        {
            return true; //Успешно или нет
        }

        public override List<DataParameter> ReadCurrentNow()
        {
            return new List<DataParameter>();
        }


        public override bool ConfigBeforeReadArchive()
        {
            try
            {


            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override ArcHiveReadedPack ReadArchiveByDate(int ArchivesType, int day, int month, int year, int hour)
        {
            ArcHiveReadedPack returning = new ArcHiveReadedPack();
            List<List<DataParameter>> ret = new List<List<DataParameter>>();
            

            List<DataParameter> tData = new List<DataParameter>();
            DateTime when = new DateTime();
            bool somebad = false;//Если неправильно запись считается несчитанной

            for (int ii1 = 0; ii1 < 2; ii1++)
            {
                byte archIndex = 0;

                switch (Config.ArchivesType)
                {
                    case 1:
                        archIndex = 0; break;
                    case 2:
                        archIndex = 1; break;
                    case 3:
                        archIndex = 2; break;
                    default: archIndex = 255; break;
                }
                archIndex += (byte)(ii1 * 3);
                int readNumber = 1;

                byte[] arch = { (byte)Config.DeviseAdress, 0x41, 0, archIndex, 0, BitConverter.GetBytes(readNumber)[0], 1,
                                     (byte)1,  (byte)1,  (byte)hour,  (byte)day,  (byte)month,  (byte)(year%100), 0, 0 };
                //Device_Adress  Function   archIndex_Little archIndex_Big (archIndex)  WhatRead(0-ByIndex 1-ByTime)
                // Seconds Minuter Hours Days Month Year  CRC1 СRС2

                byte[] receive = { 0 };

                try
                {
                    receive = comm.MessagebyCRC(arch, CommunicateCurrent.CRCtype_CRC16, timeout: 5000);

                    // receive = executeAsyncMessage(port, arch, 177, 4000).Result;

                    //попытка использовать внутреннюю CRC
                    //for (int trys = 0; trys < 3; trys++)
                    //{
                    //    receive = comm.MessagebyCRC(arch, CommunicateCurrent.CRCtype_CRC16, timeout: 5000);
                    
                    //    byte crc1 = receive[170 + 3];
                    //    byte crc2 = receive[171 + 3];
                    //    byte[] rec22 = receive.Skip(3).Take(172).ToArray();
                    //    CRC.CRC16(rec22, 172);
                    
                    //    byte crctt1 = rec22[170];
                    //    byte crctt2 = rec22[171];

                    //    if (crc1 == crctt1 && crc2 == crctt2)
                    //    {
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR,
                    //            "Прибор ответил, но контрольная сумма на внюютреннюю структуру не совпала повытка " + trys + 1 + "/3");
                    //        continue;
                    //    }

                    //}
                }
                catch (Exception ex)
                {
                    somebad = true;
                    ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, ex.Message);                   
                    continue;
                }

                int otstup = 3;

                for (int ij = 0; ij < readNumber; ij++)
                {
                    otstup += ij * 172;
                    try
                    {
                        int io = 0;
                        byte[] rec = receive;
                        
                        ulong _0TimeArchive = asBitConverter.ToUInt32_HL(rec, 0 + otstup);
                        when = new DateTime(1970, 1, 1).AddSeconds(_0TimeArchive + 1).AddHours(-1);

                        //tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _0TimeArchive, 18, when, "Время архивирования", "Дата с 01.01.1970")); 
                        int temp = 099;
                        
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, 0, 18, when, "Предполагаемо номер теплосистемы", (ii1 + 1).ToString()));
                        
                        int _4 = asBitConverter.ToUInt16_HL(rec, 4 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 10, ii1, _4, 18, when, "Чистое время работы ТС в штатном режиме", "мин"));

                        int _6 = asBitConverter.ToUInt16_HL(rec, 6 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _6, 18, when, "Чистое время работы ТС при пропадании питания", "мин"));

                        int _8 = asBitConverter.ToUInt16_HL(rec, 8 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _8, 18, when, "Время простоя ТС из-за отказа датчиков", "мин"));

                        int _10 = asBitConverter.ToUInt16_HL(rec, 10 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 51, ii1, _10, 18, when, "Общее время простоя из-за действия НС", "мин"));

                        int _12 = asBitConverter.ToUInt16_HL(rec, 12 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _12, 18, when, "Время простоя ТС при выходе из режима «Работа»", "мин"));

                        for (int i = 0; i < 32; i++)
                        {
                            int _14_76 = asBitConverter.ToUInt16_HL(rec, 14 + i * 2 + otstup);
                            tData.Add(new DataParameter(Config.DriverFeatures, 0, i, _14_76, 18, when, "Счетчики времени действия по отдельной НС [" + i + "]", "мин"));
                        }

                        int _78 = asBitConverter.ToUInt16_HL(rec, 78 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _78, 18, when, "Текущий алгоритм учета (схема см.п.6 потребления, статус ТС)", "-"));

                        ulong _80 = asBitConverter.ToUInt32_HL(rec, 80 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _80, 18, when, "Набор флагов нештатных ситуаций", "-"));

                        double _84 = asBitConverter.ToFloat32_HL(rec, 84 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _84, 18, when, "Общее тепло, потребленное абонентом", "Гкал"));

                        double _88 = asBitConverter.ToFloat32_HL(rec, 88 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _88, 18, when, "Тепло, потребленное с водоразбором", "Гкал"));

                        double _92 = asBitConverter.ToFloat32_HL(rec, 92 + otstup);
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _92, 18, when, "Общая потребленная масса", "т"));
                        
                        for (int i = 0; i < 4; i++)
                        {
                            double _96_108 = asBitConverter.ToFloat32_HL(rec, 96 + i * 4 + otstup);
                            tData.Add(new DataParameter(Config.DriverFeatures, 13, ii1 * 4 + i, _96_108, 18, when, "Тепловая энергия, перенесенное по каждому трубопроводу [" + i + "]", "Гкал"));
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            double _112_124 = asBitConverter.ToFloat32_HL(rec, 112 + i * 4 + otstup);
                            tData.Add(new DataParameter(Config.DriverFeatures, 90, ii1 * 4 + i, _112_124, 18, when, "Масса, перенесенная по каждому трубопроводу [" + i + "]", "т"));
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            double _128_140 = asBitConverter.ToFloat32_HL(rec, 128 + i * 4 + otstup);
                            tData.Add(new DataParameter(Config.DriverFeatures, 91, ii1 * 4 + i, _128_140, 18, when, "Объем, перенесенный по каждому трубопроводу [" + i + "]", "м^3"));
                        }

                        /*
                        for (int i = 0; i < 4; i++)
                        {
                            double _144_150 = asBitConverter.ToUInt16_HL(rec, 144 + i * 2 + otstup);
                            _144_150= _144_150* 0.01;
                            tData.Add(new DataParameter(Config.DriverFeatures, 0, i, _144_150, 18, when, "Средневзвешенная температура по каждому трубопроводу [" + i + "]", " °С"));
                        }
                        */

                        for (int i = 0; i < 4; i++)
                        {
                            double _152_158 = asBitConverter.ToUInt16_HL(rec, 152 + i * 2 + otstup);
                            _152_158 = _152_158 * 0.01;
                            tData.Add(new DataParameter(Config.DriverFeatures, 95, ii1 * 4 + i, _152_158, 18, when, "Средняя температура по каждому трубопроводу [" + i + "]", " °С"));
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            double _160_166 = asBitConverter.ToUInt16_HL(rec, 160 + i * 2 + otstup);
                            _160_166 = _160_166 * 0.001;
                            tData.Add(new DataParameter(Config.DriverFeatures, 1, ii1 * 4 + i, _160_166, 18, when, "Среднее давление по каждому трубопроводу [" + i + "]", " МПа"));
                        }

                        double _168 = asBitConverter.ToUInt16_HL(rec, 168 + otstup);
                        _168 = _168 * 0.01;
                        tData.Add(new DataParameter(Config.DriverFeatures, 0, 0, _168, 18, when, "Температура холодной воды", " °С"));

                        // Thread.Sleep(1000);

                        ColoredConsole.WriteLine(ColoredConsole.TYPE_MESSAGE, "Успешно считано за " + when + " TC(" + (ii1 + 1) + ")");
                    }
                    catch (Exception ex)
                    {
                        somebad = true;
                        ColoredConsole.WriteLine(ColoredConsole.TYPE_ERROR, "Не удалось считать за: " + new DateTime(year, month, day, hour, 0, 0) + " TC(" + (ii1 + 1) + ")" + " Ошибка:(" + ex.Message + ") /n");

                    }
                }
            }
            ret.Add(tData);
            returning.Data = ret;
            if (!somebad)
            {
                DataParameter param1 = new DataParameter(Config.DriverFeatures, 999, 0, 1, Config.EquipmentTypeId, when);//зАПИСЬ СЧИТАНА
                tData.Add(param1);
                 
                returning.Status = STATUS_GOD; 
            }
            else
            {
                returning.Status = STATUS_BAD;
          
            }
            return returning;

        }

        #endregion





        //private int[] VolumeQuantityAddresses = { 0xC05A, 0xC05C, 0xC05E, 0xC060, 0xC062, 0xC064, 0xC066, 0xC068, 0xC06A }; //DONE 2 BYTES

        private int[] TemperatureAddresses = { 0xC048, 0xC04A, 0xC04C, 0xC04E, 0xC050, 0xC052, 0xC4F6, 0xC4F8, 0xC4FA, 0xC6BE }; //DONE 2 BYTES
        private int[] PressureAddresses = { 0xC03C, 0xC03E, 0xC040, 0xC042, 0xC044, 0xC046, 0xC4FC, 0xC4FE, 0xC500 }; //DONE 2 BYTES

        private int[] VolumeChannelAddresses = { 0xC06C, 0xC070, 0xC074, 0xC078, 0xC07C, 0xC080, 0xC084, 0xC088, 0xC08C }; //DONE 2 BYTES

        //private int[] MassiveQuantityAddresses = { 0xC6B0, 0xC6B6, 0xC6BC }; //DONE 2 BYTES
        // private int[] EntalphyAddresses = { 0x8064, 0x8066, 0x8068 }; //DONE 4 BYTES

        private int[] HeatAddresses = { 0xC234, 0xC270, 0xC2AC, 0xC2E8, 0xC324, 0xC360, 0xC39C, 0xC3D8, 0xC414, 0xC450, 0xC48C, 0xC4C8, 0xC0AE, 0xC0BE, 0xC0C2, 0xC0D6, 0xC0DA, 0xC0EE0, 0xC0F2, 0xC69A, 0xC69C, 0xC6A0, 0xC6A2, 0xC6A6, 0xC6A8 };//DONE 2 BYTES
        private int[] WeightAddresses = { 0xC238, 0xC274, 0xC2B0, 0xC2EC, 0xC328, 0xC364, 0xC3A0, 0xC3DC, 0xC418, 0xC454, 0xC490, 0xC4CC, 0xC0B2, 0xC0C6, 0xC0DE, 0xC0F6, 0xC69E, 0xC6A4, 0xC6AA };//DONE 2 BYTES

        private int[] EnergyAddresses = { 0xC6AC, 0xC6AE, 0xC6B2, 0xC6B4, 0xC6B8, 0xC6BA }; //DONE 2 BYTES

        private int[] Time = { 0x8016, 0x8024, 0x8032 };
        private int[] NoTime = { 0x8018, 0x8026, 0x8034 };


    }
}
